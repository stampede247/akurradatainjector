﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace AkurraDataInjector
{
    [Serializable]
    public class AppConfig
    {
        [Serializable]
        public struct PastOperationList
        {
            public string srcFilePath;
            public RoomOperation[] operations;
        }

        public string srcPath;
        public string srcDirectory;
        public string destPath;
        public string destDirectory;
        public string levelDirectory;

        public List<PastOperationList> pastOperationLists;

        //GmlCompilationCompare Form
        public string compSrcFilePath;
        public string compTempDirPath;
        public string compCodeName;

        public AppConfig()
        {
            srcPath = "";
            destPath = "";
            srcPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            destPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            levelDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            compSrcFilePath = "";
            compTempDirPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            compCodeName = "";
            pastOperationLists = new List<PastOperationList>();
        }

        public void SetPastOperationList(PastOperationList list)
        {
            for (int oIndex = 0; oIndex < pastOperationLists.Count; oIndex++)
            {
                if (pastOperationLists[oIndex].srcFilePath.ToLower() == list.srcFilePath.ToLower())
                {
                    pastOperationLists.RemoveAt(oIndex);
                    oIndex--;
                }
            }
            pastOperationLists.Add(list);
        }
        public PastOperationList? GetPastOperationList(string srcFilePath)
        {
            for (int oIndex = 0; oIndex < pastOperationLists.Count; oIndex++)
            {
                if (pastOperationLists[oIndex].srcFilePath.ToLower() == srcFilePath.ToLower())
                {
                    return pastOperationLists[oIndex];
                }
            }
            return null;
        }

        public static AppConfig LoadFromFile(string filePath)
        {
            //TODO: Handle exceptions?
            var serializer = new XmlSerializer(typeof(AppConfig));
            try
            {
                FileStream configFile = File.OpenRead(filePath);
                try
                {
                    AppConfig result = (AppConfig)serializer.Deserialize(configFile);
                    configFile.Close();
                    return result;
                }
                catch (InvalidOperationException ex)
                {
                    Console.WriteLine("Configuration file is corrupt at \"" + filePath + "\". Defaulting to empty configuration");
                    configFile.Close();
                    return new AppConfig();
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine("No configuration file found at \"" + filePath + "\"");
                return new AppConfig();
            }
        }
        public bool SaveToFile(string filePath)
        {
            var serializer = new XmlSerializer(typeof(AppConfig));
            try
            {
                FileStream configFile = File.Open(filePath, FileMode.Create);
                serializer.Serialize(configFile, this);
                configFile.Close();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to save configuration file to \"" + filePath + "\"");
                return false;
            }
            
        }
    }
}

