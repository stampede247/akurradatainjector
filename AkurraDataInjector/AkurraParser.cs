﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AkurraDataInjector
{
    public class AkurraParser
    {
        /*private class MyMarshaler : ICustomMarshaler
        {
            static ICustomMarshaler GetInstance(string pstrCookie)
            {
                return new MyMarshaler();
            }

            void ICustomMarshaler.CleanUpManagedData(object ManagedObj)
            {
            }

            void ICustomMarshaler.CleanUpNativeData(IntPtr pNativeData)
            {
                // to be defined
            }

            int ICustomMarshaler.GetNativeDataSize()
            {
                return -1;
            }

            IntPtr ICustomMarshaler.MarshalManagedToNative(object ManagedObj)
            {
                var casted = ManagedObj as ParserInitInfo_t;
                if (casted == null)
                {
                    return IntPtr.Zero;
                }
                var ptr = Marshal.AllocHGlobal(sizeof(int) + Marshal.SizeOf(typeof(IntPtr)));
                Marshal.WriteInt32(ptr, casted.test1);
                var bytes = Encoding.UTF8.GetBytes(casted.test2);
                var strPtr = Marshal.AllocHGlobal(bytes.Length + 1);
                Marshal.Copy(bytes, 0, strPtr, bytes.Length);
                Marshal.WriteByte(strPtr + bytes.Length, 0);
                Marshal.WriteIntPtr(ptr + sizeof(int), strPtr);
                return ptr;
            }

            object ICustomMarshaler.MarshalNativeToManaged(IntPtr pNativeData)
            {
                throw new NotImplementedException();
            }
        }

        private class ParserInitInfo_t
        {
            public int test1 { get; set; }
            public string test2 { get; set; }
        };

        [DllImport("C:\\Users\\robbitay\\Documents\\MyStuff\\Programming\\Tools\\AkurraDataInjector\\AkurraDataInjector\\bin\\Debug\\AkurraParser.dll")]
        private static extern void Parser_Initialize([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(MyMarshaler))] ParserInitInfo_t info);
        */

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void DialogueDelegate(string text, string caption);
        private static void DialogueCallback(string text, string caption)
        {
            if (ShowMessageBoxEvent != null) { ShowMessageBoxEvent(text, caption);  }
        }

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void DebugOutputDelegate(int level, string message, bool newLine);
        private static string workingLine = "";
        private static int workingLineLevel = 0;
        private static void ParserDebugOutput(int level, string message, bool newLine)
        {
            if (level > workingLineLevel) { workingLineLevel = level; }
            workingLine += message;
            if (newLine) { workingLine += "\n"; }
            int nextNewLine = workingLine.IndexOf("\n");
            while (nextNewLine >= 0)
            {
                String nextLine = workingLine.Substring(0, nextNewLine);
                //if (workingLineLevel == 1) { Console.Write("-- "); } //Debug
                //else if (workingLineLevel == 2) { Console.Write("== "); } //Regular
                //else if (workingLineLevel == 3) { Console.Write(">> "); } //Info
                //else if (workingLineLevel == 4) { Console.Write("]] "); } //Notify
                //else if (workingLineLevel == 5) { Console.Write(")) "); } //Other
                //else if (workingLineLevel == 6) { Console.Write("** "); } //Warning
                //else if (workingLineLevel == 7) { Console.Write("!! "); } //Error
                //else { Console.Write("?? "); } //unknown level
                //Console.WriteLine(nextLine);
                if (DebugLineAvailableEvent != null) { DebugLineAvailableEvent(workingLineLevel, nextLine); }
                workingLine = workingLine.Substring(nextNewLine+1);
                nextNewLine = workingLine.IndexOf("\n");
            }
            if (workingLine.Length == 0) { workingLineLevel = 0; }
        }

        public delegate void ShowMessageBoxDelegate(string text, string caption);
        public static event ShowMessageBoxDelegate ShowMessageBoxEvent = null;

        public delegate void DebugLineAvailableDelegate(int level, string line);
        public static event DebugLineAvailableDelegate DebugLineAvailableEvent = null;

        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern void Parser_Initialize(DialogueDelegate dialogueCallback, DebugOutputDelegate debugOutpuCallback);

        private static DialogueDelegate dialogueDelegate;
        private static DebugOutputDelegate debugOutputDelegate;

        public static void Initialize(ShowMessageBoxDelegate showMessageBoxDelegate, DebugLineAvailableDelegate debugLineAvailableDelegate)
        {
            ShowMessageBoxEvent += showMessageBoxDelegate;
            DebugLineAvailableEvent += debugLineAvailableDelegate;
            dialogueDelegate = new DialogueDelegate(DialogueCallback);
            debugOutputDelegate = new DebugOutputDelegate(ParserDebugOutput);
            Parser_Initialize(dialogueDelegate, debugOutputDelegate);
        }

        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool Parser_Import(string filePath);
        
        public struct ExportOperation_t
        {
            public int type;
            public string str;
        }
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool Parser_StartExportList(int numReplacedRooms);
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void Parser_ExportOperation(int roomIndex, ExportOperation_t operation);
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void Parser_ExportRenameRoom(string originalName, string newName);
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool Parser_Export(string filePath);
        
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void Parser_PushTempMark();
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void Parser_PopTempMark();

        public struct RoomInfo_t
        {
            public IntPtr name;
            public Int32 width;
            public Int32 height;
            public UInt32 numObjects;
            public UInt32 numCollisions;
            public UInt32 numOtherInstances;
        };
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int Parser_GetNumRooms();
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern RoomInfo_t Parser_GetRoomInfo(int roomIndex);
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr Parser_GetRoomImage(int roomIndex, out int imageSizeOut);
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void Parser_FreeRoomImage(IntPtr iamgeData);
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool Parser_HandleCommand(string commandStr);

        public struct ObjectInfo_t
        {
            public IntPtr name;
            public IntPtr spriteName;
            public UInt32 flags;
            public Int32 parentId;
            public Int32 maskId;
            public Int32 depth;
        };
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int Parser_GetNumObjects();
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern ObjectInfo_t Parser_GetObjectInfo(int objectIndex);

        public struct SpriteInfo_t
        {
            public IntPtr name;
            public Int32 width;
            public Int32 height;
            public Int32 originX;
            public Int32 originY;
            public UInt32 numFrames;
            public float speed;
            public UInt32 isSpeedGameFrames;
            public UInt32 formatNum;
        };
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int Parser_GetNumSprites();
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern SpriteInfo_t Parser_GetSpriteInfo(int spriteIndex);

        public struct StringInfo_t
        {
            public UInt32 offset;
            public UInt32 length;
            public IntPtr pntr;
        };
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int Parser_GetNumStrings();
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern StringInfo_t Parser_GetStringInfo(int stringIndex);

        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int Parser_GetNumCodes();
        public struct CodeInfo_t
        {
            public IntPtr name;
            public UInt32 bytecodeOffset;
            public UInt32 bytecodeLength;
            public UInt32 numVariables;
            public UInt32 numFunctions;
        };
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern CodeInfo_t Parser_GetCodeInfo(int codeIndex);
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr Parser_GetCodeBytecode(int codeIndex);
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr Parser_GetCodeFormattedBytecode(int codeIndex, bool showAddresses, bool showInterpretations, bool showVariables, bool showDecompiles);

        public struct VariableInfo_t
        {
            public IntPtr name;
            public UInt32 numCodeReferences;
            public Int32 firstCodeReference;
            public Int32 codeIndex;
        };
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int Parser_GetNumVariables();
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern VariableInfo_t Parser_GetVariableInfo(int variableIndex);

        public struct FunctionInfo_t
        {
            public IntPtr name;
            public Int32 unknown0;
            public Int32 firstCodeReference;
            public Int32 codeIndex;
        };
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int Parser_GetNumFunctions();
        [DllImport("AkurraParser.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern FunctionInfo_t Parser_GetFunctionInfo(int functionIndex);
    }
}
