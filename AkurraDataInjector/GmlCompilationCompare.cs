﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkurraDataInjector
{
    public partial class GmlCompilationCompare : Form
    {
        AppConfig config;
        string foundPackPath;

        public bool OpenPackRequested = false;
        public string OpenPackPath;

        public GmlCompilationCompare(AppConfig config)
        {
            InitializeComponent();
            this.config = config;
            this.OpenPackRequested = false;
        }

        private void GmlCompilationCompare_Load(object sender, EventArgs e)
        {
            this.SrcPathTextbox.Text = config.compSrcFilePath;
            this.SrcPathTextbox.Select(this.SrcPathTextbox.Text.Length, 0);
            this.TempPathTextbox.Text = config.compTempDirPath;
            this.TempPathTextbox.Select(this.TempPathTextbox.Text.Length, 0);
            this.CodeNameTextbox.Text = config.compCodeName;
            this.CodeNameTextbox.Select(this.CodeNameTextbox.Text.Length, 0);
        }

        void ShowMessageBox(string title, string message)
        {
            MessageBox.Show(message, title);
        }

        private void CheckButton_Click(object sender, EventArgs e)
        {
            this.LeftTextbox.Text = "Loading...";
            this.RightTextbox.Text = "Loading...";
            this.foundPackPath = null;
            this.OpenPackButton.Enabled = false;

            string tempPath = this.TempPathTextbox.Text;
            if (!Directory.Exists(tempPath))
            {
                ShowMessageBox("Temp Path Doesn't Exist", "The temporary path that was selected does not exist:\n\"" + tempPath + "\"");
                return;
            }

            string sourceFilePath = this.SrcPathTextbox.Text;
            if (!File.Exists(sourceFilePath))
            {
                ShowMessageBox("Source File Doesn't Exist", "The source file path that was selected does not exist:\n\"" + sourceFilePath + "\"");
                return;
            }

            AkurraParser.Parser_PushTempMark();
            string sourceFileContents = File.ReadAllText(sourceFilePath);
            this.LeftTextbox.Text = sourceFileContents;

            int numSubDirsSearched = 0;
            string newestDataFilePath = null;
            DateTime newestFileWriteTime = DateTime.MinValue;
            IEnumerable<string> tempDirs = Directory.EnumerateDirectories(tempPath);
            foreach (string tempSubDir in tempDirs)
            {
                IEnumerable<string> subDirFiles = Directory.EnumerateFiles(tempSubDir);
                foreach (string filePath in subDirFiles)
                {
                    string fileExt = Path.GetExtension(filePath);
                    if (fileExt.ToLower() == ".win")
                    {
                        DateTime fileWriteTime = File.GetLastWriteTime(filePath);
                        if (newestDataFilePath == null)
                        {
                            newestDataFilePath = filePath;
                            newestFileWriteTime = fileWriteTime;
                        }
                        else if (fileWriteTime.CompareTo(newestFileWriteTime) > 0)
                        {
                            newestDataFilePath = filePath;
                            newestFileWriteTime = fileWriteTime;
                        }
                    }
                }
                numSubDirsSearched++;
            }
            if (newestDataFilePath == null)
            {
                ShowMessageBox("Couldn't find .win file", "Found no .win files inside any of the sub-directories in the selected temporary directory path (searched " + numSubDirsSearched + " sub directories):\n\"" + tempPath + "\"");
                AkurraParser.Parser_PopTempMark();
                return;
            }

            Console.WriteLine("Loading \"" + newestDataFilePath + "\"...");
            this.foundPackPath = newestDataFilePath;
            this.OpenPackButton.Enabled = true;
            AkurraParser.Parser_Import(newestDataFilePath);

            bool codeNameSpecified = (CodeNameTextbox.Text != "");
            int numCodes = AkurraParser.Parser_GetNumCodes();
            if (!codeNameSpecified) { this.RightTextbox.Text = "Found " + numCodes + " codes\r\n"; }
            bool foundCodeByName = false;
            for (int cIndex = 0; cIndex < numCodes; cIndex++)
            {
                AkurraParser.Parser_PushTempMark();
                AkurraParser.CodeInfo_t codeInfo = AkurraParser.Parser_GetCodeInfo(cIndex);
                string codeName = Marshal.PtrToStringAnsi(codeInfo.name);
                UInt32 codeOffset = codeInfo.bytecodeOffset;
                UInt32 codeLength = codeInfo.bytecodeLength;
                if (!codeNameSpecified || CodeNameTextbox.Text.ToLower() == codeName.ToLower())
                {
                    if (codeNameSpecified)
                    {
                        foundCodeByName = true;
                        IntPtr bytecodeStrPntr = AkurraParser.Parser_GetCodeFormattedBytecode(cIndex, true, true, true, false);
                        string bytecodeStr = Marshal.PtrToStringAnsi(bytecodeStrPntr);
                        Helpers.PutFormattedBytecodeInRichTextbox(this.RightTextbox, bytecodeStr);
                    }
                    else
                    {
                        this.RightTextbox.Text += "Code[" + cIndex + "] \"" + codeName + "\": " + (codeLength/4) + " words\r\n";
                    }
                }
                AkurraParser.Parser_PopTempMark();
            }

            if (!foundCodeByName && codeNameSpecified)
            {
                this.RightTextbox.Text = "Couldn't find code with name \"" + this.CodeNameTextbox.Text + "\"\r\n";
            }

            config.compCodeName = CodeNameTextbox.Text;
            AkurraParser.Parser_PopTempMark();
        }

        private void SrcBrowseBtn_Click(object sender, EventArgs e)
        {
            this.SrcFileDialogue.CheckFileExists = true;
            this.SrcFileDialogue.CheckPathExists = true;
            this.SrcFileDialogue.InitialDirectory = config.compSrcFilePath;
            this.SrcFileDialogue.DefaultExt = ".gml";
            this.SrcFileDialogue.Filter = "Game Maker Script(*.gml)|*.gml|All Files(*.*)|*.*";
            DialogResult dialogResult = this.SrcFileDialogue.ShowDialog();
            if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes)
            {
                Console.WriteLine("Selected \"" + this.SrcFileDialogue.FileName + "\"");
                this.SrcPathTextbox.Text = this.SrcFileDialogue.FileName;
                this.SrcPathTextbox.Select(this.SrcPathTextbox.Text.Length, 0);
                this.config.compSrcFilePath = this.SrcFileDialogue.FileName;
            }
        }

        private void TempBrowseBtn_Click(object sender, EventArgs e)
        {
            this.TempFolderDialogue.SelectedPath = config.compSrcFilePath;
            this.TempFolderDialogue.ShowNewFolderButton = false;
            DialogResult dialogResult = this.TempFolderDialogue.ShowDialog();
            if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes)
            {
                Console.WriteLine("Selected \"" + this.TempFolderDialogue.SelectedPath + "\"");
                this.TempPathTextbox.Text = this.TempFolderDialogue.SelectedPath;
                this.TempPathTextbox.Select(this.TempPathTextbox.Text.Length, 0);
                this.config.compTempDirPath = this.TempFolderDialogue.SelectedPath;
            }
        }

        private void OpenPackButton_Click(object sender, EventArgs e)
        {
            if (this.foundPackPath != null && foundPackPath != "")
            {
                this.OpenPackRequested = true;
                this.OpenPackPath = this.foundPackPath;
                this.Close();
            }
        }
    }
}
