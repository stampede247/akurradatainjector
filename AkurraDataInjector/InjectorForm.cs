﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkurraDataInjector
{
    public partial class InjectorForm : Form 
    {
        AppConfig config;
        ImageViewForm imageForm = null;
        GmlCompilationCompare compilationCompareForm = null;
        RenameRoomForm renameRoomForm = null;
        List<RoomOperation> roomOperations = new List<RoomOperation>();

        List<ListViewItem> roomItems = null;
        List<ListViewItem> objectItems = null;
        List<ListViewItem> spriteItems = null;
        List<ListViewItem> stringItems = null;
        List<ListViewItem> variableItems = null;
        List<ListViewItem> codeItems = null;

        string roomListAppliedSearch = "";
        string objectListAppliedSearch = "";
        string spriteListAppliedSearch = "";
        string stringListAppliedSearch = "";
        string variableListAppliedSearch = "";
        string codeListAppliedSearch = "";

        void DebugLineAvailable(int level, string line)
        {
        	//background 0x00444C
            if (level == 1)      { this.DebugOutputBox.SelectionColor = Color.FromArgb(0xB1D8DD); } //Debug
            else if (level == 2) { this.DebugOutputBox.SelectionColor = Color.FromArgb(0xC0C0C0); } //Regular
            else if (level == 3) { this.DebugOutputBox.SelectionColor = Color.FromArgb(0x3FBC7C); } //Info
            else if (level == 4) { this.DebugOutputBox.SelectionColor = Color.FromArgb(0x01B8DF); } //Notify
            else if (level == 5) { this.DebugOutputBox.SelectionColor = Color.FromArgb(0xFFF89F); } //Other
            else if (level == 6) { this.DebugOutputBox.SelectionColor = Color.FromArgb(0xF58B59); } //Warning
            else if (level == 7) { this.DebugOutputBox.SelectionColor = Color.FromArgb(0xD74D40); } //Error
            else { this.DebugOutputBox.SelectionColor = Color.White; } //unknown level
            this.DebugOutputBox.AppendText(line + Environment.NewLine);
            this.DebugOutputBox.ScrollToCaret();
        }

        void ShowMessageBox(string title, string message)
        {
            MessageBox.Show(message, title);
        }

        public InjectorForm() 
        {
            InitializeComponent();
            config = AppConfig.LoadFromFile("config.cfg");//TODO: Make this path absolute or relative to the exe location despite working directory changes
        }

        public void SaveConfig()
        {
            config.srcPath = this.SrcFileBox.Text;
            config.destPath = this.DestFileBox.Text;
            config.SaveToFile("config.cfg");
        }

        private void Form_Load(object sender, EventArgs e)
        {
            this.SrcFileBox.Text = config.srcPath;
            this.SrcFileBox.Select(this.SrcFileBox.Text.Length, 0);
            this.DestFileBox.Text = config.destPath;
            this.DestFileBox.Select(this.DestFileBox.Text.Length, 0);
            AkurraParser.Initialize(new AkurraParser.ShowMessageBoxDelegate(ShowMessageBox), new AkurraParser.DebugLineAvailableDelegate(DebugLineAvailable));
            this.StatusLabel.Text = "Ready...";
        }
        private void InjectorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveConfig();
        }
        
        public void AddRoomOperation(RoomOperation newOperation)
        {
            for (int oIndex = 0; oIndex < this.roomOperations.Count; oIndex++)
            {
                RoomOperation existingOperation = this.roomOperations[oIndex];
                if (existingOperation.roomIndex == newOperation.roomIndex)
                {
                    if (RoomOperation.DoOperationTypesConflict(existingOperation.type, newOperation.type))
                    {
                        this.roomOperations.RemoveAt(oIndex);
                        oIndex--;
                    }
                }
            }
            this.roomOperations.Add(newOperation);
        }
        public void RemoveRoomOperationsFor(int roomIndex)
        {
            for (int oIndex = 0; oIndex < this.roomOperations.Count; oIndex++)
            {
                RoomOperation existingOperation = this.roomOperations[oIndex];
                if (existingOperation.roomIndex == roomIndex)
                {
                    this.roomOperations.RemoveAt(oIndex);
                    oIndex--;
                }
            }
        }
        public void RemoveRoomOperationByTypeFor(int roomIndex, RoomOperation.RoomOperationType type)
        {
            for (int oIndex = 0; oIndex < this.roomOperations.Count; oIndex++)
            {
                RoomOperation existingOperation = this.roomOperations[oIndex];
                if (existingOperation.roomIndex == roomIndex && existingOperation.type == type)
                {
                    this.roomOperations.RemoveAt(oIndex);
                    oIndex--;
                }
            }
        }
        public List<RoomOperation> GetRoomOperationsFor(int roomIndex)
        {
            List<RoomOperation> result = new List<RoomOperation>();
            foreach (RoomOperation operation in this.roomOperations)
            {
                if (operation.roomIndex == roomIndex)
                {
                    result.Add(operation);
                }
            }
            return result;
        }

        private void SrcBrowseBtn_Click(object sender, EventArgs e)
        {
            this.SrcFileDialogue.CheckFileExists = true;
            this.SrcFileDialogue.CheckPathExists = true;
            this.SrcFileDialogue.InitialDirectory = config.srcDirectory;
            this.SrcFileDialogue.DefaultExt = ".win";
            this.SrcFileDialogue.Filter = "Game Maker Data Files(*.win)|*.win|All Files(*.*)|*.*";
            DialogResult dialogResult = this.SrcFileDialogue.ShowDialog();
            if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes)
            {
                Console.WriteLine("Selected \"" + this.SrcFileDialogue.FileName + "\"");
                this.SrcFileBox.Text = this.SrcFileDialogue.FileName;
                this.SrcFileBox.Select(this.SrcFileBox.Text.Length, 0);
                this.config.srcDirectory = Path.GetDirectoryName(this.SrcFileDialogue.FileName);
            }
        }

        private void InjectButton_Click(object sender, EventArgs e)
        {
            if (AkurraParser.Parser_StartExportList(this.roomOperations.Count))
            {
                foreach (RoomOperation operation in this.roomOperations)
                {
                    AkurraParser.ExportOperation_t op = new AkurraParser.ExportOperation_t();
                    op.type = (int)operation.type;
                    if (operation.type == RoomOperation.RoomOperationType.Rename)
                    {
                        op.str = operation.newName;
                    }
                    else if (operation.type == RoomOperation.RoomOperationType.Replace)
                    {
                        op.str = operation.filePath;
                    }
                    AkurraParser.Parser_ExportOperation(operation.roomIndex, op);
                }
                if (AkurraParser.Parser_Export(this.DestFileBox.Text))
                {
                    this.StatusLabel.Text = "Injection Complete!";
                    AppConfig.PastOperationList operationList = new AppConfig.PastOperationList()
                    {
                        srcFilePath = this.SrcFileBox.Text,
                        operations = this.roomOperations.ToArray()
                    };
                    this.config.SetPastOperationList(operationList);
                    SaveConfig();
                }
                else
                {
                    this.StatusLabel.Text = "Injection failed!";
                }
            }
            else
            {
                this.StatusLabel.Text = "Failed to start injection";
            }
        }

        private void PopulateRoomsList()
        {
            this.RoomsList.Items.Clear();
            this.roomItems = new List<ListViewItem>();
            int numRooms = AkurraParser.Parser_GetNumRooms();
            Console.WriteLine("Found " + numRooms.ToString() + " rooms in pack file");
            for (int rIndex = 0; rIndex < numRooms; rIndex++)
            {
                AkurraParser.Parser_PushTempMark();
                AkurraParser.RoomInfo_t roomInfo = AkurraParser.Parser_GetRoomInfo(rIndex);
                var newItem = Helpers.CreateRoomViewItemFromInfo(rIndex, roomInfo, GetRoomOperationsFor(rIndex));
                this.RoomsList.Items.Add(newItem);
                this.roomItems.Add(newItem);
                AkurraParser.Parser_PopTempMark();
            }
            SaveConfig();
        }
        
        private void PopulateObjectsList()
        {
            this.ObjectsList.Items.Clear();
            this.objectItems = new List<ListViewItem>();
            int numObjects = AkurraParser.Parser_GetNumObjects();
            Console.WriteLine("Found " + numObjects.ToString() + " objects in pack file");
            for (int oIndex = 0; oIndex < numObjects; oIndex++)
            {
                AkurraParser.Parser_PushTempMark();
                AkurraParser.ObjectInfo_t objInfo = AkurraParser.Parser_GetObjectInfo(oIndex);
                ListViewItem newItem = Helpers.CreateObjectViewItemFromInfo(oIndex, objInfo);
                this.ObjectsList.Items.Add(newItem);
                this.objectItems.Add(newItem);
                AkurraParser.Parser_PopTempMark();
            }
        }

        private void PopulateSpritesList()
        {
            this.SpritesList.Items.Clear();
            this.spriteItems = new List<ListViewItem>();
            int numSprites = AkurraParser.Parser_GetNumSprites();
            Console.WriteLine("Found " + numSprites.ToString() + " sprites in pack file");
            for (int sIndex = 0; sIndex < numSprites; sIndex++)
            {
                AkurraParser.Parser_PushTempMark();
                AkurraParser.SpriteInfo_t spriteInfo = AkurraParser.Parser_GetSpriteInfo(sIndex);
                var newItem = Helpers.CreateSpriteViewItemFromInfo(sIndex, spriteInfo);
                this.SpritesList.Items.Add(newItem);
                this.spriteItems.Add(newItem);
                AkurraParser.Parser_PopTempMark();
            }
        }

        private void PopulateStringsList()
        {
            this.StringsList.Items.Clear();
            this.stringItems = new List<ListViewItem>();
            int numStrings = AkurraParser.Parser_GetNumStrings();
            Console.WriteLine("Found " + numStrings.ToString() + " strings in pack file");
            for (int sIndex = 0; sIndex < numStrings; sIndex++)
            {
                AkurraParser.Parser_PushTempMark();
                AkurraParser.StringInfo_t stringInfo = AkurraParser.Parser_GetStringInfo(sIndex);
                string stringValue = Marshal.PtrToStringAnsi(stringInfo.pntr);
                string realStringValue = stringValue;
                if (stringValue == "" || stringValue == null) { stringValue = "[empty]"; }
                ListViewItem newItem = new ListViewItem(sIndex.ToString());
                newItem.Tag = realStringValue;
                newItem.SubItems.Add(stringValue);
                newItem.SubItems.Add("0x" + stringInfo.offset.ToString("X8"));
                newItem.SubItems.Add(stringInfo.length.ToString() + " chars");
                this.StringsList.Items.Add(newItem);
                this.stringItems.Add(newItem);
                AkurraParser.Parser_PopTempMark();
            }
        }

        private void PopulateVariablesList()
        {
            this.VariablesList.Items.Clear();
            this.variableItems = new List<ListViewItem>();
            int numVariables = AkurraParser.Parser_GetNumVariables();
            Console.WriteLine("Found " + numVariables.ToString() + " variables in pack file");
            for (int vIndex = 0; vIndex < numVariables; vIndex++)
            {
                AkurraParser.Parser_PushTempMark();
                AkurraParser.VariableInfo_t varInfo = AkurraParser.Parser_GetVariableInfo(vIndex);
                var newItem = Helpers.CreateVariableViewItemFromInfo(vIndex, varInfo);
                this.VariablesList.Items.Add(newItem);
                this.variableItems.Add(newItem);
                AkurraParser.Parser_PopTempMark();
            }
        }

        private void PopulateCodesList()
        {
            this.CodesList.Items.Clear();
            this.codeItems = new List<ListViewItem>();
            int numCodes = AkurraParser.Parser_GetNumCodes();
            Console.WriteLine("Found " + numCodes.ToString() + " codes in pack file");
            for (int cIndex = 0; cIndex < numCodes; cIndex++)
            {
                AkurraParser.Parser_PushTempMark();
                AkurraParser.CodeInfo_t codeInfo = AkurraParser.Parser_GetCodeInfo(cIndex);
                var newItem = Helpers.CreateCodeViewItemFromInfo(cIndex, codeInfo);
                this.CodesList.Items.Add(newItem);
                this.codeItems.Add(newItem);
                AkurraParser.Parser_PopTempMark();
            }
        }

        private void PopulateInfoComboBox()
        {
            bool changedItems = false;
            if (this.MainTabView.SelectedIndex == 0) //Rooms
            {
                if (this.InfoComboBox.Items.Count == 0 || this.InfoComboBox.Items[0].ToString() != "Room Info")
                {
                    changedItems = true;
                    this.InfoComboBox.Items.Clear();
                    this.InfoComboBox.Items.Add("Room Info");
                    this.InfoComboBox.Items.Add("ASCII Visual");
                    this.InfoComboBox.Items.Add("Backgrounds");
                    this.InfoComboBox.Items.Add("Views");
                    this.InfoComboBox.Items.Add("Objects");
                    this.InfoComboBox.Items.Add("Tiles");
                    this.InfoComboBox.Items.Add("Layers");
                    this.InfoComboBox.Items.Add("Object Layer");
                    this.InfoComboBox.Items.Add("Collision Layer");
                }
            }
            else if (this.MainTabView.SelectedIndex == 1) //Objects
            {
                if (this.InfoComboBox.Items.Count == 0 || this.InfoComboBox.Items[0].ToString() != "Object Info")
                {
                    changedItems = true;
                    this.InfoComboBox.Items.Clear();
                    this.InfoComboBox.Items.Add("Object Info");
                    this.InfoComboBox.Items.Add("Sprite Info");
                    this.InfoComboBox.Items.Add("Physics Info");
                }
            }
            else if (this.MainTabView.SelectedIndex == 2) //Sprites
            {
                if (this.InfoComboBox.Items.Count == 0 || this.InfoComboBox.Items[0].ToString() != "Sprite Info")
                {
                    changedItems = true;
                    this.InfoComboBox.Items.Clear();
                    this.InfoComboBox.Items.Add("Sprite Info");
                    this.InfoComboBox.Items.Add("Texture Part");
                }
            }
            else if (this.MainTabView.SelectedIndex == 3) //Strings
            {
                if (this.InfoComboBox.Items.Count == 0 || this.InfoComboBox.Items[0].ToString() != "String Info")
                {
                    changedItems = true;
                    this.InfoComboBox.Items.Clear();
                    this.InfoComboBox.Items.Add("String Info");
                    this.InfoComboBox.Items.Add("References");
                }
            }
            if (changedItems)
            {
                if (this.InfoComboBox.Items.Count > 0)
                {
                    this.InfoComboBox.Text = this.InfoComboBox.Items[0].ToString();
                }
                else
                {
                    this.InfoComboBox.Text = "";
                }
            }
        }
        
        private void UpdateListBySearchText(ListView list, List<ListViewItem> originalItems, string searchText)
        {
            if (searchText == "")
            {
                list.Items.Clear();
                list.Items.AddRange(originalItems.ToArray());
            }
            else
            {
                list.Items.Clear();
                foreach (ListViewItem item in originalItems)
                {
                    if (Helpers.DoesListViewItemMatchSearchText(item, searchText))
                    {
                        list.Items.Add(item);
                    }
                }
            }
        }

        private void SrcLoadBtn_Click(object sender, EventArgs e)
        {
            this.DebugOutputBox.Text = "";
            this.DebugOutputBox.ScrollToCaret();
            this.DebugOutputBox.Hide();
            bool importSuccess = AkurraParser.Parser_Import(this.SrcFileBox.Text);
            this.DebugOutputBox.Show();
            this.MainTabView.SelectedIndex = 0; //rooms
            if (importSuccess)
            {
                this.roomItems = null;
                this.objectItems = null;
                this.spriteItems = null;
                this.stringItems = null;
                this.variableItems = null;
                this.codeItems = null;
                this.RoomsList.Items.Clear();
                this.ObjectsList.Items.Clear();
                this.SpritesList.Items.Clear();
                this.StringsList.Items.Clear();
                this.VariablesList.Items.Clear();
                this.CodesList.Items.Clear();
                this.MainTabView.TabIndex = 0;
                PopulateRoomsList();
                PopulateInfoComboBox();
                this.RoomsList.Enabled = true;
                this.InjectButton.Enabled = true;
                this.MainTabView.Enabled = true;
                this.InfoComboBox.Enabled = true;
                this.InfoBtn.Enabled = true;
                this.StatusLabel.Text = "Import Finished";
                this.roomOperations = new List<RoomOperation>();
                AppConfig.PastOperationList? pastOperationList = this.config.GetPastOperationList(this.SrcFileBox.Text);
                if (pastOperationList.HasValue)
                {
                    foreach (RoomOperation operation in pastOperationList.Value.operations)
                    {
                        if (operation.roomIndex >= 0 && operation.roomIndex < this.RoomsList.Items.Count)
                        {
                            this.roomOperations.Add(operation);
                            Helpers.UpdateOperationColumnOnRoomItem(this.RoomsList.Items[operation.roomIndex], GetRoomOperationsFor(operation.roomIndex));
                        }
                    }
                }
            }
        }

        private void RoomsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.RoomsList.SelectedItems.Count == 1)
            {
                ListViewItem selectedItem = this.RoomsList.SelectedItems[0];
                int selectedIndex = this.RoomsList.SelectedIndices[0];
                int imageDataSize = 0;
                IntPtr imageDataPtr = AkurraParser.Parser_GetRoomImage(selectedIndex, out imageDataSize);
                if (imageDataPtr != (IntPtr)0 && imageDataSize > 0)
                {
                    byte[] imageData = new byte[imageDataSize];
                    Marshal.Copy(imageDataPtr, imageData, 0, imageDataSize);
                    Console.WriteLine(imageDataSize.ToString() + " byte image");
                    //Console.Write("{");
                    //for (int bIndex = 0; bIndex < imageDataSize; bIndex++)
                    //{
                    //    Console.Write(" " + imageData[bIndex].ToString("X2"));
                    //}
                    //Console.WriteLine(" }");
                    MemoryStream imageDataStream = new MemoryStream(imageData);
                    try
                    {
                        Image newImage = Image.FromStream(imageDataStream);
                        if (this.RoomImageBox.Image != null && this.RoomImageBox.Image != this.RoomImageBox.InitialImage)
                        {
                            this.RoomImageBox.Image.Dispose();
                        }
                        this.RoomImageBox.Image = newImage;
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine(imageDataSize.ToString() + " byte image data for room was invalid");
                    }
                    AkurraParser.Parser_FreeRoomImage(imageDataPtr);
                }
                else
                {
                    if (this.RoomImageBox.Image != null && this.RoomImageBox.Image != this.RoomImageBox.InitialImage)
                    {
                        this.RoomImageBox.Image.Dispose();
                    }
                    this.RoomImageBox.Image = this.RoomImageBox.InitialImage;
                }
            }
            else
            {
                if (this.RoomImageBox.Image != null && this.RoomImageBox.Image != this.RoomImageBox.InitialImage)
                {
                    this.RoomImageBox.Image.Dispose();
                }
                this.RoomImageBox.Image = this.RoomImageBox.InitialImage;
            }
        }

        private void DestBrowseBtn_Click(object sender, EventArgs e)
        {
            this.DestFileDialogue.OverwritePrompt = false;
            this.DestFileDialogue.DefaultExt = ".win";
            this.DestFileDialogue.Filter = "Game Maker Data Files(*.win)|*.win|All Files(*.*)|*.*";
            this.DestFileDialogue.InitialDirectory = config.destDirectory;
            DialogResult dialogResult = this.DestFileDialogue.ShowDialog();
            if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes)
            {
                Console.WriteLine("Selected \"" + this.DestFileDialogue.FileName + "\"");
                this.DestFileBox.Text = this.DestFileDialogue.FileName;
                this.DestFileBox.Select(this.DestFileBox.Text.Length, 0);
                this.config.destDirectory = Path.GetDirectoryName(this.DestFileDialogue.FileName);
            }
        }

        private void SendBtn_Click(object sender, EventArgs e)
        {
            if (this.CommandBox.Text.Length > 0)
            {
                bool validCommand = AkurraParser.Parser_HandleCommand(this.CommandBox.Text);
                if (!validCommand)
                {
                    this.StatusLabel.Text = "Unknown command";
                }
                this.CommandBox.Text = "";
            }
        }

        private void CommandBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendBtn_Click(sender, e);
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }
        
        private void InfoBtn_Click(object sender, EventArgs e)
        {
        	if (this.MainTabView.SelectedIndex == 0) //rooms
        	{
                if (this.RoomsList.SelectedItems.Count == 1)
                {
                    ListViewItem selectedItem = this.RoomsList.SelectedItems[0];
                    if (this.InfoComboBox.Text == "Room Info")
                    {
                        AkurraParser.Parser_HandleCommand("room info " + selectedItem.Text);
                    }
                    else if (this.InfoComboBox.Text == "ASCII Visual")
                    {
                        AkurraParser.Parser_HandleCommand("room ascii " + selectedItem.Text);
                    }
                    else if (this.InfoComboBox.Text == "Backgrounds")
                    {
                        AkurraParser.Parser_HandleCommand("room backgrounds " + selectedItem.Text);
                    }
                    else if (this.InfoComboBox.Text == "Views")
                    {
                        AkurraParser.Parser_HandleCommand("room views " + selectedItem.Text);
                    }
                    else if (this.InfoComboBox.Text == "Objects")
                    {
                        AkurraParser.Parser_HandleCommand("room objects " + selectedItem.Text);
                    }
                    else if (this.InfoComboBox.Text == "Tiles")
                    {
                        AkurraParser.Parser_HandleCommand("room tiles " + selectedItem.Text);
                    }
                    else if (this.InfoComboBox.Text == "Layers")
                    {
                        AkurraParser.Parser_HandleCommand("room layers " + selectedItem.Text);
                    }
                    else if (this.InfoComboBox.Text == "Object Layer")
                    {
                        AkurraParser.Parser_HandleCommand("room obj_layer " + selectedItem.Text);
                    }
                    else if (this.InfoComboBox.Text == "Collision Layer")
                    {
                        AkurraParser.Parser_HandleCommand("room col_layer " + selectedItem.Text);
                    }
                    else
                    {
                    	this.StatusLabel.Text = "Unsupported Combo Box Value \"" + this.InfoComboBox.Text + "\"";
                    }
                }
                this.RoomsList.Focus();
            }
            else if (this.MainTabView.SelectedIndex == 1) //Objects
            {
            	if (this.ObjectsList.SelectedItems.Count == 1)
                {
                    ListViewItem selectedItem = this.ObjectsList.SelectedItems[0];
                    if (this.InfoComboBox.Text == "Object Info")
                    {
                        AkurraParser.Parser_HandleCommand("object info " + selectedItem.SubItems[1].Text);
                    }
                    else if (this.InfoComboBox.Text == "Sprite Info")
                    {
                        AkurraParser.Parser_HandleCommand("object sprite " + selectedItem.SubItems[1].Text);
                    }
                    else if (this.InfoComboBox.Text == "Physics Info")
                    {
                        AkurraParser.Parser_HandleCommand("object physics " + selectedItem.SubItems[1].Text);
                    }
                    else
                    {
                    	this.StatusLabel.Text = "Unsupported Combo Box Value \"" + this.InfoComboBox.Text + "\"";
                    }
                }
            }
            else if (this.MainTabView.SelectedIndex == 2) //Sprites
            {
            	// if (this.SpritesList.SelectedItems.Count == 1)
                // {
                //     ListViewItem selectedItem = this.SpritesList.SelectedItems[0];
                //     if (this.InfoComboBox.Text == "Sprite Info")
                //     {
                //         AkurraParser.Parser_HandleCommand("sprite info " + selectedItem.SubItems[1].Text);
                //     }
                //     else if (this.InfoComboBox.Text == "Texture Part")
                //     {
                //         AkurraParser.Parser_HandleCommand("sprite texture " + selectedItem.SubItems[1].Text);
                //     }
                //     else
                //     {
                //     	this.StatusLabel.Text = "Unsupported Combo Box Value \"" + this.InfoComboBox.Text + "\"";
                //     }
                // }
            }
            else if (this.MainTabView.SelectedIndex == 3) //Strings
            {
            	if (this.StringsList.SelectedItems.Count == 1)
                {
                    ListViewItem selectedItem = this.StringsList.SelectedItems[0];
                    if (this.InfoComboBox.Text == "String Info")
                    {
                        AkurraParser.Parser_HandleCommand("string info " + this.StringsList.SelectedIndices[0].ToString());
                    }
                    else if (this.InfoComboBox.Text == "References")
                    {
                        AkurraParser.Parser_HandleCommand("string ref " + this.StringsList.SelectedIndices[0].ToString());
                    }
                    else
                    {
                    	this.StatusLabel.Text = "Unsupported Combo Box Value \"" + this.InfoComboBox.Text + "\"";
                    }
                }
            }
        }

        private void RoomImageBox_Click(object sender, EventArgs e)
        {
            if (this.imageForm == null && this.RoomImageBox.Image != null)
            {
                string imageName = "Unknown_Image";
                if (this.RoomsList.SelectedItems.Count > 0)
                {
                    imageName = this.RoomsList.SelectedItems[0].Text;
                }
                this.imageForm = new ImageViewForm(this, imageName);
                this.imageForm.FormClosed += ImageForm_FormClosed;
                this.imageForm.Show(this);
                this.imageForm.ImageView.Image = this.RoomImageBox.Image;
                this.imageForm.Size = this.RoomImageBox.Image.Size + new Size(16, 39);
            }
        }

        private void ImageForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.imageForm = null;
        }

        private void ClearLogBtn_Click(object sender, EventArgs e)
        {
            this.DebugOutputBox.Clear();
        }

        private void MainTabView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.MainTabView.SelectedTab == this.RoomsTab)
            {
                this.SearchTextBox.Enabled = false;
            }
            else
            {
                this.SearchTextBox.Enabled = true;
            }
            if (this.MainTabView.SelectedTab == this.StringsTab)
            {
                if (this.stringItems == null)
                {
                    PopulateStringsList();
                }
            }
            else if (this.MainTabView.SelectedTab == this.ObjectsTab)
            {
                if (this.objectItems == null)
                {
                    PopulateObjectsList();
                }
            }
            else if (this.MainTabView.SelectedTab == this.SpritesTab)
            {
                if (this.spriteItems == null)
                {
                    PopulateSpritesList();
                }
            }
            else if (this.MainTabView.SelectedTab == this.VariablesTab)
            {
                if (this.variableItems == null)
                {
                    PopulateVariablesList();
                }
            }
            else if (this.MainTabView.SelectedTab == this.CodesTab)
            {
                if (this.codeItems == null)
                {
                    PopulateCodesList();
                }
            }
            PopulateInfoComboBox();
            ApplySearchBox();
        }

        private void CompilationCompareButton_Click(object sender, EventArgs e)
        {
            if (compilationCompareForm == null)
            {
                compilationCompareForm = new GmlCompilationCompare(config);
                compilationCompareForm.FormClosing += CompilationCompareForm_FormClosing;
                compilationCompareForm.Show(this);
                this.Hide();
            }
            else
            {
                compilationCompareForm.Close();
                compilationCompareForm = null;
                this.Show();
            }
        }

        private void CompilationCompareForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool openPack = false;
            string packPath = null;
            if (this.compilationCompareForm != null && this.compilationCompareForm.OpenPackRequested)
            {
                openPack = true;
                packPath = compilationCompareForm.OpenPackPath;
            }
            this.compilationCompareForm = null;
            this.Show();
            if (openPack)
            {
                this.SrcFileBox.Text = packPath;
                this.SrcLoadBtn_Click(sender, null);
            }
        }

        private void RoomsList_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ListViewItem focusedItem = this.RoomsList.FocusedItem;
                if (focusedItem != null && focusedItem.Bounds.Contains(e.Location))
                {
                    List<RoomOperation> operations = GetRoomOperationsFor(focusedItem.Index);
                    this.UnremaneRoomButton.Enabled = false;
                    this.UnreplaceRoomButton.Enabled = false;
                    this.DeleteRoomButton.Enabled = true;
                    this.UndeleteRoomButton.Enabled = false;
                    foreach (RoomOperation operation in operations)
                    {
                        if (operation.type == RoomOperation.RoomOperationType.Replace)
                        {
                            this.UnreplaceRoomButton.Enabled = true;
                        }
                        else if (operation.type == RoomOperation.RoomOperationType.Delete)
                        {
                            this.DeleteRoomButton.Enabled = false;
                            this.UndeleteRoomButton.Enabled = true;
                        }
                        else if (operation.type == RoomOperation.RoomOperationType.Rename)
                        {
                            this.UnremaneRoomButton.Enabled = true;
                        }
                    }
                    this.RoomContextMenu.Show(Cursor.Position);
                }
            }
        }

        private void RenameRoomButton_Click(object sender, EventArgs e)
        {
            if (renameRoomForm == null)
            {
                ListViewItem selectedRoomItem = this.RoomsList.FocusedItem;
                if (selectedRoomItem != null && selectedRoomItem.Tag != null)
                {
                    renameRoomForm = new RenameRoomForm(this.RoomsList.FocusedItem.Index, selectedRoomItem.Text, (string)selectedRoomItem.Tag);
                    renameRoomForm.FormClosing += RenameRoomForm_FormClosing;
                    renameRoomForm.ShowDialog(this);
                }
            }
            else
            {
                renameRoomForm.success = false;
                renameRoomForm.Close();
                renameRoomForm = null;
            }
        }

        private void UnremaneRoomButton_Click(object sender, EventArgs e)
        {
            if (this.RoomsList.SelectedItems.Count == 1)
            {
                ListViewItem selectedItem = this.RoomsList.SelectedItems[0];
                RemoveRoomOperationByTypeFor(selectedItem.Index, RoomOperation.RoomOperationType.Rename);
                Helpers.UpdateOperationColumnOnRoomItem(selectedItem, GetRoomOperationsFor(selectedItem.Index));
            }
            this.RoomsList.Focus();
        }

        private void RenameRoomForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (renameRoomForm != null)
            {
                if (renameRoomForm.success)
                {
                    ListViewItem targetRoomItem = this.RoomsList.Items[renameRoomForm.targetRoomIndex];
                    Console.WriteLine("Renaming room " + renameRoomForm.targetRoomIndex + " \"" + (string)targetRoomItem.Tag + "\" to \"" + renameRoomForm.newName + "\"");
                    if ((string)targetRoomItem.Tag != renameRoomForm.newName)
                    {
                        RoomOperation newOperation = RoomOperation.Rename(renameRoomForm.targetRoomIndex, renameRoomForm.newName);
                        AddRoomOperation(newOperation);
                        Helpers.UpdateOperationColumnOnRoomItem(targetRoomItem, GetRoomOperationsFor(renameRoomForm.targetRoomIndex));
                    }
                    else
                    {
                        RemoveRoomOperationByTypeFor(renameRoomForm.targetRoomIndex, RoomOperation.RoomOperationType.Rename);
                        Helpers.UpdateOperationColumnOnRoomItem(targetRoomItem, GetRoomOperationsFor(renameRoomForm.targetRoomIndex));
                    }
                    targetRoomItem.Text = renameRoomForm.newName;
                }
                renameRoomForm = null;
            }
        }

        private void TestButton_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Nothing here right now");
        }

        private void ReplaceRoomButton_Click(object sender, EventArgs e)
        {
            if (this.RoomsList.SelectedItems.Count == 1)
            {
                ListViewItem selectedItem = this.RoomsList.SelectedItems[0];

                this.AddLevelOpenDialogue.CheckFileExists = true;
                this.AddLevelOpenDialogue.CheckPathExists = true;
                this.AddLevelOpenDialogue.DefaultExt = ".lvl";
                this.AddLevelOpenDialogue.InitialDirectory = config.levelDirectory;
                this.AddLevelOpenDialogue.Filter = "PCQ Level Files(*.lvl)|*.lvl|All Files(*.*)|*.*";
                DialogResult dialogResult = this.AddLevelOpenDialogue.ShowDialog();
                if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Yes)
                {
                    Console.WriteLine("Selected \"" + this.AddLevelOpenDialogue.FileName + "\"");
                    RoomOperation newOperation = RoomOperation.Replace(selectedItem.Index, this.AddLevelOpenDialogue.FileName);
                    AddRoomOperation(newOperation);
                    Helpers.UpdateOperationColumnOnRoomItem(selectedItem, GetRoomOperationsFor(selectedItem.Index));
                    this.config.levelDirectory = Path.GetDirectoryName(this.AddLevelOpenDialogue.FileName);
                }
            }
            this.RoomsList.Focus();
        }
        private void UnreplaceRoomButton_Click(object sender, EventArgs e)
        {
            if (this.RoomsList.SelectedItems.Count == 1)
            {
                ListViewItem selectedItem = this.RoomsList.SelectedItems[0];
                RemoveRoomOperationByTypeFor(selectedItem.Index, RoomOperation.RoomOperationType.Replace);
                Helpers.UpdateOperationColumnOnRoomItem(selectedItem, GetRoomOperationsFor(selectedItem.Index));
            }
            this.RoomsList.Focus();
        }

        private void DeleteRoomButton_Click(object sender, EventArgs e)
        {
            if (this.RoomsList.SelectedItems.Count == 1)
            {
                ListViewItem selectedItem = this.RoomsList.SelectedItems[0];
                RoomOperation newOperation = RoomOperation.Delete(selectedItem.Index);
                AddRoomOperation(newOperation);
                Helpers.UpdateOperationColumnOnRoomItem(selectedItem, GetRoomOperationsFor(selectedItem.Index));
            }
            this.RoomsList.Focus();
        }
        private void UndeleteRoomButton_Click(object sender, EventArgs e)
        {
            if (this.RoomsList.SelectedItems.Count == 1)
            {
                ListViewItem selectedItem = this.RoomsList.SelectedItems[0];
                RemoveRoomOperationByTypeFor(selectedItem.Index, RoomOperation.RoomOperationType.Delete);
                Helpers.UpdateOperationColumnOnRoomItem(selectedItem, GetRoomOperationsFor(selectedItem.Index));
            }
            this.RoomsList.Focus();
        }

        private void RoomsList_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.RoomsList.SelectedItems.Count > 0)
            {
                //ListViewItem selectedItem = this.RoomsList.SelectedItems[0];
                if (e.KeyCode == Keys.Delete)
                {
                    DeleteRoomButton_Click(sender, null);
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Insert)
                {
                    ReplaceRoomButton_Click(sender, null);
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    RenameRoomButton_Click(sender, null);
                    e.Handled = true;
                }
            }
        }

        private void ApplySearchBox()
        {
            if (this.MainTabView.SelectedTab == RoomsTab)
            {
                //TODO: Make this work with all the room list manipulate we are doing
                //if (this.roomListAppliedSearch != this.SearchTextBox.Text)
                //{
                //    UpdateListBySearchText(this.RoomsList, this.roomItems, this.SearchTextBox.Text);
                //    this.roomListAppliedSearch = this.SearchTextBox.Text;
                //}
            }
            else if (this.MainTabView.SelectedTab == ObjectsTab)
            {
                if (this.objectListAppliedSearch != this.SearchTextBox.Text)
                {
                    UpdateListBySearchText(this.ObjectsList, this.objectItems, this.SearchTextBox.Text);
                    this.objectListAppliedSearch = this.SearchTextBox.Text;
                }
            }
            else if (this.MainTabView.SelectedTab == SpritesTab)
            {
                if (this.spriteListAppliedSearch != this.SearchTextBox.Text)
                {
                    UpdateListBySearchText(this.SpritesList, this.spriteItems, this.SearchTextBox.Text);
                    this.spriteListAppliedSearch = this.SearchTextBox.Text;
                }
            }
            else if (this.MainTabView.SelectedTab == StringsTab)
            {
                if (this.stringListAppliedSearch != this.SearchTextBox.Text)
                {
                    UpdateListBySearchText(this.StringsList, this.stringItems, this.SearchTextBox.Text);
                    this.stringListAppliedSearch = this.SearchTextBox.Text;
                }
            }
            else if (this.MainTabView.SelectedTab == VariablesTab)
            {
                if (this.variableListAppliedSearch != this.SearchTextBox.Text)
                {
                    UpdateListBySearchText(this.VariablesList, this.variableItems, this.SearchTextBox.Text);
                    this.variableListAppliedSearch = this.SearchTextBox.Text;
                }
            }
            else if (this.MainTabView.SelectedTab == CodesTab)
            {
                if (this.codeListAppliedSearch != this.SearchTextBox.Text)
                {
                    UpdateListBySearchText(this.CodesList, this.codeItems, this.SearchTextBox.Text);
                    this.codeListAppliedSearch = this.SearchTextBox.Text;
                }
            }
        }
        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ApplySearchBox();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }
        private void SearchTextBox_Leave(object sender, EventArgs e)
        {
            ApplySearchBox();
        }

        private void CodesList_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ListViewItem focusedItem = this.CodesList.FocusedItem;
                if (focusedItem != null && focusedItem.Bounds.Contains(e.Location))
                {
                    this.CodeContextMenu.Show(Cursor.Position);
                }
            }
        }
        private void CodeViewButton_Click(object sender, EventArgs e)
        {
            if (CodesList.SelectedItems.Count >= 1)
            {
                ListViewItem selectedItem = CodesList.SelectedItems[0];
                CodeViewForm codeViewForm = new CodeViewForm((int)selectedItem.SubItems[0].Tag);
                codeViewForm.Show();
            }
        }
    }
}
