﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkurraDataInjector
{
    public partial class RenameRoomForm : Form
    {
        public int targetRoomIndex;
        public string originalName;
        public string oldName;
        public string newName;
        public bool success;

        public RenameRoomForm(int roomIndex, string currentRoomName, string originalName)
        {
            InitializeComponent();
            this.targetRoomIndex = roomIndex;
            this.originalName = originalName;
            this.oldName = currentRoomName;
            this.newName = currentRoomName;
            this.success = false;
            this.NameLabel.Text = originalName;
            this.NewNameTextbox.Text = currentRoomName;
        }

        private void FinalizeRename()
        {
            newName = this.NewNameTextbox.Text;
            if (newName != "" && newName != oldName)
            {
                success = true;
            }
            this.Close();
        }

        private void RenameButton_Click(object sender, EventArgs e)
        {
            FinalizeRename();
        }

        private void NewNameTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                FinalizeRename();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Close();
                e.Handled = true;
            }
        }
    }
}
