﻿namespace AkurraDataInjector
{
    partial class GmlCompilationCompare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GmlCompilationCompare));
            this.SrcPathTextbox = new System.Windows.Forms.TextBox();
            this.SrcLabel = new System.Windows.Forms.Label();
            this.TempLabel = new System.Windows.Forms.Label();
            this.TempPathTextbox = new System.Windows.Forms.TextBox();
            this.CheckButton = new System.Windows.Forms.Button();
            this.MainContainer = new System.Windows.Forms.SplitContainer();
            this.LeftTextbox = new System.Windows.Forms.TextBox();
            this.SrcBrowseBtn = new System.Windows.Forms.Button();
            this.TempBrowseBtn = new System.Windows.Forms.Button();
            this.SrcFileDialogue = new System.Windows.Forms.OpenFileDialog();
            this.TempFolderDialogue = new System.Windows.Forms.FolderBrowserDialog();
            this.CodeNameTextbox = new System.Windows.Forms.TextBox();
            this.CodeNameLabel = new System.Windows.Forms.Label();
            this.RightTextbox = new System.Windows.Forms.RichTextBox();
            this.OpenPackButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.MainContainer)).BeginInit();
            this.MainContainer.Panel1.SuspendLayout();
            this.MainContainer.Panel2.SuspendLayout();
            this.MainContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // SrcPathTextbox
            // 
            this.SrcPathTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SrcPathTextbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.SrcPathTextbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SrcPathTextbox.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SrcPathTextbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.SrcPathTextbox.Location = new System.Drawing.Point(84, 12);
            this.SrcPathTextbox.Name = "SrcPathTextbox";
            this.SrcPathTextbox.Size = new System.Drawing.Size(216, 27);
            this.SrcPathTextbox.TabIndex = 2;
            this.SrcPathTextbox.Text = "source.gml";
            // 
            // SrcLabel
            // 
            this.SrcLabel.AutoSize = true;
            this.SrcLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(204)))), ((int)(((byte)(193)))));
            this.SrcLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SrcLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.SrcLabel.Location = new System.Drawing.Point(12, 13);
            this.SrcLabel.Name = "SrcLabel";
            this.SrcLabel.Size = new System.Drawing.Size(66, 20);
            this.SrcLabel.TabIndex = 19;
            this.SrcLabel.Text = "Source";
            // 
            // TempLabel
            // 
            this.TempLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TempLabel.AutoSize = true;
            this.TempLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(204)))), ((int)(((byte)(193)))));
            this.TempLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TempLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.TempLabel.Location = new System.Drawing.Point(350, 14);
            this.TempLabel.Name = "TempLabel";
            this.TempLabel.Size = new System.Drawing.Size(53, 20);
            this.TempLabel.TabIndex = 20;
            this.TempLabel.Text = "Temp";
            // 
            // TempPathTextbox
            // 
            this.TempPathTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TempPathTextbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.TempPathTextbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TempPathTextbox.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TempPathTextbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.TempPathTextbox.Location = new System.Drawing.Point(409, 13);
            this.TempPathTextbox.Name = "TempPathTextbox";
            this.TempPathTextbox.Size = new System.Drawing.Size(384, 27);
            this.TempPathTextbox.TabIndex = 21;
            this.TempPathTextbox.Text = "C:\\Users\\robbitay\\AppData\\Local\\GameMakerStudio2\\GMS2TEMP";
            // 
            // CheckButton
            // 
            this.CheckButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(194)))));
            this.CheckButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CheckButton.Font = new System.Drawing.Font("MS Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.CheckButton.Image = ((System.Drawing.Image)(resources.GetObject("CheckButton.Image")));
            this.CheckButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CheckButton.Location = new System.Drawing.Point(632, 436);
            this.CheckButton.Name = "CheckButton";
            this.CheckButton.Size = new System.Drawing.Size(248, 33);
            this.CheckButton.TabIndex = 22;
            this.CheckButton.Text = "Check";
            this.CheckButton.UseVisualStyleBackColor = false;
            this.CheckButton.Click += new System.EventHandler(this.CheckButton_Click);
            // 
            // MainContainer
            // 
            this.MainContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainContainer.Location = new System.Drawing.Point(0, 45);
            this.MainContainer.Name = "MainContainer";
            // 
            // MainContainer.Panel1
            // 
            this.MainContainer.Panel1.Controls.Add(this.LeftTextbox);
            // 
            // MainContainer.Panel2
            // 
            this.MainContainer.Panel2.Controls.Add(this.RightTextbox);
            this.MainContainer.Size = new System.Drawing.Size(893, 385);
            this.MainContainer.SplitterDistance = 433;
            this.MainContainer.TabIndex = 23;
            // 
            // LeftTextbox
            // 
            this.LeftTextbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.LeftTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LeftTextbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftTextbox.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftTextbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(177)))), ((int)(((byte)(255)))));
            this.LeftTextbox.Location = new System.Drawing.Point(0, 0);
            this.LeftTextbox.Multiline = true;
            this.LeftTextbox.Name = "LeftTextbox";
            this.LeftTextbox.ReadOnly = true;
            this.LeftTextbox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.LeftTextbox.Size = new System.Drawing.Size(433, 385);
            this.LeftTextbox.TabIndex = 0;
            this.LeftTextbox.Text = "[Nothing opened yet...]";
            this.LeftTextbox.WordWrap = false;
            // 
            // SrcBrowseBtn
            // 
            this.SrcBrowseBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SrcBrowseBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.SrcBrowseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SrcBrowseBtn.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SrcBrowseBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(229)))), ((int)(((byte)(140)))));
            this.SrcBrowseBtn.Location = new System.Drawing.Point(306, 13);
            this.SrcBrowseBtn.Name = "SrcBrowseBtn";
            this.SrcBrowseBtn.Size = new System.Drawing.Size(38, 27);
            this.SrcBrowseBtn.TabIndex = 24;
            this.SrcBrowseBtn.Text = "...";
            this.SrcBrowseBtn.UseVisualStyleBackColor = false;
            this.SrcBrowseBtn.Click += new System.EventHandler(this.SrcBrowseBtn_Click);
            // 
            // TempBrowseBtn
            // 
            this.TempBrowseBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TempBrowseBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.TempBrowseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TempBrowseBtn.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TempBrowseBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(229)))), ((int)(((byte)(140)))));
            this.TempBrowseBtn.Location = new System.Drawing.Point(799, 14);
            this.TempBrowseBtn.Name = "TempBrowseBtn";
            this.TempBrowseBtn.Size = new System.Drawing.Size(38, 27);
            this.TempBrowseBtn.TabIndex = 25;
            this.TempBrowseBtn.Text = "...";
            this.TempBrowseBtn.UseVisualStyleBackColor = false;
            this.TempBrowseBtn.Click += new System.EventHandler(this.TempBrowseBtn_Click);
            // 
            // SrcFileDialogue
            // 
            this.SrcFileDialogue.FileName = "source.gml";
            this.SrcFileDialogue.Title = "Select the data.win file for your Akurra installation...";
            // 
            // CodeNameTextbox
            // 
            this.CodeNameTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CodeNameTextbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.CodeNameTextbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CodeNameTextbox.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CodeNameTextbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.CodeNameTextbox.Location = new System.Drawing.Point(120, 436);
            this.CodeNameTextbox.Name = "CodeNameTextbox";
            this.CodeNameTextbox.Size = new System.Drawing.Size(506, 27);
            this.CodeNameTextbox.TabIndex = 26;
            // 
            // CodeNameLabel
            // 
            this.CodeNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CodeNameLabel.AutoSize = true;
            this.CodeNameLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(204)))), ((int)(((byte)(193)))));
            this.CodeNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CodeNameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.CodeNameLabel.Location = new System.Drawing.Point(12, 437);
            this.CodeNameLabel.Name = "CodeNameLabel";
            this.CodeNameLabel.Size = new System.Drawing.Size(102, 20);
            this.CodeNameLabel.TabIndex = 27;
            this.CodeNameLabel.Text = "Code Name";
            // 
            // RightTextbox
            // 
            this.RightTextbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(194)))));
            this.RightTextbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RightTextbox.Font = new System.Drawing.Font("Consolas", 14F, System.Drawing.FontStyle.Bold);
            this.RightTextbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.RightTextbox.Location = new System.Drawing.Point(0, 0);
            this.RightTextbox.Name = "RightTextbox";
            this.RightTextbox.ReadOnly = true;
            this.RightTextbox.Size = new System.Drawing.Size(456, 385);
            this.RightTextbox.TabIndex = 25;
            this.RightTextbox.Text = "[Nothing opened yet...]";
            this.RightTextbox.WordWrap = false;
            // 
            // OpenPackButton
            // 
            this.OpenPackButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenPackButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.OpenPackButton.Enabled = false;
            this.OpenPackButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OpenPackButton.Font = new System.Drawing.Font("Consolas", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenPackButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.OpenPackButton.Location = new System.Drawing.Point(843, 14);
            this.OpenPackButton.Name = "OpenPackButton";
            this.OpenPackButton.Size = new System.Drawing.Size(38, 27);
            this.OpenPackButton.TabIndex = 28;
            this.OpenPackButton.Text = "📖";
            this.OpenPackButton.UseVisualStyleBackColor = false;
            this.OpenPackButton.Click += new System.EventHandler(this.OpenPackButton_Click);
            // 
            // GmlCompilationCompare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(892, 472);
            this.Controls.Add(this.OpenPackButton);
            this.Controls.Add(this.CodeNameLabel);
            this.Controls.Add(this.CodeNameTextbox);
            this.Controls.Add(this.TempBrowseBtn);
            this.Controls.Add(this.SrcBrowseBtn);
            this.Controls.Add(this.MainContainer);
            this.Controls.Add(this.CheckButton);
            this.Controls.Add(this.TempPathTextbox);
            this.Controls.Add(this.TempLabel);
            this.Controls.Add(this.SrcLabel);
            this.Controls.Add(this.SrcPathTextbox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GmlCompilationCompare";
            this.Text = "Compilation Compare";
            this.Load += new System.EventHandler(this.GmlCompilationCompare_Load);
            this.MainContainer.Panel1.ResumeLayout(false);
            this.MainContainer.Panel1.PerformLayout();
            this.MainContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainContainer)).EndInit();
            this.MainContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox SrcPathTextbox;
        private System.Windows.Forms.Label SrcLabel;
        private System.Windows.Forms.Label TempLabel;
        public System.Windows.Forms.TextBox TempPathTextbox;
        private System.Windows.Forms.Button CheckButton;
        private System.Windows.Forms.SplitContainer MainContainer;
        private System.Windows.Forms.TextBox LeftTextbox;
        private System.Windows.Forms.Button SrcBrowseBtn;
        private System.Windows.Forms.Button TempBrowseBtn;
        private System.Windows.Forms.OpenFileDialog SrcFileDialogue;
        private System.Windows.Forms.FolderBrowserDialog TempFolderDialogue;
        public System.Windows.Forms.TextBox CodeNameTextbox;
        private System.Windows.Forms.Label CodeNameLabel;
        private System.Windows.Forms.RichTextBox RightTextbox;
        private System.Windows.Forms.Button OpenPackButton;
    }
}