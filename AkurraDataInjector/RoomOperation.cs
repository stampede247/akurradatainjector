﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkurraDataInjector
{
    public struct RoomOperation
    {
        public enum RoomOperationType
        {
            None = 0,
            Delete = 1,
            Rename = 2,
            Replace = 3,
        }

        public int roomIndex;
        public RoomOperationType type;
        public string filePath;
        public string newName;

        public static RoomOperation None(int roomIndex)
        {
            return new RoomOperation() { roomIndex = roomIndex, type = RoomOperationType.None };
        }
        public static RoomOperation Delete(int roomIndex)
        {
            return new RoomOperation() { roomIndex = roomIndex, type = RoomOperationType.Delete };
        }
        public static RoomOperation Rename(int roomIndex, string newName)
        {
            return new RoomOperation() { roomIndex = roomIndex, type = RoomOperationType.Rename, newName = newName };
        }
        public static RoomOperation Replace(int roomIndex, string filePath)
        {
            return new RoomOperation() { roomIndex = roomIndex, type = RoomOperationType.Replace, filePath = filePath };
        }

        public static bool DoOperationTypesConflict(RoomOperationType type1, RoomOperationType type2)
        {
            if (type1 != type2)
            {
                //Delete operations conflict with everything
                if (type1 == RoomOperationType.Delete || type2 == RoomOperationType.Delete)
                {
                    return true;
                }
                //Renames don't conflict with most other operations
                if (type1 == RoomOperationType.Rename || type2 == RoomOperationType.Rename)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
