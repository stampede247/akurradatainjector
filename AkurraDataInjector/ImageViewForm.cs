﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkurraDataInjector
{
    public partial class ImageViewForm : Form
    {
        InjectorForm parent;
        string imageName;

        public ImageViewForm(InjectorForm parent, string imageName)
        {
            InitializeComponent();
            this.parent = parent;
            this.imageName = imageName;
        }

        private void ImageViewForm_Load(object sender, EventArgs e)
        {

        }

        private void ImageView_MouseClick(object sender, MouseEventArgs e)
        {
            float imageScale = Math.Min((float)this.ImageView.Bounds.Width / (float)this.ImageView.Image.Width, (float)this.ImageView.Bounds.Height / (float)this.ImageView.Image.Height);
            SizeF imageSize = new SizeF((float)this.ImageView.Image.Size.Width * imageScale, (float)this.ImageView.Image.Size.Height * imageScale);
            PointF margins = new PointF(this.ImageView.Bounds.Width - imageSize.Width, this.ImageView.Bounds.Height - imageSize.Height);
            PointF imageOffset = new PointF(margins.X / 2, margins.Y / 2);
            PointF actualMousePos = new PointF((float)e.Location.X - imageOffset.X, (float)e.Location.Y - imageOffset.Y);
            actualMousePos.X /= imageScale;
            actualMousePos.Y /= imageScale;
            Console.WriteLine(actualMousePos);

            AkurraParser.Parser_HandleCommand("image_click " + actualMousePos.X + " " + actualMousePos.Y + " " + this.imageName);
        }
    }
}
