﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkurraDataInjector
{
    public partial class CodeViewForm : Form
    {
        public struct BytecodeWord
        {
            public UInt32 address;
            public UInt32 word;
            public UInt16 Upper { get { return (UInt16)((word & 0xFFFF0000) >> 16); } }
            public UInt16 Lower { get { return (UInt16)((word & 0x0000FFFF) >>  0); } }
        }

        public int codeIndex;
        public string codeName;
        public UInt32 codeStartAddress;
        public BytecodeWord[] bytecode;
        public string formattedBytecode;

        public bool showAddresses = true;
        public bool showInterpretations = true;
        public bool showVariables = true;
        public bool showDecompiles = true;

        public CodeViewForm(int codeIndex)
        {
            InitializeComponent();
            this.codeIndex = codeIndex;
            AkurraParser.Parser_PushTempMark();

            AkurraParser.CodeInfo_t codeInfo = AkurraParser.Parser_GetCodeInfo(codeIndex);
            this.codeName = Marshal.PtrToStringAnsi(codeInfo.name);
            this.codeStartAddress = codeInfo.bytecodeOffset;
            int bytecodeLength = (int)(codeInfo.bytecodeLength / 4);

            IntPtr bytecodeStrPntr = AkurraParser.Parser_GetCodeBytecode(codeIndex);
            string bytecodeStr = Marshal.PtrToStringAnsi(bytecodeStrPntr);
            if (bytecodeStr.EndsWith(" ")) { bytecodeStr = bytecodeStr.Substring(0, bytecodeStr.Length - 1); }
            UInt32[] bytecodeWords = (bytecodeStr.Length > 0) ? bytecodeStr.Split(' ').Select(wordHexStr => Convert.ToUInt32(wordHexStr, 16)).ToArray() : new UInt32[0];

            this.bytecode = new BytecodeWord[bytecodeWords.Length];
            for (int wIndex = 0; wIndex < bytecodeWords.Length; wIndex++)
            {
                this.bytecode[wIndex] = new BytecodeWord() { word = bytecodeWords[wIndex], address = codeInfo.bytecodeOffset + (UInt32)(wIndex * 4) };
            }

            LoadFormattedBytecode();

            this.CodeNameValueLabel.Text = this.codeName;
            this.Text = this.codeName;
            this.SizeValueLabel.Text = $"{this.bytecode.Length} words";

            AkurraParser.Parser_PopTempMark();
        }

        private void LoadFormattedBytecode()
        {
            AkurraParser.Parser_PushTempMark();
            IntPtr formattedBytecodeStrPntr = AkurraParser.Parser_GetCodeFormattedBytecode(codeIndex, showAddresses, showInterpretations, showVariables, showDecompiles);
            this.formattedBytecode = Marshal.PtrToStringAnsi(formattedBytecodeStrPntr);
            Helpers.PutFormattedBytecodeInRichTextbox(this.MainTextbox, this.formattedBytecode);
            AkurraParser.Parser_PopTempMark();
        }

        private void ShowAddressButton_Click(object sender, EventArgs e)
        {
            showAddresses = !showAddresses;
            this.ShowAddressButton.Checked = showAddresses;
            LoadFormattedBytecode();
        }
        private void ShowInterpretationsButton_Click(object sender, EventArgs e)
        {
            showInterpretations = !showInterpretations;
            this.ShowInterpretationsButton.Checked = showInterpretations;
            LoadFormattedBytecode();
        }
        private void ShowVariablesButton_Click(object sender, EventArgs e)
        {
            showVariables = !showVariables;
            this.ShowVariablesButton.Checked = showVariables;
            LoadFormattedBytecode();
        }
        private void ShowDecompileButton_Click(object sender, EventArgs e)
        {
            showDecompiles = !showDecompiles;
            this.ShowDecompileButton.Checked = showDecompiles;
            LoadFormattedBytecode();
        }
    }
}
