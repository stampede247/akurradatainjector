﻿namespace AkurraDataInjector
{
    partial class CodeViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CodeViewForm));
            this.CodeNameLabel = new System.Windows.Forms.Label();
            this.CodeNameValueLabel = new System.Windows.Forms.Label();
            this.SizeLabel = new System.Windows.Forms.Label();
            this.SizeValueLabel = new System.Windows.Forms.Label();
            this.MainTextbox = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowAddressButton = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowInterpretationsButton = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowVariablesButton = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowDecompileButton = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CodeNameLabel
            // 
            this.CodeNameLabel.AutoSize = true;
            this.CodeNameLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.CodeNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CodeNameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.CodeNameLabel.Location = new System.Drawing.Point(12, 30);
            this.CodeNameLabel.Name = "CodeNameLabel";
            this.CodeNameLabel.Size = new System.Drawing.Size(60, 20);
            this.CodeNameLabel.TabIndex = 19;
            this.CodeNameLabel.Text = "Name:";
            // 
            // CodeNameValueLabel
            // 
            this.CodeNameValueLabel.AutoSize = true;
            this.CodeNameValueLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.CodeNameValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CodeNameValueLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.CodeNameValueLabel.Location = new System.Drawing.Point(72, 30);
            this.CodeNameValueLabel.Name = "CodeNameValueLabel";
            this.CodeNameValueLabel.Size = new System.Drawing.Size(98, 20);
            this.CodeNameValueLabel.TabIndex = 20;
            this.CodeNameValueLabel.Text = "[Loading...]";
            // 
            // SizeLabel
            // 
            this.SizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SizeLabel.AutoSize = true;
            this.SizeLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.SizeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SizeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.SizeLabel.Location = new System.Drawing.Point(527, 30);
            this.SizeLabel.Name = "SizeLabel";
            this.SizeLabel.Size = new System.Drawing.Size(49, 20);
            this.SizeLabel.TabIndex = 22;
            this.SizeLabel.Text = "Size:";
            // 
            // SizeValueLabel
            // 
            this.SizeValueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SizeValueLabel.AutoSize = true;
            this.SizeValueLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.SizeValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SizeValueLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.SizeValueLabel.Location = new System.Drawing.Point(573, 30);
            this.SizeValueLabel.Name = "SizeValueLabel";
            this.SizeValueLabel.Size = new System.Drawing.Size(98, 20);
            this.SizeValueLabel.TabIndex = 23;
            this.SizeValueLabel.Text = "[Loading...]";
            this.SizeValueLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // MainTextbox
            // 
            this.MainTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainTextbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(194)))));
            this.MainTextbox.Font = new System.Drawing.Font("Consolas", 14F, System.Drawing.FontStyle.Bold);
            this.MainTextbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.MainTextbox.Location = new System.Drawing.Point(-1, 53);
            this.MainTextbox.Name = "MainTextbox";
            this.MainTextbox.ReadOnly = true;
            this.MainTextbox.Size = new System.Drawing.Size(685, 592);
            this.MainTextbox.TabIndex = 24;
            this.MainTextbox.Text = "Loading...";
            this.MainTextbox.WordWrap = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(683, 24);
            this.menuStrip1.TabIndex = 25;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShowAddressButton,
            this.ShowInterpretationsButton,
            this.ShowVariablesButton,
            this.ShowDecompileButton});
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.showToolStripMenuItem.Text = "Show";
            // 
            // ShowAddressButton
            // 
            this.ShowAddressButton.Checked = true;
            this.ShowAddressButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowAddressButton.Name = "ShowAddressButton";
            this.ShowAddressButton.Size = new System.Drawing.Size(151, 22);
            this.ShowAddressButton.Text = "Address";
            this.ShowAddressButton.Click += new System.EventHandler(this.ShowAddressButton_Click);
            // 
            // ShowInterpretationsButton
            // 
            this.ShowInterpretationsButton.Checked = true;
            this.ShowInterpretationsButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowInterpretationsButton.Name = "ShowInterpretationsButton";
            this.ShowInterpretationsButton.Size = new System.Drawing.Size(151, 22);
            this.ShowInterpretationsButton.Text = "Interpretations";
            this.ShowInterpretationsButton.Click += new System.EventHandler(this.ShowInterpretationsButton_Click);
            // 
            // ShowVariablesButton
            // 
            this.ShowVariablesButton.Checked = true;
            this.ShowVariablesButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowVariablesButton.Name = "ShowVariablesButton";
            this.ShowVariablesButton.Size = new System.Drawing.Size(151, 22);
            this.ShowVariablesButton.Text = "Variables";
            this.ShowVariablesButton.Click += new System.EventHandler(this.ShowVariablesButton_Click);
            // 
            // ShowDecompileButton
            // 
            this.ShowDecompileButton.Checked = true;
            this.ShowDecompileButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowDecompileButton.Name = "ShowDecompileButton";
            this.ShowDecompileButton.Size = new System.Drawing.Size(151, 22);
            this.ShowDecompileButton.Text = "Decompile";
            this.ShowDecompileButton.Click += new System.EventHandler(this.ShowDecompileButton_Click);
            // 
            // CodeViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(148)))), ((int)(((byte)(181)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(683, 645);
            this.Controls.Add(this.MainTextbox);
            this.Controls.Add(this.SizeValueLabel);
            this.Controls.Add(this.SizeLabel);
            this.Controls.Add(this.CodeNameValueLabel);
            this.Controls.Add(this.CodeNameLabel);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "CodeViewForm";
            this.Text = "Code";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CodeNameLabel;
        private System.Windows.Forms.Label CodeNameValueLabel;
        private System.Windows.Forms.Label SizeLabel;
        private System.Windows.Forms.Label SizeValueLabel;
        private System.Windows.Forms.RichTextBox MainTextbox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowAddressButton;
        private System.Windows.Forms.ToolStripMenuItem ShowInterpretationsButton;
        private System.Windows.Forms.ToolStripMenuItem ShowVariablesButton;
        private System.Windows.Forms.ToolStripMenuItem ShowDecompileButton;
    }
}