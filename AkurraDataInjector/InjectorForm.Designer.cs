﻿namespace AkurraDataInjector
{
    partial class InjectorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InjectorForm));
            this.InjectButton = new System.Windows.Forms.Button();
            this.SrcFileBox = new System.Windows.Forms.TextBox();
            this.SrcBrowseBtn = new System.Windows.Forms.Button();
            this.DestFileBox = new System.Windows.Forms.TextBox();
            this.SrcFileDialogue = new System.Windows.Forms.OpenFileDialog();
            this.DestFileDialogue = new System.Windows.Forms.SaveFileDialog();
            this.DestBrowseBtn = new System.Windows.Forms.Button();
            this.RoomsList = new System.Windows.Forms.ListView();
            this.NameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SizeHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ObjectCountHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CollisionCountHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.OtherCountHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RoomOperationHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DebugOutputBox = new System.Windows.Forms.RichTextBox();
            this.SrcLoadBtn = new System.Windows.Forms.Button();
            this.CommandBox = new System.Windows.Forms.TextBox();
            this.SendBtn = new System.Windows.Forms.Button();
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.MainTabView = new System.Windows.Forms.TabControl();
            this.RoomsTab = new System.Windows.Forms.TabPage();
            this.ObjectsTab = new System.Windows.Forms.TabPage();
            this.ObjectsList = new System.Windows.Forms.ListView();
            this.ObjectIdHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ObjectNameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ObjectSpriteNameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ObjectFlagsHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ObjectParentIdHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ObjectMaskIdHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ObjectDepthHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SpritesTab = new System.Windows.Forms.TabPage();
            this.SpritesList = new System.Windows.Forms.ListView();
            this.SpriteIndexHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SpriteNameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SpriteSizeHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SpriteOriginHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SpriteNumFramesHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SpriteSpeedHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SpriteFormatHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.StringsTab = new System.Windows.Forms.TabPage();
            this.StringsList = new System.Windows.Forms.ListView();
            this.IndexHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ValueHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.OffsetHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.LengthHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.VariablesTab = new System.Windows.Forms.TabPage();
            this.VariablesList = new System.Windows.Forms.ListView();
            this.VarIndexHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.VarNameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.VarFirstRefHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.VarCodeIndexHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.VarNumReferencesHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CodesTab = new System.Windows.Forms.TabPage();
            this.CodesList = new System.Windows.Forms.ListView();
            this.CodeIndexHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CodeNameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CodeAddressHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CodeSizeHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CodeNumVariablesHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AddLevelOpenDialogue = new System.Windows.Forms.OpenFileDialog();
            this.InfoBtn = new System.Windows.Forms.Button();
            this.SrcLabel = new System.Windows.Forms.Label();
            this.DestLabel = new System.Windows.Forms.Label();
            this.InfoComboBox = new System.Windows.Forms.ComboBox();
            this.ClearLogBtn = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CompilationCompareButton = new System.Windows.Forms.ToolStripMenuItem();
            this.TestButton = new System.Windows.Forms.ToolStripMenuItem();
            this.RoomContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.RenameRoomButton = new System.Windows.Forms.ToolStripMenuItem();
            this.UnremaneRoomButton = new System.Windows.Forms.ToolStripMenuItem();
            this.ReplaceRoomButton = new System.Windows.Forms.ToolStripMenuItem();
            this.UnreplaceRoomButton = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteRoomButton = new System.Windows.Forms.ToolStripMenuItem();
            this.UndeleteRoomButton = new System.Windows.Forms.ToolStripMenuItem();
            this.SearchTextBox = new System.Windows.Forms.TextBox();
            this.CodeContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CodeViewButton = new System.Windows.Forms.ToolStripMenuItem();
            this.RoomImageBox = new AkurraDataInjector.PictureBoxWithInterpolationMode();
            this.CodeNumFunctionsHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.StatusBar.SuspendLayout();
            this.MainTabView.SuspendLayout();
            this.RoomsTab.SuspendLayout();
            this.ObjectsTab.SuspendLayout();
            this.SpritesTab.SuspendLayout();
            this.StringsTab.SuspendLayout();
            this.VariablesTab.SuspendLayout();
            this.CodesTab.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.RoomContextMenu.SuspendLayout();
            this.CodeContextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RoomImageBox)).BeginInit();
            this.SuspendLayout();
            // 
            // InjectButton
            // 
            this.InjectButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.InjectButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(194)))));
            this.InjectButton.Enabled = false;
            this.InjectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InjectButton.Font = new System.Drawing.Font("MS Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InjectButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.InjectButton.Image = ((System.Drawing.Image)(resources.GetObject("InjectButton.Image")));
            this.InjectButton.Location = new System.Drawing.Point(579, 519);
            this.InjectButton.Name = "InjectButton";
            this.InjectButton.Size = new System.Drawing.Size(274, 32);
            this.InjectButton.TabIndex = 0;
            this.InjectButton.Text = "Inject";
            this.InjectButton.UseVisualStyleBackColor = false;
            this.InjectButton.Click += new System.EventHandler(this.InjectButton_Click);
            // 
            // SrcFileBox
            // 
            this.SrcFileBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SrcFileBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.SrcFileBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SrcFileBox.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SrcFileBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.SrcFileBox.Location = new System.Drawing.Point(61, 26);
            this.SrcFileBox.Name = "SrcFileBox";
            this.SrcFileBox.Size = new System.Drawing.Size(672, 27);
            this.SrcFileBox.TabIndex = 1;
            this.SrcFileBox.Text = "data.win";
            // 
            // SrcBrowseBtn
            // 
            this.SrcBrowseBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SrcBrowseBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.SrcBrowseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SrcBrowseBtn.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SrcBrowseBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(229)))), ((int)(((byte)(140)))));
            this.SrcBrowseBtn.Location = new System.Drawing.Point(739, 27);
            this.SrcBrowseBtn.Name = "SrcBrowseBtn";
            this.SrcBrowseBtn.Size = new System.Drawing.Size(38, 27);
            this.SrcBrowseBtn.TabIndex = 2;
            this.SrcBrowseBtn.Text = "...";
            this.SrcBrowseBtn.UseVisualStyleBackColor = false;
            this.SrcBrowseBtn.Click += new System.EventHandler(this.SrcBrowseBtn_Click);
            // 
            // DestFileBox
            // 
            this.DestFileBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DestFileBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.DestFileBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DestFileBox.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DestFileBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.DestFileBox.Location = new System.Drawing.Point(74, 525);
            this.DestFileBox.Name = "DestFileBox";
            this.DestFileBox.Size = new System.Drawing.Size(464, 27);
            this.DestFileBox.TabIndex = 3;
            this.DestFileBox.Text = "data.win";
            // 
            // SrcFileDialogue
            // 
            this.SrcFileDialogue.FileName = "data.win";
            this.SrcFileDialogue.Title = "Select the data.win file for your Akurra installation...";
            // 
            // DestBrowseBtn
            // 
            this.DestBrowseBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.DestBrowseBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.DestBrowseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DestBrowseBtn.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DestBrowseBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(229)))), ((int)(((byte)(140)))));
            this.DestBrowseBtn.Location = new System.Drawing.Point(539, 525);
            this.DestBrowseBtn.Name = "DestBrowseBtn";
            this.DestBrowseBtn.Size = new System.Drawing.Size(38, 27);
            this.DestBrowseBtn.TabIndex = 5;
            this.DestBrowseBtn.Text = "...";
            this.DestBrowseBtn.UseVisualStyleBackColor = false;
            this.DestBrowseBtn.Click += new System.EventHandler(this.DestBrowseBtn_Click);
            // 
            // RoomsList
            // 
            this.RoomsList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(204)))), ((int)(((byte)(193)))));
            this.RoomsList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RoomsList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.NameHeader,
            this.SizeHeader,
            this.ObjectCountHeader,
            this.CollisionCountHeader,
            this.OtherCountHeader,
            this.RoomOperationHeader});
            this.RoomsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RoomsList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.RoomsList.FullRowSelect = true;
            this.RoomsList.HideSelection = false;
            this.RoomsList.Location = new System.Drawing.Point(3, 3);
            this.RoomsList.MultiSelect = false;
            this.RoomsList.Name = "RoomsList";
            this.RoomsList.Size = new System.Drawing.Size(551, 406);
            this.RoomsList.TabIndex = 7;
            this.RoomsList.UseCompatibleStateImageBehavior = false;
            this.RoomsList.View = System.Windows.Forms.View.Details;
            this.RoomsList.SelectedIndexChanged += new System.EventHandler(this.RoomsList_SelectedIndexChanged);
            this.RoomsList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RoomsList_KeyDown);
            this.RoomsList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RoomsList_MouseClick);
            // 
            // NameHeader
            // 
            this.NameHeader.Text = "Name";
            this.NameHeader.Width = 150;
            // 
            // SizeHeader
            // 
            this.SizeHeader.Text = "Size";
            // 
            // ObjectCountHeader
            // 
            this.ObjectCountHeader.Text = "# Objects";
            this.ObjectCountHeader.Width = 61;
            // 
            // CollisionCountHeader
            // 
            this.CollisionCountHeader.Text = "# Collision";
            this.CollisionCountHeader.Width = 61;
            // 
            // OtherCountHeader
            // 
            this.OtherCountHeader.Text = "# Other";
            this.OtherCountHeader.Width = 49;
            // 
            // RoomOperationHeader
            // 
            this.RoomOperationHeader.Text = "Operation";
            this.RoomOperationHeader.Width = 600;
            // 
            // DebugOutputBox
            // 
            this.DebugOutputBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DebugOutputBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.DebugOutputBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DebugOutputBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DebugOutputBox.ForeColor = System.Drawing.Color.White;
            this.DebugOutputBox.Location = new System.Drawing.Point(579, 60);
            this.DebugOutputBox.Name = "DebugOutputBox";
            this.DebugOutputBox.ReadOnly = true;
            this.DebugOutputBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.DebugOutputBox.Size = new System.Drawing.Size(274, 309);
            this.DebugOutputBox.TabIndex = 10;
            this.DebugOutputBox.Text = "";
            // 
            // SrcLoadBtn
            // 
            this.SrcLoadBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SrcLoadBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(194)))));
            this.SrcLoadBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SrcLoadBtn.Font = new System.Drawing.Font("MS Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SrcLoadBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.SrcLoadBtn.Location = new System.Drawing.Point(783, 27);
            this.SrcLoadBtn.Name = "SrcLoadBtn";
            this.SrcLoadBtn.Size = new System.Drawing.Size(70, 26);
            this.SrcLoadBtn.TabIndex = 11;
            this.SrcLoadBtn.Text = "Load";
            this.SrcLoadBtn.UseVisualStyleBackColor = false;
            this.SrcLoadBtn.Click += new System.EventHandler(this.SrcLoadBtn_Click);
            // 
            // CommandBox
            // 
            this.CommandBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CommandBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.CommandBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CommandBox.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.CommandBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(184)))), ((int)(((byte)(223)))));
            this.CommandBox.Location = new System.Drawing.Point(610, 370);
            this.CommandBox.Name = "CommandBox";
            this.CommandBox.Size = new System.Drawing.Size(217, 20);
            this.CommandBox.TabIndex = 13;
            this.CommandBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CommandBox_KeyDown);
            // 
            // SendBtn
            // 
            this.SendBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SendBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.SendBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SendBtn.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.SendBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(184)))), ((int)(((byte)(223)))));
            this.SendBtn.Location = new System.Drawing.Point(828, 370);
            this.SendBtn.Name = "SendBtn";
            this.SendBtn.Size = new System.Drawing.Size(25, 20);
            this.SendBtn.TabIndex = 14;
            this.SendBtn.Text = ">";
            this.SendBtn.UseVisualStyleBackColor = false;
            this.SendBtn.Click += new System.EventHandler(this.SendBtn_Click);
            // 
            // StatusBar
            // 
            this.StatusBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.StatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.StatusBar.Location = new System.Drawing.Point(0, 554);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(865, 22);
            this.StatusBar.TabIndex = 15;
            this.StatusBar.Text = "Status";
            // 
            // StatusLabel
            // 
            this.StatusLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(239)))));
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(59, 17);
            this.StatusLabel.Text = "Loading...";
            // 
            // MainTabView
            // 
            this.MainTabView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainTabView.Controls.Add(this.RoomsTab);
            this.MainTabView.Controls.Add(this.ObjectsTab);
            this.MainTabView.Controls.Add(this.SpritesTab);
            this.MainTabView.Controls.Add(this.StringsTab);
            this.MainTabView.Controls.Add(this.VariablesTab);
            this.MainTabView.Controls.Add(this.CodesTab);
            this.MainTabView.Enabled = false;
            this.MainTabView.Location = new System.Drawing.Point(12, 59);
            this.MainTabView.Name = "MainTabView";
            this.MainTabView.SelectedIndex = 0;
            this.MainTabView.Size = new System.Drawing.Size(565, 438);
            this.MainTabView.TabIndex = 16;
            this.MainTabView.SelectedIndexChanged += new System.EventHandler(this.MainTabView_SelectedIndexChanged);
            // 
            // RoomsTab
            // 
            this.RoomsTab.Controls.Add(this.RoomsList);
            this.RoomsTab.Location = new System.Drawing.Point(4, 22);
            this.RoomsTab.Name = "RoomsTab";
            this.RoomsTab.Padding = new System.Windows.Forms.Padding(3);
            this.RoomsTab.Size = new System.Drawing.Size(557, 412);
            this.RoomsTab.TabIndex = 0;
            this.RoomsTab.Text = "Rooms";
            this.RoomsTab.UseVisualStyleBackColor = true;
            // 
            // ObjectsTab
            // 
            this.ObjectsTab.Controls.Add(this.ObjectsList);
            this.ObjectsTab.Location = new System.Drawing.Point(4, 22);
            this.ObjectsTab.Name = "ObjectsTab";
            this.ObjectsTab.Padding = new System.Windows.Forms.Padding(3);
            this.ObjectsTab.Size = new System.Drawing.Size(557, 412);
            this.ObjectsTab.TabIndex = 1;
            this.ObjectsTab.Text = "Objects";
            this.ObjectsTab.ToolTipText = "Flags:\\n0x01=isVisible\\n0x02=isSolid\\n0x04=isPersistent\\n0x08=isPhysics\\n0x10=isS" +
    "ensor";
            this.ObjectsTab.UseVisualStyleBackColor = true;
            // 
            // ObjectsList
            // 
            this.ObjectsList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(204)))), ((int)(((byte)(193)))));
            this.ObjectsList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ObjectsList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ObjectIdHeader,
            this.ObjectNameHeader,
            this.ObjectSpriteNameHeader,
            this.ObjectFlagsHeader,
            this.ObjectParentIdHeader,
            this.ObjectMaskIdHeader,
            this.ObjectDepthHeader});
            this.ObjectsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ObjectsList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.ObjectsList.FullRowSelect = true;
            this.ObjectsList.HideSelection = false;
            this.ObjectsList.Location = new System.Drawing.Point(3, 3);
            this.ObjectsList.Name = "ObjectsList";
            this.ObjectsList.Size = new System.Drawing.Size(551, 406);
            this.ObjectsList.TabIndex = 0;
            this.ObjectsList.UseCompatibleStateImageBehavior = false;
            this.ObjectsList.View = System.Windows.Forms.View.Details;
            // 
            // ObjectIdHeader
            // 
            this.ObjectIdHeader.Text = "ID";
            // 
            // ObjectNameHeader
            // 
            this.ObjectNameHeader.Text = "Name";
            this.ObjectNameHeader.Width = 120;
            // 
            // ObjectSpriteNameHeader
            // 
            this.ObjectSpriteNameHeader.Text = "Sprite";
            this.ObjectSpriteNameHeader.Width = 100;
            // 
            // ObjectFlagsHeader
            // 
            this.ObjectFlagsHeader.Text = "Flags";
            // 
            // ObjectParentIdHeader
            // 
            this.ObjectParentIdHeader.Text = "Parent ID";
            // 
            // ObjectMaskIdHeader
            // 
            this.ObjectMaskIdHeader.Text = "Mask ID";
            // 
            // ObjectDepthHeader
            // 
            this.ObjectDepthHeader.Text = "Depth";
            // 
            // SpritesTab
            // 
            this.SpritesTab.Controls.Add(this.SpritesList);
            this.SpritesTab.Location = new System.Drawing.Point(4, 22);
            this.SpritesTab.Name = "SpritesTab";
            this.SpritesTab.Size = new System.Drawing.Size(557, 412);
            this.SpritesTab.TabIndex = 2;
            this.SpritesTab.Text = "Sprites";
            this.SpritesTab.UseVisualStyleBackColor = true;
            // 
            // SpritesList
            // 
            this.SpritesList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(204)))), ((int)(((byte)(193)))));
            this.SpritesList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.SpriteIndexHeader,
            this.SpriteNameHeader,
            this.SpriteSizeHeader,
            this.SpriteOriginHeader,
            this.SpriteNumFramesHeader,
            this.SpriteSpeedHeader,
            this.SpriteFormatHeader});
            this.SpritesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SpritesList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.SpritesList.FullRowSelect = true;
            this.SpritesList.Location = new System.Drawing.Point(0, 0);
            this.SpritesList.MultiSelect = false;
            this.SpritesList.Name = "SpritesList";
            this.SpritesList.Size = new System.Drawing.Size(557, 412);
            this.SpritesList.TabIndex = 1;
            this.SpritesList.UseCompatibleStateImageBehavior = false;
            this.SpritesList.View = System.Windows.Forms.View.Details;
            // 
            // SpriteIndexHeader
            // 
            this.SpriteIndexHeader.Text = "Index";
            this.SpriteIndexHeader.Width = 40;
            // 
            // SpriteNameHeader
            // 
            this.SpriteNameHeader.Text = "Name";
            this.SpriteNameHeader.Width = 230;
            // 
            // SpriteSizeHeader
            // 
            this.SpriteSizeHeader.Text = "Size";
            this.SpriteSizeHeader.Width = 57;
            // 
            // SpriteOriginHeader
            // 
            this.SpriteOriginHeader.Text = "Origin";
            this.SpriteOriginHeader.Width = 48;
            // 
            // SpriteNumFramesHeader
            // 
            this.SpriteNumFramesHeader.Text = "# Frames";
            this.SpriteNumFramesHeader.Width = 56;
            // 
            // SpriteSpeedHeader
            // 
            this.SpriteSpeedHeader.Text = "Speed";
            this.SpriteSpeedHeader.Width = 72;
            // 
            // SpriteFormatHeader
            // 
            this.SpriteFormatHeader.Text = "Format";
            this.SpriteFormatHeader.Width = 46;
            // 
            // StringsTab
            // 
            this.StringsTab.Controls.Add(this.StringsList);
            this.StringsTab.Location = new System.Drawing.Point(4, 22);
            this.StringsTab.Name = "StringsTab";
            this.StringsTab.Size = new System.Drawing.Size(557, 412);
            this.StringsTab.TabIndex = 3;
            this.StringsTab.Text = "Strings";
            this.StringsTab.UseVisualStyleBackColor = true;
            // 
            // StringsList
            // 
            this.StringsList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(204)))), ((int)(((byte)(193)))));
            this.StringsList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.StringsList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.IndexHeader,
            this.ValueHeader,
            this.OffsetHeader,
            this.LengthHeader});
            this.StringsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StringsList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.StringsList.FullRowSelect = true;
            this.StringsList.HideSelection = false;
            this.StringsList.Location = new System.Drawing.Point(0, 0);
            this.StringsList.MultiSelect = false;
            this.StringsList.Name = "StringsList";
            this.StringsList.Size = new System.Drawing.Size(557, 412);
            this.StringsList.TabIndex = 0;
            this.StringsList.UseCompatibleStateImageBehavior = false;
            this.StringsList.View = System.Windows.Forms.View.Details;
            // 
            // IndexHeader
            // 
            this.IndexHeader.Text = "Index";
            this.IndexHeader.Width = 38;
            // 
            // ValueHeader
            // 
            this.ValueHeader.Text = "Value";
            this.ValueHeader.Width = 343;
            // 
            // OffsetHeader
            // 
            this.OffsetHeader.Text = "Offset";
            this.OffsetHeader.Width = 86;
            // 
            // LengthHeader
            // 
            this.LengthHeader.Text = "Length";
            this.LengthHeader.Width = 69;
            // 
            // VariablesTab
            // 
            this.VariablesTab.Controls.Add(this.VariablesList);
            this.VariablesTab.Location = new System.Drawing.Point(4, 22);
            this.VariablesTab.Name = "VariablesTab";
            this.VariablesTab.Size = new System.Drawing.Size(557, 412);
            this.VariablesTab.TabIndex = 4;
            this.VariablesTab.Text = "Variables";
            this.VariablesTab.UseVisualStyleBackColor = true;
            // 
            // VariablesList
            // 
            this.VariablesList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(204)))), ((int)(((byte)(193)))));
            this.VariablesList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.VarIndexHeader,
            this.VarNameHeader,
            this.VarFirstRefHeader,
            this.VarCodeIndexHeader,
            this.VarNumReferencesHeader});
            this.VariablesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VariablesList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.VariablesList.FullRowSelect = true;
            this.VariablesList.Location = new System.Drawing.Point(0, 0);
            this.VariablesList.MultiSelect = false;
            this.VariablesList.Name = "VariablesList";
            this.VariablesList.Size = new System.Drawing.Size(557, 412);
            this.VariablesList.TabIndex = 0;
            this.VariablesList.UseCompatibleStateImageBehavior = false;
            this.VariablesList.View = System.Windows.Forms.View.Details;
            // 
            // VarIndexHeader
            // 
            this.VarIndexHeader.Text = "Index";
            this.VarIndexHeader.Width = 40;
            // 
            // VarNameHeader
            // 
            this.VarNameHeader.Text = "Name";
            this.VarNameHeader.Width = 255;
            // 
            // VarFirstRefHeader
            // 
            this.VarFirstRefHeader.Text = "First Reference";
            this.VarFirstRefHeader.Width = 139;
            // 
            // VarCodeIndexHeader
            // 
            this.VarCodeIndexHeader.Text = "Code Index";
            this.VarCodeIndexHeader.Width = 49;
            // 
            // VarNumReferencesHeader
            // 
            this.VarNumReferencesHeader.Text = "# References";
            // 
            // CodesTab
            // 
            this.CodesTab.Controls.Add(this.CodesList);
            this.CodesTab.Location = new System.Drawing.Point(4, 22);
            this.CodesTab.Name = "CodesTab";
            this.CodesTab.Size = new System.Drawing.Size(557, 412);
            this.CodesTab.TabIndex = 5;
            this.CodesTab.Text = "Codes";
            this.CodesTab.UseVisualStyleBackColor = true;
            // 
            // CodesList
            // 
            this.CodesList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(204)))), ((int)(((byte)(193)))));
            this.CodesList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.CodeIndexHeader,
            this.CodeNameHeader,
            this.CodeAddressHeader,
            this.CodeSizeHeader,
            this.CodeNumVariablesHeader,
            this.CodeNumFunctionsHeader});
            this.CodesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CodesList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.CodesList.FullRowSelect = true;
            this.CodesList.Location = new System.Drawing.Point(0, 0);
            this.CodesList.MultiSelect = false;
            this.CodesList.Name = "CodesList";
            this.CodesList.Size = new System.Drawing.Size(557, 412);
            this.CodesList.TabIndex = 1;
            this.CodesList.UseCompatibleStateImageBehavior = false;
            this.CodesList.View = System.Windows.Forms.View.Details;
            this.CodesList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.CodesList_MouseDown);
            // 
            // CodeIndexHeader
            // 
            this.CodeIndexHeader.Text = "Index";
            this.CodeIndexHeader.Width = 40;
            // 
            // CodeNameHeader
            // 
            this.CodeNameHeader.Text = "Name";
            this.CodeNameHeader.Width = 271;
            // 
            // CodeAddressHeader
            // 
            this.CodeAddressHeader.Text = "Address";
            this.CodeAddressHeader.Width = 75;
            // 
            // CodeSizeHeader
            // 
            this.CodeSizeHeader.Text = "Size (Words)";
            this.CodeSizeHeader.Width = 72;
            // 
            // CodeNumVariablesHeader
            // 
            this.CodeNumVariablesHeader.Text = "# Vars";
            this.CodeNumVariablesHeader.Width = 43;
            // 
            // AddLevelOpenDialogue
            // 
            this.AddLevelOpenDialogue.FileName = "AddLevelOpenDialogue";
            // 
            // InfoBtn
            // 
            this.InfoBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.InfoBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(194)))));
            this.InfoBtn.Enabled = false;
            this.InfoBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InfoBtn.Font = new System.Drawing.Font("MS Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfoBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.InfoBtn.Image = ((System.Drawing.Image)(resources.GetObject("InfoBtn.Image")));
            this.InfoBtn.Location = new System.Drawing.Point(547, 499);
            this.InfoBtn.Name = "InfoBtn";
            this.InfoBtn.Size = new System.Drawing.Size(28, 24);
            this.InfoBtn.TabIndex = 17;
            this.InfoBtn.TabStop = false;
            this.InfoBtn.UseVisualStyleBackColor = false;
            this.InfoBtn.Click += new System.EventHandler(this.InfoBtn_Click);
            // 
            // SrcLabel
            // 
            this.SrcLabel.AutoSize = true;
            this.SrcLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(204)))), ((int)(((byte)(193)))));
            this.SrcLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SrcLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.SrcLabel.Location = new System.Drawing.Point(6, 29);
            this.SrcLabel.Name = "SrcLabel";
            this.SrcLabel.Size = new System.Drawing.Size(51, 20);
            this.SrcLabel.TabIndex = 18;
            this.SrcLabel.Text = "Input";
            // 
            // DestLabel
            // 
            this.DestLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DestLabel.AutoSize = true;
            this.DestLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(204)))), ((int)(((byte)(193)))));
            this.DestLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DestLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.DestLabel.Location = new System.Drawing.Point(4, 528);
            this.DestLabel.Name = "DestLabel";
            this.DestLabel.Size = new System.Drawing.Size(64, 20);
            this.DestLabel.TabIndex = 19;
            this.DestLabel.Text = "Output";
            // 
            // InfoComboBox
            // 
            this.InfoComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.InfoComboBox.Enabled = false;
            this.InfoComboBox.FormattingEnabled = true;
            this.InfoComboBox.Items.AddRange(new object[] {
            "Room Info",
            "ASCII Visual",
            "Backgrounds",
            "Views",
            "Objects",
            "Tiles",
            "Layers",
            "Object Layer",
            "Collision Layer"});
            this.InfoComboBox.Location = new System.Drawing.Point(425, 500);
            this.InfoComboBox.Name = "InfoComboBox";
            this.InfoComboBox.Size = new System.Drawing.Size(121, 21);
            this.InfoComboBox.TabIndex = 20;
            this.InfoComboBox.Text = "Room Info";
            // 
            // ClearLogBtn
            // 
            this.ClearLogBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearLogBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(68)))), ((int)(((byte)(76)))));
            this.ClearLogBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClearLogBtn.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.ClearLogBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(8)))));
            this.ClearLogBtn.Location = new System.Drawing.Point(579, 370);
            this.ClearLogBtn.Name = "ClearLogBtn";
            this.ClearLogBtn.Size = new System.Drawing.Size(25, 20);
            this.ClearLogBtn.TabIndex = 21;
            this.ClearLogBtn.Text = "x";
            this.ClearLogBtn.UseVisualStyleBackColor = false;
            this.ClearLogBtn.Click += new System.EventHandler(this.ClearLogBtn_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(865, 24);
            this.menuStrip1.TabIndex = 22;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CompilationCompareButton,
            this.TestButton});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // CompilationCompareButton
            // 
            this.CompilationCompareButton.Name = "CompilationCompareButton";
            this.CompilationCompareButton.Size = new System.Drawing.Size(192, 22);
            this.CompilationCompareButton.Text = "Compilation Compare";
            this.CompilationCompareButton.Click += new System.EventHandler(this.CompilationCompareButton_Click);
            // 
            // TestButton
            // 
            this.TestButton.Name = "TestButton";
            this.TestButton.Size = new System.Drawing.Size(192, 22);
            this.TestButton.Text = "Test";
            this.TestButton.Click += new System.EventHandler(this.TestButton_Click);
            // 
            // RoomContextMenu
            // 
            this.RoomContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RenameRoomButton,
            this.UnremaneRoomButton,
            this.ReplaceRoomButton,
            this.UnreplaceRoomButton,
            this.DeleteRoomButton,
            this.UndeleteRoomButton});
            this.RoomContextMenu.Name = "contextMenuStrip1";
            this.RoomContextMenu.Size = new System.Drawing.Size(130, 136);
            // 
            // RenameRoomButton
            // 
            this.RenameRoomButton.Image = ((System.Drawing.Image)(resources.GetObject("RenameRoomButton.Image")));
            this.RenameRoomButton.Name = "RenameRoomButton";
            this.RenameRoomButton.Size = new System.Drawing.Size(129, 22);
            this.RenameRoomButton.Text = "Rename...";
            this.RenameRoomButton.Click += new System.EventHandler(this.RenameRoomButton_Click);
            // 
            // UnremaneRoomButton
            // 
            this.UnremaneRoomButton.Image = ((System.Drawing.Image)(resources.GetObject("UnremaneRoomButton.Image")));
            this.UnremaneRoomButton.Name = "UnremaneRoomButton";
            this.UnremaneRoomButton.Size = new System.Drawing.Size(129, 22);
            this.UnremaneRoomButton.Text = "Unrename";
            this.UnremaneRoomButton.Click += new System.EventHandler(this.UnremaneRoomButton_Click);
            // 
            // ReplaceRoomButton
            // 
            this.ReplaceRoomButton.Image = ((System.Drawing.Image)(resources.GetObject("ReplaceRoomButton.Image")));
            this.ReplaceRoomButton.Name = "ReplaceRoomButton";
            this.ReplaceRoomButton.Size = new System.Drawing.Size(129, 22);
            this.ReplaceRoomButton.Text = "Replace...";
            this.ReplaceRoomButton.Click += new System.EventHandler(this.ReplaceRoomButton_Click);
            // 
            // UnreplaceRoomButton
            // 
            this.UnreplaceRoomButton.Image = ((System.Drawing.Image)(resources.GetObject("UnreplaceRoomButton.Image")));
            this.UnreplaceRoomButton.Name = "UnreplaceRoomButton";
            this.UnreplaceRoomButton.Size = new System.Drawing.Size(129, 22);
            this.UnreplaceRoomButton.Text = "Unreplace";
            this.UnreplaceRoomButton.Click += new System.EventHandler(this.UnreplaceRoomButton_Click);
            // 
            // DeleteRoomButton
            // 
            this.DeleteRoomButton.Image = ((System.Drawing.Image)(resources.GetObject("DeleteRoomButton.Image")));
            this.DeleteRoomButton.Name = "DeleteRoomButton";
            this.DeleteRoomButton.Size = new System.Drawing.Size(129, 22);
            this.DeleteRoomButton.Text = "Delete";
            this.DeleteRoomButton.Click += new System.EventHandler(this.DeleteRoomButton_Click);
            // 
            // UndeleteRoomButton
            // 
            this.UndeleteRoomButton.Image = ((System.Drawing.Image)(resources.GetObject("UndeleteRoomButton.Image")));
            this.UndeleteRoomButton.Name = "UndeleteRoomButton";
            this.UndeleteRoomButton.Size = new System.Drawing.Size(129, 22);
            this.UndeleteRoomButton.Text = "Undelete";
            this.UndeleteRoomButton.Click += new System.EventHandler(this.UndeleteRoomButton_Click);
            // 
            // SearchTextBox
            // 
            this.SearchTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchTextBox.Enabled = false;
            this.SearchTextBox.Location = new System.Drawing.Point(310, 59);
            this.SearchTextBox.Name = "SearchTextBox";
            this.SearchTextBox.Size = new System.Drawing.Size(267, 20);
            this.SearchTextBox.TabIndex = 23;
            this.SearchTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SearchTextBox_KeyDown);
            this.SearchTextBox.Leave += new System.EventHandler(this.SearchTextBox_Leave);
            // 
            // CodeContextMenu
            // 
            this.CodeContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CodeViewButton});
            this.CodeContextMenu.Name = "CodeContextMenu";
            this.CodeContextMenu.Size = new System.Drawing.Size(109, 26);
            // 
            // CodeViewButton
            // 
            this.CodeViewButton.Image = ((System.Drawing.Image)(resources.GetObject("CodeViewButton.Image")));
            this.CodeViewButton.Name = "CodeViewButton";
            this.CodeViewButton.Size = new System.Drawing.Size(108, 22);
            this.CodeViewButton.Text = "View...";
            this.CodeViewButton.Click += new System.EventHandler(this.CodeViewButton_Click);
            // 
            // RoomImageBox
            // 
            this.RoomImageBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RoomImageBox.BackColor = System.Drawing.Color.Transparent;
            this.RoomImageBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RoomImageBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RoomImageBox.Image = ((System.Drawing.Image)(resources.GetObject("RoomImageBox.Image")));
            this.RoomImageBox.InitialImage = ((System.Drawing.Image)(resources.GetObject("RoomImageBox.InitialImage")));
            this.RoomImageBox.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Default;
            this.RoomImageBox.Location = new System.Drawing.Point(579, 391);
            this.RoomImageBox.Name = "RoomImageBox";
            this.RoomImageBox.Size = new System.Drawing.Size(274, 127);
            this.RoomImageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.RoomImageBox.TabIndex = 12;
            this.RoomImageBox.TabStop = false;
            this.RoomImageBox.Click += new System.EventHandler(this.RoomImageBox_Click);
            // 
            // CodeNumFunctionsHeader
            // 
            this.CodeNumFunctionsHeader.Text = "# Funcs";
            this.CodeNumFunctionsHeader.Width = 51;
            // 
            // InjectorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(148)))), ((int)(((byte)(181)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(865, 576);
            this.Controls.Add(this.SearchTextBox);
            this.Controls.Add(this.ClearLogBtn);
            this.Controls.Add(this.InfoComboBox);
            this.Controls.Add(this.DestLabel);
            this.Controls.Add(this.SrcLabel);
            this.Controls.Add(this.InfoBtn);
            this.Controls.Add(this.MainTabView);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.SendBtn);
            this.Controls.Add(this.CommandBox);
            this.Controls.Add(this.RoomImageBox);
            this.Controls.Add(this.SrcLoadBtn);
            this.Controls.Add(this.DebugOutputBox);
            this.Controls.Add(this.DestBrowseBtn);
            this.Controls.Add(this.SrcFileBox);
            this.Controls.Add(this.SrcBrowseBtn);
            this.Controls.Add(this.InjectButton);
            this.Controls.Add(this.DestFileBox);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(700, 500);
            this.Name = "InjectorForm";
            this.Text = "Akurra Data Injector";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.InjectorForm_FormClosing);
            this.Load += new System.EventHandler(this.Form_Load);
            this.StatusBar.ResumeLayout(false);
            this.StatusBar.PerformLayout();
            this.MainTabView.ResumeLayout(false);
            this.RoomsTab.ResumeLayout(false);
            this.ObjectsTab.ResumeLayout(false);
            this.SpritesTab.ResumeLayout(false);
            this.StringsTab.ResumeLayout(false);
            this.VariablesTab.ResumeLayout(false);
            this.CodesTab.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.RoomContextMenu.ResumeLayout(false);
            this.CodeContextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RoomImageBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button InjectButton;
        private System.Windows.Forms.Button SrcBrowseBtn;
        private System.Windows.Forms.OpenFileDialog SrcFileDialogue;
        private System.Windows.Forms.SaveFileDialog DestFileDialogue;
        private System.Windows.Forms.Button DestBrowseBtn;
        private System.Windows.Forms.ListView RoomsList;
        public System.Windows.Forms.RichTextBox DebugOutputBox;
        public System.Windows.Forms.Button SrcLoadBtn;
        public System.Windows.Forms.TextBox SrcFileBox;
        public System.Windows.Forms.TextBox DestFileBox;
        private System.Windows.Forms.ColumnHeader NameHeader;
        private System.Windows.Forms.ColumnHeader ObjectCountHeader;
        private System.Windows.Forms.ColumnHeader CollisionCountHeader;
        private System.Windows.Forms.ColumnHeader OtherCountHeader;
        private System.Windows.Forms.ColumnHeader RoomOperationHeader;
        public PictureBoxWithInterpolationMode RoomImageBox;
        public System.Windows.Forms.TextBox CommandBox;
        public System.Windows.Forms.Button SendBtn;
        private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.TabControl MainTabView;
        private System.Windows.Forms.TabPage RoomsTab;
        private System.Windows.Forms.TabPage ObjectsTab;
        private System.Windows.Forms.TabPage SpritesTab;
        private System.Windows.Forms.OpenFileDialog AddLevelOpenDialogue;
        private System.Windows.Forms.Button InfoBtn;
        private System.Windows.Forms.Label SrcLabel;
        private System.Windows.Forms.Label DestLabel;
        private System.Windows.Forms.ComboBox InfoComboBox;
        private System.Windows.Forms.ColumnHeader SizeHeader;
        public System.Windows.Forms.Button ClearLogBtn;
        private System.Windows.Forms.TabPage StringsTab;
        private System.Windows.Forms.ListView StringsList;
        private System.Windows.Forms.ColumnHeader IndexHeader;
        private System.Windows.Forms.ColumnHeader ValueHeader;
        private System.Windows.Forms.ListView ObjectsList;
        private System.Windows.Forms.ColumnHeader ObjectIdHeader;
        private System.Windows.Forms.ColumnHeader ObjectNameHeader;
        private System.Windows.Forms.ColumnHeader ObjectSpriteNameHeader;
        private System.Windows.Forms.ColumnHeader ObjectFlagsHeader;
        private System.Windows.Forms.ColumnHeader ObjectParentIdHeader;
        private System.Windows.Forms.ColumnHeader ObjectMaskIdHeader;
        private System.Windows.Forms.ColumnHeader ObjectDepthHeader;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CompilationCompareButton;
        private System.Windows.Forms.ContextMenuStrip RoomContextMenu;
        private System.Windows.Forms.ToolStripMenuItem RenameRoomButton;
        private System.Windows.Forms.ToolStripMenuItem TestButton;
        private System.Windows.Forms.ColumnHeader OffsetHeader;
        private System.Windows.Forms.ColumnHeader LengthHeader;
        private System.Windows.Forms.TabPage VariablesTab;
        private System.Windows.Forms.ListView VariablesList;
        private System.Windows.Forms.ColumnHeader VarNameHeader;
        private System.Windows.Forms.ColumnHeader VarFirstRefHeader;
        private System.Windows.Forms.ColumnHeader VarNumReferencesHeader;
        private System.Windows.Forms.ColumnHeader VarIndexHeader;
        private System.Windows.Forms.ListView SpritesList;
        private System.Windows.Forms.ColumnHeader SpriteIndexHeader;
        private System.Windows.Forms.ColumnHeader SpriteNameHeader;
        private System.Windows.Forms.ColumnHeader SpriteSizeHeader;
        private System.Windows.Forms.ColumnHeader SpriteOriginHeader;
        private System.Windows.Forms.ColumnHeader SpriteNumFramesHeader;
        private System.Windows.Forms.ColumnHeader SpriteSpeedHeader;
        private System.Windows.Forms.ColumnHeader SpriteFormatHeader;
        private System.Windows.Forms.ToolStripMenuItem ReplaceRoomButton;
        private System.Windows.Forms.ToolStripMenuItem UnreplaceRoomButton;
        private System.Windows.Forms.ToolStripMenuItem DeleteRoomButton;
        private System.Windows.Forms.ToolStripMenuItem UndeleteRoomButton;
        private System.Windows.Forms.ToolStripMenuItem UnremaneRoomButton;
        private System.Windows.Forms.TabPage CodesTab;
        private System.Windows.Forms.ListView CodesList;
        private System.Windows.Forms.ColumnHeader CodeIndexHeader;
        private System.Windows.Forms.ColumnHeader CodeNameHeader;
        private System.Windows.Forms.ColumnHeader CodeAddressHeader;
        private System.Windows.Forms.ColumnHeader CodeSizeHeader;
        private System.Windows.Forms.ColumnHeader CodeNumVariablesHeader;
        private System.Windows.Forms.TextBox SearchTextBox;
        private System.Windows.Forms.ColumnHeader VarCodeIndexHeader;
        private System.Windows.Forms.ContextMenuStrip CodeContextMenu;
        private System.Windows.Forms.ToolStripMenuItem CodeViewButton;
        private System.Windows.Forms.ColumnHeader CodeNumFunctionsHeader;
    }
}

