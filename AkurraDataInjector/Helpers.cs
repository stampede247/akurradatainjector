﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkurraDataInjector
{
    public static class Helpers
    {
        public static bool IsCharClassNumber(char character)
        {
            return (character >= '0' && character <= '9');
        }
        
        public static ListViewItem CreateRoomViewItemFromInfo(int roomIndex, AkurraParser.RoomInfo_t roomInfo, IEnumerable<RoomOperation> operations)
        {
            string roomName = Marshal.PtrToStringAnsi(roomInfo.name);
            
            ListViewItem result = new ListViewItem(roomName);
            result.Tag = roomName; //Original name
            result.SubItems[0].Tag = roomName;

            ListViewItem.ListViewSubItem sizeSubItem = new ListViewItem.ListViewSubItem(result, $"{roomInfo.width}x{roomInfo.height}");
            sizeSubItem.Tag = new Tuple<Int32, Int32>(roomInfo.width, roomInfo.height);
            result.SubItems.Add(sizeSubItem);

            ListViewItem.ListViewSubItem numObjectsSubItem = new ListViewItem.ListViewSubItem(result, roomInfo.numObjects.ToString());
            numObjectsSubItem.Tag = roomInfo.numObjects;
            result.SubItems.Add(numObjectsSubItem);

            ListViewItem.ListViewSubItem numCollisionsSubItem = new ListViewItem.ListViewSubItem(result, roomInfo.numCollisions.ToString());
            numCollisionsSubItem.Tag = roomInfo.numCollisions;
            result.SubItems.Add(numCollisionsSubItem);

            ListViewItem.ListViewSubItem numOtherSubItem = new ListViewItem.ListViewSubItem(result, roomInfo.numOtherInstances.ToString());
            numOtherSubItem.Tag = roomInfo.numOtherInstances;
            result.SubItems.Add(numOtherSubItem);
            
            ListViewItem.ListViewSubItem pathSubItem = new ListViewItem.ListViewSubItem(result, "");
            result.SubItems.Add(pathSubItem);

            UpdateOperationColumnOnRoomItem(result, operations);

            return result;
        }

        public static Color Blend(this Color color, Color backColor, double amount)
        {
            byte r = (byte)((color.R * amount) + backColor.R * (1 - amount));
            byte g = (byte)((color.G * amount) + backColor.G * (1 - amount));
            byte b = (byte)((color.B * amount) + backColor.B * (1 - amount));
            return Color.FromArgb(r, g, b);
        }

        public static void UpdateOperationColumnOnRoomItem(ListViewItem roomItem, IEnumerable<RoomOperation> operations)
        {
            roomItem.SubItems[0].Text = (string)roomItem.Tag;
            roomItem.SubItems[5].Text = "";
            roomItem.ForeColor = Color.FromArgb(0, 68, 76);
            roomItem.BackColor = Color.FromArgb(132, 204, 193);
            int backColorPriority = 0;
            foreach (RoomOperation operation in operations)
            {
                if (roomItem.SubItems[5].Text != "") { roomItem.SubItems[5].Text += " + ";  }
                if (operation.type == RoomOperation.RoomOperationType.Rename)
                {
                    roomItem.SubItems[0].Text = operation.newName;
                    roomItem.SubItems[5].Text += $"Rename (\"{(string)roomItem.Tag}\")";
                    if (backColorPriority < 1)
                    {
                        roomItem.BackColor = Color.FromArgb(175, 255, 246);
                        backColorPriority = 1;
                    }
                }
                else if (operation.type == RoomOperation.RoomOperationType.Delete)
                {
                    roomItem.SubItems[5].Text += "Delete";
                    if (backColorPriority < 3)
                    {
                        roomItem.BackColor = Color.FromArgb(238, 28, 36);
                        backColorPriority = 3;
                    }
                    roomItem.ForeColor = Color.White;
                }
                else if (operation.type == RoomOperation.RoomOperationType.Replace)
                {
                    roomItem.SubItems[5].Text += $"Replace \"{Path.GetFileName(operation.filePath)}\"";
                    if (backColorPriority < 2)
                    {
                        roomItem.BackColor = Color.FromArgb(0, 68, 76);
                        backColorPriority = 2;
                    }
                    roomItem.ForeColor = Color.White;
                }
            }
        }
        
        public static ListViewItem CreateObjectViewItemFromInfo(int objectIndex, AkurraParser.ObjectInfo_t objInfo)
        {
            string objName = Marshal.PtrToStringAnsi(objInfo.name);
            string objSpriteName = Marshal.PtrToStringAnsi(objInfo.spriteName);
            
            ListViewItem result = new ListViewItem(objectIndex.ToString());
            result.SubItems[0].Tag = objectIndex;

            ListViewItem.ListViewSubItem nameSubItem = new ListViewItem.ListViewSubItem(result, objName);
            nameSubItem.Tag = objName;
            result.SubItems.Add(nameSubItem);

            ListViewItem.ListViewSubItem spriteNameSubItem = new ListViewItem.ListViewSubItem(result, objSpriteName);
            spriteNameSubItem.Tag = objSpriteName;
            result.SubItems.Add(spriteNameSubItem);

            ListViewItem.ListViewSubItem flagsSubItem = new ListViewItem.ListViewSubItem(result, "0x" + objInfo.flags.ToString("X2"));
            flagsSubItem.Tag = objInfo.flags;
            result.SubItems.Add(flagsSubItem);

            ListViewItem.ListViewSubItem parentIdSubItem = new ListViewItem.ListViewSubItem(result, objInfo.parentId.ToString());
            parentIdSubItem.Tag = objInfo.parentId;
            result.SubItems.Add(parentIdSubItem);

            ListViewItem.ListViewSubItem maskIdSubItem = new ListViewItem.ListViewSubItem(result, objInfo.maskId.ToString());
            maskIdSubItem.Tag = objInfo.maskId;
            result.SubItems.Add(maskIdSubItem);

            ListViewItem.ListViewSubItem depthSubItem = new ListViewItem.ListViewSubItem(result, objInfo.depth.ToString());
            depthSubItem.Tag = objInfo.depth;
            result.SubItems.Add(depthSubItem);
            
            return result;
        }

        public static ListViewItem CreateSpriteViewItemFromInfo(int spriteIndex, AkurraParser.SpriteInfo_t spriteInfo)
        {
            string spriteName = Marshal.PtrToStringAnsi(spriteInfo.name);

            ListViewItem result = new ListViewItem(spriteIndex.ToString());
            result.SubItems[0].Tag = spriteIndex;
            result.Tag = spriteName;

            ListViewItem.ListViewSubItem nameSubItem = new ListViewItem.ListViewSubItem(result, spriteName);
            nameSubItem.Tag = spriteName;
            result.SubItems.Add(nameSubItem);

            ListViewItem.ListViewSubItem sizeSubItem = new ListViewItem.ListViewSubItem(result, $"{spriteInfo.width}x{spriteInfo.height}");
            sizeSubItem.Tag = new Tuple<Int32, Int32>(spriteInfo.width, spriteInfo.height);
            result.SubItems.Add(sizeSubItem);

            ListViewItem.ListViewSubItem originSubItem = new ListViewItem.ListViewSubItem(result, $"({spriteInfo.originX}, {spriteInfo.originY})");
            originSubItem.Tag = new Tuple<Int32, Int32>(spriteInfo.originX, spriteInfo.originY);
            result.SubItems.Add(originSubItem);

            ListViewItem.ListViewSubItem numFramesSubItem = new ListViewItem.ListViewSubItem(result, spriteInfo.numFrames.ToString());
            numFramesSubItem.Tag = spriteInfo.numFrames;
            result.SubItems.Add(numFramesSubItem);
            
            ListViewItem.ListViewSubItem speedSubItem = new ListViewItem.ListViewSubItem(result, $"{spriteInfo.speed} {((spriteInfo.isSpeedGameFrames != 0) ? "gfps" : "fps")}");
            speedSubItem.Tag = spriteInfo.speed;
            result.SubItems.Add(speedSubItem);

            ListViewItem.ListViewSubItem formatSubItem = new ListViewItem.ListViewSubItem(result, spriteInfo.formatNum.ToString());
            formatSubItem.Tag = spriteInfo.formatNum;
            result.SubItems.Add(formatSubItem);

            return result;
        }

        public static ListViewItem CreateVariableViewItemFromInfo(int variableIndex, AkurraParser.VariableInfo_t varInfo)
        {
            string varName = Marshal.PtrToStringAnsi(varInfo.name);

            ListViewItem result = new ListViewItem(variableIndex.ToString());
            result.SubItems[0].Tag = variableIndex;
            result.Tag = varName;

            ListViewItem.ListViewSubItem nameSubItem = new ListViewItem.ListViewSubItem(result, varName);
            nameSubItem.Tag = varName;
            result.SubItems.Add(nameSubItem);

            ListViewItem.ListViewSubItem firstRefSubItem = new ListViewItem.ListViewSubItem(result, "0x" + varInfo.firstCodeReference.ToString("X8"));
            if (varInfo.firstCodeReference == -1) { firstRefSubItem.Text = "-"; result.BackColor = Color.FromArgb(175, 255, 246); }
            firstRefSubItem.Tag = varInfo.firstCodeReference;
            result.SubItems.Add(firstRefSubItem);

            ListViewItem.ListViewSubItem codeIndexSubItem = new ListViewItem.ListViewSubItem(result, varInfo.codeIndex.ToString());
            if (varInfo.codeIndex == -1) { codeIndexSubItem.Text = "-"; }
            codeIndexSubItem.Tag = varInfo.codeIndex;
            result.SubItems.Add(codeIndexSubItem);

            ListViewItem.ListViewSubItem numReferencesSubItem = new ListViewItem.ListViewSubItem(result, varInfo.numCodeReferences.ToString());
            numReferencesSubItem.Tag = varInfo.numCodeReferences;
            result.SubItems.Add(numReferencesSubItem);
            
            return result;
        }

        public static ListViewItem CreateCodeViewItemFromInfo(int codeIndex, AkurraParser.CodeInfo_t codeInfo)
        {
            string codeName = Marshal.PtrToStringAnsi(codeInfo.name);

            ListViewItem result = new ListViewItem(codeIndex.ToString());
            result.SubItems[0].Tag = codeIndex;
            result.Tag = codeName;

            ListViewItem.ListViewSubItem nameSubItem = new ListViewItem.ListViewSubItem(result, codeName);
            nameSubItem.Tag = codeName;
            result.SubItems.Add(nameSubItem);

            ListViewItem.ListViewSubItem addressSubItem = new ListViewItem.ListViewSubItem(result, "0x" + codeInfo.bytecodeOffset.ToString("X8"));
            addressSubItem.Tag = codeInfo.bytecodeOffset;
            result.SubItems.Add(addressSubItem);

            ListViewItem.ListViewSubItem sizeSubItem = new ListViewItem.ListViewSubItem(result, (codeInfo.bytecodeLength / 4).ToString());
            sizeSubItem.Tag = codeInfo.bytecodeLength;
            result.SubItems.Add(sizeSubItem);
            
            ListViewItem.ListViewSubItem numVariablesSubItem = new ListViewItem.ListViewSubItem(result, codeInfo.numVariables.ToString());
            numVariablesSubItem.Tag = codeInfo.numVariables;
            result.SubItems.Add(numVariablesSubItem);

            ListViewItem.ListViewSubItem numFunctionsSubItem = new ListViewItem.ListViewSubItem(result, codeInfo.numFunctions.ToString());
            numFunctionsSubItem.Tag = codeInfo.numFunctions;
            result.SubItems.Add(numFunctionsSubItem);

            return result;
        }

        public static bool DoesListViewItemMatchSearchText(ListViewItem item, string searchText)
        {
            if (searchText == "" || searchText == null) { return true; }
            foreach (ListViewItem.ListViewSubItem subItem in item.SubItems)
            {
                if (subItem.Text.ToLower().Contains(searchText.ToLower()))
                {
                    return true;
                }
            }
            return false;
        }

        public static void PutFormattedBytecodeInRichTextbox(RichTextBox textbox, string formattedBytecode)
        {
            string[] formattedBytecodeLines = formattedBytecode.Split('\n');
            textbox.Hide();
            textbox.Text = "";
            foreach (string line in formattedBytecodeLines)
            {
                string addressPart = "";
                string mainLinePart = line;
                string commentPart = "";
                string variablePart = "";
                int colonIndex = mainLinePart.IndexOf(": ");
                if (colonIndex >= 0)
                {
                    addressPart = mainLinePart.Substring(0, colonIndex + 1);
                    mainLinePart = mainLinePart.Substring(colonIndex + 1);
                }
                int commentIndex = mainLinePart.IndexOf("//");
                if (commentIndex >= 0)
                {
                    commentPart = mainLinePart.Substring(commentIndex);
                    mainLinePart = mainLinePart.Substring(0, commentIndex);
                }
                int arrowIndex = mainLinePart.IndexOf("<-");
                if (arrowIndex >= 0)
                {
                    variablePart = mainLinePart.Substring(arrowIndex);
                    mainLinePart = mainLinePart.Substring(0, arrowIndex);
                }

                textbox.SelectionColor = Color.FromArgb(45, 229, 140);
                textbox.AppendText(addressPart);
                textbox.SelectionColor = textbox.ForeColor;
                textbox.AppendText(mainLinePart);
                textbox.SelectionColor = Color.FromArgb(45, 229, 140);
                textbox.AppendText(commentPart);
                textbox.SelectionColor = Color.FromArgb(204, 172, 102);
                textbox.AppendText(variablePart);
                textbox.AppendText("\r\n");
            }
            textbox.Show();
        }
    }
}
