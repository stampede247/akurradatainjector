/*
File:   helpers.cpp
Author: Taylor Robbins
Date:   12\13\2020
Description: 
	** Holds some functions that are used by the whole application
*/

bool IsChunkType(const GmsHeader_Chunk_t* header, const char* typeStr)
{
	Assert(header != nullptr);
	Assert(MyStrLength32(typeStr) == ArrayCount(header->type));
	if (StrCompareIgnoreCase(&header->type[0], typeStr, ArrayCount(header->type))) { return true; }
	else { return false; }	
}

inline u32 SwizzleColorForBmp(u32 colorValue)
{
	return (((colorValue >> 24) & 0xFF) << 24) | //alpha
	       (((colorValue >>  0) & 0xFF) << 16) | //blue
	       (((colorValue >>  8) & 0xFF) <<  8) | //green
	       (((colorValue >> 16) & 0xFF) <<  0);  //red
}
