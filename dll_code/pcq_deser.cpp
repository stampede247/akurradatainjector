/*
File:   pcq_deser.cpp
Author: Taylor Robbins
Date:   12\16\2020
Description: 
	** Holds some functions that help us deserialize Princess Castle Quest level files
	** into a usable format that we can use to generate Akurra rooms
*/

typedef enum
{
	AkurraLayer_Object = 0,
	AkurraLayer_Floor,
	AkurraLayer_Collision,
	AkurraLayer_System,
	AkurraLayer_Effects,
	AkurraLayer_Shadow,
} AkurraLayer_t;

bool LookupAkurraObjectIds(GmsPack_t* pack)
{
	NotNull(pack);
	ClearStruct(pack->akurraIds);
	for (u32 iIndex = 0; iIndex < ArrayCount(pack->akurraIds.values); iIndex++)
	{
		pack->akurraIds.values[iIndex] = -1;
	}
	for (u32 oIndex = 0; oIndex < pack->objects.length; oIndex++)
	{
		GmsObject_t* object = DynArrayGet(&pack->objects, GmsObject_t, oIndex);
		Assert(object != nullptr);
		// PrintLine_D("Object[%u]: \"%s\"", oIndex, object->name);
		if (StrCompareIgnoreCaseNt(object->name, "obj_solid"))                      { pack->akurraIds.solidWallId          = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_block_blocker"))         { pack->akurraIds.blockerId            = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "code_game_controller"))      { pack->akurraIds.gameControllerId     = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_music_controller"))      { pack->akurraIds.musicControllerId    = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_ambiance_parent"))       { pack->akurraIds.ambianceParentId     = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_item_pedestal"))         { pack->akurraIds.pedestalId           = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_wooden_block"))          { pack->akurraIds.boxRegId             = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_stone_block"))           { pack->akurraIds.boxBlueId            = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_ancient_lock"))          { pack->akurraIds.doorId               = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_lock_tripple_ancient"))  { pack->akurraIds.largeDoorId          = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_button_square"))         { pack->akurraIds.greenButtonId        = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_button_triangle"))       { pack->akurraIds.purpleButtonId       = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_button_round"))          { pack->akurraIds.orangeButtonId       = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_pillar_square_down"))    { pack->akurraIds.greenPillarDownId    = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_pillar_triangle_down"))  { pack->akurraIds.purplePillarDownId   = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_pillar_round_down"))     { pack->akurraIds.orangePillarDownId   = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_deep_water"))            { pack->akurraIds.deepWaterId          = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_pillar_square_up"))      { pack->akurraIds.greenPillarUpId      = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_pillar_triangle_up"))    { pack->akurraIds.purplePillarUpId     = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_pillar_round_up"))       { pack->akurraIds.orangePillarUpId     = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_button_bronze"))         { pack->akurraIds.gongRed              = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_button_silver"))         { pack->akurraIds.gongBlue             = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_button_gold"))           { pack->akurraIds.gongYellow           = (i32)oIndex; }
		// else if (StrCompareIgnoreCaseNt(object->name, "obj_button_"))         { pack->akurraIds.gongOrange           = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_bronze_pillar"))         { pack->akurraIds.gongRockRed              = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_silver_pillar"))         { pack->akurraIds.gongRockBlue             = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_gold_pillar"))           { pack->akurraIds.gongRockYellow           = (i32)oIndex; }
		// else if (StrCompareIgnoreCaseNt(object->name, "obj__pillar"))         { pack->akurraIds.gongRockOrange           = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_hazard_parent"))         { pack->akurraIds.spikesId             = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_spike_pit"))             { pack->akurraIds.holeId               = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_covered_spike_pit"))     { pack->akurraIds.boardedHoleId        = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_stone_elemental"))       { pack->akurraIds.stoneyBoyId          = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_ancient_key"))           { pack->akurraIds.keyId                = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_big_key_ancient"))       { pack->akurraIds.bigGreenKeyId        = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_big_key_stone"))         { pack->akurraIds.bigYellowKeyId       = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_star_piece"))            { pack->akurraIds.starId               = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_obsidian"))              { pack->akurraIds.obsidianId           = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_topaz"))                 { pack->akurraIds.topazId              = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_crystal_active"))        { pack->akurraIds.crystalUpId          = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_crystal_inactive"))      { pack->akurraIds.crystalDownId        = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_purple_portal"))         { pack->akurraIds.portalPurpleInId     = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_purple_portal_exit"))    { pack->akurraIds.portalPurpleOutId    = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_green_portal"))          { pack->akurraIds.portalGreenInId      = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_green_portal_exit"))     { pack->akurraIds.portalGreenOutId     = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_orange_portal"))         { pack->akurraIds.portalOrangeInId     = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_orange_portal_exit"))    { pack->akurraIds.portalOrangeOutId    = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_blue_portal"))           { pack->akurraIds.portalBlueInId       = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_blue_portal_exit"))      { pack->akurraIds.portalBlueOutId      = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_snake_block_0"))         { pack->akurraIds.arrowRightId         = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_snake_block_270"))       { pack->akurraIds.arrowDownId          = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_snake_block_180"))       { pack->akurraIds.arrowLeftId          = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_snake_block_90"))        { pack->akurraIds.arrowUpId            = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_snake_block_inert_0"))   { pack->akurraIds.arrowSecRightId      = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_snake_block_270_inert")) { pack->akurraIds.arrowSecDownId       = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_snake_block_inert_180")) { pack->akurraIds.arrowSecLeftId       = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_snake_block_90_inert"))  { pack->akurraIds.arrowSecUpId         = (i32)oIndex; }
		// else if (StrCompareIgnoreCaseNt(object->name, "obj_snake_fuse_0"))          { pack->akurraIds.arrowFuseRightId     = (i32)oIndex; }
		// else if (StrCompareIgnoreCaseNt(object->name, "obj_snake_fuse_270"))        { pack->akurraIds.arrowFuseDownId      = (i32)oIndex; }
		// else if (StrCompareIgnoreCaseNt(object->name, "obj_snake_fuse_180"))        { pack->akurraIds.arrowFuseLeftId      = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_snake_fuse_90"))         { pack->akurraIds.arrowFuseUpId        = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_island_port"))           { pack->akurraIds.turtleId             = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_game_end"))              { pack->akurraIds.gameEndId            = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_lock_stone"))            { pack->akurraIds.lockStoneId          = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_lock_wind"))             { pack->akurraIds.lockWindId           = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_lock_fire"))             { pack->akurraIds.lockFireId           = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_lock_water"))            { pack->akurraIds.lockWaterId          = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_big_lock_ancient"))      { pack->akurraIds.bigGreenDoorId       = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_star_stone"))            { pack->akurraIds.starDoorId           = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_dungeon_stair"))         { pack->akurraIds.stairsId             = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_bear"))                  { pack->akurraIds.bearId               = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_cave_shadow"))           { pack->akurraIds.caveShadowId         = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_secret_area"))           { pack->akurraIds.secretAreaId         = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_cave_secret_passage"))   { pack->akurraIds.caveSecretId         = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_dark_player_spawner"))   { pack->akurraIds.darkSpawnerId        = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_music_note"))            { pack->akurraIds.musicNoteId          = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_environmental_effect"))  { pack->akurraIds.enviroEffect1Id      = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_stone_god"))             { pack->akurraIds.stoneGodId           = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_player_standing"))       { pack->akurraIds.princessId           = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_room_transition"))       { pack->akurraIds.roomTransitionId     = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_secret_stairs"))         { pack->akurraIds.secretStairsId       = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_drop_top"))              { pack->akurraIds.dropTopId            = (i32)oIndex; }
		else if (StrCompareIgnoreCaseNt(object->name, "obj_drop_bottom"))           { pack->akurraIds.dropBottomId         = (i32)oIndex; }
	}
	pack->akurraIds.filled = true;
	pack->akurraIds.foundAll = true;
	for (u32 iIndex = 0; iIndex < ArrayCount(pack->akurraIds.values); iIndex++)
	{
		if (pack->akurraIds.values[iIndex] < 0)
		{
			pack->akurraIds.foundAll = false;
			break;
		}
	}
	return pack->akurraIds.foundAll;
}

bool LookupAkurraCreateIds(GmsPack_t* pack)
{
	NotNull(pack);
	Assert(pack->akurraIds.filled);
	ClearStruct(pack->akurraCreateIds);
	for (u32 iIndex = 0; iIndex < ArrayCount(pack->akurraCreateIds.values); iIndex++)
	{
		pack->akurraCreateIds.values[iIndex] = -1;
	}
	
	GmsRoom_t* stoneIsland_a1 = FindGmsRoomByName(pack, "rm_stone_a1");
	if (stoneIsland_a1 != nullptr)
	{
		GmsRoomObject_t* starDoorObj = FindGmsRoomObjectById(stoneIsland_a1, pack->akurraIds.starDoorId);
		if (starDoorObj != nullptr)
		{
			pack->akurraCreateIds.itemCost5 = starDoorObj->creationCodeId;
		}
	}
	
	GmsRoom_t* rollingIsland_a0 = FindGmsRoomByName(pack, "rm_rolling_a0");
	if (rollingIsland_a0 != nullptr)
	{
		GmsRoomObject_t* starDoorObj = FindGmsRoomObjectById(rollingIsland_a0, pack->akurraIds.starDoorId, 0);
		if (starDoorObj != nullptr)
		{
			pack->akurraCreateIds.itemCost7 = starDoorObj->creationCodeId;
		}
	}
	
	GmsRoom_t* lockedIsland_a0 = FindGmsRoomByName(pack, "rm_locked_a0");
	if (lockedIsland_a0 != nullptr)
	{
		GmsRoomObject_t* starDoorObj = FindGmsRoomObjectById(lockedIsland_a0, pack->akurraIds.starDoorId, 0);
		if (starDoorObj != nullptr)
		{
			pack->akurraCreateIds.itemCost10 = starDoorObj->creationCodeId;
		}
	}
	
	GmsRoom_t* overworldRoom = FindGmsRoomByName(pack, "rm_overworld");
	if (overworldRoom != nullptr)
	{
		GmsRoomObject_t* starDoorObj1 = FindGmsRoomObjectById(overworldRoom, pack->akurraIds.starDoorId, 0);
		if (starDoorObj1 != nullptr)
		{
			pack->akurraCreateIds.itemCost3 = starDoorObj1->creationCodeId;
		}
		GmsRoomObject_t* starDoorObj2 = FindGmsRoomObjectById(overworldRoom, pack->akurraIds.starDoorId, 1);
		if (starDoorObj2 != nullptr)
		{
			pack->akurraCreateIds.itemCost100 = starDoorObj2->creationCodeId;
		}
	}
	
	GmsRoom_t* stoneDungeon_d2 = FindGmsRoomByName(pack, "rm_stone_dungeon_d2");
	if (stoneDungeon_d2 != nullptr)
	{
		GmsRoomObject_t* secretPassageObj = FindGmsRoomObjectById(stoneDungeon_d2, pack->akurraIds.caveSecretId, 0);
		if (secretPassageObj != nullptr)
		{
			pack->akurraCreateIds.facingDirectionUp = secretPassageObj->creationCodeId;
		}
	}
	
	GmsRoom_t* stoneDungeon_e2 = FindGmsRoomByName(pack, "rm_stone_dungeon_e2");
	if (stoneDungeon_e2 != nullptr)
	{
		GmsRoomObject_t* starDoorObj = FindGmsRoomObjectById(stoneDungeon_e2, pack->akurraIds.starDoorId, 0);
		if (starDoorObj != nullptr)
		{
			pack->akurraCreateIds.itemCost15 = starDoorObj->creationCodeId;
		}
	}
	
	GmsRoom_t* stoneDungeon_b2 = FindGmsRoomByName(pack, "rm_stone_dungeon_b2");
	if (stoneDungeon_b2 != nullptr)
	{
		GmsRoomObject_t* secretPassageObj = FindGmsRoomObjectById(stoneDungeon_b2, pack->akurraIds.caveSecretId, 1);
		if (secretPassageObj != nullptr)
		{
			pack->akurraCreateIds.facingDirectionRight = secretPassageObj->creationCodeId;
		}
	}
	
	//TODO: Find code for facingDirectionLeft
	//TODO: Find code for facingDirectionDown
	
	if (pack->akurraCreateIds.itemCost3            < 0) { WriteLine_W("Warning: Couldn't find creation code for itemCost3");            }
	if (pack->akurraCreateIds.itemCost5            < 0) { WriteLine_W("Warning: Couldn't find creation code for itemCost5");            }
	if (pack->akurraCreateIds.itemCost7            < 0) { WriteLine_W("Warning: Couldn't find creation code for itemCost7");            }
	if (pack->akurraCreateIds.itemCost10           < 0) { WriteLine_W("Warning: Couldn't find creation code for itemCost10");           }
	if (pack->akurraCreateIds.itemCost15           < 0) { WriteLine_W("Warning: Couldn't find creation code for itemCost15");           }
	if (pack->akurraCreateIds.itemCost100          < 0) { WriteLine_W("Warning: Couldn't find creation code for itemCost100");          }
	if (pack->akurraCreateIds.facingDirectionUp    < 0) { WriteLine_W("Warning: Couldn't find creation code for facingDirectionUp");    }
	if (pack->akurraCreateIds.facingDirectionRight < 0) { WriteLine_W("Warning: Couldn't find creation code for facingDirectionRight"); }
	
	pack->akurraCreateIds.filled = true;
	pack->akurraCreateIds.foundAll = true;
	for (u32 iIndex = 0; iIndex < ArrayCount(pack->akurraCreateIds.values); iIndex++)
	{
		if (pack->akurraCreateIds.values[iIndex] < 0)
		{
			pack->akurraCreateIds.foundAll = false;
			break;
		}
	}
	return pack->akurraCreateIds.foundAll;
}

GmsRoomLayer_t* AddLayerToRoom(GmsPack_t* pack, GmsRoom_t* room, MemoryArena_t* memArena, GmsEnum_LayerType_t type, const char* name, i32 depth, u32 numItemsExpected = 0)
{
	NotNull(pack);
	NotNull(room);
	NotNull(name);
	
	GmsRoomLayer_t* result = DynArrayAdd(&room->layers, GmsRoomLayer_t);
	NotNull(result);
	ClearPointer(result);
	result->name = ArenaNtString(memArena, name);
	result->id = pack->nextLayerId++;
	result->type = type;
	result->depth = depth;
	result->unknown7 = 0x00000001;
	if (type == GmsEnum_LayerType_Instance)
	{
		CreateDynamicArray(&result->instances, memArena, sizeof(u32), 16, numItemsExpected);
	}
	else if (type == GmsEnum_LayerType_Background)
	{
		CreateDynamicArray(&result->backgrounds, memArena, sizeof(GmsRoomLayerBackground_t), numItemsExpected, numItemsExpected);
	}
	else if (type == GmsEnum_LayerType_Tile)
	{
		CreateDynamicArray(&result->tiles, memArena, sizeof(u32), numItemsExpected, numItemsExpected);
	}
	else { Assert(false); } //this function doesn't support other types yet
	
	return result;
}

bool ConvertPcqEntityToRoomInstance(const GmsPack_t* pack, v2i tilePos, const StrSplitPiece_t* typeStr, u8 variant, Dir2_t rotation, u8 color, const u8* options, i32* objIdOut, AkurraLayer_t* layerOut, v2i* posOffsetOut, bool* inheritCreationCodeOut, i32* creationCodeIdOut)
{
	NotNull(pack);
	Assert(pack->akurraIds.filled);
	Assert(pack->akurraCreateIds.filled);
	NotNull(typeStr);
	NotNull(typeStr->pntr);
	NotNull(options);
	NotNull(objIdOut);
	NotNull(layerOut);
	NotNull(posOffsetOut);
	NotNull(inheritCreationCodeOut);
	NotNull(creationCodeIdOut);
	
	const AkurraObjIds_t*    objIds    = &pack->akurraIds;
	const AkurraCreateIds_t* createIds = &pack->akurraCreateIds;
	
	*inheritCreationCodeOut = false;
	*creationCodeIdOut = -1;
	if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "BX")) //box
	{
		u8 boxShape = (variant & 0x0F);
		u8 boxMaterial = ((variant >> 4) & 0x0F);
		if (boxShape == 0x01) //box
		{
			if (boxMaterial == 0x01) //wood
			{
				*objIdOut = objIds->boxRegId; *layerOut = AkurraLayer_Object; return true;
			}
			else if (boxMaterial == 0x02) //metal
			{
				*objIdOut = objIds->boxBlueId; *layerOut = AkurraLayer_Object; return true;
			}
		}
		else if (boxShape == 0x03) //boulder
		{
			if (boxMaterial == 0x03) //slime
			{
				*objIdOut = objIds->stoneyBoyId; *layerOut = AkurraLayer_Object; return true;
			}
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "GC")) //ground cover
	{
		*objIdOut = objIds->deepWaterId; *layerOut = AkurraLayer_Collision; return true;
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "RL")) //rail
	{
		if (variant == 0x05) //box
		{
			*objIdOut = objIds->pedestalId; *layerOut = AkurraLayer_Collision; return true;
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "ST")) //Trap
	{
		if (variant == 0x00) //spike
		{
			*objIdOut = objIds->spikesId; *layerOut = AkurraLayer_Floor; return true;
		}
		else if (variant == 0x01) //fire
		{
			if (IsFlagSet(options[0], 0x01)) //deactivated
			{
				*objIdOut = objIds->crystalDownId; *layerOut = AkurraLayer_Floor; return true;
			}
			else
			{
				*objIdOut = objIds->crystalUpId; *layerOut = AkurraLayer_Floor; return true;
			}
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "PD")) //Trap Door
	{
		if (IsFlagSet(options[0], 0x01)) //isOpen
		{
			*objIdOut = objIds->holeId; *layerOut = AkurraLayer_Floor; return true;
		}
		else
		{
			*objIdOut = objIds->boardedHoleId; *layerOut = AkurraLayer_Floor; return true;
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "KE")) //Key
	{
		if (variant == 0x00) //default
		{
			*objIdOut = objIds->keyId; *layerOut = AkurraLayer_Object; return true;
		}
		else if (variant == 0x01) //skeleton
		{
			if (color == 5) //yellow
			{
				*objIdOut = objIds->bigYellowKeyId; *layerOut = AkurraLayer_Object; return true;
			}
			else if (color == 8) //green
			{
				*objIdOut = objIds->bigGreenKeyId; *layerOut = AkurraLayer_Object; return true;
			}
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "DM")) //Diamond
	{
		if (variant == 0x01) //gold bar
		{
			if (color == 0x03) //black
			{
				*objIdOut = objIds->obsidianId; *layerOut = AkurraLayer_Object; return true;
			}
			else //if (color == 0x05) //yellow
			{
				*objIdOut = objIds->topazId; *layerOut = AkurraLayer_Object; return true;
			}
		}
		else if (variant == 0x02) //red diamond
		{
			*objIdOut = objIds->starId; *layerOut = AkurraLayer_Object; return true;
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "DO")) //Key Door
	{
		if (color == 2) //cyan
		{
			u8 numRequired = options[2];
			//TODO: How do we get different star door requirements?
			*objIdOut = objIds->starDoorId; *layerOut = AkurraLayer_Object;
			if (numRequired == 3)        { *creationCodeIdOut = createIds->itemCost3;   }
			else if (numRequired == 5)   { *creationCodeIdOut = createIds->itemCost5;   }
			else if (numRequired == 7)   { *creationCodeIdOut = createIds->itemCost7;   }
			else if (numRequired == 10)  { *creationCodeIdOut = createIds->itemCost10;  }
			else if (numRequired == 15)  { *creationCodeIdOut = createIds->itemCost15;  }
			else if (numRequired == 100) { *creationCodeIdOut = createIds->itemCost100; }
			else { PrintLine_W("Warning: Unsupported star door count %u! Only 3, 5, 7, 10, 15, and 100 are supported", numRequired); }
			return true;
		}
		else if (color == 1) //red
		{
			*objIdOut = objIds->lockFireId; *layerOut = AkurraLayer_Object; return true;
		}
		else if (color == 10) //other1
		{
			*objIdOut = objIds->lockWindId; *layerOut = AkurraLayer_Object; return true;
		}
		else if (color == 5) //yellow
		{
			*objIdOut = objIds->lockStoneId; *layerOut = AkurraLayer_Object; return true;
		}
		else if (color == 7) //blue
		{
			*objIdOut = objIds->lockWaterId; *layerOut = AkurraLayer_Object; return true;
		}
		else if (color == 8) //green
		{
			*objIdOut = objIds->bigGreenDoorId; *layerOut = AkurraLayer_Object; return true;
		}
		else if (color == 0) //default
		{
			if (options[0] == 1 && options[1] == 1) //1x1 door
			{
				*objIdOut = objIds->doorId; *layerOut = AkurraLayer_Object; return true;
			}
			else if (options[0] == 2 && options[1] == 2) //2x2 door
			{
				*objIdOut = objIds->largeDoorId; *layerOut = AkurraLayer_Object; return true;
			}
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "TP")) //Teleporter
	{
		if (color == 9) //purple
		{
			if (options[1] == 0x01) //sender
			{
				*objIdOut = objIds->portalPurpleInId; *layerOut = AkurraLayer_Object; return true;
			}
			else if (options[1] == 0x02) //receiver
			{
				*objIdOut = objIds->portalPurpleOutId; *layerOut = AkurraLayer_Collision; return true;
			}
		}
		else if (color == 8) //green
		{
			if (options[1] == 0x01) //sender
			{
				*objIdOut = objIds->portalGreenInId; *layerOut = AkurraLayer_Object; return true;
			}
			else if (options[1] == 0x02) //receiver
			{
				*objIdOut = objIds->portalGreenOutId; *layerOut = AkurraLayer_Collision; return true;
			}
		}
		else if (color == 6) //orange
		{
			if (options[1] == 0x01) //sender
			{
				*objIdOut = objIds->portalOrangeInId; *layerOut = AkurraLayer_Object; return true;
			}
			else if (options[1] == 0x02) //receiver
			{
				*objIdOut = objIds->portalOrangeOutId; *layerOut = AkurraLayer_Collision; return true;
			}
		}
		else if (color == 2) //cyan
		{
			if (options[1] == 0x01) //sender
			{
				*objIdOut = objIds->portalBlueInId; *layerOut = AkurraLayer_Object; return true;
			}
			else if (options[1] == 0x02) //receiver
			{
				*objIdOut = objIds->portalBlueOutId; *layerOut = AkurraLayer_Collision; return true;
			}
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "PP")) //Pressure Plate
	{
		if (variant == 0x01) //PressurePlateVariant_Round
		{
			if (color == 4) //white
			{
				*objIdOut = objIds->roomTransitionId; *layerOut = AkurraLayer_Collision; return true;
			}
			else if (color == 9) //purple
			{
				*objIdOut = objIds->darkSpawnerId; *layerOut = AkurraLayer_Collision; return true;
			}
		}
		else if (variant == 0x02) //PressurePlateVariant_Stone
		{
			if (color == 6) //orange
			{
				*objIdOut = objIds->orangeButtonId; *layerOut = AkurraLayer_Floor; return true;
			}
			else if (color == 8) //green
			{
				*objIdOut = objIds->greenButtonId; *layerOut = AkurraLayer_Floor; return true;
			}
			else if (color == 9) //purple
			{
				*objIdOut = objIds->purpleButtonId; *layerOut = AkurraLayer_Floor; return true;
			}
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "UP")) //Popup Wall
	{
		if (color == 6) //orange
		{
			if (IsFlagSet(options[0], 0x01)) //activated
			{
				*objIdOut = objIds->orangePillarUpId; *layerOut = AkurraLayer_Object; return true;
			}
			else
			{
				*objIdOut = objIds->orangePillarDownId; *layerOut = AkurraLayer_Floor; return true;
			}
		}
		else if (color == 8) //green
		{
			if (IsFlagSet(options[0], 0x01)) //activated
			{
				*objIdOut = objIds->greenPillarUpId; *layerOut = AkurraLayer_Object; return true;
			}
			else
			{
				*objIdOut = objIds->greenPillarDownId; *layerOut = AkurraLayer_Floor; return true;
			}
		}
		else if (color == 9) //purple
		{
			if (IsFlagSet(options[0], 0x01)) //activated
			{
				*objIdOut = objIds->purplePillarUpId; *layerOut = AkurraLayer_Object; return true;
			}
			else
			{
				*objIdOut = objIds->purplePillarDownId; *layerOut = AkurraLayer_Floor; return true;
			}
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "PH")) //Pusher
	{
		if (variant == 0x00) //PusherEntVariant_Default
		{
			if (IsFlagSet(options[0], 0x10)) //silent
			{
				/*if (rotation == Dir2_Right)     { *objIdOut = objIds->arrowSecRightId; *layerOut = AkurraLayer_Floor; return true; }
				else if (rotation == Dir2_Down) { *objIdOut = objIds->arrowSecDownId;  *layerOut = AkurraLayer_Floor; return true; }
				else if (rotation == Dir2_Left) { *objIdOut = objIds->arrowSecLeftId;  *layerOut = AkurraLayer_Floor; return true; }
				else */if (rotation == Dir2_Up)   { *objIdOut = objIds->arrowFuseUpId;    *layerOut = AkurraLayer_Floor; return true; }
			}
			else if (IsFlagSet(options[0], 0x01)) //deactivated
			{
				if (rotation == Dir2_Right)     { *objIdOut = objIds->arrowSecRightId; *layerOut = AkurraLayer_Object; return true; }
				else if (rotation == Dir2_Down) { *objIdOut = objIds->arrowSecDownId;  *layerOut = AkurraLayer_Object; return true; }
				else if (rotation == Dir2_Left) { *objIdOut = objIds->arrowSecLeftId;  *layerOut = AkurraLayer_Object; return true; }
				else if (rotation == Dir2_Up)   { *objIdOut = objIds->arrowSecUpId;    *layerOut = AkurraLayer_Object; return true; }
			}
			else
			{
				if (rotation == Dir2_Right)     { *objIdOut = objIds->arrowRightId;    *layerOut = AkurraLayer_Object; return true; }
				else if (rotation == Dir2_Down) { *objIdOut = objIds->arrowDownId;     *layerOut = AkurraLayer_Object; return true; }
				else if (rotation == Dir2_Left) { *objIdOut = objIds->arrowLeftId;     *layerOut = AkurraLayer_Object; return true; }
				else if (rotation == Dir2_Up)   { *objIdOut = objIds->arrowUpId;       *layerOut = AkurraLayer_Object; return true; }
			}
		}
		else if (variant == 0x01) //PusherEntVariant_Arrows
		{
			if (rotation == Dir2_Down)
			{
				if (IsFlagSet(options[0], 0x01)) //deactivated
				{
					*objIdOut = objIds->dropBottomId; *layerOut = AkurraLayer_Collision; return true;
				}
				else
				{
					*objIdOut = objIds->dropTopId; *layerOut = AkurraLayer_Collision; return true;
				}
			}
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "AR")) //Progress Arrow
	{
		if (variant == 0x00) //ProgressArrowVariant_Large
		{
			*objIdOut = objIds->turtleId; *layerOut = AkurraLayer_Collision; *posOffsetOut = NewVec2i(-32, -32); *inheritCreationCodeOut = true; return true;
		}
		else if (variant == 0x01) //ProgressArrowVariant_Small
		{
			*objIdOut = objIds->gameEndId; *layerOut = AkurraLayer_Collision; return true;
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "PS")) //Path Stones
	{
		if (options[3] != 0x00) //emboss
		{
			*objIdOut = objIds->blockerId; *layerOut = AkurraLayer_Collision; return true;
		}
		else
		{
			*objIdOut = objIds->secretStairsId; *layerOut = AkurraLayer_Collision; return true;
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "SP")) //Spawner
	{
		if (variant == 0x01) //Pipe
		{
			*objIdOut = objIds->stairsId; *layerOut = AkurraLayer_Collision; *inheritCreationCodeOut = true; return true;
		}
		if (variant == 0x02) //Invisible
		{
			//TODO: Figure out how the upward facing stairs are distinguished
			*objIdOut = objIds->stairsId; *layerOut = AkurraLayer_Collision; *inheritCreationCodeOut = true; return true;
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "PE")) //Pet
	{
		if (variant == 0x00) //DogBrown
		{
			*objIdOut = objIds->bearId; *layerOut = AkurraLayer_Object; return true;
		}
		if (variant == 0x02) //DogOrange
		{
			*objIdOut = objIds->stoneGodId; *layerOut = AkurraLayer_Effects; return true;
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "HS")) //Hit Switch
	{
		if (color == 1) //red
		{
			*objIdOut = objIds->gongRed; *layerOut = AkurraLayer_Object; return true;
		}
		else if (color == 2) //cyan
		{
			*objIdOut = objIds->gongBlue; *layerOut = AkurraLayer_Object; return true;
		}
		else if (color == 5) //yellow
		{
			*objIdOut = objIds->gongYellow; *layerOut = AkurraLayer_Object; return true;
		}
		// else if (color == 6) //orange
		// {
		// 	*objIdOut = objIds->gongOrange; *layerOut = AkurraLayer_Object; return true;
		// }
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "LD")) //LED Door
	{
		if (color == 1) //red
		{
			*objIdOut = objIds->gongRockRed; *layerOut = AkurraLayer_Object; return true;
		}
		else if (color == 2) //cyan
		{
			*objIdOut = objIds->gongRockBlue; *layerOut = AkurraLayer_Object; return true;
		}
		else if (color == 5) //yellow
		{
			*objIdOut = objIds->gongRockYellow; *layerOut = AkurraLayer_Object; return true;
		}
		// else if (color == 6) //orange
		// {
		// 	*objIdOut = objIds->gongRockOrange; *layerOut = AkurraLayer_Object; return true;
		// }
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "FC")) //Focal Point
	{
		if (variant == 0x00) //FocalPointVariant_Puzzle
		{
			*objIdOut = objIds->secretAreaId; *layerOut = AkurraLayer_Collision; return true;
		}
		else if (variant == 0x01) //FocalPointVariant_Secret
		{
			*objIdOut = objIds->musicNoteId; *layerOut = AkurraLayer_Floor; return true;
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "LO")) //Level Options
	{
		*objIdOut = objIds->enviroEffect1Id; *layerOut = AkurraLayer_System; return true;
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "PL")) //Player
	{
		if (variant == 0x03) //PlayerVariant_Fake
		{
			*objIdOut = objIds->princessId; *layerOut = AkurraLayer_Object; *inheritCreationCodeOut = true; return true;
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "OC")) //Occlusion
	{
		if (variant == 0x00) //OcclusionVariant_Cloud
		{
			*objIdOut = objIds->caveShadowId; *layerOut = AkurraLayer_Shadow; *inheritCreationCodeOut = true; return true;
		}
	}
	else if (DoesSplitPieceEqualIgnoreCaseNt(typeStr, "DS")) //Disruptor
	{
		if (variant == 0x00) //DisruptorVariant_Default
		{
			*objIdOut = objIds->caveSecretId; *layerOut = AkurraLayer_Shadow;
			if (rotation == Dir2_Up)    { *creationCodeIdOut = createIds->facingDirectionUp;    }
			if (rotation == Dir2_Down)  { *creationCodeIdOut = createIds->facingDirectionDown;  } //TODO: This creationCodeId isn't filled yet!
			if (rotation == Dir2_Left)  { *creationCodeIdOut = createIds->facingDirectionLeft;  } //TODO: This creationCodeId isn't filled yet!
			if (rotation == Dir2_Right) { *creationCodeIdOut = createIds->facingDirectionRight; }
			return true;
		}
	}
	
	return false;
}

bool ConvertPcqTileToTileIdAndInstance(const AkurraObjIds_t* objIds, v2i tilePos, const StrSplitPiece_t* tileTypeStr, u8 tileVariant, u8 tileColor, bool hasWall, u8 wallVariant, u32* tileIdOut, i32* objIdOut, bool* isCollisionOut)
{
	NotNull(objIds);
	NotNull(tileTypeStr);
	NotNull(tileTypeStr->pntr);
	NotNull(tileIdOut);
	NotNull(objIdOut);
	NotNull(isCollisionOut);
	
	bool result = false;
	*tileIdOut = 0;
	if (DoesSplitPieceEqualIgnoreCaseNt(tileTypeStr, "AK")) { *tileIdOut = (u32)tileVariant; }
	if (DoesSplitPieceEqualIgnoreCaseNt(tileTypeStr, "A2")) { *tileIdOut = 255 + (u32)tileVariant; }
	if (hasWall) { *isCollisionOut = true; *objIdOut = objIds->solidWallId; result = true; }
	else if (DoesSplitPieceEqualIgnoreCaseNt(tileTypeStr, "CP")) { *isCollisionOut = true; *objIdOut = objIds->blockerId; result = true; }
	
	return result;
}

bool TryParseEntityVariantId(const char* variantStrPntr, u32 variantStrLength, const StrSplitPiece_t* typeStr, u8* variantOut = nullptr)
{
	if (DoesSplitPieceEqual(typeStr, "BX")) //box
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "WB", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x11; } return true; } //BoxVariant_WoodBox
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "MB", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x21; } return true; } //BoxVariant_MetalBox
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "SB", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x31; } return true; } //BoxVariant_SlimeBox
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "IB", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x41; } return true; } //BoxVariant_IceBox
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "VB", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x51; } return true; } //BoxVariant_VoidBox
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "TB", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x61; } return true; } //BoxVariant_TetrisBox
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "WR", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x12; } return true; } //BoxVariant_WoodBarrel
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "MR", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x22; } return true; } //BoxVariant_MetalBarrel
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "SR", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x32; } return true; } //BoxVariant_SlimeBarrel
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "IR", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x42; } return true; } //BoxVariant_IceBarrel
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "VR", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x52; } return true; } //BoxVariant_VoidBarrel
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "TR", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x62; } return true; } //BoxVariant_TetrisBarrel
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "WO", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x13; } return true; } //BoxVariant_WoodBoulder
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "MO", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x23; } return true; } //BoxVariant_MetalBoulder
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "SO", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x33; } return true; } //BoxVariant_SlimeBoulder
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "IO", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x43; } return true; } //BoxVariant_IceBoulder
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "VO", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x53; } return true; } //BoxVariant_VoidBoulder
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "TO", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x63; } return true; } //BoxVariant_TetrisBoulder
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "GC")) //ground cover
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "DU", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //GroundCoverVariant_Dust
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "SN", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //GroundCoverVariant_Snow
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "RL")) //rail
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "AL", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //RailVariant_AllDirections
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "TJ", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //RailVariant_TJunction
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "CO", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x02; } return true; } //RailVariant_Corner
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "ST", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x03; } return true; } //RailVariant_Straight
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "EN", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x04; } return true; } //RailVariant_DeadEnd
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "BX", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x05; } return true; } //RailVariant_Box
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "ST")) //Trap
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "SP", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //TrapVariant_Spike
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "FI", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //TrapVariant_Fire
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "PD")) //Trap Door
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "WO", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //TrapDoorVariant_Wood
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "GL", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //TrapDoorVariant_Glass
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "ME", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x02; } return true; } //TrapDoorVariant_Metal
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "CA", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x03; } return true; } //TrapDoorVariant_Caution
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "SP", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x04; } return true; } //TrapDoorVariant_Space
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "KE")) //Key
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "DF", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //KeyVariant_Default
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "SK", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //KeyVariant_Skeleton
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "DM")) //Diamond
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "DI", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //DiamondVariant_Default
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "GO", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //DiamondVariant_GoldBar
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "RD", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x02; } return true; } //DiamondVariant_Red
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "DO")) //Key Door
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "DF", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //default
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "TP")) //Teleporter
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "DF", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //default
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "PP")) //Pressure Plate
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "RO", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //PressurePlateVariant_Round
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "SQ", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //PressurePlateVariant_Square
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "ST", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x02; } return true; } //PressurePlateVariant_Stone
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "UP")) //Popup Wall
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "DF", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //default
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "PH")) //Pusher
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "DF", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //PusherEntVariant_Default
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "AR", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //PusherEntVariant_Arrows
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "CV", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x02; } return true; } //PusherEntVariant_Conveyor
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "AR")) //Progress Arrow
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "LA", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //ProgressArrowVariant_Large
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "SM", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //ProgressArrowVariant_Small
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "PS")) //Path Stones
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "DF", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //default
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "SP")) //Spawner
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "PI", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //SpawnerVariant_Pipe
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "EL", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //SpawnerVariant_Elevator
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "IN", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x02; } return true; } //SpawnerVariant_Invisible
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "PE")) //Pet
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "B1", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //PetVariant_DogBrown
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "G1", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //PetVariant_DogGrey
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "O1", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x02; } return true; } //PetVariant_DogOrange
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "B2", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x03; } return true; } //PetVariant_DogBrown2
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "G2", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x04; } return true; } //PetVariant_DogGrey2
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "O2", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x05; } return true; } //PetVariant_DogOrange2
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "SH", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x06; } return true; } //PetVariant_Shiba
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "HS")) //Hit Switch
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "DF", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //default
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "LD")) //LED Door
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "DF", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //default
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "FC")) //Focal Point
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "PU", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //default
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "SE", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //default
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "LO")) //Level Options
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "DF", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //default
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "PL")) //Player
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "SL", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //default
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "P1", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //default
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "P2", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x02; } return true; } //default
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "FK", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x03; } return true; } //default
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "OC")) //Occlusion
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "CL", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //default
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "BB", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //default
		return false;
	}
	else if (DoesSplitPieceEqual(typeStr, "DS")) //Disruptor
	{
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "DF", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x00; } return true; } //default
		if (variantStrLength == 2 && MyStrCompare(variantStrPntr, "BU", 2) == 0) { if (variantOut != nullptr) { *variantOut = 0x01; } return true; } //default
		return false;
	}
	else
	{
		//We don't care about other entities right now
		if (variantOut != nullptr) { *variantOut = 0x00; }
		return true;
	}
}

i32 GetOriginalCreationCodeId(const GmsPack_t* pack, const GmsRoom_t* originalRoom, u32 objTypeId)
{
	for (u32 oIndex = 0; oIndex < originalRoom->objects.length; oIndex++)
	{
		GmsRoomObject_t* originalObj = DynArrayGet(&originalRoom->objects, GmsRoomObject_t, oIndex);
		NotNull(originalObj);
		if (originalObj->id == (i32)objTypeId && originalObj->creationCodeId >= 0)
		{
			return originalObj->creationCodeId;
		}
	}
	return -1;
}

bool DeserializePcqLevel(GmsPack_t* pack, const char* filePath, MemoryArena_t* memArena, GmsRoom_t* roomOut, GmsRoom_t* originalRoom)
{
	NotNull(pack);
	NotNull(filePath);
	NotNull(memArena);
	NotNull(roomOut);
	
	bool initializedRoom = false;
	bool openedFile = false;
	File_t levelFile = {};
	TextParser_t parser = {};
	StrSplitPiece_t prevTileTypeStr = {};
	StrSplitPiece_t title = {};
	
	if (!pack->akurraIds.filled) { LookupAkurraObjectIds(pack); }
	if (!pack->akurraCreateIds.filled) { LookupAkurraCreateIds(pack); }
	
	if (!ReadEntireFile(filePath, &levelFile))
	{
		PrintLine_E("Failed to open PCQ level file at \"%s\"", filePath);
		goto cleanup;
	}
	openedFile = true;
	
	CreateTextParser(&parser, (const char*)levelFile.data, (u32)levelFile.size);
	
	bool foundTitle = false;
	// StrSplitPiece_t title = {}; //initialized above
	bool foundLevelSize = false;
	v2i levelSize = Vec2i_Zero;
	u32 numChunkTiles = 0;
	u32 numTiles = 0;
	bool foundNumEntities = false;
	u32 numEntities = 0;
	bool foundChunkOffset = false;
	v2i chunkOffset = Vec2i_Zero;
	bool foundChunkSize = false;
	v2i chunkSize = Vec2i_Zero;
	reci baseChunkRec = Reci_Zero;
	bool foundTilesetIndex = false;
	u8 tilesetIndex = 0;
	bool foundFirstHeader = false;
	
	GmsRoomLayer_t* objLayer = nullptr;
	GmsRoomLayer_t* colLayer = nullptr;
	GmsRoomLayer_t* floorLayer = nullptr;
	GmsRoomLayer_t* sysLayer = nullptr;
	GmsRoomLayer_t* shadowLayer = nullptr;
	GmsRoomLayer_t* effectsLayer = nullptr;
	GmsRoomLayer_t* tileLayer = nullptr;
	
	bool parsingTiles = false;
	u32 numTilesParsed = 0;
	u8 prevTileVariant = 0x00;
	u8 prevTileColor = 0x00;
	bool prevTileHasWall = false;
	u8 prevTileWallVariant = 0x00;
	bool parsingEntities = false;
	while (parser.position < parser.contentLength)
	{
		ParsingToken_t token;
		const char* errorStr = ParserConsumeToken(&parser, &token);
		if (errorStr != nullptr)
		{
			PrintLine_E("Parsing error while parsing level file: %s", errorStr);
			break;
		}
		
		if (token.type == ParsingTokenType_Header)
		{
			if (!foundFirstHeader)
			{
				if (!foundTitle || !foundLevelSize || !foundChunkOffset || !foundChunkSize || !foundNumEntities || levelSize.width <= 0 || levelSize.height <= 0)
				{
					WriteLine_D("Level file is missing a required header or level size is <= 0.");
					goto cleanup;
				}
				
				baseChunkRec = NewReci(chunkOffset, chunkSize);
				
				// +==============================+
				// |       Initialize Room        |
				// +==============================+
				ClearPointer(roomOut);
				roomOut->name = ArenaString(memArena, title.pntr, title.length);
				NotNull(roomOut->name);
				roomOut->caption = nullptr;
				roomOut->size = NewVec2i(chunkSize.width * 16, chunkSize.height * 16 - 8);
				roomOut->isPersistent = false;
				roomOut->creationCodeId = -1;
				roomOut->flags = 0x00030001;
				roomOut->physicsEnabled = false;
				roomOut->gravity = NewVec2(0, 10);
				roomOut->metersPerPixel = 0.1f;
				roomOut->unknown1 = 0x00000000;
				roomOut->unknown2 = 0x00000000;
				roomOut->unknown3 = 0x00000000;
				roomOut->unknown4[0] = 0x00000000;
				roomOut->unknown4[1] = 0x00000000;
				roomOut->unknown4[2] = 0x00000000;
				roomOut->unknown4[3] = 0x00000000;
				roomOut->hasUnknownList = true;
				roomOut->unknownListLength = 0;
				
				CreateDynamicArray(&roomOut->backgrounds, memArena, sizeof(GmsRoomBackground_t));
				CreateDynamicArray(&roomOut->views,       memArena, sizeof(GmsView_t), 8, 8);
				CreateDynamicArray(&roomOut->objects,     memArena, sizeof(GmsRoomObject_t), 256, 256);
				CreateDynamicArray(&roomOut->tiles,       memArena, sizeof(GmsRoomTile_t));
				CreateDynamicArray(&roomOut->layers,      memArena, sizeof(GmsRoomLayer_t), 13, 13);
				
				for (u32 vIndex = 0; vIndex < 8; vIndex++)
				{
					GmsView_t* newView = DynArrayAdd(&roomOut->views, GmsView_t);
					NotNull(newView);
					ClearPointer(newView);
					newView->isEnabled = (vIndex == 0);
					newView->position     = Vec2i_Zero;
					newView->size         = roomOut->size;
					newView->portPosition = Vec2i_Zero;
					newView->portSize     = roomOut->size;
					newView->border       = NewVec2i(32, 32);
					newView->speed        = NewVec2i(-1, -1);
					newView->objectId     = -1;
				}
				
				GmsRoomLayer_t* menuEffectsLayer  = AddLayerToRoom(pack, roomOut, memArena, GmsEnum_LayerType_Instance, "menu_effects_layer",     0);
				GmsRoomLayer_t* menuLayer         = AddLayerToRoom(pack, roomOut, memArena, GmsEnum_LayerType_Instance, "menu_layer",           100);
				sysLayer                          = AddLayerToRoom(pack, roomOut, memArena, GmsEnum_LayerType_Instance, "system_layer",         200, 3);
				shadowLayer                       = AddLayerToRoom(pack, roomOut, memArena, GmsEnum_LayerType_Instance, "shadow_layer",         300);
				effectsLayer                      = AddLayerToRoom(pack, roomOut, memArena, GmsEnum_LayerType_Instance, "effects_layer",        400);
				GmsRoomLayer_t* effectsLayer2     = AddLayerToRoom(pack, roomOut, memArena, GmsEnum_LayerType_Instance, "effects_layer_2",      500);
				colLayer                          = AddLayerToRoom(pack, roomOut, memArena, GmsEnum_LayerType_Instance, "collision_layer",      600, 256);
				GmsRoomLayer_t* playerLayer       = AddLayerToRoom(pack, roomOut, memArena, GmsEnum_LayerType_Instance, "player_layer",         800);
				objLayer                          = AddLayerToRoom(pack, roomOut, memArena, GmsEnum_LayerType_Instance, "object_layer",         900, 32);
				GmsRoomLayer_t* floorEffectsLayer = AddLayerToRoom(pack, roomOut, memArena, GmsEnum_LayerType_Instance, "floor_effects_layer", 1000);
				floorLayer                        = AddLayerToRoom(pack, roomOut, memArena, GmsEnum_LayerType_Instance, "floor_layer",         1100);
				GmsRoomLayer_t* backgroundLayer   = AddLayerToRoom(pack, roomOut, memArena, GmsEnum_LayerType_Background, "bg_1",              1300, 1);
				//NOTE: Make sure you change number of layers predicted above whenever adding new layers otherwise these pointers we are storing might become invalid as the DynArray_t reallocs
				
				numTiles = (u32)(levelSize.width * levelSize.height);
				numChunkTiles = (u32)(chunkSize.width * chunkSize.height);
				tileLayer = AddLayerToRoom(pack, roomOut, memArena, GmsEnum_LayerType_Tile, "tiles", 1200, numChunkTiles);
				tileLayer->tilesetId = (foundTilesetIndex ? tilesetIndex : 4);
				tileLayer->gridSize = chunkSize;
				for (u32 tIndex = 0; tIndex < numChunkTiles; tIndex++)
				{
					u32* newTileValue = DynArrayAdd(&tileLayer->tiles, u32);
					NotNull(newTileValue);
					*newTileValue = 0;
				}
				
				//TODO: Do we need to insert the Background "bg_1" layer?
				
				GmsRoomObject_t* gameControllerObj = DynArrayAdd(&roomOut->objects, GmsRoomObject_t);
				Assert(gameControllerObj != nullptr);
				ClearPointer(gameControllerObj);
				gameControllerObj->position = NewVec2i(0, 0);
				gameControllerObj->id = pack->akurraIds.gameControllerId;
				gameControllerObj->instanceId = pack->nextInstanceId++;
				gameControllerObj->oldCodeId = -1;
				gameControllerObj->scale = Vec2_One;
				gameControllerObj->color = 0xFFFFFFFF;
				gameControllerObj->creationCodeId = GetOriginalCreationCodeId(pack, originalRoom, gameControllerObj->id);
				u32* gameControllerInstance = DynArrayAdd(&sysLayer->instances, u32);
				NotNull(gameControllerInstance);
				*gameControllerInstance = gameControllerObj->instanceId;
				
				GmsRoomObject_t* musicControllerObj = DynArrayAdd(&roomOut->objects, GmsRoomObject_t);
				Assert(musicControllerObj != nullptr);
				ClearPointer(musicControllerObj);
				musicControllerObj->position = NewVec2i(16, 0);
				musicControllerObj->id = pack->akurraIds.musicControllerId;
				musicControllerObj->instanceId = pack->nextInstanceId++;
				musicControllerObj->oldCodeId = -1;
				musicControllerObj->scale = Vec2_One;
				musicControllerObj->color = 0xFFFFFFFF;
				musicControllerObj->creationCodeId = GetOriginalCreationCodeId(pack, originalRoom, musicControllerObj->id);
				u32* musicControllerInstance = DynArrayAdd(&sysLayer->instances, u32);
				NotNull(musicControllerInstance);
				*musicControllerInstance = gameControllerObj->instanceId;
				
				GmsRoomObject_t* ambianceControllerObj = DynArrayAdd(&roomOut->objects, GmsRoomObject_t);
				Assert(ambianceControllerObj != nullptr);
				ClearPointer(ambianceControllerObj);
				ambianceControllerObj->position = NewVec2i(32, 0);
				ambianceControllerObj->id = pack->akurraIds.ambianceParentId;
				ambianceControllerObj->instanceId = pack->nextInstanceId++;
				ambianceControllerObj->oldCodeId = -1;
				ambianceControllerObj->scale = Vec2_One;
				ambianceControllerObj->color = 0xFFFFFFFF;
				ambianceControllerObj->creationCodeId = GetOriginalCreationCodeId(pack, originalRoom, ambianceControllerObj->id);
				u32* ambianceControllerInstance = DynArrayAdd(&sysLayer->instances, u32);
				NotNull(ambianceControllerInstance);
				*ambianceControllerInstance = ambianceControllerObj->instanceId;
				
				GmsRoomLayerBackground_t* background1 = DynArrayAdd(&backgroundLayer->backgrounds, GmsRoomLayerBackground_t);
				NotNull(background1);
				ClearPointer(background1);
				background1->unknown1 = 0x00000000;
				background1->spriteId = -1;
				background1->tileX = false;
				background1->tileY = false;
				background1->stretch = false;
				background1->backgroundColor = 0xFF000000; //black
				background1->unknown5 = 0x00000000;
				background1->animationSpeed = 15.0f;
				background1->isSpeedGameFrames = false;
				
				initializedRoom = true;
				foundFirstHeader = true;
			}
			
			if (DoesSplitPieceEqualIgnoreCaseNt(&token.header, "Tiles"))
			{
				parsingTiles    = true;
				parsingEntities = false;
			}
			else if (DoesSplitPieceEqualIgnoreCaseNt(&token.header, "Entities"))
			{
				parsingTiles    = false;
				parsingEntities = true;
			}
			else
			{
				parsingTiles    = false;
				parsingEntities = false;
			}
		}
		else if (token.type == ParsingTokenType_KeyValue)
		{
			if (foundFirstHeader)
			{
				WriteLine_D("Found Key-Value after first header");
				goto cleanup;
			}
			
			if (DoesSplitPieceEqualIgnoreCaseNt(&token.key, "Title"))
			{
				title = token.value;
				foundTitle = true;
			}
			else if (DoesSplitPieceEqualIgnoreCaseNt(&token.key, "Size"))
			{
				errorStr = TryDeserializeVec2i(token.value.pntr, token.value.length, &levelSize);
				if (errorStr != nullptr)
				{
					PrintLine_E("Failed to parse Size as v2i: \"%.*s\"", token.value.length, token.value.pntr);
					goto cleanup;
				}
				foundLevelSize = true;
			}
			else if (DoesSplitPieceEqualIgnoreCaseNt(&token.key, "NumEntities"))
			{
				errorStr = TryDeserializeU32(token.value.pntr, token.value.length, &numEntities);
				if (errorStr != nullptr)
				{
					PrintLine_E("Failed to parse NumEntities as u32: \"%.*s\"", token.value.length, token.value.pntr);
					goto cleanup;
				}
				foundNumEntities = true;
			}
			else if (DoesSplitPieceEqualIgnoreCaseNt(&token.key, "ChunkOffset"))
			{
				errorStr = TryDeserializeVec2i(token.value.pntr, token.value.length, &chunkOffset);
				if (errorStr != nullptr)
				{
					PrintLine_E("Failed to parse ChunkOffset as v2i: \"%.*s\"", token.value.length, token.value.pntr);
					goto cleanup;
				}
				foundChunkOffset = true;
			}
			else if (DoesSplitPieceEqualIgnoreCaseNt(&token.key, "ChunkSize"))
			{
				errorStr = TryDeserializeVec2i(token.value.pntr, token.value.length, &chunkSize);
				if (errorStr != nullptr)
				{
					PrintLine_E("Failed to parse ChunkOffset as v2i: \"%.*s\"", token.value.length, token.value.pntr);
					goto cleanup;
				}
				foundChunkSize = true;
			}
			else if (DoesSplitPieceEqualIgnoreCaseNt(&token.key, "AkurraTileset"))
			{
				errorStr = TryDeserializeU8(token.value.pntr, token.value.length, &tilesetIndex);
				if (errorStr != nullptr)
				{
					PrintLine_E("Failed to parse AkurraTileset as u8: \"%.*s\"", token.value.length, token.value.pntr);
					goto cleanup;
				}
				foundTilesetIndex = true;
			}
		}
		else if (token.type == ParsingTokenType_ValueList)
		{
			if (!foundFirstHeader)
			{
				WriteLine_D("Found Value-List before first header");
				goto cleanup;
			}
			
			// +==============================+
			// |         Parse Tiles          |
			// +==============================+
			if (parsingTiles)
			{
				u32 numPieces = 0;
				StrSplitPiece_t* pieces = SplitString(TempArena, token.content.pntr, token.content.length, ",", 1, &numPieces);
				Assert(pieces != nullptr || numPieces == 0);
				if (numPieces == 1 && numTilesParsed > 0)
				{
					u32 numRepeats = 0;
					if (!TryParseU32(token.content.pntr, token.content.length, &numRepeats))
					{
						PrintLine_E("Failed to parse repeat tile designater as u32: \"%.*s\"", token.content.length, token.content.pntr);
						goto cleanup;
					}
					// PrintLine_D("Repeating tile %u x %u", previousTileId, numRepeats);
					Assert(numTilesParsed + numRepeats <= numTiles);
					for (u32 rIndex = 0; rIndex < numRepeats; rIndex++)
					{
						v2i tilePos = NewVec2i(numTilesParsed % levelSize.width, numTilesParsed / levelSize.width);
						
						if (IsCoordInsideReci(baseChunkRec, tilePos))
						{
							v2i relativePos = tilePos - baseChunkRec.topLeft;
							u32 tileId = 0;
							i32 objectId = -1;
							bool isObjectCollision = true;
							if (ConvertPcqTileToTileIdAndInstance(&pack->akurraIds, tilePos, &prevTileTypeStr, prevTileVariant, prevTileColor, prevTileHasWall, prevTileWallVariant, &tileId, &objectId, &isObjectCollision))
							{
								GmsRoomObject_t* newObject = DynArrayAdd(&roomOut->objects, GmsRoomObject_t);
								Assert(newObject != nullptr);
								ClearPointer(newObject);
								newObject->position = NewVec2i(relativePos.x * 16, relativePos.y * 16);
								newObject->id = objectId;
								newObject->instanceId = pack->nextInstanceId++;
								newObject->oldCodeId = -1;
								newObject->scale = Vec2_One;
								newObject->imageSpeed = 0.0f;
								newObject->imageFrame = 0;
								newObject->color = 0xFFFFFFFF;
								newObject->rotation = 0.0f;
								newObject->creationCodeId = -1;
								if (isObjectCollision)
								{
									u32* newInstancePntr = DynArrayAdd(&colLayer->instances, u32);
									NotNull(newInstancePntr);
									*newInstancePntr = newObject->instanceId;
								}
								else
								{
									u32* newInstancePntr = DynArrayAdd(&objLayer->instances, u32);
									NotNull(newInstancePntr);
									*newInstancePntr = newObject->instanceId;
								}
							}
							u32* tileIdPntr = DynArrayGet(&tileLayer->tiles, u32, (relativePos.y * tileLayer->gridSize.width) + relativePos.x);
							NotNull(tileIdPntr);
							*tileIdPntr = tileId;
						}
						
						numTilesParsed++;
					}
				}
				else if (numPieces < 3)
				{
					PrintLine_E("Only found %u parts in tile: \"%.*s\"", numPieces, token.content.length, token.content.pntr);
					goto cleanup;
				}
				else
				{
					v2i tilePos = NewVec2i(numTilesParsed % levelSize.width, numTilesParsed / levelSize.width);
					Assert(numTilesParsed < numTiles);
					
					StrSplitPiece_t* tileTypeStr = &pieces[0];
					u8 tileVariant = 0x00;
					u8 tileColor = 0x00;
					bool hasWall = false;
					u8 wallVariant = 0x00;
					if (!TryParseU8(pieces[1].pntr, pieces[1].length, &tileVariant))
					{
						PrintLine_E("Failed to parse tile variant as u8: \"%.*s\"", pieces[1].length, pieces[1].pntr);
						goto cleanup;
					}
					if (!TryParseU8(pieces[2].pntr, pieces[2].length, &tileColor))
					{
						PrintLine_E("Failed to parse tile color as u8: \"%.*s\"", pieces[2].length, pieces[2].pntr);
						goto cleanup;
					}
					if (numPieces >= 4) { hasWall = DoesSplitPieceEqualIgnoreCaseNt(&pieces[3], "T"); }
					if (hasWall && numPieces >= 5)
					{
						if (!TryParseU8(pieces[4].pntr, pieces[4].length, &wallVariant))
						{
							PrintLine_E("Failed to parse tile wall variant as u8: \"%.*s\"", pieces[4].length, pieces[4].pntr);
							goto cleanup;
						}
					}
					
					if (IsCoordInsideReci(baseChunkRec, tilePos))
					{
						v2i relativePos = tilePos - baseChunkRec.topLeft;
						u32 tileId = 0;
						i32 objectId = -1;
						bool isObjectCollision = true;
						if (ConvertPcqTileToTileIdAndInstance(&pack->akurraIds, tilePos, tileTypeStr, tileVariant, tileColor, hasWall, wallVariant, &tileId, &objectId, &isObjectCollision))
						{
							GmsRoomObject_t* newObject = DynArrayAdd(&roomOut->objects, GmsRoomObject_t);
							Assert(newObject != nullptr);
							ClearPointer(newObject);
							newObject->position = NewVec2i(relativePos.x * 16, relativePos.y * 16);
							newObject->id = objectId;
							newObject->instanceId = pack->nextInstanceId++;
							newObject->oldCodeId = -1;
							newObject->scale = Vec2_One;
							newObject->imageSpeed = 0.0f;
							newObject->imageFrame = 0;
							newObject->color = 0xFFFFFFFF;
							newObject->rotation = 0.0f;
							newObject->creationCodeId = -1;
							if (isObjectCollision)
							{
								u32* newInstancePntr = DynArrayAdd(&colLayer->instances, u32);
								NotNull(newInstancePntr);
								*newInstancePntr = newObject->instanceId;
							}
							else
							{
								u32* newInstancePntr = DynArrayAdd(&objLayer->instances, u32);
								NotNull(newInstancePntr);
								*newInstancePntr = newObject->instanceId;
							}
						}
						u32* tileIdPntr = DynArrayGet(&tileLayer->tiles, u32, (relativePos.y * tileLayer->gridSize.width) + relativePos.x);
						NotNull(tileIdPntr);
						*tileIdPntr = tileId;
					}
					MyMemCopy(&prevTileTypeStr, tileTypeStr, sizeof(StrSplitPiece_t));
					prevTileVariant = tileVariant;
					prevTileColor = tileColor;
					prevTileHasWall = hasWall;
					prevTileWallVariant = wallVariant;
					
					numTilesParsed++;
				}
			}
			// +==============================+
			// |        Parse Entities        |
			// +==============================+
			else if (parsingEntities)
			{
				u32 numPieces = 0;
				StrSplitPiece_t* pieces = SplitString(TempArena, token.content.pntr, token.content.length, ",", 1, &numPieces);
				Assert(pieces != nullptr || numPieces == 0);
				if (numPieces < 9)
				{
					PrintLine_E("Entity has %u/9 expected parts: \"%.*s\"", numPieces, token.content.length, token.content.pntr);
					goto cleanup;
				}
				StrSplitPiece_t* typeStr = &pieces[3];
				v2i tilePos = Vec2i_Zero;
				u8 variant = 0x00;
				Dir2_t rotation = Dir2_Down;
				u8 color = 0;
				u8 options[8] = {};
				if (!TryParseI32(pieces[1].pntr, pieces[1].length, &tilePos.x))
				{
					PrintLine_E("Failed to parse entity tilePos.x as i32: \"%.*s\"", pieces[1].length, pieces[1].pntr);
					goto cleanup;
				}
				if (!TryParseI32(pieces[2].pntr, pieces[2].length, &tilePos.y))
				{
					PrintLine_E("Failed to parse entity tilePos.y as i32: \"%.*s\"", pieces[2].length, pieces[2].pntr);
					goto cleanup;
				}
				if (!TryParseU8(pieces[4].pntr, pieces[4].length, &variant))
				{
					if (!TryParseEntityVariantId(pieces[4].pntr, pieces[4].length, typeStr, &variant))
					{
						PrintLine_E("Failed to parse entity variant as u8 or known entity variant for entity type \"%.*s\": \"%.*s\"", typeStr->length, typeStr->pntr, pieces[4].length, pieces[4].pntr);
						goto cleanup;
					}
				}
				errorStr = TryDeserializeDir2(pieces[5].pntr, pieces[5].length, &rotation);
				if (errorStr != nullptr)
				{
					PrintLine_E("Failed to parse entity rotation as Dir2: \"%.*s\" %s", pieces[5].length, pieces[5].pntr, errorStr);
					goto cleanup;
				}
				if (!TryParseU8(pieces[7].pntr, pieces[7].length, &color))
				{
					PrintLine_E("Failed to parse entity color as u8: \"%.*s\"", pieces[7].length, pieces[7].pntr);
					goto cleanup;
				}
				for (u32 opIndex = 0; opIndex < ArrayCount(options); opIndex++)
				{
					if (numPieces >= 9+opIndex+1)
					{
						if (!TryParseU8(pieces[9+opIndex].pntr, pieces[9+opIndex].length, &options[opIndex]))
						{
							PrintLine_E("Failed to parse entity option[%u] as u8: \"%.*s\"", opIndex, pieces[9+opIndex].length, pieces[9+opIndex].pntr);
							goto cleanup;
						}
						// PrintLine_D("parsed entity option[%u]: 0x%02X", opIndex, options[opIndex]);
					}
				}
				
				// +--------------------------------------------------------------+
				// |                    Interpret PCQ Entities                    |
				// +--------------------------------------------------------------+
				if (IsCoordInsideReci(ReciInflate(baseChunkRec, 1), tilePos))
				{
					v2i relativePos = tilePos - baseChunkRec.topLeft;
					i32 objectId = -1;
					AkurraLayer_t objectLayer = AkurraLayer_Object;
					v2i posOffset = Vec2i_Zero;
					bool inheritCreationCode = false;
					i32 explicitCreationCodeId = -1;
					if (ConvertPcqEntityToRoomInstance(pack, tilePos, typeStr, variant, rotation, color, &options[0], &objectId, &objectLayer, &posOffset, &inheritCreationCode, &explicitCreationCodeId))
					{
						GmsRoomObject_t* newObject = DynArrayAdd(&roomOut->objects, GmsRoomObject_t);
						NotNull(newObject);
						ClearPointer(newObject);
						newObject->position = NewVec2i(relativePos.x * 16, relativePos.y * 16) + posOffset;
						newObject->id = objectId;
						newObject->instanceId = pack->nextInstanceId++;
						newObject->oldCodeId = -1;
						newObject->scale = Vec2_One;
						newObject->imageSpeed = 0.0f;
						newObject->imageFrame = 0;
						newObject->color = 0xFFFFFFFF;
						newObject->rotation = 0.0f;
						newObject->creationCodeId = -1;
						GmsRoomLayer_t* layerPntr = nullptr;
						if (objectLayer == AkurraLayer_Object)    { layerPntr = objLayer;     }
						if (objectLayer == AkurraLayer_Collision) { layerPntr = colLayer;     }
						if (objectLayer == AkurraLayer_Floor)     { layerPntr = floorLayer;   }
						if (objectLayer == AkurraLayer_System)    { layerPntr = sysLayer;     }
						if (objectLayer == AkurraLayer_Effects)   { layerPntr = effectsLayer; }
						if (objectLayer == AkurraLayer_Shadow)    { layerPntr = shadowLayer;  }
						NotNull(layerPntr);
						u32* newInstancePntr = DynArrayAdd(&layerPntr->instances, u32);
						NotNull(newInstancePntr);
						*newInstancePntr = newObject->instanceId;
						
						if (explicitCreationCodeId >= 0)
						{
							newObject->creationCodeId = explicitCreationCodeId;
						}
						else if (inheritCreationCode && originalRoom != nullptr)
						{
							newObject->creationCodeId = GetOriginalCreationCodeId(pack, originalRoom, newObject->id);
						}
					}
				}
			}
		}
	}
	
	if (!foundFirstHeader) { WriteLine_D("Found no headers in level file"); goto cleanup; }
	if (numTilesParsed != numTiles) { PrintLine_E("Found %u/%u tiles in level file", numTilesParsed, numTiles); goto cleanup; }
	
	PrintLine_D("Found %u tiles for %dx%d level", numTiles, levelSize.width, levelSize.height);
	
	Assert(initializedRoom);
	FreeFileMemory(&levelFile);
	return true;
	
	cleanup:
	if (openedFile) { FreeFileMemory(&levelFile); }
	if (initializedRoom) { DestroyGmsRoom(roomOut, memArena); }
	return false;
}
