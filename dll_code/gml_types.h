/*
File:   gml_types.h
Author: Taylor Robbins
Date:   10\26\2021
Description:
	** Holds a bunch of types that help us interpret GML bytecode
*/

#ifndef _GML_TYPES_H
#define _GML_TYPES_H

typedef enum
{
	GmlOpCode_Conv     = 0x07,
	GmlOpCode_Mul      = 0x08,
	GmlOpCode_Div      = 0x09,
	GmlOpCode_Rem      = 0x0A,
	GmlOpCode_Mod      = 0x0B,
	GmlOpCode_Add      = 0x0C,
	GmlOpCode_Sub      = 0x0D,
	GmlOpCode_And      = 0x0E,
	GmlOpCode_Or       = 0x0F,
	GmlOpCode_Xor      = 0x10,
	GmlOpCode_Neg      = 0x11,
	GmlOpCode_Not      = 0x12,
	GmlOpCode_Shl      = 0x13,
	GmlOpCode_Shr      = 0x14,
	GmlOpCode_Cmp      = 0x15,
	GmlOpCode_Pop      = 0x45,
	GmlOpCode_Push16   = 0x84, //push(i16)
	GmlOpCode_Dup      = 0x86,
	GmlOpCode_CallV    = 0x99,
	GmlOpCode_Ret      = 0x9C,
	GmlOpCode_Exit     = 0x9D,
	GmlOpCode_Popz     = 0x9E,
	GmlOpCode_Goto     = 0xB6,
	GmlOpCode_If       = 0xB7,
	GmlOpCode_IfNot    = 0xB8,
	GmlOpCode_PushEnv  = 0xBA,
	GmlOpCode_PopEnv   = 0xBB,
	GmlOpCode_Push     = 0xC0,
	GmlOpCode_PushLoc  = 0xC1,
	GmlOpCode_PushGlb  = 0xC2,
	GmlOpCode_PushBltn = 0xC3,
	GmlOpCode_Call     = 0xD9,
	GmlOpCode_Break    = 0xFF,
} GmlOpCode_t;
const char* GetGmlOpCodeStr(u8 opCode)
{
	switch (opCode)
	{
		case GmlOpCode_Conv:     return "Conv";
		case GmlOpCode_Mul:      return "Mul";
		case GmlOpCode_Div:      return "Div";
		case GmlOpCode_Rem:      return "Rem";
		case GmlOpCode_Mod:      return "Mod";
		case GmlOpCode_Add:      return "Add";
		case GmlOpCode_Sub:      return "Sub";
		case GmlOpCode_And:      return "And";
		case GmlOpCode_Or:       return "Or";
		case GmlOpCode_Xor:      return "Xor";
		case GmlOpCode_Neg:      return "Neg";
		case GmlOpCode_Not:      return "Not";
		case GmlOpCode_Shl:      return "Shl";
		case GmlOpCode_Shr:      return "Shr";
		case GmlOpCode_Cmp:      return "Cmp";
		case GmlOpCode_Pop:      return "Pop";
		case GmlOpCode_Push16:   return "Push16";
		case GmlOpCode_Dup:      return "Dup";
		case GmlOpCode_CallV:    return "CallV";
		case GmlOpCode_Ret:      return "Ret";
		case GmlOpCode_Exit:     return "Exit";
		case GmlOpCode_Popz:     return "Popz";
		case GmlOpCode_Goto:     return "Goto";
		case GmlOpCode_If:       return "If";
		case GmlOpCode_IfNot:    return "IfNot";
		case GmlOpCode_PushEnv:  return "PushEnv";
		case GmlOpCode_PopEnv:   return "PopEnv";
		case GmlOpCode_Push:     return "Push";
		case GmlOpCode_PushLoc:  return "PushLoc";
		case GmlOpCode_PushGlb:  return "PushGlb";
		case GmlOpCode_PushBltn: return "PushBltn";
		case GmlOpCode_Call:     return "Call";
		case GmlOpCode_Break:    return "Break";
		default: return "Unknown";
	}
}
bool IsKnownGmlOpCode(u8 opCode)
{
	switch (opCode)
	{
		case GmlOpCode_Conv:     return true;
		case GmlOpCode_Mul:      return true;
		case GmlOpCode_Div:      return true;
		case GmlOpCode_Rem:      return true;
		case GmlOpCode_Mod:      return true;
		case GmlOpCode_Add:      return true;
		case GmlOpCode_Sub:      return true;
		case GmlOpCode_And:      return true;
		case GmlOpCode_Or:       return true;
		case GmlOpCode_Xor:      return true;
		case GmlOpCode_Neg:      return true;
		case GmlOpCode_Not:      return true;
		case GmlOpCode_Shl:      return true;
		case GmlOpCode_Shr:      return true;
		case GmlOpCode_Cmp:      return true;
		case GmlOpCode_Pop:      return true;
		case GmlOpCode_Push16:   return true;
		case GmlOpCode_Dup:      return true;
		case GmlOpCode_CallV:    return true;
		case GmlOpCode_Ret:      return true;
		case GmlOpCode_Exit:     return true;
		case GmlOpCode_Popz:     return true;
		case GmlOpCode_Goto:     return true;
		case GmlOpCode_If:       return true;
		case GmlOpCode_IfNot:    return true;
		case GmlOpCode_PushEnv:  return true;
		case GmlOpCode_PopEnv:   return true;
		case GmlOpCode_Push:     return true;
		case GmlOpCode_PushLoc:  return true;
		case GmlOpCode_PushGlb:  return true;
		case GmlOpCode_PushBltn: return true;
		case GmlOpCode_Call:     return true;
		case GmlOpCode_Break:    return true;
		default: return false;
	}
}

typedef enum
{
	GmlDataType_Double      = 0x00,
	GmlDataType_Float       = 0x01,
	GmlDataType_Int32       = 0x02,
	GmlDataType_Int64       = 0x03,
	GmlDataType_Boolean     = 0x04,
	GmlDataType_Variable    = 0x05,
	GmlDataType_String      = 0x06,
	GmlDataType_Unused1     = 0x07,
	GmlDataType_Instance    = 0x08,
	GmlDataType_Delete      = 0x09,
	GmlDataType_Undefined   = 0x0A,
	GmlDataType_UnsignedInt = 0x0B,
	GmlDataType_Unused2     = 0x0C,
	GmlDataType_Unused3     = 0x0D,
	GmlDataType_Unused4     = 0x0E,
	GmlDataType_Int16       = 0x0F,
} GmlDataType_t;
const char* GetGmlDataTypeStr(u8 dataType)
{
	switch (dataType)
	{
		case GmlDataType_Double:      return "Double";
		case GmlDataType_Float:       return "Float";
		case GmlDataType_Int32:       return "Int32";
		case GmlDataType_Int64:       return "Int64";
		case GmlDataType_Boolean:     return "Boolean";
		case GmlDataType_Variable:    return "Variable";
		case GmlDataType_String:      return "String";
		case GmlDataType_Unused1:     return "Unused1";
		case GmlDataType_Instance:    return "Instance";
		case GmlDataType_Delete:      return "Delete";
		case GmlDataType_Undefined:   return "Undefined";
		case GmlDataType_UnsignedInt: return "UnsignedInt";
		case GmlDataType_Unused2:     return "Unused2";
		case GmlDataType_Unused3:     return "Unused3";
		case GmlDataType_Unused4:     return "Unused4";
		case GmlDataType_Int16:       return "Int16";
		default: return "Unknown";
	}
}
bool IsKnownGmlDataType(u8 dataType)
{
	switch (dataType)
	{
		case GmlDataType_Double:      return true;
		case GmlDataType_Float:       return true;
		case GmlDataType_Int32:       return true;
		case GmlDataType_Int64:       return true;
		case GmlDataType_Boolean:     return true;
		case GmlDataType_Variable:    return true;
		case GmlDataType_String:      return true;
		case GmlDataType_Instance:    return true;
		case GmlDataType_Delete:      return true;
		case GmlDataType_Undefined:   return true;
		case GmlDataType_UnsignedInt: return true;
		case GmlDataType_Int16:       return true;
		default: return false;
	}
}

typedef enum
{
	GmlCompareType_LessThan         = 0x1,
	GmlCompareType_LessThanEqual    = 0x2,
	GmlCompareType_Equal            = 0x3,
	GmlCompareType_NotEqual         = 0x4,
	GmlCompareType_GreaterThanEqual = 0x5,
	GmlCompareType_GreaterThan      = 0x6,
} GmlCompareType_t;
const char* GetGmlCompareTypeStr(u8 compareType)
{
	switch (compareType)
	{
		case GmlCompareType_LessThan:         return "LessThan";
		case GmlCompareType_LessThanEqual:    return "LessThanEqual";
		case GmlCompareType_Equal:            return "Equal";
		case GmlCompareType_NotEqual:         return "NotEqual";
		case GmlCompareType_GreaterThanEqual: return "GreaterThanEqual";
		case GmlCompareType_GreaterThan:      return "GreaterThan";
		default: return "Unknown";
	}
}
const char* GetGmlCompareTypeSymbols(u8 compareType)
{
	switch (compareType)
	{
		case GmlCompareType_LessThan:         return "<";
		case GmlCompareType_LessThanEqual:    return "<=";
		case GmlCompareType_Equal:            return "==";
		case GmlCompareType_NotEqual:         return "!=";
		case GmlCompareType_GreaterThanEqual: return ">=";
		case GmlCompareType_GreaterThan:      return ">";
		default: return "[?]";
	}
}
bool IsKnownGmlCompareType(u8 compareType)
{
	switch (compareType)
	{
		case GmlCompareType_LessThan:         return true;
		case GmlCompareType_LessThanEqual:    return true;
		case GmlCompareType_Equal:            return true;
		case GmlCompareType_NotEqual:         return true;
		case GmlCompareType_GreaterThanEqual: return true;
		case GmlCompareType_GreaterThan:      return true;
		default: return false;
	}
}

typedef enum
{
	//NOTE: Anything >= is an object index
	GmlInstanceValue_Undefined =  0,
	GmlInstanceValue_Self      = -1,
	GmlInstanceValue_Other     = -2,
	GmlInstanceValue_All       = -3,
	GmlInstanceValue_Noone     = -4,
	GmlInstanceValue_Global    = -5,
	GmlInstanceValue_Builtin   = -6,
	GmlInstanceValue_Local     = -7,
	GmlInstanceValue_Stacktop  = -9,
	GmlInstanceValue_Arg       = -15,
	GmlInstanceValue_Static    = -16,
} GmlInstanceValue_t;
const char* GetGmlInstanceValueStr(i32 value)
{
	switch (value)
	{
		case GmlInstanceValue_Undefined: return "Undefined";
		case GmlInstanceValue_Self:      return "Self";
		case GmlInstanceValue_Other:     return "Other";
		case GmlInstanceValue_All:       return "All";
		case GmlInstanceValue_Noone:     return "Noone";
		case GmlInstanceValue_Global:    return "Global";
		case GmlInstanceValue_Builtin:   return "Builtin";
		case GmlInstanceValue_Local:     return "Local";
		case GmlInstanceValue_Stacktop:  return "Stacktop";
		case GmlInstanceValue_Arg:       return "Arg";
		case GmlInstanceValue_Static:    return "Static";
		default: return "Unknown";
	}
}
bool IsKnownGmlInstanceValue(i32 type)
{
	switch (type)
	{
		case GmlInstanceValue_Undefined: return true;
		case GmlInstanceValue_Self:      return true;
		case GmlInstanceValue_Other:     return true;
		case GmlInstanceValue_All:       return true;
		case GmlInstanceValue_Noone:     return true;
		case GmlInstanceValue_Global:    return true;
		case GmlInstanceValue_Builtin:   return true;
		case GmlInstanceValue_Local:     return true;
		case GmlInstanceValue_Stacktop:  return true;
		case GmlInstanceValue_Arg:       return true;
		case GmlInstanceValue_Static:    return true;
		default: return false;
	}
}

// +--------------------------------------------------------------+
// |                 Word Manipulation Functions                  |
// +--------------------------------------------------------------+
u8 GetUppestByte(u32 bytecodeWord)
{
	return (u8)((bytecodeWord & 0xFF000000) >> 24);
}
u8 GetUpperByte(u32 bytecodeWord)
{
	return (u8)((bytecodeWord & 0x00FF0000) >> 16);
}
u8 GetLowerByte(u32 bytecodeWord)
{
	return (u8)((bytecodeWord & 0x0000FF00) >> 8);
}
u8 GetLowestByte(u32 bytecodeWord)
{
	return (u8)((bytecodeWord & 0x000000FF) >> 0);
}
#define GetOpCode(bytecodeWord) GetUppestByte(bytecodeWord)
u32 GetNonOpCode(u32 bytecodeWord)
{
	return ((bytecodeWord & 0x00FFFFFF) >> 0);
}
u16 GetUpperU16(u32 bytecodeWord)
{
	return (u16)((bytecodeWord & 0xFFFF0000) >> 16);
}
u16 GetLowerU16(u32 bytecodeWord)
{
	return (u16)((bytecodeWord & 0x0000FFFF) >> 0);
}
i16 GetUpperI16(u32 bytecodeWord)
{
	u16 unsignedValue = (u16)((bytecodeWord & 0xFFFF0000) >> 16);
	return *(i16*)&unsignedValue;
}
i16 GetLowerI16(u32 bytecodeWord)
{
	u16 unsignedValue = (u16)((bytecodeWord & 0x0000FFFF) >> 0);
	return *(i16*)&unsignedValue;
}
i32 GetI32FromU32(u32 bytecodeWord)
{
	return *(i32*)&bytecodeWord;
}
r32 GetR32FromU32(u32 bytecodeWord)
{
	return *(r32*)&bytecodeWord;
}
u64 GetU64FromWords(u32 word1, u32 word2)
{
	return (((u64)word2) << 32) | ((u64)word1);
}
i64 GetI64FromWords(u32 word1, u32 word2)
{
	u64 unsignedValue = (((u64)word2) << 32) | ((u64)word1);
	return *(i64*)&unsignedValue;
}
r64 GetR64FromWords(u32 word1, u32 word2)
{
	u64 unsignedValue = (((u64)word2) << 32) | ((u64)word1);
	return *(r64*)&unsignedValue;
}

#endif //  _GML_TYPES_H
