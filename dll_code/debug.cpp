/*
File:   debug.cpp
Author: Taylor Robbins
Date:   12\13\2020
Description: 
	** Holds a bunch of macros that help us route debug output appropriately
*/

void MyDebugOutput(DbgLevel_t level, bool newLine, const char* message)
{
	printf("%s%s", message, newLine ? "\n" : "");
	if (main != nullptr && main->debugOutputFunc != nullptr)
	{
		main->debugOutputFunc(level, message, newLine);
	}
}
void MyDebugPrint(DbgLevel_t level, bool newLine, const char* formatString, ...)
{
	Assert(main != nullptr);
	TempPushMark();
	ArenaPrintVa(TempArena, messageStr, messageLength, formatString);
	if (messageStr != nullptr)
	{
		MyDebugOutput(level, newLine, messageStr);
	}
	else 
	{
		MyDebugOutput(level, false, "[PRINT_FAILURE] \"");
		MyDebugOutput(level, false, formatString);
		MyDebugOutput(level, true, "\"");
	}
	TempPopMark();
}

#define Write_D(message)          MyDebugOutput(DbgLevel_Debug,   false, message)
#define WriteLine_D(message)      MyDebugOutput(DbgLevel_Debug,   true,  message)
#define Print_D(message, ...)     MyDebugPrint (DbgLevel_Debug,   false, message, ##__VA_ARGS__)
#define PrintLine_D(message, ...) MyDebugPrint (DbgLevel_Debug,   true,  message, ##__VA_ARGS__)

#define Write_R(message)          MyDebugOutput(DbgLevel_Regular, false, message)
#define WriteLine_R(message)      MyDebugOutput(DbgLevel_Regular, true,  message)
#define Print_R(message, ...)     MyDebugPrint (DbgLevel_Regular, false, message, ##__VA_ARGS__)
#define PrintLine_R(message, ...) MyDebugPrint (DbgLevel_Regular, true,  message, ##__VA_ARGS__)

#define Write_I(message)          MyDebugOutput(DbgLevel_Info,    false, message)
#define WriteLine_I(message)      MyDebugOutput(DbgLevel_Info,    true,  message)
#define Print_I(message, ...)     MyDebugPrint (DbgLevel_Info,    false, message, ##__VA_ARGS__)
#define PrintLine_I(message, ...) MyDebugPrint (DbgLevel_Info,    true,  message, ##__VA_ARGS__)

#define Write_N(message)          MyDebugOutput(DbgLevel_Notify,  false, message)
#define WriteLine_N(message)      MyDebugOutput(DbgLevel_Notify,  true,  message)
#define Print_N(message, ...)     MyDebugPrint (DbgLevel_Notify,  false, message, ##__VA_ARGS__)
#define PrintLine_N(message, ...) MyDebugPrint (DbgLevel_Notify,  true,  message, ##__VA_ARGS__)

#define Write_O(message)          MyDebugOutput(DbgLevel_Other,   false, message)
#define WriteLine_O(message)      MyDebugOutput(DbgLevel_Other,   true,  message)
#define Print_O(message, ...)     MyDebugPrint (DbgLevel_Other,   false, message, ##__VA_ARGS__)
#define PrintLine_O(message, ...) MyDebugPrint (DbgLevel_Other,   true,  message, ##__VA_ARGS__)

#define Write_W(message)          MyDebugOutput(DbgLevel_Warning, false, message)
#define WriteLine_W(message)      MyDebugOutput(DbgLevel_Warning, true,  message)
#define Print_W(message, ...)     MyDebugPrint (DbgLevel_Warning, false, message, ##__VA_ARGS__)
#define PrintLine_W(message, ...) MyDebugPrint (DbgLevel_Warning, true,  message, ##__VA_ARGS__)

#define Write_E(message)          MyDebugOutput(DbgLevel_Error,   false, message)
#define WriteLine_E(message)      MyDebugOutput(DbgLevel_Error,   true,  message)
#define Print_E(message, ...)     MyDebugPrint (DbgLevel_Error,   false, message, ##__VA_ARGS__)
#define PrintLine_E(message, ...) MyDebugPrint (DbgLevel_Error,   true,  message, ##__VA_ARGS__)

