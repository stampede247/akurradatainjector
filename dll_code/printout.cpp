/*
File:   printout.cpp
Author: Taylor Robbins
Date:   12\16\2020
Description: 
	** Holds some functions that help us print out various structures 
*/

void PrintGmsRoomObject(const char* linePrefix, u32 objIndex, const GmsRoomObject_t* roomObject)
{
	GmsObject_t* object = nullptr;
	if (roomObject != nullptr && roomObject->id >= 0 && (u32)roomObject->id < main->pack.objects.length)
	{
		object = DynArrayGet(&main->pack.objects, GmsObject_t, (u32)roomObject->id);
		NotNull(object);
	}
	if (object != nullptr)
	{
		PrintLine_I("%sObj[%u]: \"%s\" (%d, %d) (0x%08X-0x%08X)", linePrefix, objIndex, object->name, roomObject->position.x, roomObject->position.y, roomObject->offset, roomObject->endOffset);
		PrintLine_D("%s  id:             %d", linePrefix, roomObject->id);
		PrintLine_D("%s  instanceId:     %d", linePrefix, roomObject->instanceId);
		PrintLine_D("%s  oldCodeId:      %d", linePrefix, roomObject->oldCodeId);
		PrintLine_D("%s  scale:          (%f, %f)", linePrefix, roomObject->scale.x, roomObject->scale.y);
		PrintLine_D("%s  imageSpeed:     %f", linePrefix, roomObject->imageSpeed);
		PrintLine_D("%s  imageFrame:     %u", linePrefix, roomObject->imageFrame);
		PrintLine_D("%s  color:          0x%08X", linePrefix, roomObject->color);
		PrintLine_D("%s  rotation:       %f", linePrefix, roomObject->rotation);
		PrintLine_D("%s  creationCodeId: %d", linePrefix, roomObject->creationCodeId);
	}
	else
	{
		PrintLine_W("%sObj[%u]: Invalid Object ID %d", linePrefix, objIndex, roomObject->id);
	}
}
