/*
File:   gms_deser.h
Author: Taylor Robbins
Date:   12\13\2020
*/

#ifndef _GMS_DESER_H
#define _GMS_DESER_H

struct GmsHeader_Chunk_t
{
	char type[4];
	u32 size;
};

START_PACK

struct GmsHeader_Gen8_t
{
	u8 isDebug;
	u8 unknown1[3]; //11 00 00
	u32 fileNameOffset;
	u32 configStrOffset;
	i32 nextInstanceId;
	u32 unknown3; //80 96 98 00
	u32 unknown4[5]; //00 00 00 00 00 00 00 00 00 00 00 00
	u32 nameOffset;
	u32 versionMajor, versionMinor, versionRelease, versionBuild;
	u32 defaultWindowWidth, defaultWindowHeight;
	u32 unknown5; //30 00 00 00
	u32 unknown6; //00 00 00 00
	u32 unknown7; //C3 4C 25 00
	u32 unknown8[3]; //00000000 00000000 00000000
	u64 timestamp;
	u32 displayNameOffset;
	u32 unknown11[3]; //00 00 00 00 00 00 00 00 00 00 00 00
	u32 unknown12; //00 20 00 00 (8192)
	u32 unknown13; //00 00 00 00
	u32 unknown14; //0x0000C844
	
	// u32 lastObj; // possibly offset, not used by my code atm
	// u32 lastTile; // idem
	// u32 gameID;
	// u32 unknown2[4]; // 0, 0, 0, 0
	
	// u32 infoFlags;
	// u8 licenseMD5[10]; // the file itself has no checksum or anything
	// u32 licenseCRC32;
	// u32 displayNameOffset; // to string
	// u32 activeTargets; // probably a flags value indicating for which platforms the file is built for no target flag values are known at the time of writing this
	// u32 unknown3[4]; // unknown function (0, 0x00000016, 0, 0xFFFA068C)
	// u32 steamAppID;
};

struct GmsFooter_Gen8_t
{
	u32 unknown18[10]; //CC4E9435 C3963974 E7E1D26F 1FC2B618 12EA5B07 0CDAF6E0 94214908 FA6B8A6F 3A8C965E 179BC870
	r32 targetFramerate;
	u32 unknown20; //01 00 00 00
	u32 unknown21[4]; //703EF2C2 4880684D 8A5CC77F 8DAE80C6
	u32 unknown22; //00 00 00 00
};

END_PACK

// 19*4 = 76 bytes (0x4C)
struct GmsHeader_Sprite_t
{
	u32 nameOffset;
	v2i size;
	i32 left, right, bottom, top;
	u32 unknown1; //00 00 00 00
	u32 isSeparateColMasks;
	v2i position;
	u32 boundingBoxMode;
	v2i origin;
	u32 unknown4; //FF FF FF FF
	u32 formatNum; //01 00 00 00 (Akurra: 02 00 00 00) (New GMS 2.3+: 03 00 00 00)
	u32 unknown6; //00 00 00 00
	r32 speed;
	u32 isSpeedGameFrames;
};

struct GmsHeader_Object_t
{
	u32 nameOffset;
	i32 spriteId;
	u32 isVisible;
	u32 isSolid;
	i32 depth;
	u32 isPersistent;
	i32 parentId;
	i32 maskId;
	u32 hasPhysics;
	u32 isSensor;
	u32 collisionShape;
	
	r32 density;
	r32 restitution;
	r32 group;
	r32 linearDamping;
	r32 angularDamping;
	r32 unknown1;
	r32 friction;
	r32 unknown2;
	r32 kinematic; //TODO: bool?
};

struct GmsHeader_Sound_t
{
	u32 nameOffset;
	u32 flags;
	u32 fileTypeOffset;
	u32 fileNameOffset;
	u32 unknown;
	r32 volume;
	r32 pitch;
	i32 groupId;
	i32 audioId;
};

typedef enum
{
	GmsFlag_Room_ViewportsEnabled         = 0x00000001,
	GmsFlag_Room_ClearViewportsBackground = 0x00000002,
	GmsFlag_Room_DontClearDisplayBuffer   = 0x00000004,
	GmsFlag_Room_Unknown1                 = 0x00000100,
	GmsFlag_Room_Unknown2                 = 0x00000200,
} GmsFlags_Room_t;

struct GmsHeader_Room_t
{
	u32 nameOffset;
	u32 captionOffset; //unused?
	v2i size;
	u32 unknown1; //00 00 00 00
	u32 isPersistent;
	u32 unknown2; //00 00 00 00
	u32 unknown3; //00 00 00 00
	i32 creationCodeId;
	u32 flags;
	u32 backgroundsOffset;
	u32 viewsOffset;
	u32 objectsOffset;
	u32 tilesOffset;
	u32 physicsEnabled;
	i32 unknown4[4]; //00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
	v2 gravity;
	r32 metersPerPixel;
	u32 layersOffset;
};
struct GmsHeader_RoomBackground_t
{
	u32 isEnabled;
	u32 isForeground;
	i32 id;
	v2i position;
	u32 isTileX;
	u32 isTileY;
	v2i speed;
	u32 isStretch;
};
struct GmsHeader_RoomView_t
{
	u32 isEnabled;
	v2i position;
	v2i size;
	v2i portPosition;
	v2i portSize;
	v2i border;
	v2i speed;
	i32 objectId;
};
struct GmsHeader_RoomObject_t
{
	v2i position;
	i32 id;
	i32 instanceId;
	i32 oldCodeId; //This seems to be old?
	v2  scale;
	r32 imageSpeed;
	u32 imageFrame;
	u32 color; //rgba (0xAABBGGRR)
	r32 rotation; //degrees
	i32 creationCodeId;
};
struct GmsHeader_RoomTile_t
{
	v2i position;
	i32 id;
	v2i sourcePosition;
	v2i size;
	u32 depth;
	i32 instanceId;
	v2  scale;
	u32 color;
};
typedef enum
{
	GmsEnum_LayerType_Background = 1,
	GmsEnum_LayerType_Instance   = 2,
	GmsEnum_LayerType_Asset      = 3,
	GmsEnum_LayerType_Tile       = 4,
} GmsEnum_LayerType_t;
struct GmsHeader_RoomLayer_t // 8*4 = 40 bytes (0x28)
{
	//Still need to determine: Horizontal Tile, Vertical Tile, Stretch
	u32 nameOffset;
	i32 id;
	u32 type;
	i32 depth;
	v2 offset;
	v2 speed;
	u32 unknown7; // 01 00 00 00  flags?
};
struct GmsFooter_RoomLayerTiles_t
{
	i32 tilesetId;
	v2i gridSize;
};
struct GmsHeader_RoomLayerBackground_t
{
	u32 unknown1;  //00 00 00 00 id?
	i32 spriteId;
	u32 tileX;
	u32 tileY;
	u32 stretch;
	u32 backgroundColor;
	u32 unknown5;  //00 00 00 00
	r32 animationSpeed;
	u32 isSpeedGameFrames;
};
struct GmsFooter_RoomLayerAssets_t
{
	u32 unknownListOffset0;
	u32 unknownListOffset1;
	u32 unknownListOffset2;
	u32 unknownListOffset3;
};

struct GmsHeader_String_t
{
	u32 length;
};

struct GmsHeader_Texture_t
{
	u32 padding;
	u32 unknown;
	u32 offset;
};

struct GmsHeader_Audio_t
{
	u32 size;
};

struct GmsHeader_Background_t
{
	u32 nameOffset;
	u32 unknown[3];
	u32 textureAddress;
};

struct GmsHeader_PathPoint_t
{
	v2 position;
	r32 speed;
};
struct GmsHeader_Path_t
{
	u32 nameOffset;
	u32 isSmooth;
	u32 isClosed;
	u32 precision;
};

struct GmsHeader_Script_t
{
	u32 nameOffset;
	i32 id;
};

struct GmsHeader_FontGlyph_t
{
	u16 codePoint;
	u16 x;
	u16 y;
	u16 width;
	u16 height;
	u16 shift;
	u32 offset;
};
struct GmsHeader_Font_t
{
	u32 nameOffset;
	u32 fileNameOffset;
	r32 pointSize;
	i32 isBold;
	i32 isItalic;
	u8 unknown1; //20
	u8 unknown2; //00
	u8 unknown3; //00
	u8 antiAliasMode;
	u32 defaultChar;
	u32 partOffset;
	v2 scale;
	u32 flags; //0x02 is "No-Scale" enabled
	
	// char charset;
	// i32 texturePageId;
	// u16 unknown;
};

struct GmsHeader_TexturePart_t
{
	u16 x, y, width, height;
	u16 drawOffsetX, drawOffsetY;
	u16 boundingX, boundingY, boundingWidth, boundingHeight;//NOTE: This seems like 2 size values rather than a proper box
	u16 textureId;
};

// 5*4 = 20 bytes (0x14)
struct GmsHeader_Code_t
{
	u32 nameOffset;
	u32 codeLength;
	i32 unknown1; //1
	i32 codeRelativeOffset;
	i32 unknown2; //0
};

// 5*4 = 20 bytes (0x14)
struct GmsHeader_Variable_t
{
	u32 nameOffset;
	i32 unknown0; //FFFFFFFF for regular vars but FFFFFFF9 for vars named "arguments"
	u32 id; //maybe? 1,2,3, and 0 have all been seen. 0 on "arguments" and "prototype"
	u32 numCodeReferences;
	i32 firstCodeReference;
};

// 3*4 = 12 bytes (0x0C)
struct GmsHeader_Function_t
{
	u32 nameOffset;
	u32 unknown0; //01 00 00 00, 02 00 00 00, 03 00 00 00, ...
	u32 firstCodeReference;
};

const char* GetLayerTypeStr(GmsEnum_LayerType_t type)
{
	switch (type)
	{
		case GmsEnum_LayerType_Background: return "Background";
		case GmsEnum_LayerType_Instance:   return "Instance";
		case GmsEnum_LayerType_Asset:      return "Asset";
		case GmsEnum_LayerType_Tile:       return "Tile";
		default: return "Unknown";
	}
}

#endif //  _GMS_DESER_H
