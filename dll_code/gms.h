/*
File:   gms.h
Author: Taylor Robbins
Date:   12\13\2020
*/

#ifndef _GMS_H
#define _GMS_H

struct GmsTexturePart_t
{
	u32 fileOffset;
	reci sourceRec;
	v2i drawOffset;
	reci boundingRec;
	u32 textureId;
};

struct GmsSpriteFrame_t
{
	u32 partOffset;
	GmsTexturePart_t* partPntr;
};
struct GmsSprite_t
{
	u32 offset;
	char* name;
	v2i size;
	reci bounds;
	u32 boundingBoxMode;
	bool isSeparateColMasks;
	v2i position;
	u32 unknown1;
	v2i origin;
	u32 unknown4;
	u32 formatNum;
	u32 unknown6;
	r32 speed;
	bool isSpeedGameFrames;
	DynArray_t frames;
};

struct GmsObject_t
{
	u32 offset;
	
	char* name;
	i32  spriteId;
	bool isVisible;
	bool isSolid;
	i32  depth;
	bool isPersistent;
	i32  parentId;
	i32  maskId;
	bool hasPhysics;
	bool isSensor;
	u32  collisionShape;
	
	r32  density;
	r32  restitution;
	r32  group;
	r32  linearDamping;
	r32  angularDamping;
	r32  unknown1;
	r32  friction;
	r32  unknown2;
	r32  kinematic;
};

struct GmsSound_t
{
	u32 offset;
	char* name;
	char* fileType;
	char* fileName;
	u32 flags;
	u32 unknown;
	r32 volume;
	r32 pitch;
	i32 groupId;
	i32 audioId;
};

struct GmsRoomBackground_t
{
	u32 offset;
	bool isEnabled;
	bool isForeground;
	i32 id;
	v2i position;
	bool isTileX;
	bool isTileY;
	v2i speed;
	bool isStretch;
};
struct GmsView_t
{
	u32 offset;
	bool isEnabled;
	v2i position;
	v2i size;
	v2i portPosition;
	v2i portSize;
	v2i border;
	v2i speed;
	i32 objectId;
};
struct GmsRoomObject_t
{
	u32 offset;
	u32 endOffset;
	v2i position;
	i32 id;
	i32 instanceId;
	i32 oldCodeId;
	v2 scale;
	r32 imageSpeed;
	u32 imageFrame;
	u32 color;
	r32 rotation;
	i32 creationCodeId;
};
struct GmsRoomTile_t
{
	u32 offset;
	v2i position;
	i32 id;
	v2i sourcePosition;
	v2i size;
	u32 depth;
	i32 instanceId;
	v2  scale;
	u32 color;
};
struct GmsRoomLayerBackground_t
{
	u32 unknown1;
	i32 spriteId;
	bool tileX;
	bool tileY;
	bool stretch;
	u32 backgroundColor;
	u32 unknown5;
	r32 animationSpeed;
	bool isSpeedGameFrames;
};
struct GmsRoomLayer_t
{
	u32 fileOffset;
	u32 endOffset;
	char* name;
	i32 id;
	GmsEnum_LayerType_t type;
	i32 depth;
	v2 offset;
	v2 speed;
	u32 unknown7;
	union
	{
		struct
		{
			DynArray_t backgrounds;
		};
		struct
		{
			DynArray_t instances;
		};
		struct
		{
			//TODO: These don't get filled right now. They are just placeholder
			DynArray_t assetList0;
			DynArray_t assetList1;
			DynArray_t assetList2;
			DynArray_t assetList3;
		};
		struct
		{
			i32 tilesetId;
			v2i gridSize;
			DynArray_t tiles;
			u32 tilesFileOffset;
			u32 tilesFileEndOffset;
		};
	};
};

struct GmsRoom_t
{
	u32 offset;
	u32 endOffset;
	bool deleted;
	
	char* name;
	char* caption;
	v2i size;
	bool isPersistent;
	i32 creationCodeId;
	u32 flags;
	bool physicsEnabled;
	v2 gravity;
	r32 metersPerPixel;
	u32 unknown1;
	u32 unknown2;
	u32 unknown3;
	u32 unknown4[4];
	
	u32 backgroundsOffset;
	DynArray_t backgrounds;
	u32 viewsOffset;
	DynArray_t views;
	u32 objectsOffset;
	DynArray_t objects;
	u32 tilesOffset;
	DynArray_t tiles;
	u32 layersOffset;
	DynArray_t layers;
	bool hasUnknownList;
	u32 unknownListLength;
	u32 unknownListOffset;
	u32 unknownListEndOffset;
};

struct GmsString_t
{
	u32 index;
	u32 offset;
	u32 charsOffset;
	u32 length;
	char* pntr;
	char* replacedValue;
};

struct GmsTexture_t
{
	u32 offset;
	u32 dataOffset;
	u32 size;
	u8* pngData;
};

struct GmsAudio_t
{
	u32 offset;
	u32 size;
	u8* wavData;
};

struct GmsBackground_t
{
	u32 offset;
	char* name;
	u32 unknown[3];
	u32 textureAddress;
};

struct GmsPathPoint_t
{
	u32 offset;
	v2 position;
	r32 speed;
};
struct GmsPath_t
{
	u32 offset;
	char* name;
	bool isSmooth;
	bool isClosed;
	u32 precision;
	DynArray_t points;
};

struct GmsScript_t
{
	u32 offset;
	char* name;
	i32 id;
};

struct GmsFontGlyph_t
{
	u32 fileOffset;
	u16 codePoint;
	reci drawRec;
	u16 shift;
	u32 offset;
};
struct GmsFont_t
{
	u32 offset;
	char* name;
	char* fileName;
	r32 pointSize;
	bool isBold;
	bool isItalic;
	u8 unknown1;
	u8 unknown2;
	u8 unknown3;
	u8 antiAliasMode;
	u32 defaultChar;
	u32 partOffset;
	GmsTexturePart_t* partPntr;
	v2 scale;
	u32 flags;
	DynArray_t glyphs;
};

struct GmsGenInfo_t
{
	u32 offset;
	bool filled;
	bool isDebug;
	u8 unknown1[3];
	char* fileName;
	char* configStr;
	i32 nextInstanceId;
	u32 unknown3;
	u32 unknown4[5];
	char* name;
	u32 versionMajor, versionMinor, versionRelease, versionBuild;
	u32 defaultWindowWidth, defaultWindowHeight;
	u32 unknown5;
	u32 unknown6;
	u32 unknown7;
	u32 unknown8[3];
	u64 timestamp;
	char* displayName;
	u32 unknown11[3];
	u32 unknown12;
	u32 unknown13;
	u32 unknown14;
	DynArray_t numbers;
};

struct GmsCode_t
{
	u32 offset;
	
	char* name;
	i32 unknown1;
	i32 codeRelativeOffset;
	i32 unknown2;
	
	u32 bytecodeOffset;
	u32 bytecodeLength; //bytes
	u8* bytecodePntr;
	
	DynArray_t variableIndices;
	DynArray_t functionIndices;
};

struct GmsVariable_t
{
	u32 offset;
	i32 codeIndex;
	
	char* name;
	i32 unknown0;
	u32 id;
	u32 numCodeReferences;
	i32 firstCodeReference;
	DynArray_t codeRefOffsets; //u32
};

struct GmsFunction_t
{
	u32 offset;
	i32 codeIndex;
	
	char* name;
	i32 unknown0;
	i32 firstCodeReference;
	DynArray_t codeRefOffsets; //u32
};

struct AkurraObjIds_t
{
	bool filled;
	bool foundAll;
	union
	{
		i32 values[79];
		struct
		{
			i32 gameControllerId;
			i32 musicControllerId;
			i32 ambianceParentId;
			i32 solidWallId;
			i32 blockerId;
			i32 pedestalId;
			i32 holeId;
			i32 boardedHoleId;
			i32 spikesId;
			i32 boxRegId;
			i32 boxBlueId;
			i32 stoneyBoyId;
			i32 starId;
			i32 obsidianId;
			i32 topazId;
			i32 keyId;
			i32 bigGreenKeyId;
			i32 bigYellowKeyId;
			i32 doorId;
			i32 largeDoorId;
			i32 lockStoneId;
			i32 lockWindId;
			i32 lockFireId;
			i32 lockWaterId;
			i32 bigGreenDoorId;
			i32 greenButtonId;
			i32 purpleButtonId;
			i32 orangeButtonId;
			i32 greenPillarUpId;
			i32 purplePillarUpId;
			i32 orangePillarUpId;
			i32 greenPillarDownId;
			i32 purplePillarDownId;
			i32 orangePillarDownId;
			i32 deepWaterId;
			i32 gongRed;
			i32 gongBlue;
			i32 gongYellow;
			i32 gongOrange;
			i32 gongRockRed;
			i32 gongRockBlue;
			i32 gongRockYellow;
			i32 gongRockOrange;
			i32 crystalUpId;
			i32 crystalDownId;
			i32 portalPurpleInId;
			i32 portalPurpleOutId;
			i32 portalGreenInId;
			i32 portalGreenOutId;
			i32 portalOrangeInId;
			i32 portalOrangeOutId;
			i32 portalBlueInId;
			i32 portalBlueOutId;
			i32 arrowRightId;
			i32 arrowDownId;
			i32 arrowLeftId;
			i32 arrowUpId;
			i32 arrowSecRightId;
			i32 arrowSecDownId;
			i32 arrowSecLeftId;
			i32 arrowSecUpId;
			// i32 arrowFuseRightId;
			// i32 arrowFuseDownId;
			// i32 arrowFuseLeftId;
			i32 arrowFuseUpId;
			i32 turtleId;
			i32 gameEndId;
			i32 starDoorId;
			i32 stairsId;
			i32 bearId;
			i32 caveShadowId;
			i32 secretAreaId;
			i32 caveSecretId;
			i32 darkSpawnerId;
			i32 musicNoteId;
			i32 enviroEffect1Id;
			i32 stoneGodId;
			i32 princessId;
			i32 roomTransitionId;
			i32 secretStairsId;
			i32 dropTopId;
			i32 dropBottomId;
		};
	};
};

struct AkurraCreateIds_t
{
	bool filled;
	bool foundAll;
	union
	{
		i32 values[10];
		struct
		{
			i32 itemCost3;
			i32 itemCost5;
			i32 itemCost7;
			i32 itemCost10;
			i32 itemCost15;
			i32 itemCost100;
			i32 facingDirectionUp;
			i32 facingDirectionDown;
			i32 facingDirectionLeft;
			i32 facingDirectionRight;
		};
	};
};

struct GmsPack_t
{
	bool filled;
	GmsGenInfo_t genInfo;
	
	i32 nextLayerId;
	i32 nextInstanceId;
	
	u32 roomChunkAddress;
	u32 roomChunkSize;
	
	DynArray_t sprites; //GmsSprite_t
	DynArray_t objects; //GmsObject_t
	DynArray_t sounds; //GmsSound_t
	DynArray_t rooms; //GmsRoom_t
	DynArray_t strings; //GmsString_t
	DynArray_t textures; //GmsTexture_t
	DynArray_t audios; //GmsAudio_t
	DynArray_t backgrounds; //GmsBackground_t
	DynArray_t paths; //GmsPath_t
	DynArray_t scripts; //GmsScript_t
	DynArray_t fonts; //GmsFont_t
	DynArray_t textureParts; //GmsTexturePart_t
	DynArray_t codes; //GmsCode_t
	DynArray_t variables; //GmsVariable_t
	DynArray_t functions; //GmsFunction_t
	
	AkurraObjIds_t akurraIds;
	AkurraCreateIds_t akurraCreateIds;
};

#endif //  _GMS_H
