/*
File:   gms_serialization.cpp
Author: Taylor Robbins
Date:   12\16\2020
Description: 
	** Holds some functions that help us turn parts of the GmsPack_t back into serialized data
	** and inject the new serializations back into the original file
*/

#define BIN_SER_TYPE(result, resultSize, byteIndex, type, value) do \
{                                                                   \
	if (result != nullptr)                                          \
	{                                                               \
		Assert(byteIndex + sizeof(type) <= resultSize);             \
		type* writePntr = (type*)&result[byteIndex];                \
		*writePntr = (value);                                       \
	}                                                               \
	byteIndex += sizeof(type);                                      \
} while(0)
#define BIN_SER_BYTES(result, resultSize, byteIndex, numBytes, pntrVarName) \
	u8* pntrVarName = nullptr;                                              \
	if (result != nullptr)                                                  \
	{                                                                       \
		Assert(byteIndex + numBytes <= resultSize);                         \
		pntrVarName = (u8*)&result[byteIndex];                              \
	}                                                                       \
	byteIndex += numBytes;                                                  \
	if (pntrVarName != nullptr)
#define BIN_SER_STRUCT(result, resultSize, byteIndex, structType, pntrVarName) \
	structType* pntrVarName = nullptr;                                         \
	if (result != nullptr)                                                     \
	{                                                                          \
		Assert(byteIndex + sizeof(structType) <= resultSize);                  \
		pntrVarName = (structType*)&result[byteIndex];                         \
	}                                                                          \
	byteIndex += sizeof(structType);                                           \
	if (pntrVarName != nullptr)

#define TEXT_SER_PRINTF(result, resultSize, charIndex, formatString, ...) do                                      \
{                                                                                                                 \
	if (result != nullptr)                                                                                        \
	{                                                                                                             \
		Assert(charIndex < resultSize);                                                                           \
		int snprintfResult = snprintf(&result[charIndex], resultSize+1 - charIndex, formatString, ##__VA_ARGS__); \
		Assert(snprintfResult >= 0 && (u32)snprintfResult <= resultSize - charIndex);                             \
		charIndex += (u32)snprintfResult;                                                                         \
	}                                                                                                             \
	else                                                                                                          \
	{                                                                                                             \
		int snprintfResult = snprintf(nullptr, 0, formatString, ##__VA_ARGS__);                                   \
		Assert(snprintfResult >= 0);                                                                              \
		charIndex += (u32)snprintfResult;                                                                         \
	}                                                                                                             \
} while(0)

u8* SerializeGmsRoom(MemoryArena_t* memArena, GmsPack_t* pack, GmsRoom_t* room, u32 beginAddress, u32* numBytesOut = nullptr)
{
	NotNull(pack);
	NotNull(room);
	Assert(room->caption == nullptr);
	
	GmsString_t* roomNameGmsStr = GetGmsStringInPack(pack, room->name, true);
	if (roomNameGmsStr == nullptr)
	{
		WriteLine_E("We do not support injecting custom room names into the the GMS pack file yet. Please use an existing name as the name of your level.");
		PrintLine_E("Requested name: \"%s\"", room->name);
		return nullptr;
	}
	u32 roomNameOffset = roomNameGmsStr->charsOffset;
	
	u32 resultSize = 0;
	u8* result = nullptr;
	for (u32 pass = 0; pass < 2; pass++)
	{
		u32 byteIndex = 0;
		
		// +==============================+
		// |    Serialize Room Header     |
		// +==============================+
		u32* backgroundsOffsetPntr = nullptr;
		u32* viewsOffsetPntr = nullptr;
		u32* objectsOffsetPntr = nullptr;
		u32* tilesOffsetPntr = nullptr;
		u32* layersOffsetPntr = nullptr;
		BIN_SER_STRUCT(result, resultSize, byteIndex, GmsHeader_Room_t, roomHeader)
		{
			roomHeader->nameOffset        = roomNameOffset;
			roomHeader->captionOffset     = 0;
			roomHeader->size              = room->size;
			roomHeader->unknown1          = room->unknown1;
			roomHeader->isPersistent      = (u32)(room->isPersistent ? 0x00000001 : 0x00000000);
			roomHeader->unknown2          = room->unknown2;
			roomHeader->unknown3          = room->unknown3;
			roomHeader->creationCodeId    = room->creationCodeId;
			roomHeader->flags             = room->flags;
			roomHeader->backgroundsOffset = 0; backgroundsOffsetPntr = &roomHeader->backgroundsOffset;
			roomHeader->viewsOffset       = 0; viewsOffsetPntr       = &roomHeader->viewsOffset;
			roomHeader->objectsOffset     = 0; objectsOffsetPntr     = &roomHeader->objectsOffset;
			roomHeader->tilesOffset       = 0; tilesOffsetPntr       = &roomHeader->tilesOffset;
			roomHeader->physicsEnabled    = (u32)(room->physicsEnabled ? 0x00000001 : 0x00000000);
			roomHeader->unknown4[0]       = room->unknown4[0];
			roomHeader->unknown4[1]       = room->unknown4[1];
			roomHeader->unknown4[2]       = room->unknown4[2];
			roomHeader->unknown4[3]       = room->unknown4[3];
			roomHeader->gravity           = room->gravity;
			roomHeader->metersPerPixel    = room->metersPerPixel;
			roomHeader->layersOffset      = 0; layersOffsetPntr = &roomHeader->layersOffset;
		}
		
		u32* uknownsOffsetPntr = nullptr;
		if (room->hasUnknownList)
		{
			Assert(room->unknownListLength == 0);
			if (result != nullptr) { uknownsOffsetPntr = (u32*)&result[byteIndex]; }
			BIN_SER_TYPE(result, resultSize, byteIndex, u32, 0);
		}
		
		// +==============================+
		// |    Serialize Backgrounds     |
		// +==============================+
		if (backgroundsOffsetPntr != nullptr) { *backgroundsOffsetPntr = beginAddress + byteIndex; }
		BIN_SER_TYPE(result, resultSize, byteIndex, u32, room->backgrounds.length);
		u32* backgroundOffsetsListPntr = nullptr;
		if (result != nullptr) { backgroundOffsetsListPntr = (u32*)&result[byteIndex]; }
		for (u32 bIndex = 0; bIndex < room->backgrounds.length; bIndex++)
		{
			BIN_SER_TYPE(result, resultSize, byteIndex, u32, 0); //will be filled in below using backgroundOffsetsListPntr
		}
		for (u32 bIndex = 0; bIndex < room->backgrounds.length; bIndex++)
		{
			GmsRoomBackground_t* background = DynArrayGet(&room->backgrounds, GmsRoomBackground_t, bIndex);
			NotNull(background);
			if (backgroundOffsetsListPntr != nullptr) { backgroundOffsetsListPntr[bIndex] = beginAddress + byteIndex; }
			BIN_SER_STRUCT(result, resultSize, byteIndex, GmsHeader_RoomBackground_t, backgroundHeader)
			{
				backgroundHeader->isEnabled    = (u32)(background->isEnabled ? 0x00000001 : 0x00000000);
				backgroundHeader->isForeground = (u32)(background->isForeground ? 0x00000001 : 0x00000000);
				backgroundHeader->id           = background->id;
				backgroundHeader->position     = background->position;
				backgroundHeader->isTileX      = (u32)(background->isTileX ? 0x00000001 : 0x00000000);
				backgroundHeader->isTileY      = (u32)(background->isTileY ? 0x00000001 : 0x00000000);
				backgroundHeader->speed        = background->speed;
				backgroundHeader->isStretch    = (u32)(background->isStretch ? 0x00000001 : 0x00000000);
			}
		}
		
		// +==============================+
		// |       Serialize Views        |
		// +==============================+
		if (viewsOffsetPntr != nullptr) { *viewsOffsetPntr = beginAddress + byteIndex; }
		BIN_SER_TYPE(result, resultSize, byteIndex, u32, room->views.length);
		u32* viewOffsetsListPntr = nullptr;
		if (result != nullptr) { viewOffsetsListPntr = (u32*)&result[byteIndex]; }
		for (u32 vIndex = 0; vIndex < room->views.length; vIndex++)
		{
			BIN_SER_TYPE(result, resultSize, byteIndex, u32, 0); //will be filled in below using viewOffsetsListPntr
		}
		for (u32 vIndex = 0; vIndex < room->views.length; vIndex++)
		{
			GmsView_t* view = DynArrayGet(&room->views, GmsView_t, vIndex);
			NotNull(view);
			if (viewOffsetsListPntr != nullptr) { viewOffsetsListPntr[vIndex] = beginAddress + byteIndex; }
			BIN_SER_STRUCT(result, resultSize, byteIndex, GmsHeader_RoomView_t, viewHeader)
			{
				viewHeader->isEnabled    = (u32)(view->isEnabled ? 0x00000001 : 0x00000000);
				viewHeader->position     = view->position;
				viewHeader->size         = view->size;
				viewHeader->portPosition = view->portPosition;
				viewHeader->portSize     = view->portSize;
				viewHeader->border       = view->border;
				viewHeader->speed        = view->speed;
				viewHeader->objectId     = view->objectId;
			}
		}
		
		// +==============================+
		// |      Serialize Objects       |
		// +==============================+
		if (objectsOffsetPntr != nullptr) { *objectsOffsetPntr = beginAddress + byteIndex; }
		BIN_SER_TYPE(result, resultSize, byteIndex, u32, room->objects.length);
		u32* objectOffsetsListPntr = nullptr;
		if (result != nullptr) { objectOffsetsListPntr = (u32*)&result[byteIndex]; }
		for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
		{
			BIN_SER_TYPE(result, resultSize, byteIndex, u32, 0); //will be filled in below using objectOffsetsListPntr
		}
		for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
		{
			GmsRoomObject_t* object = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex);
			NotNull(object);
			if (objectOffsetsListPntr != nullptr) { objectOffsetsListPntr[oIndex] = beginAddress + byteIndex; }
			BIN_SER_STRUCT(result, resultSize, byteIndex, GmsHeader_RoomObject_t, objectHeader)
			{
				objectHeader->position       = object->position;
				objectHeader->id             = object->id;
				objectHeader->instanceId     = object->instanceId;
				objectHeader->oldCodeId      = object->oldCodeId;
				objectHeader->scale          = object->scale;
				objectHeader->imageSpeed     = object->imageSpeed;
				objectHeader->imageFrame     = object->imageFrame;
				objectHeader->color          = object->color;
				objectHeader->rotation       = object->rotation;
				objectHeader->creationCodeId = object->creationCodeId;
			}
		}
		
		// +==============================+
		// |       Serialize Tiles        |
		// +==============================+
		if (tilesOffsetPntr != nullptr) { *tilesOffsetPntr = beginAddress + byteIndex; }
		BIN_SER_TYPE(result, resultSize, byteIndex, u32, room->tiles.length);
		u32* tileOffsetsListPntr = nullptr;
		if (result != nullptr) { tileOffsetsListPntr = (u32*)&result[byteIndex]; }
		for (u32 tIndex = 0; tIndex < room->tiles.length; tIndex++)
		{
			BIN_SER_TYPE(result, resultSize, byteIndex, u32, 0); //will be filled in below using tileOffsetsListPntr
		}
		for (u32 tIndex = 0; tIndex < room->tiles.length; tIndex++)
		{
			GmsRoomTile_t* tile = DynArrayGet(&room->tiles, GmsRoomTile_t, tIndex);
			NotNull(tile);
			if (tileOffsetsListPntr != nullptr) { tileOffsetsListPntr[tIndex] = beginAddress + byteIndex; }
			BIN_SER_STRUCT(result, resultSize, byteIndex, GmsHeader_RoomTile_t, tileHeader)
			{
				tileHeader->position       = tile->position;
				tileHeader->id             = tile->id;
				tileHeader->sourcePosition = tile->sourcePosition;
				tileHeader->size           = tile->size;
				tileHeader->depth          = tile->depth;
				tileHeader->instanceId     = tile->instanceId;
				tileHeader->scale          = tile->scale;
				tileHeader->color          = tile->color;
			}
		}
		
		// +==============================+
		// |       Serialize Layers       |
		// +==============================+
		if (layersOffsetPntr != nullptr) { *layersOffsetPntr = beginAddress + byteIndex; }
		BIN_SER_TYPE(result, resultSize, byteIndex, u32, room->layers.length);
		u32* layerOffsetsListPntr = nullptr;
		if (result != nullptr) { layerOffsetsListPntr = (u32*)&result[byteIndex]; }
		for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
		{
			BIN_SER_TYPE(result, resultSize, byteIndex, u32, 0); //will be filled in below using layerOffsetsListPntr
		}
		for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
		{
			GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
			NotNull(layer);
			
			GmsString_t* layerNameGmsStr = GetGmsStringInPack(pack, layer->name, true);
			NotNull(layerNameGmsStr);
			
			if (layerOffsetsListPntr != nullptr) { layerOffsetsListPntr[lIndex] = beginAddress + byteIndex; }
			BIN_SER_STRUCT(result, resultSize, byteIndex, GmsHeader_RoomLayer_t, layerHeader)
			{
				layerHeader->nameOffset = layerNameGmsStr->charsOffset;
				layerHeader->id         = layer->id;
				layerHeader->type       = layer->type;
				layerHeader->depth      = layer->depth;
				layerHeader->offset     = layer->offset;
				layerHeader->speed      = layer->speed;
				layerHeader->unknown7   = layer->unknown7;
			}
			
			if (layer->type == GmsEnum_LayerType_Background)
			{
				BIN_SER_TYPE(result, resultSize, byteIndex, u32, layer->backgrounds.length);
				for (u32 bIndex = 0; bIndex < layer->backgrounds.length; bIndex++)
				{
					GmsRoomLayerBackground_t* background = DynArrayGet(&layer->backgrounds, GmsRoomLayerBackground_t, bIndex);
					NotNull(background);
					BIN_SER_STRUCT(result, resultSize, byteIndex, GmsHeader_RoomLayerBackground_t, backgroundHeader)
					{
						backgroundHeader->unknown1          = background->unknown1;
						backgroundHeader->spriteId          = background->spriteId;
						backgroundHeader->tileX             = background->tileX;
						backgroundHeader->tileY             = background->tileY;
						backgroundHeader->stretch           = (u32)(background->stretch ? 0x00000001 : 0x00000000);
						backgroundHeader->backgroundColor   = background->backgroundColor;
						backgroundHeader->unknown5          = background->unknown5;
						backgroundHeader->animationSpeed    = background->animationSpeed;
						backgroundHeader->isSpeedGameFrames = (u32)(background->isSpeedGameFrames ? 0x00000001 : 0x00000000);
					}
				}
			}
			else if (layer->type == GmsEnum_LayerType_Instance)
			{
				BIN_SER_TYPE(result, resultSize, byteIndex, u32, layer->instances.length);
				for (u32 iIndex = 0; iIndex < layer->instances.length; iIndex++)
				{
					u32* instanceIdPntr = DynArrayGet(&layer->instances, u32, iIndex);
					NotNull(instanceIdPntr);
					u32 instanceId = *instanceIdPntr;
					BIN_SER_TYPE(result, resultSize, byteIndex, u32, instanceId);
				}
			}
			else if (layer->type == GmsEnum_LayerType_Asset)
			{
				//TODO: We don't really handle the asset layer format. We just handle an empty one since that's our only use case right now
				Assert(layer->assetList0.length == 0);
				Assert(layer->assetList1.length == 0);
				Assert(layer->assetList2.length == 0);
				Assert(layer->assetList3.length == 0);
				BIN_SER_STRUCT(result, resultSize, byteIndex, GmsFooter_RoomLayerAssets_t, layerFooter)
				{
					//These will get filled by the if statements below
					layerFooter->unknownListOffset0 = 0x00000000;
					layerFooter->unknownListOffset1 = 0x00000000;
					layerFooter->unknownListOffset2 = 0x00000000;
					layerFooter->unknownListOffset3 = 0x00000000;
				}
				if (layerFooter != nullptr) { layerFooter->unknownListOffset0 = (beginAddress + byteIndex); }
				BIN_SER_TYPE(result, resultSize, byteIndex, u32, 0x00000000);
				if (layerFooter != nullptr) { layerFooter->unknownListOffset1 = (beginAddress + byteIndex); }
				BIN_SER_TYPE(result, resultSize, byteIndex, u32, 0x00000000);
				if (layerFooter != nullptr) { layerFooter->unknownListOffset2 = (beginAddress + byteIndex); }
				BIN_SER_TYPE(result, resultSize, byteIndex, u32, 0x00000000);
				if (layerFooter != nullptr) { layerFooter->unknownListOffset3 = (beginAddress + byteIndex); }
				BIN_SER_TYPE(result, resultSize, byteIndex, u32, 0x00000000);
			}
			else if (layer->type == GmsEnum_LayerType_Tile)
			{
				Assert(layer->gridSize.width >= 0);
				Assert(layer->gridSize.height >= 0);
				Assert(layer->tiles.length == (u32)(layer->gridSize.width * layer->gridSize.height));
				BIN_SER_STRUCT(result, resultSize, byteIndex, GmsFooter_RoomLayerTiles_t, layerFooter)
				{
					layerFooter->tilesetId = layer->tilesetId;
					layerFooter->gridSize = layer->gridSize;
				}
				for (u32 yOffset = 0; yOffset < (u32)layer->gridSize.height; yOffset++)
				{
					for (u32 xOffset = 0; xOffset < (u32)layer->gridSize.width; xOffset++)
					{
						u32* tileIdPntr = DynArrayGet(&layer->tiles, u32, (yOffset * (u32)layer->gridSize.width) + xOffset);
						NotNull(tileIdPntr);
						u32 tileId = *tileIdPntr;
						BIN_SER_TYPE(result, resultSize, byteIndex, u32, tileId);
					}
				}
			}
		}
		
		// +==============================+
		// |   Serialize Unknown Items    |
		// +==============================+
		if (room->hasUnknownList)
		{
			if (uknownsOffsetPntr != nullptr) { *uknownsOffsetPntr = beginAddress + byteIndex; }
			BIN_SER_TYPE(result, resultSize, byteIndex, u32, room->unknownListLength);
			Assert(room->unknownListLength == 0);
		}
		
		if (pass == 0)
		{
			if (numBytesOut != nullptr) { *numBytesOut = byteIndex; }
			if (memArena == nullptr) { return nullptr; }
			Assert(byteIndex > 0);
			resultSize = byteIndex;
			result = PushArray(memArena, u8, resultSize);
			NotNull(result);
		}
		else
		{
			Assert(byteIndex == resultSize);
		}
	}
	return result;
}

u8* SerializeGmsRoomsChunk(MemoryArena_t* memArena, GmsPack_t* pack, DynArray_t* rooms, u32 beginAddress, u32 chunkFixSize, u32* numBytesOut = nullptr)
{
	NotNull(pack);
	NotNull(rooms);
	
	u32 resultSize = 0;
	u8* result = nullptr;
	
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 byteIndex = 0;
		
		BIN_SER_STRUCT(result, resultSize, byteIndex, GmsHeader_Chunk_t, chunkHeader)
		{
			chunkHeader->type[0] = 'R';
			chunkHeader->type[1] = 'O';
			chunkHeader->type[2] = 'O';
			chunkHeader->type[3] = 'M';
			Assert(chunkFixSize >= sizeof(GmsHeader_Chunk_t));
			chunkHeader->size = chunkFixSize - sizeof(GmsHeader_Chunk_t);
		}
		
		BIN_SER_TYPE(result, resultSize, byteIndex, u32, rooms->length);
		
		u32* roomPntrListStart = (result != nullptr) ? (u32*)&result[byteIndex] : nullptr;
		for (u32 rIndex = 0; rIndex < rooms->length; rIndex++)
		{
			BIN_SER_TYPE(result, resultSize, byteIndex, u32, 0); //Temporary 0 value, will be replaced later using roomPntrListStart
		}
		
		for (u32 rIndex = 0; rIndex < rooms->length; rIndex++)
		{
			GmsRoom_t* room = DynArrayGet(rooms, GmsRoom_t, rIndex);
			NotNull(room);
			if (roomPntrListStart != nullptr) { roomPntrListStart[rIndex] = beginAddress + byteIndex; }
			
			u32 serializedRoomSize = 0;
			u8* serializedRoom = SerializeGmsRoom(nullptr, pack, room, beginAddress + byteIndex, &serializedRoomSize);
			if (serializedRoomSize == 0)
			{
				PrintLine_E("Failed to serialize room %u/%u. Cannot serialize ROOM chunk", rIndex+1, rooms->length);
				return nullptr;
			}
			if (result != nullptr && serializedRoomSize > 0)
			{
				Assert(byteIndex + serializedRoomSize <= resultSize);
				MemoryArena_t fakeArena;
				InitBufferArena(&fakeArena, &result[byteIndex], serializedRoomSize, true);
				SerializeGmsRoom(&fakeArena, pack, room, beginAddress + byteIndex);
			}
			byteIndex += serializedRoomSize;
		}
		
		if (pass == 0)
		{
			if (numBytesOut != nullptr) { *numBytesOut = byteIndex; }
			if (memArena == nullptr) { return nullptr; }
			Assert(byteIndex > 0);
			resultSize = byteIndex;
			result = PushArray(memArena, u8, resultSize);
			NotNull(result);
		}
		else
		{
			Assert(byteIndex == resultSize);
		}
	}
	
	return result;
}

GmsVariable_t* GetVariableForCodeWord(GmsPack_t* pack, GmsCode_t* code, u32 wordAddress, u32* varIndexOut = nullptr)
{
	NotNull(pack);
	NotNull(code);
	for (u32 vIndex = 0; vIndex < code->variableIndices.length; vIndex++)
	{
		u32* varIndexPntr = DynArrayGet(&code->variableIndices, u32, vIndex);
		NotNull(varIndexPntr);
		u32 varIndex = *varIndexPntr;
		Assert(varIndex < pack->variables.length);
		GmsVariable_t* variable = DynArrayGet(&pack->variables, GmsVariable_t, varIndex);
		NotNull(variable);
		for (u32 oIndex = 0; oIndex < variable->codeRefOffsets.length; oIndex++)
		{
			u32* refAddressPntr = DynArrayGet(&variable->codeRefOffsets, u32, oIndex);
			NotNull(refAddressPntr);
			if (*refAddressPntr == wordAddress)
			{
				if (varIndexOut != nullptr) { *varIndexOut = varIndex; }
				return variable;
			}
		}
	}
	return nullptr;
}
GmsFunction_t* GetFunctionForCodeWord(GmsPack_t* pack, GmsCode_t* code, u32 wordAddress, u32* funcIndexOut = nullptr)
{
	NotNull(pack);
	NotNull(code);
	for (u32 fIndex = 0; fIndex < code->functionIndices.length; fIndex++)
	{
		u32* funcIndexPntr = DynArrayGet(&code->functionIndices, u32, fIndex);
		NotNull(funcIndexPntr);
		u32 funcIndex = *funcIndexPntr;
		Assert(funcIndex < pack->functions.length);
		GmsFunction_t* function = DynArrayGet(&pack->functions, GmsFunction_t, funcIndex);
		NotNull(function);
		for (u32 oIndex = 0; oIndex < function->codeRefOffsets.length; oIndex++)
		{
			u32* refAddressPntr = DynArrayGet(&function->codeRefOffsets, u32, oIndex);
			NotNull(refAddressPntr);
			if (*refAddressPntr == wordAddress)
			{
				if (funcIndexOut != nullptr) { *funcIndexOut = funcIndex; }
				return function;
			}
		}
	}
	return nullptr;
}

char* SerializeBytecode(MemoryArena_t* memArena, GmsPack_t* pack, u32 codeIndex, bool showAddresses, bool showInterpretations, bool showVariables, bool showDecompiles, u32* numCharsOut = nullptr)
{
	NotNull(pack);
	Assert(pack->filled);
	
	GmsCode_t* code = DynArrayGet(&pack->codes, GmsCode_t, codeIndex);
	NotNull(code);
	
	u32 resultSize = 0;
	char* result = nullptr;
	
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 charIndex = 0;
		
		Assert(code->bytecodePntr != nullptr || code->bytecodeLength == 0);
		bool nextBytecodeIsValue = false;
		bool nextBytecodeIsStringIndex = false;
		bool nextIsFunctionId = false;
		GmsFunction_t* nextFunctionPntr = nullptr;
		u32 nextFunctionIndex = 0;
		bool nextIsValue = false;
		bool nextNextIsValue = false;
		u8 nextValueType = 0x00;
		u32* words = (u32*)code->bytecodePntr;
		u32 numWords = code->bytecodeLength/4;
		for (u32 wordIndex = 0; wordIndex < numWords; wordIndex++)
		{
			if (wordIndex > 0) { TEXT_SER_PRINTF(result, resultSize, charIndex, "\n"); }
			
			u32 address = code->bytecodeOffset + wordIndex*sizeof(u32);
			u32 word = words[wordIndex];
			u8 opCode = GetOpCode(word);
			u32 nextWord = (wordIndex+1 < numWords) ? words[wordIndex+1] : 0x00000000;
			u8 nextOpCode = GetOpCode(nextWord);
			u32 nextNextWord = (wordIndex+2 < numWords) ? words[wordIndex+2] : 0x00000000;
			u8 nextNextOpCode = GetOpCode(nextNextWord);
			
			if (showDecompiles)
			{
				#if 0
				if (wordIndex + sizeof(u32)*3 <= code->bytecodeLength) //3 words sequences
				{
					u32 words[3];
					u16 uppers[3];
					u16 lowers[3];
					for (u32 bIndex = 0; bIndex < 3; bIndex++)
					{
						words[bIndex] = *(u32*)(code->bytecodePntr + byteIndex + bIndex*sizeof(u32));
						uppers[bIndex] = (u16)((words[bIndex] & 0xFFFF0000) >> 16);
						lowers[bIndex] = (u16)((words[bIndex] & 0x0000FFFF) >> 0);
					}
					if (uppers[0] == 0x840F && uppers[1] == 0x4525 && (uppers[2] & 0xFFF0) == 0xA000) //Push, IntVar, Addr
					{
						u16 value = lowers[0];
						u32 addrValue = (words[2] & 0x000FFFFF);
						GmsVariable_t* variable = GetVariableForCodeWord(pack, code, address + sizeof(u32));
						if (variable != nullptr)
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, "%s = %d; //%05X", variable->name, *(i16*)&value, addrValue);
						}
						else
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, "[%04X] = %d;", addrValue, *(i16*)&value);
						}
						byteIndex += 2 * sizeof(u32);
						continue;
					}
				}
				#endif
				
				//Decompile straight assignments like "my_var = 100;"
				if (opCode == GmlOpCode_Push16 || opCode == GmlOpCode_Push)
				{
					u8 pushType = (opCode == GmlOpCode_Push16) ? GmlDataType_Int16 : GetUpperByte(word);
					u32 popWord = 0;
					u32 pushSize = 0;
					bool isPastEnd = false;
					if (pushType == GmlDataType_Int16)
					{
						isPastEnd = ((wordIndex+1) >= numWords);
						popWord = nextWord;
						pushSize = 1;
					}
					else if (pushType == GmlDataType_Int64 || pushType == GmlDataType_Double)
					{
						isPastEnd = ((wordIndex+3) >= numWords);
						popWord = (!isPastEnd) ? words[wordIndex+3] : 0x00000000;
						pushSize = 3;
					}
					else
					{
						isPastEnd = ((wordIndex+2) >= numWords);
						popWord = nextNextWord;
						pushSize = 2;
					}
					if (!isPastEnd && GetOpCode(popWord) == GmlOpCode_Pop && wordIndex + pushSize + 2 <= numWords)
					{
						u32 popAddress = address + pushSize*sizeof(u32);
						i16 instanceValue = GetLowerI16(popWord); //TODO: Handle this by prefixing with "global." or "local." or whatever
						u8 popType1 = ((GetUpperByte(popWord) & 0xF0) >> 4);
						u8 popType2 = ((GetUpperByte(popWord) & 0x0F) >> 0);
						u32 popTarget = words[wordIndex + pushSize + 1];
						GmsVariable_t* variable = GetVariableForCodeWord(pack, code, popAddress);
						if (variable != nullptr)
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, "%s = ", variable->name);
						}
						else
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, "[%08X] = ", popTarget);
						}
						r64 valueR64;
						r32 valueR32;
						i32 valueI32;
						i64 valueI64;
						bool valueBool;
						u32 varIndex;
						u32 strIndex;
						i32 valueInstance;
						i16 valueI16;
						switch (pushType)
						{
							case GmlDataType_Double:   valueR64  = GetR64FromWords(nextWord, nextNextWord); TEXT_SER_PRINTF(result, resultSize, charIndex, "%lf", valueR64); break;
							case GmlDataType_Float:    valueR32  = GetR32FromU32(nextWord);                 TEXT_SER_PRINTF(result, resultSize, charIndex, "%f", valueR32); break;
							case GmlDataType_Int32:    valueI32  = GetI32FromU32(nextWord);                 TEXT_SER_PRINTF(result, resultSize, charIndex, "%d", valueI32); break;
							case GmlDataType_Int64:    valueI64  = GetI64FromWords(nextWord, nextNextWord); TEXT_SER_PRINTF(result, resultSize, charIndex, "%lld", valueI64); break;
							case GmlDataType_Boolean:  valueBool = (nextWord != 0);                         TEXT_SER_PRINTF(result, resultSize, charIndex, "%s", valueBool ? "true" : "false"); break;
							case GmlDataType_Variable: varIndex  = nextWord;                                TEXT_SER_PRINTF(result, resultSize, charIndex, "var[%u]", varIndex); break; //TODO: Look up names
							case GmlDataType_String:
							{
								strIndex  = nextWord;
								if (strIndex < pack->strings.length)
								{
									GmsString_t* gmsString = DynArrayGet(&pack->strings, GmsString_t, strIndex);
									NotNull(gmsString);
									TEXT_SER_PRINTF(result, resultSize, charIndex, "\"%s\"", gmsString->pntr);
								}
								else
								{
									TEXT_SER_PRINTF(result, resultSize, charIndex, "str[%u]", strIndex);
								}
							} break;
							case GmlDataType_Instance:
							{
								valueInstance = GetI32FromU32(nextWord);
								if (IsKnownGmlInstanceValue(valueInstance))
								{
									TEXT_SER_PRINTF(result, resultSize, charIndex, "%s", GetGmlInstanceValueStr(valueInstance));
								}
								else
								{
									TEXT_SER_PRINTF(result, resultSize, charIndex, "instance[%d]", valueInstance);
								}
							} break;
							case GmlDataType_Int16: valueI16 = GetLowerI16(word); TEXT_SER_PRINTF(result, resultSize, charIndex, "%d", valueI16); break;
						}
						if (variable != nullptr)
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, "; //%08X", popTarget);
						}
						else
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, ";");
						}
						wordIndex += (pushSize + 2) - 1; //-1 is to account for wordIndex++ in loop, + 2 is for Call instruction + Value argument
						continue;
					}
				}
				if (opCode == GmlOpCode_Call)
				{
					u8 dataType = GetUpperByte(word);
					u16 numArguments = GetLowerU16(word);
					u32 functionIndex = 0;
					GmsFunction_t* function = GetFunctionForCodeWord(pack, code, address, &functionIndex);
					if (function != nullptr)
					{
						bool discardReturn = false;
						if (GetOpCode(nextNextWord) == GmlOpCode_Popz)
						{
							discardReturn = true;
						}
						
						if (discardReturn)
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, "%s(", function->name);
						}
						else
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, "%s %s(", GetGmlDataTypeStr(dataType), function->name);
						}
						for (u32 argIndex = 0; argIndex < numArguments; argIndex++)
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, "%sarg%u", (argIndex > 0) ? ", " : "", argIndex);
						}
						TEXT_SER_PRINTF(result, resultSize, charIndex, ");");
						
						wordIndex++;
						if (discardReturn) { wordIndex++; }
						continue;
					}
				}
			}
			if (showAddresses)
			{
				if (nextIsValue || nextNextIsValue || nextIsFunctionId)
				{
					TEXT_SER_PRINTF(result, resultSize, charIndex, "          : ");
				}
				else
				{
					TEXT_SER_PRINTF(result, resultSize, charIndex, "0x%08X: ", address);
				}
			}
			TEXT_SER_PRINTF(result, resultSize, charIndex, "%04X %04X", GetUpperU16(word), GetLowerU16(word));
			if (showInterpretations)
			{
				#if 1
				if (nextIsFunctionId)
				{
					if (nextFunctionPntr != nullptr)
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": Func(%s)", nextFunctionPntr->name);
					}
					else if (word < pack->strings.length) //TODO: Is this really right?
					{
						GmsString_t* string = DynArrayGet(&pack->strings, GmsString_t, word);
						NotNull(string);
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": Func(%s)", string->pntr);
					}
					else
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": Func(%08X)", word);
					}
					nextIsFunctionId = false;
				}
				else if (nextIsValue || nextNextIsValue)
				{
					bool isSecondWord = !nextIsValue;
					if (nextValueType == GmlDataType_Double)
					{
						if (!isSecondWord)
						{
							r64 value = GetR64FromWords(word, nextWord);
							TEXT_SER_PRINTF(result, resultSize, charIndex, ": r64(%lf)", value);
						}
						else
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, ": ^ ^ ^");
						}
					}
					else if (nextValueType == GmlDataType_Float)
					{
						r32 value = GetR32FromU32(word);
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": r32(%f)", value);
					}
					else if (nextValueType == GmlDataType_Int32)
					{
						i32 value = GetI32FromU32(word);
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": i32(%d)", value);
					}
					else if (nextValueType == GmlDataType_Int64)
					{
						if (!isSecondWord)
						{
							i64 value = GetI64FromWords(word, nextWord);
							TEXT_SER_PRINTF(result, resultSize, charIndex, ": i64(%lld)", value);
						}
						else
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, ": ^ ^ ^");
						}
					}
					else if (nextValueType == GmlDataType_Boolean)
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": bool(%s)", (word != 0) ? "True" : "False");
					}
					else if (nextValueType == GmlDataType_Variable)
					{
						u32 variableIndex = GetNonOpCode(word);
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": Variable(%u or %06X)", variableIndex, variableIndex);
					}
					else if (nextValueType == GmlDataType_String)
					{
						if (word < pack->strings.length)
						{
							GmsString_t* string = DynArrayGet(&pack->strings, GmsString_t, word);
							NotNull(string);
							TEXT_SER_PRINTF(result, resultSize, charIndex, ": \"%s\"", string->pntr);
						}
						else
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, ": str[%u]", word);
						}
					}
					else if (nextValueType == GmlDataType_Instance)
					{
						i32 value = GetI32FromU32(word);
						if (value > 0)
						{
							if ((u32)value < pack->objects.length)
							{
								GmsObject_t* object = DynArrayGet(&pack->objects, GmsObject_t, (u32)value);
								NotNull(object);
								TEXT_SER_PRINTF(result, resultSize, charIndex, ": Object(%s)", object->name);
							}
							else
							{
								TEXT_SER_PRINTF(result, resultSize, charIndex, ": Object[%d]", value);
							}
						}
						else if (IsKnownGmlInstanceValue(value))
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, ": Instance(%s)", GetGmlInstanceValueStr(value));
						}
						else
						{
							TEXT_SER_PRINTF(result, resultSize, charIndex, ": Instance(%08X)", value);
						}
					}
					// else if (nextValueType == GmlDataType_Delete)
					// {
					// 	r32 value = GetR32FromU32(word);
					// 	TEXT_SER_PRINTF(result, resultSize, charIndex, ": Value(%f)", value);
					// }
					// else if (nextValueType == GmlDataType_UnsignedInt)
					// {
					// 	r32 value = GetR32FromU32(word);
					// 	TEXT_SER_PRINTF(result, resultSize, charIndex, ": Value(%f)", value);
					// }
					// else if (nextValueType == GmlDataType_Int16)
					// {
					// 	r32 value = GetR32FromU32(word);
					// 	TEXT_SER_PRINTF(result, resultSize, charIndex, ": Value(%f)", value);
					// }
					else
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": Value(%08X)", word);
					}
					if (nextIsValue) { nextIsValue = false; }
					else { nextNextIsValue = false; }
				}
				else if (opCode == GmlOpCode_Conv)
				{
					u8 extraByte = GetLowestByte(word); //TODO: Does this ever do anything
					u8 comparisonType = GetLowerByte(word); //TODO: Does this ever do anything
					u8 type1 = ((GetUpperByte(word) & 0x0F) >> 0);
					u8 type2 = ((GetUpperByte(word) & 0xF0) >> 4);
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": Convert(%s => %s)", GetGmlDataTypeStr(type1), GetGmlDataTypeStr(type2));
				}
				else if (opCode == GmlOpCode_Call)
				{
					u8 dataType = GetUpperByte(word);
					u16 numArguments = GetLowerU16(word);
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": Call(%s %u arg%s)", GetGmlDataTypeStr(dataType), numArguments, (numArguments == 1) ? "" : "s");
					nextFunctionPntr = GetFunctionForCodeWord(pack, code, address, &nextFunctionIndex);
					nextIsFunctionId = true;
				}
				else if (opCode == GmlOpCode_Push16)
				{
					i16 value = GetLowerI16(word);
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": Push16(%d)", value);
				}
				else if (opCode == GmlOpCode_Push)
				{
					u8 type = GetUpperByte(word);
					if (type == GmlDataType_Int16)
					{
						i16 value = GetLowerI16(word);
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": Push(%d)", value);
					}
					else
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": Push(%s)", GetGmlDataTypeStr(type));
						nextIsValue = true;
						if (type == GmlDataType_Double) { nextNextIsValue = true; }
						if (type == GmlDataType_Int64) { nextNextIsValue = true; }
						nextValueType = type;
					}
				}
				else if (opCode == GmlOpCode_Pop)
				{
					i16 instanceValue = GetLowerI16(word);
					u8 type1 = ((GetUpperByte(word) & 0xF0) >> 4);
					u8 type2 = ((GetUpperByte(word) & 0x0F) >> 0);
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": Pop(%s => %s %s)", GetGmlDataTypeStr(type1), GetGmlInstanceValueStr(instanceValue), GetGmlDataTypeStr(type2));
					nextIsValue = true;
					if (type2 == GmlDataType_Double) { nextNextIsValue = true; }
					if (type2 == GmlDataType_Int64) { nextNextIsValue = true; }
					nextValueType = type2;
				}
				else if (opCode == GmlOpCode_Cmp)
				{
					u8 unknownByte = GetLowestByte(word);
					u8 compareType = GetLowerByte(word);
					u8 typeLeft = ((GetUpperByte(word) & 0xF0) >> 4);
					u8 typeRight = ((GetUpperByte(word) & 0x0F) >> 0);
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": Cmp(%s %s %s)", GetGmlDataTypeStr(typeLeft), GetGmlCompareTypeSymbols(compareType), GetGmlDataTypeStr(typeRight));
				}
				else if (opCode == GmlOpCode_Goto || opCode == GmlOpCode_If || opCode == GmlOpCode_IfNot)
				{
					u32 numWordsToJump = GetNonOpCode(word); //TODO: Could be NonOpCode?
					u32 targetAddress = address + numWordsToJump*sizeof(u32);
					if (targetAddress == code->bytecodeOffset + code->bytecodeLength)
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": %s(+%u or end)", GetGmlOpCodeStr(opCode), numWordsToJump);
					}
					else
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": %s(+%u or %08X)", GetGmlOpCodeStr(opCode), numWordsToJump, targetAddress);
					}
					//TODO: Should we indent the lines in-between to help with readability
				}
				else if (IsKnownGmlOpCode(opCode))
				{
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": %s(%06X)", GetGmlOpCodeStr(opCode), GetNonOpCode(word));
				}
				#else
				if (nextBytecodeIsValue)
				{
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": Value(%u or %d or 0x%08X)", word, word, word);
					nextBytecodeIsValue = false;
				}
				else if (nextBytecodeIsStringIndex)
				{
					GmsString_t* string = (word < pack->strings.length) ? DynArrayGet(&pack->strings, GmsString_t, word) : nullptr;
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": String(%u or \"%s\")", word, (string != nullptr) ? string->pntr : "?");
					nextBytecodeIsStringIndex = false;
				}
				else if (upper == 0x840F)
				{
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": Push(%d)", lowerSigned);
				}
				else if (upper == 0xC006)
				{
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": PushStr(%04X)", lower);
					nextBytecodeIsStringIndex = true;
				}
				else if (upper == 0xC005)
				{
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": If(%04X)", lower);
				}
				else if (upper == 0xB800)
				{
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": JumpEq(+%u or 0x%08X)", lower, address + lower*sizeof(u32));
				}
				else if (upper == 0x1552)
				{
					if (lower == 0x0300)
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": Compare(==)");
					}
					else if (lower == 0x0400)
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": Compare(!=)");
					}
					else
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": Compare(%04X)", lower);
					}
				}
				else if (upper == 0x1525)
				{
					if (lower == 0x0300)
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": PreCompare(==)");
					}
					else if (lower == 0x0400)
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": PreCompare(!=)");
					}
					else
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": PreCompare(%04X)", lower);
					}
				}
				else if (upper == 0x4525)
				{
					if (lower == 0xFFFB)
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": NewInt(Global)");
					}
					else if (lower == 0xFFFF)
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": NewInt(Normal)");
					}
					else
					{
						TEXT_SER_PRINTF(result, resultSize, charIndex, ": Assign(%04X)", lower);
					}
				}
				else if (upper == 0x4565)
				{
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": NewStr(%04X)", lower);
				}
				else if ((upper & 0xFFF0) == 0xA000)
				{
					u32 addrValue = (word & 0x000FFFFF);
					TEXT_SER_PRINTF(result, resultSize, charIndex, ": Addr(%u or 0x%04X)", addrValue, addrValue);
				}
				#endif
			};
			if (showVariables)
			{
				u32 varIndex = 0;
				GmsVariable_t* variable = GetVariableForCodeWord(pack, code, address, &varIndex);
				if (variable != nullptr)
				{
					TEXT_SER_PRINTF(result, resultSize, charIndex, " <- %s[%u]", variable->name, varIndex);
				}
				u32 funcIndex = 0;
				GmsFunction_t* function = GetFunctionForCodeWord(pack, code, address, &funcIndex);
				if (function != nullptr)
				{
					TEXT_SER_PRINTF(result, resultSize, charIndex, " <- %s[%u]", function->name, funcIndex);
				}
			}
		}
		
		if (pass == 0)
		{
			if (numCharsOut != nullptr) { *numCharsOut = charIndex; }
			if (memArena == nullptr) { return nullptr; }
			if (charIndex == 0) { return ArenaNtString(memArena, ""); }
			resultSize = charIndex;
			result = PushArray(memArena, char, resultSize);
			NotNull(result);
		}
		else
		{
			Assert(charIndex == resultSize);
		}
	}
	
	return result;
}
