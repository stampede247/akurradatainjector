/*
File:   deser_helpers.cpp
Author: Taylor Robbins
Date:   12\13\2020
Description: 
	** Holds a bunch of helper functions that are useful when deserializing strings 
*/

const char* TryDeserializeNumber(const char* strPntr, u32 strLength, u32* absValueOut, bool* isNegativeOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as number"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as number"; }
	
	bool isHex = false;
	bool isNegative = false;
	bool isPositive = false;
	if (numCharsLeft >= 2 && charPntr[0] == '0' && GetLowercaseChar(charPntr[1]) == 'x' && !isHex)
	{
		isHex = true;
		charPntr += 2;
		numCharsLeft -= 2;
	}
	else if (numCharsLeft >= 1 && charPntr[0] == '-' && !isNegative)
	{
		isNegative = true;
		charPntr++;
		numCharsLeft--;
	}
	else if (numCharsLeft >= 1 && charPntr[0] == '+' && !isPositive)
	{
		isPositive = true;
		charPntr++;
		numCharsLeft--;
	}
	
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off any more whitespace
	if (numCharsLeft == 0) { return "No number found after prefix"; }
	
	if (isNegativeOut != nullptr) { *isNegativeOut = isNegative; }
	if (isHex)
	{
		if (TryParseHexU32(charPntr, numCharsLeft, absValueOut)) { return nullptr; }
		else { return TempPrint("Couldn't parse \"%.*s\" as hex number", numCharsLeft, charPntr); }
	}
	else
	{
		if (TryParseU32(charPntr, numCharsLeft, absValueOut)) { return nullptr; }
		else { return TempPrint("Couldn't parse \"%.*s\" as number", numCharsLeft, charPntr); }
	}
}

StrSplitPiece_t* FindNumbersInString(MemoryArena_t* memArena, const char* strPntr, u32 strLength, bool floatingPoint, bool signedValue, u32* numNumbersOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (numNumbersOut != nullptr) { *numNumbersOut = 0; }
	if (strLength == 0) { return nullptr; }
	
	StrSplitPiece_t* result = nullptr;
	u32 resultNumNumbers = 0;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 prevSep = 0;
		u32 numNumbers = 0;
		for (u32 cIndex = 0; cIndex <= strLength; cIndex++)
		{
			char newChar = (cIndex < strLength) ? strPntr[cIndex] : '\0';
			if (IsCharClassNumeric(newChar) || (floatingPoint && newChar == '.') || (signedValue && cIndex == prevSep && newChar == '-') || (signedValue && cIndex == prevSep && newChar == '+'))
			{
				//this character is part of a number, we'll start moving away from prevSep
			}
			else
			{
				if (cIndex > prevSep)
				{
					if (result != nullptr)
					{
						Assert(numNumbers < resultNumNumbers);
						result[numNumbers].pntr = &strPntr[prevSep];
						result[numNumbers].length = cIndex - prevSep;
					}
					numNumbers++;
				}
				prevSep = cIndex+1;
			}
		}
		
		if (pass == 0)
		{
			if (numNumbersOut != nullptr) { *numNumbersOut = numNumbers; }
			if (memArena == nullptr || numNumbers == 0) { return nullptr; }
			resultNumNumbers = numNumbers;
			result = PushArray(memArena, StrSplitPiece_t, numNumbers);
			MyMemSet(result, 0x00, sizeof(StrSplitPiece_t) * numNumbers);
			Assert(result != nullptr);
		}
		else { Assert(numNumbers == resultNumNumbers); }
	}
	
	return result;
}

const char* TryDeserializeU8(const char* strPntr, u32 strLength, u8* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as u8"; }
	
	u32 absValue = 0;
	bool isNegative = false;
	const char* errorStr = TryDeserializeNumber(strPntr, strLength, &absValue, &isNegative);
	if (errorStr != nullptr) { return errorStr; }
	
	if (isNegative) { return TempPrint("Negative number can't be u8: \"%.*s\"", strLength, strPntr); }
	if (absValue > 0xFF) { return TempPrint("%u is too large for u8! \"%.*s\"", absValue, strLength, strPntr); }
	
	if (valueOut != nullptr) { *valueOut = (u8)absValue; }
	return nullptr;
}
const char* TryDeserializeU16(const char* strPntr, u32 strLength, u16* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as u16"; }
	
	u32 absValue = 0;
	bool isNegative = false;
	const char* errorStr = TryDeserializeNumber(strPntr, strLength, &absValue, &isNegative);
	if (errorStr != nullptr) { return errorStr; }
	
	if (isNegative) { return TempPrint("Negative number can't be u16: \"%.*s\"", strLength, strPntr); }
	if (absValue > 0xFFFF) { return TempPrint("%u is too large for u16! \"%.*s\"", absValue, strLength, strPntr); }
	
	if (valueOut != nullptr) { *valueOut = (u16)absValue; }
	return nullptr;
}
const char* TryDeserializeU32(const char* strPntr, u32 strLength, u32* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as u32"; }
	
	u32 absValue = 0;
	bool isNegative = false;
	const char* errorStr = TryDeserializeNumber(strPntr, strLength, &absValue, &isNegative);
	if (errorStr != nullptr) { return errorStr; }
	
	if (isNegative) { return TempPrint("Negative number can't be u32: \"%.*s\"", strLength, strPntr); }
	
	if (valueOut != nullptr) { *valueOut = absValue; }
	return nullptr;
}

const char* TryDeserializeI32(const char* strPntr, u32 strLength, i32* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as i32"; }
	
	u32 absValue = 0;
	bool isNegative = false;
	const char* errorStr = TryDeserializeNumber(strPntr, strLength, &absValue, &isNegative);
	if (errorStr != nullptr) { return errorStr; }
	
	if ((isNegative && absValue >= 0x80000000) || (!isNegative && absValue >= 0x7FFFFFFF))
	{
		return TempPrint("%s%u is too large for i32! \"%.*s\"", isNegative ? "-" : "", absValue, strLength, strPntr);
	}
	
	if (valueOut != nullptr) { *valueOut = (i32)absValue * (isNegative ? -1 : 1); }
	return nullptr;
}

const char* TryDeserializeVec2i(const char* strPntr, u32 strLength, v2i* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Vec2i"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Vec2i"; }
	
	u32 numNumbers = 0;
	StrSplitPiece_t* numbers = FindNumbersInString(TempArena, charPntr, numCharsLeft, false, true, &numNumbers);
	if (numNumbers != 2) { return TempPrint("Found %u instead of 2 numbers for Vec2i: \"%.*s\"", numCharsLeft, charPntr); }
	
	v2i result = {};
	const char* errorStr = TryDeserializeI32(numbers[0].pntr, numbers[0].length, &result.x);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeI32(numbers[1].pntr, numbers[1].length, &result.y);
	if (errorStr != nullptr) { return errorStr; }
	
	if (valueOut != nullptr) { *valueOut = result; }
	return nullptr;
}

const char* TryDeserializeDir2(const char* strPntr, u32 strLength, Dir2_t* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Dir2"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Dir2"; }
	
	u8 u8Value = 0;
	const char* errorStr = TryDeserializeU8(charPntr, numCharsLeft, &u8Value);
	if (errorStr == nullptr)
	{
		if (u8Value >= 0x10) { return TempPrint("Invalid number for Dir2: 0x%02X", u8Value); }
		if (valueOut != nullptr) { *valueOut = (Dir2_t)u8Value; }
		return nullptr;
	}
	else
	{
		if (StrCompareIgnoreCase(charPntr, "left",  numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2_Left;  } return nullptr; }
		if (StrCompareIgnoreCase(charPntr, "right", numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2_Right; } return nullptr; }
		if (StrCompareIgnoreCase(charPntr, "up",    numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2_Up;    } return nullptr; }
		if (StrCompareIgnoreCase(charPntr, "down",  numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2_Down;  } return nullptr; }
		if (StrCompareIgnoreCase(charPntr, "all",   numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2_All;   } return nullptr; }
		if (StrCompareIgnoreCase(charPntr, "none",  numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2_None;  } return nullptr; }
		return TempPrint("Invalid string given for Dir2: \"%.*s\"", numCharsLeft, charPntr);
	}
}
void CreateTextParser(TextParser_t* parser, const char* content, u32 contentLength)
{
	Assert(parser != nullptr);
	Assert(content != nullptr || contentLength == 0);
	
	ClearPointer(parser);
	parser->content = content;
	parser->contentLength = contentLength;
	parser->position = 0;
	parser->lineNumber = 0;
	parser->lineStart = 0;
	parser->numTokensFound = 0;
}

const char* ParserConsumeToken(TextParser_t* parser, ParsingToken_t* tokenOut = nullptr)
{
	Assert(parser != nullptr);
	Assert(parser->content != nullptr || parser->contentLength == 0);
	
	parser->foundToken = false;
	if (tokenOut != nullptr) { tokenOut->type = ParsingTokenType_Unknown; }
	if (parser->position >= parser->contentLength) { return nullptr; }
	
	bool foundToken = false;
	ParsingToken_t token = {};
	
	u32 startPosition = parser->position;
	bool inComment = false;
	u32 commentStart = 0;
	bool inCurlyBraces = false;
	u32 curlyContentStart = 0;
	bool inKeyValue = false;
	u32 keyEnd = 0;
	u32 valueStart = 0;
	bool inHeader = false;
	u32 headerContentStart = 0;
	while (parser->position <= parser->contentLength)
	{
		char newChar = (parser->position < parser->contentLength) ? parser->content[parser->position] : '\0';
		char nextChar = (parser->position+1 < parser->contentLength) ? parser->content[parser->position+1] : '\0';
		bool isEscaped = false;
		if (!parser->justEscaped && parser->position > 0 && parser->content[parser->position-1] == '\\')
		{
			isEscaped = true;
			parser->justEscaped = true;
		}
		else { parser->justEscaped = false; }
		
		if (newChar == '\n' || newChar == '\r' || newChar == '\0') //new-line or end of file
		{
			if (!inComment && !inCurlyBraces && !inHeader && !inKeyValue && parser->position > startPosition)
			{
				token.type = ParsingTokenType_Other;
				token.start = startPosition;
				token.length = parser->position - startPosition;
				token.pieces[0].pntr = &parser->content[startPosition];
				token.pieces[0].length = parser->position - startPosition;
				foundToken = true;
			}
			if (inComment)
			{
				token.type = ParsingTokenType_Comment;
				token.start = commentStart-2;
				token.length = parser->position - (commentStart-2);
				token.comment.pntr = &parser->content[commentStart];
				token.comment.length = parser->position - commentStart;
				foundToken = true;
				inComment = false;
			}
			if (inCurlyBraces)
			{
				return TempPrint("No closing curly brace found: \"%.*s\"", (parser->position+2) - (curlyContentStart-1), &parser->content[curlyContentStart-1]);
			}
			if (inHeader)
			{
				return TempPrint("No closing square bracket found: \"%.*s\"", (parser->position+2) - (headerContentStart-1), &parser->content[headerContentStart-1]);
			}
			if (inKeyValue)
			{
				token.type = ParsingTokenType_KeyValue;
				token.start = parser->lineStart;
				token.length = parser->position - parser->lineStart;
				token.key.pntr = &parser->content[parser->lineStart];
				token.key.length = keyEnd - parser->lineStart;
				token.value.pntr = &parser->content[valueStart];
				token.value.length = parser->position - valueStart;
				foundToken = true;
			}
			parser->position++;
			if ((newChar == '\n' && nextChar == '\r') || (newChar == '\r' && nextChar == '\n'))
			{
				parser->position++;
			}
			parser->lineNumber++;
			startPosition = parser->position;
			parser->lineStart = parser->position;
		}
		else if (!inComment && !isEscaped && newChar == '/' && nextChar == '/')
		{
			if (inCurlyBraces)
			{
				return TempPrint("Found comment in curly braces: \"%.*s\"", (parser->position+2) - (curlyContentStart-1), &parser->content[curlyContentStart-1]);
			}
			else if (inHeader)
			{
				return TempPrint("Found comment in header: \"%.*s\"", (parser->position+2) - (headerContentStart-1), &parser->content[headerContentStart-1]);
			}
			else if (inKeyValue)
			{
				token.type = ParsingTokenType_KeyValue;
				token.start = parser->lineStart;
				token.length = parser->position - parser->lineStart;
				token.key.pntr = &parser->content[parser->lineStart];
				token.key.length = keyEnd - parser->lineStart;
				token.value.pntr = &parser->content[valueStart];
				token.value.length = parser->position - valueStart;
				foundToken = true;
				//NOTE: Don't increment parser->position
			}
			else if (parser->position > startPosition)
			{
				token.type = ParsingTokenType_Other;
				token.start = startPosition;
				token.length = parser->position - startPosition;
				token.pieces[0].pntr = &parser->content[startPosition];
				token.pieces[0].length = parser->position - startPosition;
				foundToken = true;
				//NOTE: Don't increment parser->position
			}
			else
			{
				inComment = true;
				commentStart = parser->position+2;
				parser->position += 2;
			}
		}
		else if (!inKeyValue && !inComment && !inCurlyBraces && !inHeader && !isEscaped && newChar == '{')
		{
			if (parser->position > startPosition)
			{
				token.type = ParsingTokenType_Other;
				token.start = startPosition;
				token.length = parser->position - startPosition;
				token.pieces[0].pntr = &parser->content[startPosition];
				token.pieces[0].length = parser->position - startPosition;
				foundToken = true;
			}
			else
			{
				inCurlyBraces = true;
				curlyContentStart = parser->position+1;
				parser->position++;
			}
		}
		else if (inCurlyBraces && !inComment && !isEscaped && newChar == '}')
		{
			token.type = ParsingTokenType_ValueList;
			token.start = curlyContentStart-1;
			token.length = (parser->position+1) - (curlyContentStart-1);
			token.content.pntr = &parser->content[curlyContentStart];
			token.content.length = parser->position - curlyContentStart;
			foundToken = true;
			parser->position++;
		}
		else if (!inCurlyBraces && !inComment && !inKeyValue && !inHeader && !isEscaped && newChar == ':')
		{
			inKeyValue = true;
			keyEnd = parser->position;
			valueStart = parser->position+1;
			parser->position++;
		}
		else if (inKeyValue && parser->position == valueStart && IsCharClassWhitespace(newChar))
		{
			valueStart = parser->position+1;
			parser->position++;
		}
		else if (!inComment && !inCurlyBraces && !inKeyValue && !inHeader && !isEscaped && newChar == '[')
		{
			if (parser->position > startPosition)
			{
				token.type = ParsingTokenType_Other;
				token.start = startPosition;
				token.length = parser->position - startPosition;
				token.pieces[0].pntr = &parser->content[startPosition];
				token.pieces[0].length = parser->position - startPosition;
				foundToken = true;
			}
			else
			{
				inHeader = true;
				headerContentStart = parser->position+1;
				parser->position++;
			}
		}
		else if (inHeader && !inComment && !isEscaped && newChar == ']')
		{
			token.type = ParsingTokenType_Header;
			token.start = headerContentStart-1;
			token.length = (parser->position+1) - (headerContentStart-1);
			token.header.pntr = &parser->content[headerContentStart];
			token.header.length = parser->position - headerContentStart;
			foundToken = true;
			parser->position++;
		}
		else
		{
			if (IsCharClassWhitespace(newChar) && parser->position == startPosition) { startPosition++; }
			parser->position++;
		}
		
		if (foundToken) { break; }
	}
	
	if (!foundToken && parser->position >= parser->contentLength)
	{
		if (inCurlyBraces || inKeyValue || inComment || inHeader) { return "Unexpected end of file"; }
	}
	
	if (foundToken)
	{
		if (token.type == ParsingTokenType_Unknown) { Assert(false); }
		parser->numTokensFound++;
		if (tokenOut != nullptr) { MyMemCopy(tokenOut, &token, sizeof(ParsingToken_t)); }
		parser->foundToken = true;
	}
	return nullptr;
}

