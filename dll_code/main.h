/*
File:   main.h
Author: Taylor Robbins
Date:   12\13\2020
*/

#ifndef _MAIN_H
#define _MAIN_H

enum ExportOperationType_t
{
	ExportOperationType_None = 0,
	ExportOperationType_Delete,
	ExportOperationType_Rename,
	ExportOperationType_Replace,
	ExportOperationType_NumTypes,
};
const char* GetExportOperationTypeStr(ExportOperationType_t type)
{
	switch (type)
	{
		case ExportOperationType_None:    return "None";
		case ExportOperationType_Delete:  return "Delete";
		case ExportOperationType_Rename:  return "Rename";
		case ExportOperationType_Replace: return "Replace";
		default: return "Unknown";
	}
}

struct ExportOperation_t
{
	u32 roomIndex;
	GmsRoom_t* roomPntr;
	ExportOperationType_t type;
	char* levelFilePath;
	char* newName;
};

struct StringChange_t
{
	u32 stringIndex;
	char* newValue;
	u32 newLength;
};

struct MainData_t
{
	bool initialized;
	MemoryArena_t mainHeap;
	MemoryArena_t tempArena;
	MemoryArena_t stdHeap;
	
	DialogueCallback_f* dialogueFunc;
	DebugOutputCallback_f* debugOutputFunc;
	
	char* packPath;
	GmsPack_t pack;
	
	bool exportStarted;
	u32 declaredExportLength;
	u32 numOperationsSoFar;
	u32 exportNumDeletes;
	u32 exportNumRenames;
	u32 exportNumReplaces;
	DynArray_t stringChanges; //StringChange_t
	DynArray_t exportOperations; //ExportOperation_t
};

#endif //  _MAIN_H
