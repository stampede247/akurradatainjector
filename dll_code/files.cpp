/*
File:   files.cpp
Author: Taylor Robbins
Date:   12\13\2020
Description: 
	** Holds functions that help us read, write, and manipulate files in various ways 
*/

void FreeFileMemory(File_t* file)
{
	Assert(file != nullptr);
	if (file->data != nullptr)
	{
		VirtualFree(file->data, 0, MEM_RELEASE);
	}
	ClearPointer(file);
}

bool ReadEntireFile(const char* filePath, File_t* fileOut)
{
	Assert(filePath != nullptr);
	Assert(fileOut != nullptr);
	
	HANDLE fileHandle = CreateFileA(filePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		PrintLine_E("Failed to open file at \"%s\"", filePath);
		return false;
	}
	
	LARGE_INTEGER fileSize;
	BOOL getSizeResult = GetFileSizeEx(fileHandle, &fileSize);
	if (getSizeResult == 0)
	{
		PrintLine_E("Failed to get size of file at \"%s\"", filePath);
		CloseHandle(fileHandle);
		return false;
	}
	
	ClearPointer(fileOut);
	fileOut->size = fileSize.QuadPart;
	Assert(fileOut->size <= 0xFFFFFFFEUL);
	fileOut->data = (u8*)VirtualAlloc(0, (u32)fileOut->size+1, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
	if (fileOut->data == nullptr)
	{
		PrintLine_E("Failed to allocate space to read %s file", FormattedSizeStr(fileOut->size));
		CloseHandle(fileHandle);
		return false;
	}
	
	u64 numBytesRead = 0;
	while (numBytesRead < fileOut->size)
	{
		u64 numBytesToRead = fileOut->size - numBytesRead;
		if (numBytesToRead >= 0xFFFFFFFFUL) { numBytesToRead = 0xFFFFFFFFUL; }
		DWORD actualReadCount = 0;
		BOOL readResult = ReadFile(fileHandle, &fileOut->data[numBytesRead], (DWORD)numBytesToRead, &actualReadCount, 0);
		if (readResult == 0)
		{
			PrintLine_E("Failed to read %lu bytes from file at offset %lu", numBytesToRead, numBytesRead);
			VirtualFree(fileOut->data, 0, MEM_RELEASE);
			CloseHandle(fileHandle);
			return false;
		}
		Assert(actualReadCount <= numBytesToRead);
		if (actualReadCount == 0)
		{
			PrintLine_E("Failed to read any more bytes from file after %lu/%lu bytes", numBytesRead, fileOut->size);
			VirtualFree(fileOut->data, 0, MEM_RELEASE);
			CloseHandle(fileHandle);
			return false;
		}
		numBytesRead += actualReadCount;
	}
	
	CloseHandle(fileHandle);
	return true;
}

bool WriteEntireFile(const char* filePath, const void* dataPntr, u64 dataSize)
{
	Assert(filePath != nullptr);
	Assert(dataPntr != nullptr);
	Assert(dataSize > 0);
	bool result = false;
	
	Assert(dataSize <= 0xFFFFFFFFUL);
	
	HANDLE fileHandle = CreateFileA(
		filePath,              //Name of the file
		GENERIC_WRITE,         //Open for writing
		0,                     //Do not share
		NULL,                  //Default security
		CREATE_ALWAYS,         //Always overwrite
		FILE_ATTRIBUTE_NORMAL, //Default file attributes
		0                      //No Template File
	);
	if (fileHandle != INVALID_HANDLE_VALUE)
	{
		DWORD bytesWritten;
		if (WriteFile(fileHandle, dataPntr, (DWORD)dataSize, &bytesWritten, 0))
		{
			if (bytesWritten == (DWORD)dataSize)
			{
				result = true;
			}
			else
			{
				PrintLine_E("Only wrote %u/%u bytes to file at \"%s\"", bytesWritten, dataSize, filePath);
			}
		}
		else
		{
			PrintLine_E("Failed to write %u bytes to file at \"%s\"", dataSize, filePath);
		}
		CloseHandle(fileHandle);
	}
	else
	{
		PrintLine_E("Failed to open file for writing at \"%s\"", filePath);
	}
	
	return result;
}

bool MyCopyFile(const char* fromPath, const char* toPath)
{
	File_t fromFile = {};
	bool readSuccess = ReadEntireFile(fromPath, &fromFile);
	if (!readSuccess) { return false; }
	bool writeSuccess = WriteEntireFile(toPath, fromFile.data, fromFile.size);
	FreeFileMemory(&fromFile);
	return writeSuccess;
}

bool ReplacePortionOfFile(const char* filePath, u32 offset, const void* dataPntr, u32 dataSize)
{
	Assert(filePath != nullptr);
	Assert(dataPntr != nullptr);
	Assert(dataSize > 0);
	bool result = false;
	
	Assert(offset <= 0xFFFFFFFFUL);
	Assert(dataSize <= 0xFFFFFFFFUL);
	
	// PrintLine_D("Injecting %u bytes at %u in file \"%s\"", dataSize, offset, filePath);
	
	HANDLE fileHandle = CreateFileA(
		filePath,              //Name of the file
		GENERIC_WRITE,         //Open for writing
		0,                     //Do not share
		NULL,                  //Default security
		OPEN_EXISTING,         //Open existing file
		FILE_ATTRIBUTE_NORMAL, //Default file attributes
		0                      //No Template File
	);
	if (fileHandle != INVALID_HANDLE_VALUE)
	{
		DWORD setFilePntrResult = SetFilePointer(fileHandle, (DWORD)offset, NULL, FILE_BEGIN);
		if (setFilePntrResult == INVALID_SET_FILE_POINTER)
		{
			PrintLine_E("Failed to seek to %u in file at \"%s\"", offset, filePath);
			CloseHandle(fileHandle);
			return false;
		}
		
		DWORD bytesWritten;
		if (WriteFile(fileHandle, dataPntr, (DWORD)dataSize, &bytesWritten, 0))
		{
			if (bytesWritten == (DWORD)dataSize)
			{
				result = true;
			}
			else
			{
				PrintLine_E("Only wrote %u/%u bytes to file at \"%s\"", bytesWritten, dataSize, filePath);
			}
		}
		else
		{
			PrintLine_E("Failed to write %u bytes to file at \"%s\"", dataSize, filePath);
		}
		CloseHandle(fileHandle);
	}
	else
	{
		PrintLine_E("Failed to open file for writing at \"%s\"", filePath);
	}
	
	return result;
}

bool TryOpenFile(const char* filePath, OpenFile_t* openFileOut)
{
	Assert(filePath != nullptr);
	Assert(openFileOut != nullptr);
	ClearPointer(openFileOut);
	
	HANDLE fileHandle = CreateFileA(filePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		PrintLine_E("Failed to open file at \"%s\"", filePath);
		return false;
	}
	
	openFileOut->handle = fileHandle;
	openFileOut->isOpen = true;
	openFileOut->numBytesRead = 0;
	return true;
}

bool ReadFromFile(OpenFile_t* openFile, u32 numBytes, void* dataOut)
{
	Assert(openFile != nullptr);
	Assert(openFile->isOpen);
	// Assert(dataOut != nullptr);
	Assert(numBytes > 0);
	
	DWORD numBytesRead = 0;
	BOOL readFileResult = ReadFile(
		openFile->handle,
		dataOut,
		numBytes,
		&numBytesRead,
		NULL
	);
	if (readFileResult == 0)
	{
		DWORD errorCode = GetLastError();
		PrintLine_E("Failed to read %u bytes at cursorIndex %u", numBytes, openFile->numBytesRead);
		return false;
	}
	Assert(numBytesRead <= numBytes);
	if (numBytesRead < numBytes)
	{
		PrintLine_E("Read only %u/%u bytes at cursorIndex %u", numBytesRead, numBytes, openFile->numBytesRead);
		return false;
	}
	
	openFile->numBytesRead += numBytes;
	return true;
}
#define HardReadFromFile(openFile, numBytes, dataOut) if (!ReadFromFile((openFile), (numBytes), (dataOut))) { Assert(false && "Failed to read from file!"); }

u32 GetFilePntrOffset(const File_t* file, const void* fileDataPntr)
{
	Assert(file != nullptr);
	Assert(((u8*)fileDataPntr) >= file->data);
	Assert(((u8*)fileDataPntr) <= file->data + file->size);
	u64 result = (u64)(((u8*)fileDataPntr) - file->data);
	Assert(result <= 0xFFFFFFFFUL);
	return (u32)result;
}
const u8* GetFilePntr(const File_t* file, u64 offset)
{
	Assert(file != nullptr);
	Assert(file->data != nullptr);
	if (offset <= file->size)
	{
		return &file->data[offset];
	}
	else { return nullptr; };
}
const u8* GetFilePntrSized(const File_t* file, u64 offset, u64 expectedSize)
{
	Assert(file != nullptr);
	Assert(file->data != nullptr);
	if (offset + expectedSize <= file->size)
	{
		return &file->data[offset];
	}
	else { return nullptr; };
}
const char* GetFileString(const File_t* file, u64 offset)
{
	Assert(file != nullptr);
	Assert(file->data != nullptr);
	if (offset != 0 && offset < file->size)
	{
		return (const char*)&file->data[offset];
	}
	else { return nullptr; }
}

// +--------------------------------------------------------------+
// |                    File Consume Functions                    |
// +--------------------------------------------------------------+
const u8* ConsumeFileBytes(File_t* file, u64 numBytes)
{
	Assert(file != nullptr);
	Assert(file->data != nullptr);
	Assert(file->readCursor + numBytes <= file->size);
	const u8* result = &file->data[file->readCursor];
	file->readCursor += numBytes;
	return result;
}
#define ConsumeFileType(filePntr, type, varName) type varName = *((const type*)ConsumeFileBytes((filePntr), sizeof(type)));
#define ConsumeFileStruct(filePntr, type, pntrVarName) const type* pntrVarName = (const type*)ConsumeFileBytes((filePntr), sizeof(type));

void ConsumeFileList_(File_t* file, MemoryArena_t* memArena, DynArray_t* arrayOut, u64 itemSize)
{
	Assert(file != nullptr);
	Assert(memArena != nullptr);
	Assert(arrayOut != nullptr);
	
	ConsumeFileType(file, u32, numListItems);
	CreateDynamicArray(arrayOut, memArena, sizeof(const u8*), 16, numListItems);
	for (u32 iIndex = 0; iIndex < numListItems; iIndex++)
	{
		ConsumeFileType(file, u32, newOffset);
		const u8** newPntrSpace = DynArrayAdd(arrayOut, const u8*);
		Assert(newPntrSpace != nullptr);
		*newPntrSpace = GetFilePntrSized(file, newOffset, itemSize);
		Assert(*newPntrSpace != nullptr);
	}
}

#define ConsumeFileList(file, memArena, arrayOut, itemType) ConsumeFileList_((file), (memArena), (arrayOut), sizeof(itemType))
