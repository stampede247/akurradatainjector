/*
File:   main.c
Author: Taylor Robbins
Date:   12\13\2020
Description: 
	** Holds the main entry point for the DLL and also includes all other files that are needed to compile the DLL
*/

#define USE_ASSERT_FAILURE_FUNCTION true
#define USE_MY_STD_LIB              false
#define USE_MIN_STD_LIB             false
#define MyMalloc(size)              malloc(size)
#define MyRealloc(pntr, newSize)    realloc(pntr, newSize)
#define MyFree(size)                free(size)

#include "mylib/mylib.h"

#include <windows.h>
#include <stdio.h>
#include <stdint.h>

#include "mylib/my_dynamicArray.h"
#include "mylib/my_tempMemory.cpp"

#define STBIW_ASSERT(x) Assert(x)
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb/stb_image_write.h"

// +--------------------------------------------------------------+
// |                         Header Files                         |
// +--------------------------------------------------------------+
#include "types.h"
#include "gms_deser.h"
#include "gms.h"
#include "gml_types.h"
#include "main.h"

// +--------------------------------------------------------------+
// |                           Globals                            |
// +--------------------------------------------------------------+
MainData_t* main = nullptr;
MemoryArena_t* mainHeap = nullptr;

// +--------------------------------------------------------------+
// |                         Source Files                         |
// +--------------------------------------------------------------+
#include "debug.cpp"
#include "printout.cpp"
#include "helpers.cpp"
#include "deser_helpers.cpp"
#include "files.cpp"

#include "gms_deser.cpp"
#include "pcq_deser.cpp"
#include "gms_serialization.cpp"

#include "commands.cpp"

// +--------------------------------------------------------------+
// |                           DLL Main                           |
// +--------------------------------------------------------------+
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved)
{
	
	return TRUE;
}

// +--------------------------------------------------------------+
// |                      Parser_Initialize                       |
// +--------------------------------------------------------------+
__declspec(dllexport) void __stdcall Parser_Initialize(DialogueCallback_f* dialogueCallback, DebugOutputCallback_f* debugOutputCallback)
{
	NotNull_(dialogueCallback);
	NotNull_(debugOutputCallback);
	Assert_(main == nullptr);
	main = (MainData_t*)malloc(sizeof(MainData_t));
	Assert_(main != nullptr);
	ClearPointer(main);
	main->dialogueFunc = dialogueCallback;
	main->debugOutputFunc = debugOutputCallback;
	
	// +==============================+
	// |     Create Memory Arenas     |
	// +==============================+
	{
		main->tempArena.size = Megabytes(4);
		main->tempArena.base = malloc(main->tempArena.size);
		Assert(main->tempArena.base != nullptr);
		InitializeMemoryArenaTemp(&main->tempArena, main->tempArena.base, main->tempArena.size, 32);
		TempArena = &main->tempArena;
		
		InitializeMemoryArenaStdHeap(&main->stdHeap);
		
		InitializeMemoryArenaGrowingHeapOutOfArena(&main->mainHeap, &main->stdHeap, Kilobytes(128), 1);
		mainHeap = &main->mainHeap;
	}
	
	TempPushMark();
	
	WriteLine_O("+==============================+");
	WriteLine_O("|        Akurra Parser         |");
	WriteLine_O("+==============================+");
	WriteLine_O("Ready to import...");
	
	TempPopMark();
}

__declspec(dllexport) void __stdcall Parser_PushTempMark()
{
	Assert(TempArena != nullptr);
	TempPushMark();
}

__declspec(dllexport) void __stdcall Parser_PopTempMark()
{
	Assert(TempArena != nullptr);
	TempPopMark();
}

// +--------------------------------------------------------------+
// |                        Parser_Import                         |
// +--------------------------------------------------------------+
__declspec(dllexport) bool __stdcall Parser_Import(const char* filePath)
{
	NotNull(main);
	
	if (main->pack.filled)
	{
		WriteLine_D("Releasing old pack data");
		DestroyGmsPack(&main->pack, mainHeap);
	}
	if (main->packPath != nullptr) { ArenaPop(mainHeap, main->packPath); main->packPath = nullptr; }
	
	PrintLine_N("Importing data from \"%s\"...", filePath);
	if (DeserializeGmsPackFile(filePath, mainHeap, &main->pack))
	{
		WriteLine_N("Pack Deserialization Succeeded!");
		main->packPath = ArenaNtString(mainHeap, filePath);
		NotNull(main->packPath);
		return true;
	}
	else
	{
		main->dialogueFunc("Failed to Deserialize", "There was a problem during the deserialization of the selected file.\n\nCheck the debug log for more information.");
		return false;
	}
}

struct Ex_RoomInfo_t
{
	char* name;
	i32 width;
	i32 height;
	u32 numObjects;
	u32 numCollisions;
	u32 numOtherInstances;
};
__declspec(dllexport) int __stdcall Parser_GetNumRooms()
{
	NotNull(main);
	if (!main->pack.filled) { return 0; }
	return (int)main->pack.rooms.length;
}
__declspec(dllexport) Ex_RoomInfo_t __stdcall Parser_GetRoomInfo(int roomIndex)
{
	NotNull(main);
	Assert(main->pack.filled);
	Assert(roomIndex >= 0 && (u32)roomIndex < main->pack.rooms.length);
	GmsRoom_t* room = DynArrayGet(&main->pack.rooms, GmsRoom_t, (u32)roomIndex);
	NotNull(room);
	
	Ex_RoomInfo_t result = {};
	result.width = room->size.width;
	result.height = room->size.height;
	
	result.name = (room->name != nullptr) ? ArenaNtString(TempArena, room->name) : nullptr;
	
	for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
	{
		GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
		NotNull(layer);
		if (layer->type == 2)
		{
			if (StrCompareIgnoreCaseNt(layer->name, "object_layer"))
			{
				result.numObjects += layer->instances.length;
			}
			else if (StrCompareIgnoreCaseNt(layer->name, "collision_layer"))
			{
				result.numCollisions += layer->instances.length;
			}
			else
			{
				result.numOtherInstances += layer->instances.length;
			}
		}
	}
	
	return result;
}

struct Ex_ObjectInfo_t
{
	char* name;
	char* spriteName;
	u32 flags;
	i32 parentId;
	i32 maskId;
	i32 depth;
};
__declspec(dllexport) int __stdcall Parser_GetNumObjects()
{
	NotNull(main);
	if (!main->pack.filled) { return 0; }
	return (int)main->pack.objects.length;
}
__declspec(dllexport) Ex_ObjectInfo_t __stdcall Parser_GetObjectInfo(int objectIndex)
{
	NotNull(main);
	Assert(main->pack.filled);
	Assert(objectIndex >= 0 && (u32)objectIndex < main->pack.objects.length);
	GmsObject_t* object = DynArrayGet(&main->pack.objects, GmsObject_t, (u32)objectIndex);
	NotNull(object);
	
	Ex_ObjectInfo_t result = {};
	result.parentId = object->parentId;
	result.maskId = object->maskId;
	result.depth = object->depth;
	
	result.flags = 0;
	if (object->isVisible)    { FlagSet(result.flags, 0x01); }
	if (object->isSolid)      { FlagSet(result.flags, 0x02); }
	if (object->isPersistent) { FlagSet(result.flags, 0x04); }
	if (object->hasPhysics)   { FlagSet(result.flags, 0x08); }
	if (object->isSensor)     { FlagSet(result.flags, 0x10); }
	
	result.name = (object->name != nullptr) ? ArenaNtString(TempArena, object->name) : nullptr;
	
	if (object->spriteId >= 0 && (u32)object->spriteId < main->pack.sprites.length)
	{
		GmsSprite_t* sprite = DynArrayGet(&main->pack.sprites, GmsSprite_t, (u32)object->spriteId);
		NotNull(sprite);
		if (sprite->name != nullptr)
		{
			result.spriteName = ArenaNtString(TempArena, sprite->name);
		}
		else
		{
			result.spriteName = TempPrint("Unnamed[%d]", object->spriteId);
		}
	}
	if (result.spriteName == nullptr)
	{
		result.spriteName = TempPrint("Sprite[%d]", object->spriteId);
	}
	NotNull(result.spriteName);
	
	return result;
}

struct Ex_SpriteInfo_t
{
	char* name;
	i32 width;
	i32 height;
	i32 originX;
	i32 originY;
	u32 numFrames;
	r32 speed;
	u32 isSpeedGameFrames;
	u32 formatNum;
};
__declspec(dllexport) int __stdcall Parser_GetNumSprites()
{
	NotNull(main);
	if (!main->pack.filled) { return 0; }
	return (int)main->pack.sprites.length;
}
__declspec(dllexport) Ex_SpriteInfo_t __stdcall Parser_GetSpriteInfo(int spriteIndex)
{
	NotNull(main);
	Assert(main->pack.filled);
	Assert(spriteIndex >= 0 && (u32)spriteIndex < main->pack.sprites.length);
	GmsSprite_t* sprite = DynArrayGet(&main->pack.sprites, GmsSprite_t, (u32)spriteIndex);
	NotNull(sprite);
	
	Ex_SpriteInfo_t result = {};
	result.width = sprite->size.width;
	result.height = sprite->size.height;
	result.originX = sprite->origin.x;
	result.originY = sprite->origin.y;
	result.speed = sprite->speed;
	result.isSpeedGameFrames = (sprite->isSpeedGameFrames ? 1 : 0);
	result.formatNum = sprite->formatNum;
	result.numFrames = sprite->frames.length;
	
	result.name = (sprite->name != nullptr) ? ArenaNtString(TempArena, sprite->name) : nullptr;
	
	return result;
}

struct Ex_StringInfo_t
{
	u32 offset;
	u32 length;
	char* pntr;
};
__declspec(dllexport) int __stdcall Parser_GetNumStrings()
{
	NotNull(main);
	if (!main->pack.filled) { return 0; }
	return (int)main->pack.strings.length;
}
__declspec(dllexport) Ex_StringInfo_t __stdcall Parser_GetStringInfo(int stringIndex)
{
	NotNull(main);
	Assert(main->pack.filled);
	Assert(stringIndex >= 0 && (u32)stringIndex < main->pack.strings.length);
	GmsString_t* string = DynArrayGet(&main->pack.strings, GmsString_t, (u32)stringIndex);
	NotNull(string);
	Ex_StringInfo_t result = {};
	result.offset = string->offset;
	result.length = string->length;
	result.pntr = (string->pntr != nullptr) ? ArenaNtString(TempArena, string->pntr) : nullptr;
	return result;
}

struct Ex_CodeInfo_t
{
	char* name;
	u32 bytecodeOffset;
	u32 bytecodeLength;
	u32 numVariables;
	u32 numFunctions;
};
__declspec(dllexport) int __stdcall Parser_GetNumCodes()
{
	NotNull(main);
	if (!main->pack.filled) { return 0; }
	return (int)main->pack.codes.length;
}
__declspec(dllexport) Ex_CodeInfo_t __stdcall Parser_GetCodeInfo(int codeIndex)
{
	NotNull(main);
	Assert(main->pack.filled);
	Assert(codeIndex >= 0 && (u32)codeIndex < main->pack.codes.length);
	GmsCode_t* code = DynArrayGet(&main->pack.codes, GmsCode_t, (u32)codeIndex);
	NotNull(code);
	
	Ex_CodeInfo_t result = {};
	result.name = ArenaNtString(TempArena, (code->name != nullptr) ? code->name : "[Unnamed]");
	NotNull(result.name);
	result.bytecodeOffset = code->bytecodeOffset;
	result.bytecodeLength = code->bytecodeLength;
	result.numVariables = code->variableIndices.length;
	result.numFunctions = code->functionIndices.length;
	
	return result;
}
__declspec(dllexport) char* __stdcall Parser_GetCodeBytecode(int codeIndex)
{
	NotNull(main);
	Assert(main->pack.filled);
	Assert(codeIndex >= 0 && (u32)codeIndex < main->pack.codes.length);
	GmsCode_t* code = DynArrayGet(&main->pack.codes, GmsCode_t, (u32)codeIndex);
	NotNull(code);
	//NOTE: Each word will be printed in the format "######## "
	u32 numCharsForBytecode = (code->bytecodeLength/4) * (8+1);
	char* result = PushArray(TempArena, char, numCharsForBytecode+1);
	MyMemSet(result, 0x00, numCharsForBytecode+1);
	NotNull(result);
	for (u32 wordIndex = 0; (wordIndex*4) + 4 <= code->bytecodeLength; wordIndex++)
	{
		char* bufferTargetPntr = &result[wordIndex * (8+1)];
		u32 wordValue = ((u32*)code->bytecodePntr)[wordIndex];
		BufferPrintEx(bufferTargetPntr, 8+1 + 1, "%08X ", wordValue);
	}
	result[numCharsForBytecode] = '\0';
	Assert(MyStrLength32(result) == numCharsForBytecode);
	return result;
}
__declspec(dllexport) char* __stdcall Parser_GetCodeFormattedBytecode(int codeIndex, bool showAddresses, bool showInterpretations, bool showVariables, bool showDecompiles)
{
	NotNull(main);
	Assert(main->pack.filled);
	Assert(codeIndex >= 0 && (u32)codeIndex < main->pack.codes.length);
	return SerializeBytecode(TempArena, &main->pack, codeIndex, showAddresses, showInterpretations, showVariables, showDecompiles);
}

struct Ex_VariableInfo_t
{
	char* name;
	u32 numCodeReferences;
	i32 firstCodeReference;
	i32 codeIndex;
};
__declspec(dllexport) int __stdcall Parser_GetNumVariables()
{
	NotNull(main);
	if (!main->pack.filled) { return 0; }
	return (int)main->pack.variables.length;
}
__declspec(dllexport) Ex_VariableInfo_t __stdcall Parser_GetVariableInfo(int variableIndex)
{
	NotNull(main);
	Assert(main->pack.filled);
	Assert(variableIndex >= 0 && (u32)variableIndex < main->pack.variables.length);
	GmsVariable_t* variable = DynArrayGet(&main->pack.variables, GmsVariable_t, (u32)variableIndex);
	NotNull(variable);
	
	Ex_VariableInfo_t result = {};
	result.numCodeReferences = variable->numCodeReferences;
	result.firstCodeReference = variable->firstCodeReference;
	result.codeIndex = variable->codeIndex;
	
	result.name = (variable->name != nullptr) ? ArenaNtString(TempArena, variable->name) : nullptr;
	
	return result;
}

struct Ex_FunctionInfo_t
{
	char* name;
	i32 unknown0;
	i32 firstCodeReference;
	i32 codeIndex;
};
__declspec(dllexport) int __stdcall Parser_GetNumFunctions()
{
	NotNull(main);
	if (!main->pack.filled) { return 0; }
	return (int)main->pack.functions.length;
}
__declspec(dllexport) Ex_FunctionInfo_t __stdcall Parser_GetFunctionInfo(int functionIndex)
{
	NotNull(main);
	Assert(main->pack.filled);
	Assert(functionIndex >= 0 && (u32)functionIndex < main->pack.functions.length);
	GmsFunction_t* function = DynArrayGet(&main->pack.functions, GmsFunction_t, (u32)functionIndex);
	NotNull(function);
	
	Ex_FunctionInfo_t result = {};
	result.unknown0 = function->unknown0;
	result.firstCodeReference = function->firstCodeReference;
	result.codeIndex = function->codeIndex;
	
	result.name = (function->name != nullptr) ? ArenaNtString(TempArena, function->name) : nullptr;
	
	return result;
}

struct BitmapImage_t
{
	MemoryArena_t* allocArena;
	u32 dataSize;
	u32 allocSize;
	u8* dataPntr;
};
void DestroyBitmapImage(BitmapImage_t* image)
{
	NotNull(image);
	if (image->dataPntr != nullptr)
	{
		NotNull(image->allocArena);
		ArenaPop(image->allocArena, image->dataPntr);
	}
	ClearPointer(image);
}
void StbBitmapWriteCallback(void* context, void* data, int size)
{
	NotNull(context);
	Assert(data != nullptr || size == 0);
	if (size == 0) { return; }
	BitmapImage_t* image = (BitmapImage_t*)context;
	NotNull(image->allocArena);
	Assert(image->dataSize <= image->allocSize);
	if (image->dataSize + (u32)size >= image->allocSize)
	{
		u32 numBytesNeeded = (u32)size - (image->allocSize - image->dataSize);
		u32 newSpaceSize = image->dataSize + CeilDivU32(numBytesNeeded, Kilobytes(4));
		u8* newSpace = PushArray(image->allocArena, u8, newSpaceSize);
		if (image->dataSize > 0)
		{
			Assert(image->dataPntr != nullptr);
			MyMemCopy(newSpace, image->dataPntr, image->dataSize);
			ArenaPop(image->allocArena, image->dataPntr);
		}
		MyMemCopy(&newSpace[image->dataSize], data, size);
		image->dataPntr = newSpace;
		image->dataSize += size;
		image->allocSize = newSpaceSize;
	}
	else
	{
		MyMemCopy(&image->dataPntr[image->dataSize], data, (u32)size);
		image->dataSize += (u32)size;
	}
}

__declspec(dllexport) u8* __stdcall Parser_GetRoomImage(int roomIndex, int* imageSizeOut)
{
	NotNull(main);
	Assert(main->pack.filled);
	Assert(roomIndex >= 0 && (u32)roomIndex < main->pack.rooms.length);
	NotNull(imageSizeOut);
	GmsRoom_t* room = DynArrayGet(&main->pack.rooms, GmsRoom_t, (u32)roomIndex);
	NotNull(room);
	
	rec roomRec = NewRec(Vec2_Zero, NewVec2(room->size));
	for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
	{
		GmsRoomObject_t* roomObject = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex);
		NotNull(roomObject);
		GmsObject_t* object = nullptr;
		if (roomObject->id >= 0 && (u32)roomObject->id < main->pack.objects.length) { object = DynArrayGet(&main->pack.objects, GmsObject_t, (u32)roomObject->id); }
		GmsSprite_t* sprite = nullptr;
		if (object != nullptr && object->spriteId >= 0 && (u32)object->spriteId < main->pack.sprites.length) { sprite = DynArrayGet(&main->pack.sprites, GmsSprite_t, (u32)object->spriteId); }
		v2 objectSize = Vec2_Zero;
		if (sprite != nullptr)
		{
			objectSize = Vec2Multiply(NewVec2(sprite->size), roomObject->scale);
		}
		if (objectSize.width > 0 && objectSize.height > 0)
		{
			rec objectRec = NewRec(NewVec2(roomObject->position), objectSize);
			if (roomRec == Rec_Zero)
			{
				roomRec = objectRec;
			}
			else
			{
				roomRec = RecBoth(roomRec, objectRec);
			}
		}
	}
	
	reci roomReci = NewReci(Vec2Floori(roomRec.topLeft), Vec2i_Zero);
	roomReci.size = Vec2Ceili(roomRec.topLeft + roomRec.size) - roomReci.topLeft;
	if (roomReci.width <= 0 && roomReci.height <= 0)
	{
		WriteLine_W("This room is empty. No image generated");
		*imageSizeOut = 0;
		return nullptr;
	}
	
	v2i bitmapSize = roomReci.size;
	u32 pixelDataSize = (u32)(bitmapSize.width * bitmapSize.height) * 4;
	u8* pixelData = PushArray(mainHeap, u8, pixelDataSize);
	NotNull(pixelData);
	for (i32 yOffset = 0; yOffset < bitmapSize.height; yOffset++)
	{
		for (i32 xOffset = 0; xOffset < bitmapSize.width; xOffset++)
		{
			u32* pixelPntr = (u32*)(&pixelData[((yOffset * bitmapSize.width) + xOffset) * 4]);
			*pixelPntr = SwizzleColorForBmp(ColorBlack_Value);
		}
	}
	
	for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
	{
		GmsRoomObject_t* roomObject = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex);
		NotNull(roomObject);
		GmsObject_t* object = nullptr;
		if (roomObject->id >= 0 && (u32)roomObject->id < main->pack.objects.length) { object = DynArrayGet(&main->pack.objects, GmsObject_t, (u32)roomObject->id); }
		GmsSprite_t* sprite = nullptr;
		if (object != nullptr && object->spriteId >= 0 && (u32)object->spriteId < main->pack.sprites.length) { sprite = DynArrayGet(&main->pack.sprites, GmsSprite_t, (u32)object->spriteId); }
		v2 objectSize = Vec2_Zero;
		if (sprite != nullptr)
		{
			objectSize = Vec2Multiply(NewVec2(sprite->size), roomObject->scale);
		}
		if (objectSize.width > 0 && objectSize.height > 0)
		{
			u32 color = GetPredefPalColorByIndex(roomObject->id).value;
			u32 secondaryColor = color;
			if (StrCompareIgnoreCaseNt(object->name, "obj_solid")) { color = 0xFF008000; secondaryColor = color; } //dark green
			else if (StrCompareIgnoreCaseNt(object->name, "obj_block_blocker")) { color = 0xFF005000; secondaryColor = ColorBlack_Value; } //darker green and black
			else if (StrCompareIgnoreCaseNt(object->name, "obj_deep_water")) { color = 0xFF00AEF0; secondaryColor = 0xFF5DD1FF; } //light blues
			// if (roomObject->unknownId != -1) { color = SwizzleColorForBmp(ColorWhite_Value); }
			v2i topLeft = roomObject->position;
			v2i bottomRight = Vec2Ceili(NewVec2(roomObject->position) + objectSize);
			for (i32 yOffset = topLeft.y; yOffset < bottomRight.y; yOffset++)
			{
				for (i32 xOffset = topLeft.x; xOffset < bottomRight.x; xOffset++)
				{
					Assert((xOffset - roomReci.x) < bitmapSize.width);
					Assert((yOffset - roomReci.y) < bitmapSize.height);
					u32* pixelPntr = (u32*)(&pixelData[(((yOffset - roomReci.y) * bitmapSize.width) + (xOffset - roomReci.x)) * 4]);
					if ((yOffset%2 == 0) || (*pixelPntr == ColorBlack_Value))
					{
						*pixelPntr = SwizzleColorForBmp(((xOffset + yOffset)%2 == 0) ? color : secondaryColor);
					}
				}
			}
		}
	}
	
	for (i32 yOffset = 0; yOffset < bitmapSize.height; yOffset++)
	{
		for (i32 xOffset = 0; xOffset < bitmapSize.width; xOffset++)
		{
			v2i actualPixelPos = NewVec2i(roomReci.x + xOffset, roomReci.y + yOffset);
			if (actualPixelPos.x == 0 || actualPixelPos.y == 0 || actualPixelPos.x == room->size.width-1 || actualPixelPos.y == room->size.height-1)
			{
				u32* pixelPntr = (u32*)(&pixelData[((yOffset * bitmapSize.width) + xOffset) * 4]);
				*pixelPntr = SwizzleColorForBmp(ColorWhite_Value);
			}
		}
	}
	
	BitmapImage_t image = {};
	image.allocArena = mainHeap;
	image.dataSize = 0;
	image.allocSize = pixelDataSize;
	image.dataPntr = PushArray(mainHeap, u8, image.allocSize);
	NotNull(image.dataPntr);
	
	int stbResult = stbi_write_bmp_to_func(StbBitmapWriteCallback, &image, bitmapSize.width, bitmapSize.height, 4, pixelData);
	ArenaPop(mainHeap, pixelData);
	
	if (stbResult == 0)
	{
		main->dialogueFunc("stbi_write_btmp_to_func Failed", TempPrint("Ran into an error when creating bmp image:\nError %d", stbResult));
		DestroyBitmapImage(&image);
		*imageSizeOut = 0;
		return nullptr;
	}
	
	*imageSizeOut = image.dataSize;
	return image.dataPntr;
}
__declspec(dllexport) void __stdcall Parser_FreeRoomImage(u8* imageDataPntr)
{
	NotNull(imageDataPntr);
	NotNull(mainHeap);
	ArenaPop(mainHeap, imageDataPntr);
}

// +--------------------------------------------------------------+
// |                        Parser_Export                         |
// +--------------------------------------------------------------+
struct Ex_RoomOperation_t
{
	int type;
	const char* str;
};
void DestroyExportList()
{
	NotNull(main);
	for (u32 oIndex = 0; oIndex < main->exportOperations.length; oIndex++)
	{
		ExportOperation_t* operation = DynArrayGet(&main->exportOperations, ExportOperation_t, oIndex);
		NotNull(operation);
		if (operation->levelFilePath != nullptr) { ArenaPop(mainHeap, operation->levelFilePath); }
		if (operation->newName != nullptr) { ArenaPop(mainHeap, operation->newName); }
	}
	for (u32 sIndex = 0; sIndex < main->stringChanges.length; sIndex++)
	{
		StringChange_t* stringChange = DynArrayGet(&main->stringChanges, StringChange_t, sIndex);
		NotNull(stringChange);
		if (stringChange->newValue != nullptr) { ArenaPop(mainHeap, stringChange->newValue); }
	}
	DestroyDynamicArray(&main->exportOperations);
	DestroyDynamicArray(&main->stringChanges);
	
}
__declspec(dllexport) bool __stdcall Parser_StartExportList(int numOperations)
{
	NotNull(main);
	if (!main->pack.filled)
	{
		main->dialogueFunc("No pack loaded", "You must first import the existing game data before you can inject custom data and export");
		return false;
	}
	if (main->exportStarted)
	{
		main->dialogueFunc("Export Already Started", "A previous export operation is still in progress. (This is probably a bug. Please restart the application)");
		return false;
	}
	main->declaredExportLength = (u32)numOperations;
	main->numOperationsSoFar = 0;
	main->exportNumReplaces = 0;
	main->exportNumRenames = 0;
	main->exportNumDeletes = 0;
	CreateDynamicArray(&main->exportOperations, mainHeap, sizeof(ExportOperation_t), 16, numOperations);
	CreateDynamicArray(&main->stringChanges, mainHeap, sizeof(StringChange_t));
	main->exportStarted = true;
	for (u32 sIndex = 0; sIndex < main->pack.strings.length; sIndex++)
	{
		GmsString_t* gmsString = DynArrayGet(&main->pack.strings, GmsString_t, sIndex);
		NotNull(gmsString);
		if (gmsString->replacedValue != nullptr) { ArenaPop(mainHeap, gmsString->replacedValue); gmsString->replacedValue = nullptr; }
	}
	return true;
}
__declspec(dllexport) void __stdcall Parser_ExportOperation(int roomIndex, Ex_RoomOperation_t operation)
{
	NotNull(main);
	Assert(main->exportStarted);
	Assert(main->numOperationsSoFar < main->declaredExportLength);
	Assert(roomIndex >= 0 && (u32)roomIndex < main->pack.rooms.length);
	GmsRoom_t* room = DynArrayGet(&main->pack.rooms, GmsRoom_t, roomIndex);
	NotNull(room);
	
	ExportOperation_t* newItem = DynArrayAdd(&main->exportOperations, ExportOperation_t);
	NotNull(newItem);
	ClearPointer(newItem);
	newItem->roomPntr = room;
	newItem->roomIndex = roomIndex;
	newItem->type = (ExportOperationType_t)operation.type;
	
	main->numOperationsSoFar++;
	
	if (operation.type == ExportOperationType_Delete)
	{
		main->exportNumDeletes++;
	}
	else if (operation.type == ExportOperationType_Rename)
	{
		NotNull(operation.str);
		newItem->newName = ArenaNtString(mainHeap, operation.str);
		main->exportNumRenames++;
	}
	else if (operation.type == ExportOperationType_Replace)
	{
		NotNull(operation.str);
		newItem->levelFilePath = ArenaNtString(mainHeap, operation.str);
		main->exportNumReplaces++;
	}
	else
	{
		Assert(false); //Unsupported operation type
	}
}
bool ReplaceRoomFromLevelFile(MemoryArena_t* memArena, GmsPack_t* pack, const char* filePath, GmsRoom_t* room, bool takeLevelTitle)
{
	NotNull(memArena);
	NotNull(pack);
	Assert(pack->filled);
	NotNull(filePath);
	NotNull(room);
	
	PrintLine_D("Deserializing room at \"%s\"", filePath);
	GmsRoom_t newRoom = {};
	if (!DeserializePcqLevel(pack, filePath, memArena, &newRoom, room))
	{
		PrintLine_E("Failed to deserialize PCQ level file at \"%s\"", filePath);
		return false;
	}
	if (!takeLevelTitle)
	{
		if (newRoom.name != nullptr) { ArenaPop(memArena, newRoom.name); }
		newRoom.name = (room->name != nullptr) ? ArenaNtString(memArena, room->name) : nullptr;
	}
	WriteLine_I("Deserialization Success!");
	
	DestroyGmsRoom(memArena, room);
	MyMemCopy(room, &newRoom, sizeof(GmsRoom_t));
	return true;
}
__declspec(dllexport) bool __stdcall Parser_Export(const char* filePath)
{
	NotNull(main);
	Assert(main->exportStarted);
	Assert(main->pack.filled);
	NotNull(main->packPath);
	PrintLine_N("Exporting data from \"%s\" to \"%s\"", main->packPath, filePath);
	
	WriteLine_D("Copying file from source to destination...");
	if (!MyCopyFile(main->packPath, filePath))
	{
		main->dialogueFunc("Failed to copy file", TempPrint(
			"We were unable to copy the source data file to the output location.\n"
			"Please make sure the application has write permissions to the output file path\n"
			"Source Path: \"%s\"\n"
			"Destination Path: \"%s\"",
			main->packPath, filePath)
		);
		DestroyExportList();
		main->exportStarted = false;
		return false;
	}
	WriteLine_D("Copy Success!");
	
	PrintLine_N("Replacing %u/%u room(s)", main->exportNumReplaces, main->pack.rooms.length);
	PrintLine_N("Renaming %u/%u room(s)", main->exportNumRenames, main->pack.rooms.length);
	
	bool replaceEntireRoomChunk = true; //TODO: Should we ever do the old way?
	u32 numRoomsReplaced = 0;
	u32 numRoomsRenamed = 0;
	u32 numRoomsDeleted = 0;
	
	// +==============================+
	// |         Modify Rooms         |
	// +==============================+
	if (replaceEntireRoomChunk)
	{
		TempPushMark();
		DynArray_t newRooms;
		CreateDynamicArray(&newRooms, mainHeap, sizeof(GmsRoom_t), 16, main->pack.rooms.length);
		
		// +==============================+
		// |          Copy Rooms          |
		// +==============================+
		for (u32 rIndex = 0; rIndex < main->pack.rooms.length; rIndex++)
		{
			GmsRoom_t* oldRoom = DynArrayGet(&main->pack.rooms, GmsRoom_t, rIndex);
			NotNull(oldRoom);
			GmsRoom_t* newRoom = DynArrayAdd(&newRooms, GmsRoom_t);
			NotNull(newRoom);
			CopyGmsRoom(mainHeap, newRoom, oldRoom);
		}
		
		// +==============================+
		// |         Modify Rooms         |
		// +==============================+
		for (u32 oIndex = 0; oIndex < main->exportOperations.length; oIndex++)
		{
			ExportOperation_t* operation = DynArrayGet(&main->exportOperations, ExportOperation_t, oIndex);
			NotNull(operation);
			Assert(operation->roomIndex < newRooms.length);
			GmsRoom_t* room = DynArrayGet(&newRooms, GmsRoom_t, operation->roomIndex);
			NotNull(room);
			
			if (operation->type == ExportOperationType_Delete)
			{
				DynArrayClear(&room->backgrounds);
				DynArrayClear(&room->views);
				DynArrayClear(&room->objects);
				DynArrayClear(&room->tiles);
				for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
				{
					GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
					NotNull(layer);
					if (layer->type == GmsEnum_LayerType_Background)
					{
						DestroyDynamicArray(&layer->backgrounds);
					}
					else if (layer->type == GmsEnum_LayerType_Instance)
					{
						DestroyDynamicArray(&layer->instances);
					}
					else if (layer->type == GmsEnum_LayerType_Tile)
					{
						DestroyDynamicArray(&layer->tiles);
					}
				}
				DynArrayClear(&room->layers);
				if (room->name != nullptr) { ArenaPop(mainHeap, room->name); }
				room->name = ArenaNtString(mainHeap, "prototype");
				room->deleted = true;
				numRoomsDeleted++;
			}
			else if (operation->type == ExportOperationType_Rename)
			{
				bool isNameInStrings = (GetGmsStringInPack(&main->pack, operation->newName, false) != nullptr);
				if (!isNameInStrings)
				{
					GmsString_t* currentRoomGmsString = GetGmsStringInPack(&main->pack, room->name, false);
					NotNull(currentRoomGmsString);
					if (MyStrLength32(operation->newName) <= currentRoomGmsString->length)
					{
						PrintLine_I("In-place changing the string for room[%u] \"%s\" -> \"%s\"", operation->roomIndex, room->name, operation->newName);
						StringChange_t* newChange = DynArrayAdd(&main->stringChanges, StringChange_t);
						NotNull(newChange);
						newChange->stringIndex = currentRoomGmsString->index;
						newChange->newValue = ArenaNtString(mainHeap, operation->newName);
						newChange->newLength = MyStrLength32(operation->newName);
						NotNull(newChange->newValue);
						isNameInStrings = true;
					}
				}
				
				if (isNameInStrings)
				{
					if (room->name != nullptr) { ArenaPop(mainHeap, room->name); }
					room->name = ArenaNtString(mainHeap, operation->newName);
					NotNull(room->name);
					numRoomsRenamed++;
				}
				else
				{
					PrintLine_E("Couldn't find the desired name \"%s\" in the existing pack strings. Please choose a name that already exists for another room, object, or sprite", operation->newName);
				}
			}
			else if (operation->type == ExportOperationType_Replace)
			{
				bool takeLevelTitle = false;
				if (ReplaceRoomFromLevelFile(mainHeap, &main->pack, operation->levelFilePath, room, takeLevelTitle))
				{
					numRoomsReplaced++;
				}
				else
				{
					PrintLine_E("Failed to replace room[%u] %u/%u \"%s\" with level file at \"%s\"", operation->roomIndex, numRoomsReplaced+1, main->exportNumReplaces, room->name, operation->levelFilePath);
				}
			}
		}
		
		// +==============================+
		// |        Change Strings        |
		// +==============================+
		if (main->stringChanges.length > 0)
		{
			for (u32 sIndex = 0; sIndex < main->stringChanges.length; sIndex++)
			{
				StringChange_t* stringChange = DynArrayGet(&main->stringChanges, StringChange_t, sIndex);
				NotNull(stringChange);
				NotNull(stringChange->newValue);
				GmsString_t* gmsString = DynArrayGet(&main->pack.strings, GmsString_t, stringChange->stringIndex);
				NotNull(gmsString);
				Assert(stringChange->newLength <= gmsString->length);
				if (ReplacePortionOfFile(filePath, gmsString->charsOffset, stringChange->newValue, stringChange->newLength+1))
				{
					PrintLine_I("Replaced string[%u] at 0x%08X (%u chars -> %u chars)", stringChange->stringIndex, gmsString->charsOffset, gmsString->length, stringChange->newLength);
					if (gmsString->replacedValue != nullptr) { ArenaPop(mainHeap, gmsString->replacedValue); }
					gmsString->replacedValue = ArenaNtString(mainHeap, stringChange->newValue);
				}
				else
				{
					PrintLine_E("Failed to replace string[%u] at 0x%08X (%u chars -> %u chars) in \"%s\"", stringChange->stringIndex, gmsString->charsOffset, gmsString->length, stringChange->newLength, filePath);
				}
			}
		}
		
		u32 newChunkSize = 0;
		u8* newChunkContents = SerializeGmsRoomsChunk(TempArena, &main->pack, &newRooms, main->pack.roomChunkAddress, main->pack.roomChunkSize, &newChunkSize);
		if (newChunkContents != nullptr)
		{
			if (newChunkSize <= main->pack.roomChunkSize)
			{
				if (ReplacePortionOfFile(filePath, main->pack.roomChunkAddress, newChunkContents, newChunkSize))
				{
					PrintLine_I("Successfully injected %s ROOM chunk into %s original section", FormattedSizeStr(newChunkSize), FormattedSizeStr(main->pack.roomChunkSize));
				}
				else
				{
					PrintLine_E("Failed to replace %s ROOM chunk with %s new chunk contents", FormattedSizeStr(main->pack.roomChunkSize), FormattedSizeStr(newChunkSize));
					numRoomsReplaced = 0;
					numRoomsRenamed = 0;
					numRoomsDeleted = 0;
				}
			}
			else
			{
				PrintLine_E("The total list of rooms is too big by %s and does not fit in the original chunk section. Please delete rooms or make some rooms less complicated", FormattedSizeStr(newChunkSize - main->pack.roomChunkSize));
				numRoomsReplaced = 0;
				numRoomsRenamed = 0;
				numRoomsDeleted = 0;
			}
		}
		else
		{
			PrintLine_E("Failed to serialize %u rooms into a new chunk", newRooms.length);
		}
		
		DestroyGmsRoomList(mainHeap, &newRooms);
		TempPopMark();
	}
	else
	{
		for (u32 oIndex = 0; oIndex < main->exportOperations.length; oIndex++)
		{
			ExportOperation_t* operation = DynArrayGet(&main->exportOperations, ExportOperation_t, oIndex);
			NotNull(operation);
			NotNull(operation->roomPntr);
			GmsRoom_t* room = operation->roomPntr;
			
			if (operation->type == ExportOperationType_Replace)
			{
				PrintLine_D("Deserializing room %u/%u at \"%s\"", numRoomsReplaced+1, main->exportNumReplaces, operation->levelFilePath);
				GmsRoom_t newRoom = {};
				if (!DeserializePcqLevel(&main->pack, operation->levelFilePath, mainHeap, &newRoom, room))
				{
					main->dialogueFunc("PCQ Level File Deserialization Failed", TempPrint(
						"We were unable to properly deserialize one of the PCQ level files.\n"
						"Check the debug output log for more information on the deserialization failure\n"
						"Room Name: \"%s\"\n"
						"Level File Path: \"%s\"",
						room->name, operation->levelFilePath)
					);
					DestroyExportList();
					main->exportStarted = false;
					return false;
				}
				WriteLine_I("Deserialization Success!");
				
				#if 0
				GmsRoomObject_t* firstObj = DynArrayGet(&room->objects, GmsRoomObject_t, 0);
				NotNull(firstObj);
				main->pack.nextInstanceId = firstObj->instanceId;
				GmsRoomLayer_t* firstLayer = DynArrayGet(&room->layers, GmsRoomLayer_t, 0);
				NotNull(firstLayer);
				main->pack.nextLayerId = firstLayer->id;
				#endif
				
				u32 serializationAddress = room->offset;
				u32 maxSerializationSize = room->endOffset - room->offset;
				PrintLine_D("Serializing room %u/%u at offset 0x%08X", numRoomsReplaced+1, main->exportNumReplaces, serializationAddress);
				u32 roomSerializationSize = 0;
				u8* roomSerializationPntr = SerializeGmsRoom(mainHeap, &main->pack, &newRoom, serializationAddress, &roomSerializationSize);
				if (roomSerializationPntr == nullptr)
				{
					main->dialogueFunc("Failed to serialize a Room", TempPrint(
						"We were unable to properly serialize on of the Rooms\n"
						"Check the debug output log for more information on the serialization failure\n"
						"Room Name: \"%s\"\n"
						"Level File Path: \"%s\"",
						room->name, operation->levelFilePath)
					);
					DestroyGmsRoom(&newRoom, mainHeap);
					DestroyExportList();
					main->exportStarted = false;
					return false;
				}
				PrintLine_D("Serialized room %u/%u to %s (%u bytes)", numRoomsReplaced+1, main->exportNumReplaces, FormattedSizeStr(roomSerializationSize), roomSerializationSize);
				if (roomSerializationSize > maxSerializationSize)
				{
					main->dialogueFunc("Room Serialization Too Large", TempPrint(
						"One of the rooms that is being replaced has too many entities in it.\n"
						"This program only support replacing rooms with new rooms that are of equal or lesser size in the file in order to maintain file offset integrity throughout the rest of the file\n"
						"Room Name: \"%s\"\n"
						"Level File Path: \"%s\""
						"Available Space: %s (%u bytes)"
						"Serialized Size: %s (%u bytes)",
						room->name, operation->levelFilePath, FormattedSizeStr(maxSerializationSize), maxSerializationSize, FormattedSizeStr(roomSerializationSize), roomSerializationSize)
					);
					DestroyGmsRoom(&newRoom, mainHeap);
					ArenaPop(mainHeap, roomSerializationPntr);
					DestroyExportList();
					main->exportStarted = false;
					return false;
				}
				WriteLine_I("Serialization Success!");
				
				DestroyGmsRoom(&newRoom, mainHeap);
				Assert(roomSerializationSize > 0);
				
				PrintLine_D("Injecting room %u/%u into space %s (%u bytes)", numRoomsReplaced+1, main->exportNumReplaces, FormattedSizeStr(maxSerializationSize), maxSerializationSize);
				if (!ReplacePortionOfFile(filePath, room->offset, roomSerializationPntr, roomSerializationSize))
				{
					main->dialogueFunc("Failed to Inject Room", TempPrint(
						"One of the rooms failed to be injected into the output file\n"
						"Check the debug output log for more information on the injection failure\n"
						"Room Name: \"%s\"\n"
						"Level File Path: \"%s\""
						"Inject Offset: 0x%08X"
						"Inject Size: %s (%u bytes)",
						room->name, operation->levelFilePath, room->offset, FormattedSizeStr(roomSerializationSize), roomSerializationSize)
					);
					ArenaPop(mainHeap, roomSerializationPntr);
					DestroyExportList();
					main->exportStarted = false;
					return false;
				}
				//TODO: Should we fill up any extra unused space with 0xFF or similar?
				WriteLine_I("Injection Success!");
				
				ArenaPop(mainHeap, roomSerializationPntr);
				
				numRoomsReplaced++;
			}
			else if (operation->type == ExportOperationType_Rename)
			{
				PrintLine_D("Trying to rename room[%u] \"%s\" -> \"%s\"...", operation->roomIndex, room->name, operation->newName);
				bool foundNewString = false;
				u32 newStringOffset = 0;
				for (u32 sIndex = 0; sIndex < main->pack.strings.length; sIndex++)
				{
					GmsString_t* gmsString = DynArrayGet(&main->pack.strings, GmsString_t, sIndex);
					NotNull(gmsString);
					if (gmsString->pntr != nullptr && MyStrCompareNt(gmsString->pntr, operation->newName) == 0)
					{
						foundNewString = true;
						newStringOffset = gmsString->charsOffset;
						break;
					}
				}
				if (foundNewString)
				{
					u32 injectionOffset = room->offset + STRUCT_VAR_OFFSET(GmsHeader_Room_t, nameOffset);
					PrintLine_D("Using string at offset 0x%08X (injected in room at offset 0x%08X)", newStringOffset, injectionOffset);
					if (ReplacePortionOfFile(filePath, injectionOffset, &newStringOffset, sizeof(u32)))
					{
						WriteLine_I("Rename success!");
						numRoomsRenamed++;
					}
					else
					{
						PrintLine_E("Failed to write new nameOffset to rename room[%u] \"%s\"", operation->roomIndex, room->name);
					}
				}
				else
				{
					PrintLine_E("Failed to rename room[%u] because we couldn't find \"%s\" in the existing pack strings", operation->roomIndex, operation->newName);
				}
			}
			else if (operation->type == ExportOperationType_Delete)
			{
				//TODO: Implement me?
			}
		}
	}
	
	// +==============================+
	// |    Update nextInstanceId     |
	// +==============================+
	PrintLine_D("Changing the max instance ID from %u to %u", main->pack.genInfo.nextInstanceId, main->pack.nextInstanceId);
	// #define STRUCT_VAR_OFFSET(structureName, variableName)
	u32 nextInstanceIdWriteOffset = main->pack.genInfo.offset + STRUCT_VAR_OFFSET(GmsHeader_Gen8_t, nextInstanceId);
	if (!ReplacePortionOfFile(filePath, nextInstanceIdWriteOffset, &main->pack.nextInstanceId, sizeof(i32)))
	{
		main->dialogueFunc("Failed to change max instance ID", TempPrint(
			"We failed to write the max instance ID to the gen info header\n"
			"Check the debug output log for more information on the write failure\n"
			"Gen Header Offset: 0x%08X",
			main->pack.genInfo.offset)
		);
		DestroyExportList();
		main->exportStarted = false;
		return false;
	}
	
	if (main->exportNumReplaces > 0)
	{
		if (numRoomsReplaced == main->exportNumReplaces)
		{
			PrintLine_I("Replaced %u room%s successfully!", main->exportNumReplaces, (main->exportNumReplaces == 1) ? "" : "s");
		}
		else
		{
			PrintLine_E("Failed to replace all rooms. Only %u/%u successful", numRoomsReplaced, main->exportNumReplaces);
			main->dialogueFunc("Failed to replace all rooms", TempPrint("Only %u/%u successful replaced", numRoomsReplaced, main->exportNumReplaces));
		}
	}
	if (main->exportNumRenames > 0)
	{
		if (numRoomsRenamed == main->exportNumRenames)
		{
			PrintLine_I("Renamed %u room%s successfully!", main->exportNumRenames, (main->exportNumRenames == 1) ? "" : "s");
		}
		else
		{
			PrintLine_E("Failed to rename all rooms. Only %u/%u successful", numRoomsRenamed, main->exportNumRenames);
			main->dialogueFunc("Failed to rename all rooms", TempPrint("Only %u/%u successful renamed", numRoomsRenamed, main->exportNumRenames));
		}
	}
	
	DestroyExportList();
	main->exportStarted = false;
	return true;
}

// +--------------------------------------------------------------+
// |                     Parser_HandleCommand                     |
// +--------------------------------------------------------------+
__declspec(dllexport) bool __stdcall Parser_HandleCommand(const char* commandStr)
{
	NotNull(main);
	NotNull(TempArena);
	TempPushMark();
	bool result = HandleCommand(commandStr);
	TempPopMark();
	return result;
}

#if (ASSERTIONS_ACTIVE && USE_ASSERT_FAILURE_FUNCTION)
//This function is declared in my_assert.h and needs to be implemented by us for a debug build to compile successfully
void AssertFailure(const char* function, const char* filename, int lineNumber, const char* expressionStr)
{
	u32 fileNameStart = 0;
	for (u32 cIndex = 0; filename[cIndex] != '\0'; cIndex++)
	{
		if (filename[cIndex] == '\\' || filename[cIndex] == '/')
		{
			fileNameStart = cIndex+1;
		}
	}
	if (TempArena != nullptr)
	{
		char* message = TempPrint("An Assertion failed in %s:\n\"%s\" line %d:\n\n(%s) is not true", function, &filename[fileNameStart], lineNumber, expressionStr);
		WriteLine_E(message);
		if (main != nullptr && main->dialogueFunc != nullptr)
		{
			main->dialogueFunc("Assertion Failure!", message);
		}
	}
}
#endif
