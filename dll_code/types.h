/*
File:   types.h
Author: Taylor Robbins
Date:   12\13\2020
*/

#ifndef _TYPES_H
#define _TYPES_H

typedef void __stdcall DialogueCallback_f(const char* text, const char* caption);
typedef void __stdcall DebugOutputCallback_f(DbgLevel_t level, const char* message, bool newLine);

struct OpenFile_t
{
	bool isOpen;
	HANDLE handle;
	u32 numBytesRead;
};
struct File_t
{
	u64 size;
	u8* data;
	u64 readCursor;
};

typedef enum
{
	ParsingTokenType_Unknown = 0,
	ParsingTokenType_Header,
	ParsingTokenType_KeyValue,
	ParsingTokenType_ValueList,
	ParsingTokenType_Comment,
	ParsingTokenType_Other,
	ParsingTokenType_NumTypes,
} ParsingTokenType_t;

struct ParsingToken_t
{
	ParsingTokenType_t type;
	u32 start;
	u32 length;
	union
	{
		StrSplitPiece_t pieces[2];
		struct { StrSplitPiece_t header;  StrSplitPiece_t unused1; };
		struct { StrSplitPiece_t content; StrSplitPiece_t unused2; };
		struct { StrSplitPiece_t key;     StrSplitPiece_t value;   };
		struct { StrSplitPiece_t comment; StrSplitPiece_t unused3;   };
	};
};

struct TextParser_t
{
	const char* content;
	u32 contentLength;
	u32 position;
	u32 lineNumber;
	bool justEscaped;
	u32 numTokensFound;
	bool foundToken;
	u32 lineStart;
};

#endif //  _TYPES_H
