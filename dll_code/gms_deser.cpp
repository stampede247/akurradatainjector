/*
File:   gms_deser.cpp
Author: Taylor Robbins
Date:   12\13\2020
Description: 
	** Handles parsing the data.win file using the GmsHeader_ structures and expanding the data
	** into a more usable GmsPack_t structure
*/

// #define DATA_FILE_PATH "..\\..\\data_files\\data.win"
// #define DATA_FILE_PATH "..\\akurra.win"

// #define OUTPUT_DATA_FILE_PATH "..\\data.win"
#define OUTPUT_DATA_FILE_PATH "C:\\Users\\robbitay\\Documents\\MyStuff\\Games\\Akurra Demo v3.0f\\data.win"

#define REPLACE_LEVEL_NAME "rm_stone_e3"

// #define LEVEL_FILE_PATH "..\\Akurra_Level.lvl"
#define LEVEL_FILE_PATH "C:\\Users\\robbitay\\AppData\\Roaming\\PrincessCastleQuest\\Levels\\Player\\rm_stone_e3.lvl"

// +--------------------------------------------------------------+
// |                           Destroy                            |
// +--------------------------------------------------------------+
void DestroyGmsSprite(GmsSprite_t* sprite, MemoryArena_t* allocArena)
{
	NotNull(sprite);
	NotNull(allocArena);
	if (sprite->name != nullptr) { ArenaPop(allocArena, sprite->name); }
	DestroyDynamicArray(&sprite->frames);
}
void DestroyGmsObject(GmsObject_t* object, MemoryArena_t* allocArena)
{
	NotNull(object);
	NotNull(allocArena);
	if (object->name != nullptr) { ArenaPop(allocArena, object->name); }
}
void DestroyGmsSound(GmsSound_t* sound, MemoryArena_t* allocArena)
{
	NotNull(sound);
	NotNull(allocArena);
	if (sound->name != nullptr) { ArenaPop(allocArena, sound->name); }
	if (sound->fileType != nullptr) { ArenaPop(allocArena, sound->fileType); }
	if (sound->fileName != nullptr) { ArenaPop(allocArena, sound->fileName); }
}
void DestroyGmsRoom(GmsRoom_t* room, MemoryArena_t* allocArena)
{
	NotNull(room);
	NotNull(allocArena);
	if (room->name != nullptr) { ArenaPop(allocArena, room->name); }
	if (room->caption != nullptr) { ArenaPop(allocArena, room->caption); }
	for (u32 bIndex = 0; bIndex < room->backgrounds.length; bIndex++)
	{
		GmsRoomBackground_t* background = DynArrayGet(&room->backgrounds, GmsRoomBackground_t, bIndex);
		NotNull(background);
		//NOTE: Currently there is nothing we need to deallocate
	}
	DestroyDynamicArray(&room->backgrounds);
	for (u32 vIndex = 0; vIndex < room->views.length; vIndex++)
	{
		GmsView_t* view = DynArrayGet(&room->views, GmsView_t, vIndex);
		NotNull(view);
		//NOTE: Currently there is nothing we need to deallocate
	}
	DestroyDynamicArray(&room->views);
	for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
	{
		GmsRoomObject_t* object = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex);
		NotNull(object);
		//NOTE: Currently there is nothing we need to deallocate
	}
	DestroyDynamicArray(&room->objects);
	for (u32 tIndex = 0; tIndex < room->tiles.length; tIndex++)
	{
		GmsRoomTile_t* tile = DynArrayGet(&room->tiles, GmsRoomTile_t, tIndex);
		NotNull(tile);
		//NOTE: Currently there is nothing we need to deallocate
	}
	DestroyDynamicArray(&room->tiles);
	for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
	{
		GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
		NotNull(layer);
		if (layer->type == GmsEnum_LayerType_Background)
		{
			for (u32 bIndex = 0; bIndex < layer->backgrounds.length; bIndex++)
			{
				GmsRoomLayerBackground_t* background = DynArrayGet(&layer->backgrounds, GmsRoomLayerBackground_t, bIndex);
				NotNull(background);
				//NOTE: Currently there is nothing we need to deallocate
			}
			DestroyDynamicArray(&layer->backgrounds);
		}
		else if (layer->type == GmsEnum_LayerType_Instance)
		{
			DestroyDynamicArray(&layer->instances);
		}
		else if (layer->type == GmsEnum_LayerType_Asset)
		{
			DestroyDynamicArray(&layer->assetList0);
			DestroyDynamicArray(&layer->assetList1);
			DestroyDynamicArray(&layer->assetList2);
			DestroyDynamicArray(&layer->assetList3);
		}
		else if (layer->type == GmsEnum_LayerType_Tile)
		{
			DestroyDynamicArray(&layer->tiles);
		}
	}
	DestroyDynamicArray(&room->layers);
}
void DestroyGmsString(GmsString_t* string, MemoryArena_t* allocArena)
{
	NotNull(string);
	NotNull(allocArena);
	if (string->pntr != nullptr) { ArenaPop(allocArena, string->pntr); }
	if (string->replacedValue != nullptr) { ArenaPop(allocArena, string->replacedValue); }
}
void DestroyGmsTexture(GmsTexture_t* texture, MemoryArena_t* allocArena)
{
	NotNull(texture);
	NotNull(allocArena);
	if (texture->pngData != nullptr) { ArenaPop(allocArena, texture->pngData); }
}
void DestroyGmsAudio(GmsAudio_t* audio, MemoryArena_t* allocArena)
{
	NotNull(audio);
	NotNull(allocArena);
	if (audio->wavData != nullptr) { ArenaPop(allocArena, audio->wavData); }
}
void DestroyGmsBackground(GmsBackground_t* background, MemoryArena_t* allocArena)
{
	NotNull(background);
	NotNull(allocArena);
	if (background->name != nullptr) { ArenaPop(allocArena, background->name); }
}
void DestroyGmsPath(GmsPath_t* path, MemoryArena_t* allocArena)
{
	NotNull(path);
	NotNull(allocArena);
	if (path->name != nullptr) { ArenaPop(allocArena, path->name); }
	DestroyDynamicArray(&path->points);
}
void DestroyGmsScript(GmsScript_t* script, MemoryArena_t* allocArena)
{
	NotNull(script);
	NotNull(allocArena);
	if (script->name != nullptr) { ArenaPop(allocArena, script->name); }
}
void DestroyGmsFont(GmsFont_t* font, MemoryArena_t* allocArena)
{
	NotNull(font);
	NotNull(allocArena);
	if (font->name != nullptr) { ArenaPop(allocArena, font->name); }
	if (font->fileName != nullptr) { ArenaPop(allocArena, font->fileName); }
	DestroyDynamicArray(&font->glyphs);
}
void DestroyGmsTexturePart(GmsTexturePart_t* texturePart, MemoryArena_t* allocArena)
{
	NotNull(texturePart);
	NotNull(allocArena);
	//NOTE: Nothing to deallocate here yet
}
void DestroyGmsCode(GmsCode_t* code, MemoryArena_t* allocArena)
{
	NotNull(code);
	NotNull(allocArena);
	if (code->name != nullptr) { ArenaPop(allocArena, code->name); }
	if (code->bytecodePntr != nullptr) { ArenaPop(allocArena, code->bytecodePntr); }
	DestroyDynamicArray(&code->variableIndices);
	DestroyDynamicArray(&code->functionIndices);
}
void DestroyGmsVariable(GmsVariable_t* variable, MemoryArena_t* allocArena)
{
	NotNull(variable);
	NotNull(allocArena);
	if (variable->name != nullptr) { ArenaPop(allocArena, variable->name); }
}
void DestroyGmsFunction(GmsFunction_t* function, MemoryArena_t* allocArena)
{
	NotNull(function);
	NotNull(allocArena);
	if (function->name != nullptr) { ArenaPop(allocArena, function->name); }
	DestroyDynamicArray(&function->codeRefOffsets);
}
void DestroyGmsPack(GmsPack_t* pack, MemoryArena_t* allocArena)
{
	NotNull(pack);
	NotNull(allocArena);
	
	for (u32 sIndex = 0; sIndex < pack->sprites.length; sIndex++)
	{
		GmsSprite_t* sprite = DynArrayGet(&pack->sprites, GmsSprite_t, sIndex);
		NotNull(sprite);
		DestroyGmsSprite(sprite, allocArena);
	}
	DestroyDynamicArray(&pack->sprites);
	
	for (u32 oIndex = 0; oIndex < pack->objects.length; oIndex++)
	{
		GmsObject_t* object = DynArrayGet(&pack->objects, GmsObject_t, oIndex);
		NotNull(object);
		DestroyGmsObject(object, allocArena);
	}
	DestroyDynamicArray(&pack->objects);
	
	for (u32 sIndex = 0; sIndex < pack->sounds.length; sIndex++)
	{
		GmsSound_t* sound = DynArrayGet(&pack->sounds, GmsSound_t, sIndex);
		NotNull(sound);
		DestroyGmsSound(sound, allocArena);
	}
	DestroyDynamicArray(&pack->sounds);
	
	for (u32 rIndex = 0; rIndex < pack->rooms.length; rIndex++)
	{
		GmsRoom_t* room = DynArrayGet(&pack->rooms, GmsRoom_t, rIndex);
		NotNull(room);
		DestroyGmsRoom(room, allocArena);
	}
	DestroyDynamicArray(&pack->rooms);
	
	for (u32 sIndex = 0; sIndex < pack->strings.length; sIndex++)
	{
		GmsString_t* string = DynArrayGet(&pack->strings, GmsString_t, sIndex);
		NotNull(string);
		DestroyGmsString(string, allocArena);
	}
	DestroyDynamicArray(&pack->strings);
	
	for (u32 tIndex = 0; tIndex < pack->textures.length; tIndex++)
	{
		GmsTexture_t* texture = DynArrayGet(&pack->textures, GmsTexture_t, tIndex);
		NotNull(texture);
		DestroyGmsTexture(texture, allocArena);
	}
	DestroyDynamicArray(&pack->textures);
	
	for (u32 aIndex = 0; aIndex < pack->audios.length; aIndex++)
	{
		GmsAudio_t* audio = DynArrayGet(&pack->audios, GmsAudio_t, aIndex);
		NotNull(audio);
		DestroyGmsAudio(audio, allocArena);
	}
	DestroyDynamicArray(&pack->audios);
	
	for (u32 bIndex = 0; bIndex < pack->backgrounds.length; bIndex++)
	{
		GmsBackground_t* background = DynArrayGet(&pack->backgrounds, GmsBackground_t, bIndex);
		NotNull(background);
		DestroyGmsBackground(background, allocArena);
	}
	DestroyDynamicArray(&pack->backgrounds);
	
	for (u32 pIndex = 0; pIndex < pack->paths.length; pIndex++)
	{
		GmsPath_t* path = DynArrayGet(&pack->paths, GmsPath_t, pIndex);
		NotNull(path);
		DestroyGmsPath(path, allocArena);
	}
	DestroyDynamicArray(&pack->paths);
	
	for (u32 sIndex = 0; sIndex < pack->scripts.length; sIndex++)
	{
		GmsScript_t* script = DynArrayGet(&pack->scripts, GmsScript_t, sIndex);
		NotNull(script);
		DestroyGmsScript(script, allocArena);
	}
	DestroyDynamicArray(&pack->scripts);
	
	for (u32 fIndex = 0; fIndex < pack->fonts.length; fIndex++)
	{
		GmsFont_t* font = DynArrayGet(&pack->fonts, GmsFont_t, fIndex);
		NotNull(font);
		DestroyGmsFont(font, allocArena);
	}
	DestroyDynamicArray(&pack->fonts);
	
	for (u32 tIndex = 0; tIndex < pack->textureParts.length; tIndex++)
	{
		GmsTexturePart_t* texturePart = DynArrayGet(&pack->textureParts, GmsTexturePart_t, tIndex);
		NotNull(texturePart);
		DestroyGmsTexturePart(texturePart, allocArena);
	}
	DestroyDynamicArray(&pack->textureParts);
	
	for (u32 cIndex = 0; cIndex < pack->codes.length; cIndex++)
	{
		GmsCode_t* code = DynArrayGet(&pack->codes, GmsCode_t, cIndex);
		NotNull(code);
		DestroyGmsCode(code, allocArena);
	}
	DestroyDynamicArray(&pack->codes);
	
	for (u32 vIndex = 0; vIndex < pack->variables.length; vIndex++)
	{
		GmsVariable_t* variable = DynArrayGet(&pack->variables, GmsVariable_t, vIndex);
		NotNull(variable);
		DestroyGmsVariable(variable, allocArena);
	}
	DestroyDynamicArray(&pack->variables);
	
	for (u32 fIndex = 0; fIndex < pack->functions.length; fIndex++)
	{
		GmsFunction_t* function = DynArrayGet(&pack->functions, GmsFunction_t, fIndex);
		NotNull(function);
		DestroyGmsFunction(function, allocArena);
	}
	DestroyDynamicArray(&pack->functions);
	
	ClearPointer(pack);
}

// +--------------------------------------------------------------+
// |                         Deserialize                          |
// +--------------------------------------------------------------+
bool DeserializeGmsPackFile(const char* filePath, MemoryArena_t* memArena, GmsPack_t* packOut)
{
	Assert(filePath != nullptr);
	Assert(memArena != nullptr);
	Assert(packOut != nullptr);
	ClearPointer(packOut);
	
	File_t gmsFile = {};
	if (!ReadEntireFile(filePath, &gmsFile))
	{
		PrintLine_E("Failed to open/find pack file at \"%s\"", filePath);
		return false;
	}
	
	packOut->nextLayerId = 1;
	packOut->nextInstanceId = 1;
	
	CreateDynamicArray(&packOut->sprites,      memArena, sizeof(GmsSprite_t));
	CreateDynamicArray(&packOut->objects,      memArena, sizeof(GmsObject_t));
	CreateDynamicArray(&packOut->rooms,        memArena, sizeof(GmsRoom_t));
	CreateDynamicArray(&packOut->strings,      memArena, sizeof(GmsString_t));
	CreateDynamicArray(&packOut->textures,     memArena, sizeof(GmsTexture_t));
	CreateDynamicArray(&packOut->audios,       memArena, sizeof(GmsAudio_t));
	CreateDynamicArray(&packOut->sounds,       memArena, sizeof(GmsSound_t));
	CreateDynamicArray(&packOut->backgrounds,  memArena, sizeof(GmsBackground_t));
	CreateDynamicArray(&packOut->paths,        memArena, sizeof(GmsPath_t));
	CreateDynamicArray(&packOut->scripts,      memArena, sizeof(GmsScript_t));
	CreateDynamicArray(&packOut->fonts,        memArena, sizeof(GmsFont_t));
	CreateDynamicArray(&packOut->textureParts, memArena, sizeof(GmsTexturePart_t));
	CreateDynamicArray(&packOut->codes,        memArena, sizeof(GmsCode_t));
	CreateDynamicArray(&packOut->variables,    memArena, sizeof(GmsVariable_t));
	CreateDynamicArray(&packOut->functions,    memArena, sizeof(GmsFunction_t));
	
	u32 chunkIndex = 0;
	while (gmsFile.readCursor < gmsFile.size)
	{
		ConsumeFileStruct(&gmsFile, GmsHeader_Chunk_t, chunkHeader);
		u32 chunkOffset = GetFilePntrOffset(&gmsFile, chunkHeader);
		u32 chunkDataOffset = chunkOffset + sizeof(GmsHeader_Chunk_t);
		PrintLine_D("Chunk[%u]: \"%.*s\" %s bytes at (0x%08X-0x%08X)", chunkIndex, ArrayCount(chunkHeader->type), &chunkHeader->type[0], FormattedSizeStr(chunkHeader->size), chunkDataOffset, chunkDataOffset + chunkHeader->size);
		u64 nextReadCursor = gmsFile.readCursor + chunkHeader->size;
		if (IsChunkType(chunkHeader, "FORM")) { nextReadCursor = gmsFile.readCursor; }
		Assert(nextReadCursor <= gmsFile.size);
		
		if (IsChunkType(chunkHeader, "FORM"))
		{
			//don't really need to do anything here
		}
		
		// +--------------------------------------------------------------+
		// |                        Parse Gen Info                        |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "GEN8"))
		{
			ConsumeFileStruct(&gmsFile, GmsHeader_Gen8_t, genHeader);
			u32 headerOffset = GetFilePntrOffset(&gmsFile, genHeader);
			
			// void CreateDynamicArray(DynArray_t* array, MemoryArena_t* memArena, u32 itemSize, u32 allocChunkSize = 16, u32 initialSizeRequirement = 0)
			ConsumeFileType(&gmsFile, u32, numNumbers);
			
			const char* fileNameStr    = GetFileString(&gmsFile, genHeader->fileNameOffset);
			const char* configStr      = GetFileString(&gmsFile, genHeader->configStrOffset);
			const char* nameStr        = GetFileString(&gmsFile, genHeader->nameOffset);
			const char* displayNameStr = GetFileString(&gmsFile, genHeader->displayNameOffset);
			
			PrintLine_D("Generation Info (%s at 0x%08X) expecting %s:", FormattedSizeStr(chunkHeader->size), chunkDataOffset, FormattedSizeStr(sizeof(GmsHeader_Gen8_t)));
			// PrintLine_D("  isDebug:            %s", genHeader->isDebug ? "True" : "False");
			// PrintLine_D("  unknown1:           [ %02X %02X %02X ]", genHeader->unknown1[0], genHeader->unknown1[1], genHeader->unknown1[2]);
			// PrintLine_D("  fileName:           \"%s\"", fileNameStr);
			// PrintLine_D("  configStr:          \"%s\"", configStr);
			// PrintLine_D("  unknown2:           0x%08X", genHeader->unknown2);
			// PrintLine_D("  unknown3:           0x%08X", genHeader->unknown3);
			// PrintLine_D("  unknown4:           [0x%08X 0x%08X 0x%08X 0x%08X 0x%08X]", genHeader->unknown4[0], genHeader->unknown4[1], genHeader->unknown4[2], genHeader->unknown4[3], genHeader->unknown4[4]);
			// PrintLine_D("  nameStr:            \"%s\"", nameStr);
			// PrintLine_D("  version:            %u.%u.%u(%u)", genHeader->versionMajor, genHeader->versionMinor, genHeader->versionRelease, genHeader->versionBuild);
			// PrintLine_D("  defaultWindowSize:  (%u, %u)", genHeader->defaultWindowWidth, genHeader->defaultWindowHeight);
			// PrintLine_D("  unknown5:           0x%08X", genHeader->unknown5);
			// PrintLine_D("  unknown6:           0x%08X", genHeader->unknown6);
			// PrintLine_D("  unknown7:           0x%08X", genHeader->unknown7);
			// PrintLine_D("  unknown8:           [0x%08X 0x%08X 0x%08X]", genHeader->unknown8[0], genHeader->unknown8[1], genHeader->unknown8[2]);
			// PrintLine_D("  timestamp:          %lu", genHeader->timestamp);
			// PrintLine_D("  displayName:        \"%s\"", displayNameStr);
			// PrintLine_D("  unknown11:          [0x%08X 0x%08X 0x%08X]", genHeader->unknown11[0], genHeader->unknown11[1], genHeader->unknown11[2]);
			// PrintLine_D("  unknown12:          0x%08X", genHeader->unknown12);
			// PrintLine_D("  unknown13:          0x%08X", genHeader->unknown13);
			// PrintLine_D("  unknown14:          0x%08X", genHeader->unknown14);
			// PrintLine_D("  %u numbers in this header:", numNumbers);
			
			GmsGenInfo_t* genInfo = &packOut->genInfo;
			ClearPointer(genInfo);
			genInfo->offset = headerOffset;
			if (fileNameStr    != nullptr) { genInfo->fileName    = ArenaNtString(memArena, fileNameStr);    }
			if (configStr      != nullptr) { genInfo->configStr   = ArenaNtString(memArena, configStr);      }
			if (nameStr        != nullptr) { genInfo->name        = ArenaNtString(memArena, nameStr);        }
			if (displayNameStr != nullptr) { genInfo->displayName = ArenaNtString(memArena, displayNameStr); }
			genInfo->isDebug = (genHeader->isDebug != 0);
			genInfo->unknown1[0] = genHeader->unknown1[0];
			genInfo->unknown1[1] = genHeader->unknown1[1];
			genInfo->unknown1[2] = genHeader->unknown1[2];
			genInfo->nextInstanceId = genHeader->nextInstanceId;
			genInfo->unknown3 = genHeader->unknown3;
			genInfo->unknown4[0] = genHeader->unknown4[0];
			genInfo->unknown4[1] = genHeader->unknown4[1];
			genInfo->unknown4[2] = genHeader->unknown4[2];
			genInfo->unknown4[3] = genHeader->unknown4[3];
			genInfo->unknown4[4] = genHeader->unknown4[4];
			genInfo->versionMajor = genHeader->versionMajor;
			genInfo->versionMinor = genHeader->versionMinor;
			genInfo->versionRelease = genHeader->versionRelease;
			genInfo->versionBuild = genHeader->versionBuild;
			genInfo->defaultWindowWidth = genHeader->defaultWindowWidth;
			genInfo->defaultWindowHeight = genHeader->defaultWindowHeight;
			genInfo->unknown5 = genHeader->unknown5;
			genInfo->unknown6 = genHeader->unknown6;
			genInfo->unknown7 = genHeader->unknown7;
			genInfo->unknown8[0] = genHeader->unknown8[0];
			genInfo->unknown8[1] = genHeader->unknown8[1];
			genInfo->unknown8[2] = genHeader->unknown8[2];
			genInfo->timestamp = genHeader->timestamp;
			genInfo->unknown11[0] = genHeader->unknown11[0];
			genInfo->unknown11[1] = genHeader->unknown11[1];
			genInfo->unknown11[2] = genHeader->unknown11[2];
			genInfo->unknown12 = genHeader->unknown12;
			genInfo->unknown13 = genHeader->unknown13;
			genInfo->unknown14 = genHeader->unknown14;
			genInfo->filled = true;
			//TODO: Fill the numbers DynArray_t
			
			CreateDynamicArray(&genInfo->numbers, memArena, sizeof(u32), 16, numNumbers);
			for (u32 nIndex = 0; nIndex < numNumbers; nIndex++)
			{
				u32* newNumberSpace = DynArrayAdd(&genInfo->numbers, u32);
				Assert(newNumberSpace != nullptr);
				ConsumeFileType(&gmsFile, u32, newNumber);
				*newNumberSpace = newNumber;
				// PrintLine_D("    Number[%u]: 0x%08X", nIndex, newNumber);
			}
			
			ConsumeFileStruct(&gmsFile, GmsFooter_Gen8_t, genFooter);
			u32 footerOffset = GetFilePntrOffset(&gmsFile, genFooter);
			PrintLine_D("Generation Info (%s at 0x%08X) expecting %s:", FormattedSizeStr(chunkDataOffset + chunkHeader->size - footerOffset), footerOffset, FormattedSizeStr(sizeof(GmsFooter_Gen8_t)));
			// PrintLine_D("  unknown18:          [0x%08X 0x%08X 0x%08X 0x%08X 0x%08X 0x%08X 0x%08X 0x%08X 0x%08X 0x%08X]", genFooter->unknown18[0], genFooter->unknown18[1], genFooter->unknown18[2], genFooter->unknown18[3], genFooter->unknown18[4], genFooter->unknown18[5], genFooter->unknown18[6], genFooter->unknown18[7], genFooter->unknown18[8], genFooter->unknown18[9]);
			// PrintLine_D("  targetFramerate:    %f", genFooter->targetFramerate);
			// PrintLine_D("  unknown20:          0x%08X", genFooter->unknown20);
			// PrintLine_D("  unknown21:          [0x%08X 0x%08X 0x%08X 0x%08X]", genFooter->unknown21[0], genFooter->unknown21[1], genFooter->unknown21[2], genFooter->unknown21[3]);
			// PrintLine_D("  unknown22:          0x%08X", genFooter->unknown22);
			
			// PrintLine_D("  lastObj:            0x%08X", genHeader->lastObj);
			// PrintLine_D("  lastTile:           0x%08X", genHeader->lastTile);
			// PrintLine_D("  gameID:             %u", genHeader->gameID);
			// PrintLine_D("  infoFlags:          0x%08X", genHeader->infoFlags);
			// PrintLine_D("  licenseMD5:         %02X%02X%02X%02X%02X%02X%02X%02X", genHeader->licenseMD5[0], genHeader->licenseMD5[1], genHeader->licenseMD5[2], genHeader->licenseMD5[3], genHeader->licenseMD5[4], genHeader->licenseMD5[5], genHeader->licenseMD5[6], genHeader->licenseMD5[7]);
			// PrintLine_D("  licenseCRC32:       0x%08X", genHeader->licenseCRC32);
			// PrintLine_D("  displayNameOffset:  \"%s\"", displayNameStr);
			// PrintLine_D("  activeTargets:      %u", genHeader->activeTargets);
			// PrintLine_D("  steamAppID:         %u", genHeader->steamAppID);
		}
		
		// +--------------------------------------------------------------+
		// |                         Parse Rooms                          |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "ROOM"))
		{
			packOut->roomChunkAddress = chunkOffset;
			packOut->roomChunkSize = sizeof(GmsHeader_Chunk_t) + chunkHeader->size;
			
			DynArray_t roomHeaderList = {};
			ConsumeFileList(&gmsFile, memArena, &roomHeaderList, GmsHeader_Room_t);
			
			PrintLine_D("  %u Rooms in this chunk", roomHeaderList.length);
			DynArrayExpand(&packOut->rooms, packOut->rooms.length + roomHeaderList.length);
			for (u32 rIndex = 0; rIndex < roomHeaderList.length; rIndex++)
			{
				TempPushMark();
				const GmsHeader_Room_t* roomHeader = *DynArrayGet(&roomHeaderList, const GmsHeader_Room_t*, rIndex);
				Assert(roomHeader != nullptr);
				const GmsHeader_Room_t* nextRoomHeader = ((rIndex+1 < roomHeaderList.length) ? (*DynArrayGet(&roomHeaderList, const GmsHeader_Room_t*, rIndex+1)) : nullptr);
				u32 roomOffset = GetFilePntrOffset(&gmsFile, roomHeader);
				u32 nextRoomOffset = (nextRoomHeader != nullptr) ? GetFilePntrOffset(&gmsFile, nextRoomHeader) : (chunkDataOffset + chunkHeader->size);
				const char* roomName = GetFileString(&gmsFile, roomHeader->nameOffset);
				const char* roomCaption = GetFileString(&gmsFile, roomHeader->captionOffset);
				bool printRoom = false;//(StrCompareIgnoreCaseNt(roomName, "rm_stone_d4") || StrCompareIgnoreCaseNt(roomName, "rm_stone_d0"));
				// PrintLine_D("  Room[%u] \"%s\" (%08X - %08X %s):", rIndex, roomName, roomOffset, nextRoomOffset, FormattedSizeStr(nextRoomOffset - roomOffset));
				// PrintLine_D("    name:    \"%s\"", roomName);
				// PrintLine_D("    size:    (%d,%d)", roomHeader->size.width, roomHeader->size.height);
				
				// PrintLine_D("    end_offset:        0x%08X", roomOffset + sizeof(GmsHeader_Room_t));
				// PrintLine_D("    backgroundsOffset: 0x%08X", roomHeader->backgroundsOffset);
				// PrintLine_D("    viewsOffset:       0x%08X", roomHeader->viewsOffset);
				// PrintLine_D("    objectsOffset:     0x%08X", roomHeader->objectsOffset);
				// PrintLine_D("    tilesOffset:       0x%08X", roomHeader->tilesOffset);
				// PrintLine_D("    layersOffset:      0x%08X", roomHeader->layersOffset);
				// PrintLine_D("    unknownOffset:     0x%08X", roomHeader->unknownOffset);
				
				Assert(roomHeader->backgroundsOffset + sizeof(u32) <= gmsFile.size);
				gmsFile.readCursor = roomHeader->backgroundsOffset;
				DynArray_t backgroundHeadersList = {};
				ConsumeFileList(&gmsFile, memArena, &backgroundHeadersList, GmsHeader_RoomBackground_t);
				
				Assert(roomHeader->viewsOffset + sizeof(u32) <= gmsFile.size);
				gmsFile.readCursor = roomHeader->viewsOffset;
				DynArray_t viewHeadersList = {};
				ConsumeFileList(&gmsFile, memArena, &viewHeadersList, GmsHeader_RoomView_t);
				
				Assert(roomHeader->objectsOffset + sizeof(u32) <= gmsFile.size);
				gmsFile.readCursor = roomHeader->objectsOffset;
				DynArray_t objectHeadersList = {};
				ConsumeFileList(&gmsFile, memArena, &objectHeadersList, GmsHeader_RoomObject_t);
				
				Assert(roomHeader->tilesOffset + sizeof(u32) <= gmsFile.size);
				gmsFile.readCursor = roomHeader->tilesOffset;
				DynArray_t tileHeadersList = {};
				ConsumeFileList(&gmsFile, memArena, &tileHeadersList, GmsHeader_RoomTile_t);
				
				Assert(roomHeader->layersOffset + sizeof(u32) <= gmsFile.size);
				gmsFile.readCursor = roomHeader->layersOffset;
				DynArray_t layerHeadersList = {};
				ConsumeFileList(&gmsFile, memArena, &layerHeadersList, GmsHeader_RoomLayer_t);
				
				bool hasUnknownList = false;
				u32 unknownListOffset = 0;
				u32 unknownListLength = 0;
				if (roomOffset + sizeof(GmsHeader_Room_t) + sizeof(u32) <= roomHeader->backgroundsOffset)
				{
					gmsFile.readCursor = roomOffset + sizeof(GmsHeader_Room_t);
					ConsumeFileType(&gmsFile, u32, unknownOffset);
					Assert(unknownOffset + sizeof(u32) <= gmsFile.size);
					gmsFile.readCursor = unknownOffset;
					DynArray_t unknownHeadersList = {};
					ConsumeFileList(&gmsFile, memArena, &unknownHeadersList, u32);
					if (unknownHeadersList.length > 0)
					{
						PrintLine_D("Room[%u] \"%s\" has %u unknown items", rIndex, roomName, unknownHeadersList.length);
					}
					hasUnknownList = true;
					unknownListOffset = unknownOffset;
					unknownListLength = unknownHeadersList.length;
					DestroyDynamicArray(&unknownHeadersList);
				}
				
				GmsRoom_t* room = DynArrayAdd(&packOut->rooms, GmsRoom_t);
				Assert(room != nullptr);
				ClearPointer(room);
				room->offset = roomOffset;
				room->endOffset = nextRoomOffset;
				if (roomName != nullptr) { room->name = ArenaNtString(memArena, roomName); }
				if (roomCaption != nullptr) { room->caption = ArenaNtString(memArena, roomCaption); }
				room->size                  = roomHeader->size;
				room->isPersistent          = (roomHeader->isPersistent != 0);
				room->creationCodeId        = roomHeader->creationCodeId;
				room->flags                 = roomHeader->flags;
				room->physicsEnabled        = (roomHeader->physicsEnabled != 0);
				room->gravity               = roomHeader->gravity;
				room->metersPerPixel        = roomHeader->metersPerPixel;
				room->unknown1              = roomHeader->unknown1;
				room->unknown2              = roomHeader->unknown2;
				room->unknown3              = roomHeader->unknown3;
				room->unknown4[0]           = roomHeader->unknown4[0];
				room->unknown4[1]           = roomHeader->unknown4[1];
				room->unknown4[2]           = roomHeader->unknown4[2];
				room->unknown4[3]           = roomHeader->unknown4[3];
				room->backgroundsOffset     = roomHeader->backgroundsOffset;
				room->viewsOffset           = roomHeader->viewsOffset;
				room->objectsOffset         = roomHeader->objectsOffset;
				room->tilesOffset           = roomHeader->tilesOffset;
				room->layersOffset          = roomHeader->layersOffset;
				room->hasUnknownList        = hasUnknownList;
				room->unknownListLength     = unknownListLength;
				room->unknownListOffset     = unknownListOffset;
				room->unknownListEndOffset  = nextRoomOffset;
				CreateDynamicArray(&room->backgrounds, memArena, sizeof(GmsRoomBackground_t), 16, backgroundHeadersList.length);
				CreateDynamicArray(&room->views, memArena, sizeof(GmsView_t), 16, viewHeadersList.length);
				CreateDynamicArray(&room->objects, memArena, sizeof(GmsRoomObject_t), 16, objectHeadersList.length);
				CreateDynamicArray(&room->tiles, memArena, sizeof(GmsRoomTile_t), 16, objectHeadersList.length);
				CreateDynamicArray(&room->layers, memArena, sizeof(GmsRoomLayer_t), 16, layerHeadersList.length);
				
				// +==============================+
				// |    Parse Room Background     |
				// +==============================+
				// PrintLine_D("    %u Backgrounds in this room", backgroundHeadersList.length);
				for (u32 bIndex = 0; bIndex < backgroundHeadersList.length; bIndex++)
				{
					const GmsHeader_RoomBackground_t* backgroundHeader = *DynArrayGet(&backgroundHeadersList, const GmsHeader_RoomBackground_t*, bIndex);
					Assert(backgroundHeader != nullptr);
					u32 backgroundOffset = GetFilePntrOffset(&gmsFile, backgroundHeader);
					// PrintLine_D("    Background[%u]:", bIndex);
					// PrintLine_D("      isEnabled:    %s", backgroundHeader->isEnabled ? "True" : "False");
					// PrintLine_D("      isForeground: %s", backgroundHeader->isForeground ? "True" : "False");
					// PrintLine_D("      id:           %u", backgroundHeader->id);
					// PrintLine_D("      position:     (%d, %d)", backgroundHeader->position.x, backgroundHeader->position.y);
					// PrintLine_D("      isTileX:      %s", backgroundHeader->isTileX ? "True" : "False");
					// PrintLine_D("      isTileY:      %s", backgroundHeader->isTileY ? "True" : "False");
					// PrintLine_D("      speed:        (%d, %d)", backgroundHeader->speed.x, backgroundHeader->speed.y);
					// PrintLine_D("      isStretch:    %s", backgroundHeader->isStretch ? "True" : "False");
					
					GmsRoomBackground_t* roomBack = DynArrayAdd(&room->backgrounds, GmsRoomBackground_t);
					Assert(roomBack != nullptr);
					ClearPointer(roomBack);
					roomBack->offset = backgroundOffset;
					roomBack->isEnabled    = (backgroundHeader->isEnabled != 0);
					roomBack->isForeground = (backgroundHeader->isForeground != 0);
					roomBack->id           = backgroundHeader->id;
					roomBack->position     = backgroundHeader->position;
					roomBack->isTileX      = (backgroundHeader->isTileX != 0);
					roomBack->isTileY      = (backgroundHeader->isTileY != 0);
					roomBack->speed        = backgroundHeader->speed;
					roomBack->isStretch    = (backgroundHeader->isStretch != 0);
				}
				DestroyDynamicArray(&backgroundHeadersList);
				
				// +==============================+
				// |       Parse Room Views       |
				// +==============================+
				// PrintLine_D("    %u Views in this room", viewHeadersList.length);
				for (u32 vIndex = 0; vIndex < viewHeadersList.length; vIndex++)
				{
					const GmsHeader_RoomView_t* viewHeader = *DynArrayGet(&viewHeadersList, const GmsHeader_RoomView_t*, vIndex);
					Assert(viewHeader != nullptr);
					u32 viewOffset = GetFilePntrOffset(&gmsFile, viewHeader);
					if (viewHeader->isEnabled)
					{
						// PrintLine_D("    View[%u]:", vIndex);
						// PrintLine_D("      position:     (%d, %d)", viewHeader->position.x,     viewHeader->position.y);
						// PrintLine_D("      size:         (%d, %d)", viewHeader->size.x,         viewHeader->size.y);
						// PrintLine_D("      portPosition: (%d, %d)", viewHeader->portPosition.x, viewHeader->portPosition.y);
						// PrintLine_D("      portSize:     (%d, %d)", viewHeader->portSize.x,     viewHeader->portSize.y);
						// PrintLine_D("      border:       (%d, %d)", viewHeader->border.x,       viewHeader->border.y);
						// PrintLine_D("      speed:        (%d, %d)", viewHeader->speed.x,        viewHeader->speed.y);
						// PrintLine_D("      objectId:     %d", viewHeader->objectId);
					}
					else
					{
						// PrintLine_D("    View[%u]: Disabled", vIndex);
					}
					
					GmsView_t* view = DynArrayAdd(&room->views, GmsView_t);
					Assert(view != nullptr);
					ClearPointer(view);
					view->offset = viewOffset;
					view->isEnabled    = (viewHeader->isEnabled != 0);
					view->position     = viewHeader->position;
					view->size         = viewHeader->size;
					view->portPosition = viewHeader->portPosition;
					view->portSize     = viewHeader->portSize;
					view->border       = viewHeader->border;
					view->speed        = viewHeader->speed;
					view->objectId     = viewHeader->objectId;
				}
				DestroyDynamicArray(&viewHeadersList);
				
				// +==============================+
				// |      Parse Room Objects      |
				// +==============================+
				if (printRoom) { PrintLine_D("    %u Objects in this room", objectHeadersList.length); }
				for (u32 oIndex = 0; oIndex < objectHeadersList.length; oIndex++)
				{
					const GmsHeader_RoomObject_t* objectHeader = *DynArrayGet(&objectHeadersList, const GmsHeader_RoomObject_t*, oIndex);
					Assert(objectHeader != nullptr);
					u32 objectOffset = GetFilePntrOffset(&gmsFile, objectHeader);
					
					GmsObject_t* objectDefPntr = nullptr;
					if (objectHeader->id >= 0 && (u32)objectHeader->id < packOut->objects.length)
					{
						objectDefPntr = DynArrayGet(&packOut->objects, GmsObject_t, (u32)objectHeader->id);
						Assert(objectDefPntr != nullptr);
					}
					
					if (printRoom) { PrintLine_D("    Object[%u]: \"%s\" %u at (%d, %d)", oIndex, (objectDefPntr != nullptr) ? objectDefPntr->name : TempPrint("%u", objectHeader->id), objectHeader->instanceId, objectHeader->position.x, objectHeader->position.y); }
					// PrintLine_D("      position:     (%d, %d)", objectHeader->position.x, objectHeader->position.y);
					// PrintLine_D("      id:           %d", objectHeader->id);
					// PrintLine_D("      instanceId:   %d", objectHeader->instanceId);
					// PrintLine_D("      createCodeId: %d", objectHeader->createCodeId);
					// PrintLine_D("      scale:        %f", objectHeader->scale);
					// PrintLine_D("      imageSpeed:   %f", objectHeader->imageSpeed);
					// PrintLine_D("      imageFrame:   %u", objectHeader->imageFrame);
					// PrintLine_D("      color:        0x%08X", objectHeader->color);
					// PrintLine_D("      rotation:     %f", objectHeader->rotation);
					// PrintLine_D("      unknownId:    %d", objectHeader->unknownId);
					
					GmsRoomObject_t* object = DynArrayAdd(&room->objects, GmsRoomObject_t);
					Assert(object != nullptr);
					ClearPointer(object);
					object->offset = objectOffset;
					object->endOffset = objectOffset + sizeof(GmsHeader_RoomObject_t);
					object->position       = objectHeader->position;
					object->id             = objectHeader->id;
					object->instanceId     = objectHeader->instanceId;
					object->oldCodeId      = objectHeader->oldCodeId;
					object->scale          = objectHeader->scale;
					object->imageSpeed     = objectHeader->imageSpeed;
					object->imageFrame     = objectHeader->imageFrame;
					object->color          = objectHeader->color;
					object->rotation       = objectHeader->rotation;
					object->creationCodeId = objectHeader->creationCodeId;
					
					if (packOut->nextInstanceId <= object->instanceId) { packOut->nextInstanceId = object->instanceId+1; }
				}
				DestroyDynamicArray(&objectHeadersList);
				
				// +==============================+
				// |       Parse Room Tiles       |
				// +==============================+
				// PrintLine_D("    %u Tiles in this room", tileHeadersList.length);
				for (u32 tIndex = 0; tIndex < tileHeadersList.length; tIndex++)
				{
					const GmsHeader_RoomTile_t* tileHeader = *DynArrayGet(&tileHeadersList, const GmsHeader_RoomTile_t*, tIndex);
					Assert(tileHeader != nullptr);
					u32 tileOffset = GetFilePntrOffset(&gmsFile, tileHeader);
					// PrintLine_D("    Tile[%u]:", tIndex);
					// PrintLine_D("      position:       (%d, %d)", tileHeader->position.x, tileHeader->position.y);
					// PrintLine_D("      id:             %d", tileHeader->id);
					// PrintLine_D("      sourcePosition: (%d, %d)", tileHeader->sourcePosition.x, tileHeader->sourcePosition.y);
					// PrintLine_D("      size:           (%d, %d)", tileHeader->size.x, tileHeader->size.y);
					// PrintLine_D("      depth:          %u", tileHeader->depth);
					// PrintLine_D("      instanceId:     %d", tileHeader->instanceId);
					// PrintLine_D("      scale:          (%d, %d)", tileHeader->scale.x, tileHeader->scale.y);
					// PrintLine_D("      color:          0x%08X", tileHeader->color);
					
					GmsRoomTile_t* tile = DynArrayAdd(&room->tiles, GmsRoomTile_t);
					Assert(tile != nullptr);
					ClearPointer(tile);
					tile->offset = tileOffset;
					tile->position       = tileHeader->position;
					tile->id             = tileHeader->id;
					tile->sourcePosition = tileHeader->sourcePosition;
					tile->size           = tileHeader->size;
					tile->depth          = tileHeader->depth;
					tile->instanceId     = tileHeader->instanceId;
					tile->scale          = tileHeader->scale;
					tile->color          = tileHeader->color;
				}
				DestroyDynamicArray(&tileHeadersList);
				
				// +==============================+
				// |      Parse Room Layers       |
				// +==============================+
				// PrintLine_D("    %u Layers in this room", layerHeadersList.length);
				for (u32 lIndex = 0; lIndex < layerHeadersList.length; lIndex++)
				{
					TempPushMark();
					const GmsHeader_RoomLayer_t* layerHeader = *DynArrayGet(&layerHeadersList, const GmsHeader_RoomLayer_t*, lIndex);
					Assert(layerHeader != nullptr);
					const GmsHeader_RoomLayer_t* nextLayerHeader = ((lIndex+1 < layerHeadersList.length) ? (*DynArrayGet(&layerHeadersList, const GmsHeader_RoomLayer_t*, lIndex+1)) : nullptr);
					u32 nextLayerOffset = (nextLayerHeader != nullptr) ? GetFilePntrOffset(&gmsFile, nextLayerHeader) : (hasUnknownList ? unknownListOffset : nextRoomOffset);
					u32 layerOffset = GetFilePntrOffset(&gmsFile, layerHeader);
					
					const char* layerTypeStr = "Unknown";
					u32 unknownOffset1 = 0;
					u32 unknownOffset2 = 0;
					u32 unknownOffset3 = 0;
					u32 unknownOffset4 = 0;
					u32 numInstancesInLayer = 0;
					u32 numBackgroundsInLayer = 0;
					const GmsFooter_RoomLayerTiles_t* tilesFooter = nullptr;
					if (layerHeader->type == GmsEnum_LayerType_Background)
					{
						layerTypeStr = "Background";
						Assert(layerOffset + sizeof(GmsHeader_RoomLayer_t) < gmsFile.size);
						gmsFile.readCursor = layerOffset + sizeof(GmsHeader_RoomLayer_t);
						ConsumeFileType(&gmsFile, u32, _numBackgroundsInLayer);
						numBackgroundsInLayer = _numBackgroundsInLayer;
					}
					else if (layerHeader->type == GmsEnum_LayerType_Instance)
					{
						layerTypeStr = "Instance";
						Assert(layerOffset + sizeof(GmsHeader_RoomLayer_t) < gmsFile.size);
						gmsFile.readCursor = layerOffset + sizeof(GmsHeader_RoomLayer_t);
						ConsumeFileType(&gmsFile, u32, _numInstancesInLayer);
						numInstancesInLayer = _numInstancesInLayer;
					}
					else if (layerHeader->type == GmsEnum_LayerType_Asset)
					{
						layerTypeStr = "Asset";
						Assert(layerOffset + sizeof(GmsHeader_RoomLayer_t) < gmsFile.size);
						gmsFile.readCursor = layerOffset + sizeof(GmsHeader_RoomLayer_t);
						ConsumeFileStruct(&gmsFile, GmsFooter_RoomLayerAssets_t, assetLayerFooter);
						//TODO: It seems like this layer has 4 pointers that point to u32's that are all 0 right now.
						//      Until we have a use case though we will just assert that it matches what we've seen before
						if (assetLayerFooter->unknownListOffset0 != gmsFile.readCursor) { PrintLine_W("Warning room[%u] layer[%u] asset layer has unsupported listOffset0", rIndex, lIndex); }
						ConsumeFileType(&gmsFile, u32, unknownListLength0);
						if (unknownListLength0 != 0) { PrintLine_W("Warning room[%u] layer[%u] asset layer has unsupported listLength0", rIndex, lIndex); }
						if (assetLayerFooter->unknownListOffset1 != gmsFile.readCursor) { PrintLine_W("Warning room[%u] layer[%u] asset layer has unsupported listOffset1", rIndex, lIndex); }
						ConsumeFileType(&gmsFile, u32, unknownListLength1);
						if (unknownListLength1 != 0) { PrintLine_W("Warning room[%u] layer[%u] asset layer has unsupported listLength1", rIndex, lIndex); }
						if (assetLayerFooter->unknownListOffset2 != gmsFile.readCursor) { PrintLine_W("Warning room[%u] layer[%u] asset layer has unsupported listOffset2", rIndex, lIndex); }
						ConsumeFileType(&gmsFile, u32, unknownListLength2);
						if (unknownListLength2 != 0) { PrintLine_W("Warning room[%u] layer[%u] asset layer has unsupported listLength2", rIndex, lIndex); }
						if (assetLayerFooter->unknownListOffset3 != gmsFile.readCursor) { PrintLine_W("Warning room[%u] layer[%u] asset layer has unsupported listOffset3", rIndex, lIndex); }
						ConsumeFileType(&gmsFile, u32, unknownListLength3);
						if (unknownListLength3 != 0) { PrintLine_W("Warning room[%u] layer[%u] asset layer has unsupported listLength3", rIndex, lIndex); }
					}
					else if (layerHeader->type == GmsEnum_LayerType_Tile)
					{
						layerTypeStr = "Tile";
						Assert(layerOffset + sizeof(GmsHeader_RoomLayer_t) < gmsFile.size);
						gmsFile.readCursor = layerOffset + sizeof(GmsHeader_RoomLayer_t);
						ConsumeFileStruct(&gmsFile, GmsFooter_RoomLayerTiles_t, _tilesFooter);
						tilesFooter = _tilesFooter;
					}
					else { layerTypeStr = TempPrint("Type_%u", layerHeader->type); }
					
					const char* layerName = GetFileString(&gmsFile, layerHeader->nameOffset);
					
					// if (layerHeader->type == 2) { PrintLine_D("    Layer[%u] %s \"%s\" %d with %u instances (%08X):", lIndex, layerTypeStr, layerName, layerHeader->id, numInstancesInLayer, layerOffset); }
					// else { PrintLine_D("    Layer[%u] %s \"%s\" %d (%08X):", lIndex, layerTypeStr, layerName, layerHeader->id, layerOffset); }
					// PrintLine_D("      name:       \"%s\"", layerName);
					// PrintLine_D("      id:       %d", layerHeader->id);
					// PrintLine_D("      type:     %u", layerHeader->type);
					// PrintLine_D("      depth:    %d", layerHeader->depth);
					// PrintLine_D("      offset:   (%f, %f)", layerHeader->offset.x, layerHeader->offset.y);
					// PrintLine_D("      speed:    (%f, %f)", layerHeader->speed.x, layerHeader->speed.y);
					// PrintLine_D("      unknown7: 0x%08X", layerHeader->unknown7);
					
					GmsRoomLayer_t* layer = DynArrayAdd(&room->layers, GmsRoomLayer_t);
					Assert(layer != nullptr);
					ClearPointer(layer);
					layer->fileOffset = layerOffset;
					layer->endOffset = nextLayerOffset;
					if (layerName != nullptr) { layer->name = ArenaNtString(memArena, layerName); }
					layer->id       = layerHeader->id;
					layer->type     = (GmsEnum_LayerType_t)layerHeader->type;
					layer->depth    = layerHeader->depth;
					layer->offset   = layerHeader->offset;
					layer->speed    = layerHeader->speed;
					layer->unknown7 = layerHeader->unknown7;
					
					if (packOut->nextLayerId <= layer->id) { packOut->nextLayerId = layer->id+1; }
					
					if (layerHeader->type == GmsEnum_LayerType_Background)
					{
						// PrintLine_D("      %u backgrounds in this layer:", numBackgroundsInLayer);
						CreateDynamicArray(&layer->backgrounds, memArena, sizeof(GmsHeader_RoomLayerBackground_t), 16, numBackgroundsInLayer);
						for (u32 bIndex = 0; bIndex < numBackgroundsInLayer; bIndex++)
						{
							TempPushMark();
							ConsumeFileStruct(&gmsFile, GmsHeader_RoomLayerBackground_t, backgroundHeader);
							// PrintLine_D("        Background[%u]:", bIndex);
							// PrintLine_D("          unknown1:          0x%08X", backgroundHeader->unknown1);
							// PrintLine_D("          spriteId:          %d", backgroundHeader->spriteId);
							// PrintLine_D("          tileX:             %s", backgroundHeader->tileX ? "True" : "False");
							// PrintLine_D("          tileY:             %s", backgroundHeader->tileY ? "True" : "False");
							// PrintLine_D("          stretch:           %s", backgroundHeader->stretch ? "True" : "False");
							// PrintLine_D("          backgroundColor:   0x%08X", backgroundHeader->backgroundColor);
							// PrintLine_D("          unknown5:          0x%08X", backgroundHeader->unknown5);
							// PrintLine_D("          animationSpeed:    %f", backgroundHeader->animationSpeed);
							// PrintLine_D("          isSpeedGameFrames: %s", backgroundHeader->isSpeedGameFrames ? "True" : "False");
							
							GmsRoomLayerBackground_t* background = DynArrayAdd(&layer->backgrounds, GmsRoomLayerBackground_t);
							NotNull(background);
							ClearPointer(background);
							background->unknown1          = backgroundHeader->unknown1;
							background->spriteId          = backgroundHeader->spriteId;
							background->tileX             = (backgroundHeader->tileX != 0);
							background->tileY             = (backgroundHeader->tileY != 0);
							background->stretch           = (backgroundHeader->stretch != 0);
							background->backgroundColor   = backgroundHeader->backgroundColor;
							background->unknown5          = backgroundHeader->unknown5;
							background->animationSpeed    = backgroundHeader->animationSpeed;
							background->isSpeedGameFrames = (backgroundHeader->isSpeedGameFrames != 0);
							
							TempPopMark();
						}
					}
					else if (layerHeader->type == GmsEnum_LayerType_Instance)
					{
						// PrintLine_D("      %u instances in this layer:", numInstancesInLayer);
						CreateDynamicArray(&layer->instances, memArena, sizeof(u32), 16, numInstancesInLayer);
						for (u32 iIndex = 0; iIndex < numInstancesInLayer; iIndex++)
						{
							TempPushMark();
							ConsumeFileType(&gmsFile, u32, instanceId);
							// PrintLine_D("        Instance[%u]: %u", iIndex, instanceId);
							u32* instanceIdPntr = DynArrayAdd(&layer->instances, u32);
							Assert(instanceIdPntr != nullptr);
							*instanceIdPntr = instanceId;
							TempPopMark();
						}
					}
					else if (layerHeader->type == GmsEnum_LayerType_Asset)
					{
						Assert(unknownOffset1 < gmsFile.size);
						gmsFile.readCursor = unknownOffset1;
						ConsumeFileType(&gmsFile, u32, numUnknown1);
						Assert(unknownOffset2 < gmsFile.size);
						gmsFile.readCursor = unknownOffset2;
						ConsumeFileType(&gmsFile, u32, numUnknown2);
						Assert(unknownOffset3 < gmsFile.size);
						gmsFile.readCursor = unknownOffset3;
						ConsumeFileType(&gmsFile, u32, numUnknown3);
						Assert(unknownOffset3 < gmsFile.size);
						gmsFile.readCursor = unknownOffset3;
						ConsumeFileType(&gmsFile, u32, numUnknown4);
						// PrintLine_D("      numUnknown1:       %u", numUnknown1);
						// PrintLine_D("      numUnknown2:       %u", numUnknown2);
						// PrintLine_D("      numUnknown3:       %u", numUnknown3);
						// PrintLine_D("      numUnknown4:       %u", numUnknown4);
					}
					else if (layerHeader->type == GmsEnum_LayerType_Tile)
					{
						// PrintLine_D("      tilesetId:         %d", tilesFooter->tilesetId);
						// PrintLine_D("      gridSize:          (%d, %d)", tilesFooter->gridSize.x, tilesFooter->gridSize.y);
						CreateDynamicArray(&layer->tiles, memArena, sizeof(u32), 16, tilesFooter->gridSize.x * tilesFooter->gridSize.y);
						Assert(tilesFooter->gridSize.x >= 0 && tilesFooter->gridSize.y >= 0);
						layer->tilesetId = tilesFooter->tilesetId; 
						layer->gridSize = tilesFooter->gridSize; 
						layer->tilesFileOffset = (u32)gmsFile.readCursor;
						for (u32 yOffset = 0; yOffset < (u32)tilesFooter->gridSize.y; yOffset++)
						{
							// Print_D("        [%3u]", yOffset);
							for (u32 xOffset = 0; xOffset < (u32)tilesFooter->gridSize.x; xOffset++)
							{
								ConsumeFileType(&gmsFile, u32, tileId);
								// Print_D(" %3u", tileId);
								u32* tileIdSpace = DynArrayAdd(&layer->tiles, u32);
								Assert(tileIdSpace != nullptr);
								*tileIdSpace = tileId;
							}
							// WriteLine_D("");
						}
						layer->tilesFileEndOffset = (u32)gmsFile.readCursor;
					}
					
					TempPopMark();
				}
				DestroyDynamicArray(&layerHeadersList);
				
				TempPopMark();
			}
			
			DestroyDynamicArray(&roomHeaderList);
		}
		
		// +--------------------------------------------------------------+
		// |                        Parse Objects                         |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "OBJT"))
		{
			DynArray_t objectHeaderList = {};
			ConsumeFileList(&gmsFile, memArena, &objectHeaderList, GmsHeader_Object_t);
			
			PrintLine_D("  %u Objects in this chunk", objectHeaderList.length);
			DynArrayExpand(&packOut->objects, packOut->objects.length + objectHeaderList.length);
			for (u32 oIndex = 0; oIndex < objectHeaderList.length; oIndex++)
			{
				TempPushMark();
				const GmsHeader_Object_t* objectHeader = *DynArrayGet(&objectHeaderList, const GmsHeader_Object_t*, oIndex);
				Assert(objectHeader != nullptr);
				u32 objectOffset = GetFilePntrOffset(&gmsFile, objectHeader);
				
				const char* objectName = GetFileString(&gmsFile, objectHeader->nameOffset);
				// PrintLine_D("  Object[%u] \"%s\":", oIndex, objectName);
				// PrintLine_D("    spriteId:       %d", objectHeader->spriteId);
				// PrintLine_D("    isVisible:      %s", objectHeader->isVisible ? "True" : "False");
				// PrintLine_D("    isSolid:        %s", objectHeader->isSolid ? "True" : "False");
				// PrintLine_D("    depth:          %d", objectHeader->depth);
				// PrintLine_D("    isPersistent:   %s", objectHeader->isPersistent ? "True" : "False");
				// PrintLine_D("    parentId:       %d", objectHeader->parentId);
				// PrintLine_D("    maskId:         %d", objectHeader->maskId);
				// PrintLine_D("    hasPhysics:     %s", objectHeader->hasPhysics ? "True" : "False");
				// PrintLine_D("    isSensor:       %s", objectHeader->isSensor ? "True" : "False");
				// PrintLine_D("    collisionShape: %u", objectHeader->collisionShape);
				// if (objectHeader->hasPhysics)
				// {
				// 	PrintLine_D("    density:        %f", objectHeader->density);
				// 	PrintLine_D("    restitution:    %f", objectHeader->restitution);
				// 	PrintLine_D("    group:          %f", objectHeader->group);
				// 	PrintLine_D("    linearDamping:  %f", objectHeader->linearDamping);
				// 	PrintLine_D("    angularDamping: %f", objectHeader->angularDamping);
				// 	PrintLine_D("    unknown1:       %f", objectHeader->unknown1);
				// 	PrintLine_D("    friction:       %f", objectHeader->friction);
				// 	PrintLine_D("    unknown2:       %f", objectHeader->unknown2);
				// 	PrintLine_D("    kinematic:      %f", objectHeader->kinematic);
				// }
				
				GmsObject_t* object = DynArrayAdd(&packOut->objects, GmsObject_t);
				Assert(object != nullptr);
				ClearPointer(object);
				object->offset = objectOffset;
				if (objectName != nullptr) { object->name = ArenaNtString(memArena, objectName); }
				object->spriteId       = objectHeader->spriteId;
				object->isVisible      = (objectHeader->isVisible != 0);
				object->isSolid        = (objectHeader->isSolid != 0);
				object->depth          = objectHeader->depth;
				object->isPersistent   = (objectHeader->isPersistent != 0);
				object->parentId       = objectHeader->parentId;
				object->maskId         = objectHeader->maskId;
				object->hasPhysics     = (objectHeader->hasPhysics != 0);
				object->isSensor       = (objectHeader->isSensor != 0);
				object->collisionShape = objectHeader->collisionShape;
				object->density        = objectHeader->density;
				object->restitution    = objectHeader->restitution;
				object->group          = objectHeader->group;
				object->linearDamping  = objectHeader->linearDamping;
				object->angularDamping = objectHeader->angularDamping;
				object->unknown1       = objectHeader->unknown1;
				object->friction       = objectHeader->friction;
				object->unknown2       = objectHeader->unknown2;
				object->kinematic      = objectHeader->kinematic;
				
				TempPopMark();
			}
			
			DestroyDynamicArray(&objectHeaderList);
		}
		
		// +--------------------------------------------------------------+
		// |                        Parse Sprites                         |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "SPRT"))
		{
			DynArray_t spriteHeaderList = {};
			ConsumeFileList(&gmsFile, memArena, &spriteHeaderList, GmsHeader_Sprite_t);
			
			// PrintLine_D("sizeof(GmsHeader_Sprite_t) = %u", sizeof(GmsHeader_Sprite_t));
			PrintLine_D("  %u Sprites in this chunk", spriteHeaderList.length);
			// u32 prevSpriteEnd = 0;
			DynArrayExpand(&packOut->sprites, packOut->sprites.length + spriteHeaderList.length);
			for (u32 sIndex = 0; sIndex < spriteHeaderList.length; sIndex++)
			{
				TempPushMark();
				const GmsHeader_Sprite_t* spriteHeader = *DynArrayGet(&spriteHeaderList, const GmsHeader_Sprite_t*, sIndex);
				Assert(spriteHeader != nullptr);
				u32 spriteOffset = GetFilePntrOffset(&gmsFile, spriteHeader);
				// u32 thisSpriteStart = spriteOffset - chunkDataOffset;
				// u32 thisSpriteEnd = thisSpriteStart + sizeof(GmsHeader_Sprite_t);
				// PrintLine_D("Sprite[%u] %s skipped at 0x%08X", sIndex, FormattedSizeStr(thisSpriteStart - prevSpriteEnd), spriteOffset - (thisSpriteStart - prevSpriteEnd));
				// prevSpriteEnd = thisSpriteEnd;
				
				Assert(spriteOffset + sizeof(GmsHeader_Sprite_t) <= gmsFile.size);
				gmsFile.readCursor = spriteOffset + sizeof(GmsHeader_Sprite_t);
				// ConsumeFileType(&gmsFile, u32, extraNumber);
				//NOTE: Some versions contain an offset to the frame list. Some just immediately have the frame list following the sprite header
				DynArray_t frameHeaderList = {};
				DynArray_t unknownHeaderList1 = {};
				DynArray_t unknownHeaderList2 = {};
				if (spriteHeader->formatNum == 1)
				{
					//this is our tested GMS data file. Frames come directly after
					ConsumeFileType(&gmsFile, u32, numFrames);
					CreateDynamicArray(&frameHeaderList, memArena, sizeof(const u32*), 16, numFrames);
					for (u32 fIndex = 0; fIndex < numFrames; fIndex++)
					{
						const u32** newPntrSpace = DynArrayAdd(&frameHeaderList, const u32*);
						Assert(newPntrSpace != nullptr);
						ConsumeFileStruct(&gmsFile, u32, frameValuePntr);
						*newPntrSpace = frameValuePntr;
					}
				}
				else if (spriteHeader->formatNum == 2)
				{
					//this is Akurra's GMS data file. Frames are referenced by an offset and other lists follow
					ConsumeFileType(&gmsFile, u32, framesOffset);
					ConsumeFileList(&gmsFile, memArena, &unknownHeaderList1, u32);
					ConsumeFileType(&gmsFile, u32, numUnknownItems2);
					for (u32 uIndex = 0; uIndex < numUnknownItems2; uIndex++)
					{
						const u8* unknownBytes = ConsumeFileBytes(&gmsFile, 32);
					}
					
					Assert(framesOffset <= gmsFile.size);
					gmsFile.readCursor = framesOffset;
					ConsumeFileList(&gmsFile, memArena, &frameHeaderList, u32);
				}
				//TODO: GMS 2.3+ uses formatNum = 3
				else { PrintLine_D("Found unsupported value in sprite[%u] formatNum: %u", sIndex, spriteHeader->formatNum); Assert(false); }
				
				const char* spriteName = GetFileString(&gmsFile, spriteHeader->nameOffset);
				// PrintLine_D("  Sprite[%u] \"%s\" (%d, %d) (%08X):", sIndex, spriteName, spriteHeader->size.width, spriteHeader->size.height, spriteOffset);
				// PrintLine_D("    bounds:             (%d, %d, %d, %d)", spriteHeader->left, spriteHeader->top, spriteHeader->right, spriteHeader->bottom);
				// PrintLine_D("    boundingBoxMode:    %u", spriteHeader->boundingBoxMode);
				// PrintLine_D("    isSeparateColMasks: %s", spriteHeader->isSeparateColMasks ? "True" : "False");
				// PrintLine_D("    position:           (%d, %d)", spriteHeader->position.x, spriteHeader->position.y);
				// PrintLine_D("    unknown1:           %u", spriteHeader->unknown1);
				// PrintLine_D("    origin:             (%d, %d)", spriteHeader->origin.x, spriteHeader->origin.y);
				// PrintLine_D("    unknown4:           0x%08X", spriteHeader->unknown4);
				// PrintLine_D("    formatNum:           %u", spriteHeader->formatNum);
				// PrintLine_D("    unknown6:           %u", spriteHeader->unknown6);
				// PrintLine_D("    speed:              %f", spriteHeader->speed);
				// PrintLine_D("    isSpeedGameFrames:  %s", spriteHeader->isSpeedGameFrames ? "True" : "False");
				
				GmsSprite_t* sprite = DynArrayAdd(&packOut->sprites, GmsSprite_t);
				Assert(sprite != nullptr);
				ClearPointer(sprite);
				sprite->offset = spriteOffset;
				if (spriteName != nullptr) { sprite->name = ArenaNtString(memArena, spriteName); }
				sprite->size               = spriteHeader->size;
				sprite->bounds             = NewReci(spriteHeader->left, spriteHeader->top, (spriteHeader->right - spriteHeader->left), (spriteHeader->bottom - spriteHeader->top));
				sprite->boundingBoxMode    = spriteHeader->boundingBoxMode;
				sprite->isSeparateColMasks = (spriteHeader->isSeparateColMasks != 0);
				sprite->position           = spriteHeader->position;
				sprite->unknown1           = spriteHeader->unknown1;
				sprite->origin             = spriteHeader->origin;
				sprite->unknown4           = spriteHeader->unknown4;
				sprite->formatNum          = spriteHeader->formatNum;
				sprite->unknown6           = spriteHeader->unknown6;
				sprite->speed              = spriteHeader->speed;
				sprite->isSpeedGameFrames  = (spriteHeader->isSpeedGameFrames != 0);
				CreateDynamicArray(&sprite->frames, memArena, sizeof(GmsSpriteFrame_t), 16, frameHeaderList.length);
				
				// PrintLine_D("    %u Frames in this sprite:", frameHeaderList.length);
				Assert(frameHeaderList.length <= 1024);
				for (u32 fIndex = 0; fIndex < frameHeaderList.length; fIndex++)
				{
					const u32* frameHeader = *DynArrayGet(&frameHeaderList, const u32*, fIndex);
					Assert(frameHeader != nullptr);
					GmsSpriteFrame_t* newFrame = DynArrayAdd(&sprite->frames, GmsSpriteFrame_t);
					Assert(newFrame != nullptr);
					ClearPointer(newFrame);
					newFrame->partOffset = *frameHeader;
					// PrintLine_D("      Frame[%u]: 0x%08X", fIndex, newFrame->partOffset);
				}
				DestroyDynamicArray(&frameHeaderList);
				
				TempPopMark();
			}
			
			DestroyDynamicArray(&spriteHeaderList);
		}
		
		// +--------------------------------------------------------------+
		// |                         Parse Sounds                         |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "SOND"))
		{
			DynArray_t soundHeaderList = {};
			ConsumeFileList(&gmsFile, memArena, &soundHeaderList, GmsHeader_Sound_t);
			
			PrintLine_D("  %u Sounds in this chunk", soundHeaderList.length);
			DynArrayExpand(&packOut->sounds, packOut->sounds.length + soundHeaderList.length);
			for (u32 sIndex = 0; sIndex < soundHeaderList.length; sIndex++)
			{
				TempPushMark();
				const GmsHeader_Sound_t* soundHeader = *DynArrayGet(&soundHeaderList, const GmsHeader_Sound_t*, sIndex);
				Assert(soundHeader != nullptr);
				u32 soundOffset = GetFilePntrOffset(&gmsFile, soundHeader);
				
				const char* soundName = GetFileString(&gmsFile, soundHeader->nameOffset);
				const char* fileTypeStr = GetFileString(&gmsFile, soundHeader->fileTypeOffset);
				const char* fileNameStr = GetFileString(&gmsFile, soundHeader->fileNameOffset);
				// PrintLine_D("  Sound[%u] \"%s\":", sIndex, soundName);
				// PrintLine_D("    fileType: \"%s\"", fileTypeStr);
				// PrintLine_D("    fileName: \"%s\"", fileNameStr);
				// PrintLine_D("    flags:    0x%08X", soundHeader->flags);
				// PrintLine_D("    unknown:  0x%08X", soundHeader->unknown);
				// PrintLine_D("    volume:   %f", soundHeader->volume);
				// PrintLine_D("    pitch:    %f", soundHeader->pitch);
				// PrintLine_D("    groupId:  %d", soundHeader->groupId);
				// PrintLine_D("    audioId:  %d", soundHeader->audioId);
				
				GmsSound_t* sound = DynArrayAdd(&packOut->sounds, GmsSound_t);
				Assert(sound != nullptr);
				ClearPointer(sound);
				sound->offset = soundOffset;
				if (soundName   != nullptr) { sound->name     = ArenaNtString(memArena, soundName);   }
				if (fileTypeStr != nullptr) { sound->fileType = ArenaNtString(memArena, fileTypeStr); }
				if (fileNameStr != nullptr) { sound->fileName = ArenaNtString(memArena, fileNameStr); }
				sound->flags   = soundHeader->flags;
				sound->unknown = soundHeader->unknown;
				sound->volume  = soundHeader->volume;
				sound->pitch   = soundHeader->pitch;
				sound->groupId = soundHeader->groupId;
				sound->audioId = soundHeader->audioId;
				
				TempPopMark();
			}
			
			DestroyDynamicArray(&soundHeaderList);
		}
		
		// +--------------------------------------------------------------+
		// |                        Parse Strings                         |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "STRG"))
		{
			DynArray_t stringHeaderList = {};
			ConsumeFileList(&gmsFile, memArena, &stringHeaderList, GmsHeader_String_t);
			
			PrintLine_D("  %u Strings in this chunk", stringHeaderList.length);
			DynArrayExpand(&packOut->strings, packOut->strings.length + stringHeaderList.length);
			for (u32 sIndex = 0; sIndex < stringHeaderList.length; sIndex++)
			{
				TempPushMark();
				const GmsHeader_String_t* stringHeader = *DynArrayGet(&stringHeaderList, const GmsHeader_String_t*, sIndex);
				Assert(stringHeader != nullptr);
				u32 stringOffset = GetFilePntrOffset(&gmsFile, stringHeader);
				u32 charsOffset = stringOffset + sizeof(GmsHeader_String_t);
				
				const char* stringPntr = GetFileString(&gmsFile, charsOffset);
				// PrintLine_D("  String[%u] %u chars \"%.*s\"", sIndex, stringHeader->length, stringHeader->length, stringPntr);
				
				GmsString_t* string = DynArrayAdd(&packOut->strings, GmsString_t);
				Assert(string != nullptr);
				ClearPointer(string);
				string->index = sIndex;
				string->offset = stringOffset;
				string->charsOffset = charsOffset;
				string->length = stringHeader->length;
				if (stringHeader->length > 0) { string->pntr = ArenaString(memArena, stringPntr, stringHeader->length); }
				
				TempPopMark();
			}
			
			DestroyDynamicArray(&stringHeaderList);
		}
		
		// +--------------------------------------------------------------+
		// |                        Parse Textures                        |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "TXTR"))
		{
			DynArray_t textureHeaderList = {};
			ConsumeFileList(&gmsFile, memArena, &textureHeaderList, GmsHeader_Texture_t);
			
			// ConsumeFileType(&gmsFile, u32, numTextureAddresses);
			// Assert(numTextureAddresses == textureHeaderList.length);
			// PrintLine_D("  %u Addresses in this chunk %u", textureHeaderList.length, sizeof(GmsHeader_Texture_t));
			// for (u32 tIndex = 0; tIndex < textureHeaderList.length; tIndex++)
			// {
			// 	ConsumeFileStruct(&gmsFile, GmsHeader_Texture_t, textureAddress);
			// 	PrintLine_D("    Address[%u]: 0x%08X padding %u (0x%08X)", tIndex, textureAddress->offset, textureAddress->padding, GetFilePntrOffset(&gmsFile, textureAddress));
			// }
			
			PrintLine_D("  %u Textures in this chunk", textureHeaderList.length);
			DynArrayExpand(&packOut->textures, packOut->textures.length + textureHeaderList.length);
			for (u32 tIndex = 0; tIndex < textureHeaderList.length; tIndex++)
			{
				TempPushMark();
				const GmsHeader_Texture_t* textureHeader = *DynArrayGet(&textureHeaderList, const GmsHeader_Texture_t*, tIndex);
				Assert(textureHeader != nullptr);
				u32 textureOffset = GetFilePntrOffset(&gmsFile, textureHeader);
				
				u32 textureDataSize = 0;
				if (tIndex+1 < textureHeaderList.length)
				{
					const GmsHeader_Texture_t* nextTextureHeader = *DynArrayGet(&textureHeaderList, const GmsHeader_Texture_t*, tIndex+1);
					Assert(nextTextureHeader != nullptr);
					// u32 nextTextureOffset = GetFilePntrOffset(&gmsFile, nextTextureHeader);
					Assert(nextTextureHeader->offset >= textureHeader->offset);
					textureDataSize = nextTextureHeader->offset - textureHeader->offset;
				}
				else
				{
					Assert(chunkDataOffset + chunkHeader->size >= textureHeader->offset);
					textureDataSize = chunkDataOffset + chunkHeader->size - textureHeader->offset;
				}
				
				const u8* textureDataPntr = GetFilePntrSized(&gmsFile, textureHeader->offset, textureDataSize);
				Assert(textureDataPntr != nullptr);
				
				// PrintLine_D("  Texture[%u] %s (at 0x%08X header at 0x%08X)", tIndex, FormattedSizeStr(textureDataSize), textureHeader->offset, textureOffset);
				
				GmsTexture_t* texture = DynArrayAdd(&packOut->textures, GmsTexture_t);
				Assert(texture != nullptr);
				ClearPointer(texture);
				texture->offset = textureOffset;
				texture->dataOffset = textureHeader->offset;
				texture->size = textureDataSize;
				if (textureDataSize > 0)
				{
					texture->pngData = PushArray(memArena, u8, textureDataSize);
					Assert(texture->pngData != nullptr);
					MyMemCopy(texture->pngData, textureDataPntr, textureDataSize);
				}
				#if 0
				char* textureFileName = TempPrint("texure_%u.png", tIndex);
				WriteEntireFile(textureFileName, texture->pngData, texture->size);
				#endif
				
				TempPopMark();
			}
			
			DestroyDynamicArray(&textureHeaderList);
		}
		
		// +--------------------------------------------------------------+
		// |                         Parse Audios                         |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "AUDO"))
		{
			DynArray_t audioHeaderList = {};
			ConsumeFileList(&gmsFile, memArena, &audioHeaderList, GmsHeader_Audio_t);
			
			PrintLine_D("  %u Audios in this chunk", audioHeaderList.length);
			DynArrayExpand(&packOut->audios, packOut->audios.length + audioHeaderList.length);
			for (u32 aIndex = 0; aIndex < audioHeaderList.length; aIndex++)
			{
				TempPushMark();
				const GmsHeader_Audio_t* audioHeader = *DynArrayGet(&audioHeaderList, const GmsHeader_Audio_t*, aIndex);
				Assert(audioHeader != nullptr);
				u32 audioOffset = GetFilePntrOffset(&gmsFile, audioHeader);
				
				u32 audioDataOffset = audioOffset + sizeof(GmsHeader_Audio_t);
				const u8* audioDataPntr = (const u8*)GetFilePntrSized(&gmsFile, audioDataOffset, audioHeader->size);
				// PrintLine_D("  Sound[%u] %s (at 0x%08X)", aIndex, FormattedSizeStr(audioHeader->size), audioOffset);
				
				GmsAudio_t* audio = DynArrayAdd(&packOut->audios, GmsAudio_t);
				Assert(audio != nullptr);
				ClearPointer(audio);
				audio->offset = audioOffset;
				audio->size = audioHeader->size;
				if (audioHeader->size > 0)
				{
					audio->wavData = PushArray(memArena, u8, audioHeader->size);
					Assert(audio->wavData != nullptr);
					MyMemCopy(audio->wavData, audioDataPntr, audioHeader->size);
				}
				#if 0
				char* audioFileName = TempPrint("sound_%u.wav", aIndex);
				WriteEntireFile(audioFileName, audio->wavData, audio->size);
				#endif
				
				TempPopMark();
			}
			
			DestroyDynamicArray(&audioHeaderList);
		}
		
		// +--------------------------------------------------------------+
		// |                      Parse Backgrounds                       |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "BGND"))
		{
			DynArray_t backgroundHeaderList = {};
			ConsumeFileList(&gmsFile, memArena, &backgroundHeaderList, GmsHeader_Background_t);
			
			PrintLine_D("  %u Backgrounds in this chunk", backgroundHeaderList.length);
			DynArrayExpand(&packOut->backgrounds, packOut->backgrounds.length + backgroundHeaderList.length);
			for (u32 bIndex = 0; bIndex < backgroundHeaderList.length; bIndex++)
			{
				TempPushMark();
				const GmsHeader_Background_t* backgroundHeader = *DynArrayGet(&backgroundHeaderList, const GmsHeader_Background_t*, bIndex);
				Assert(backgroundHeader != nullptr);
				u32 backgroundOffset = GetFilePntrOffset(&gmsFile, backgroundHeader);
				
				const char* backgroundName = GetFileString(&gmsFile, backgroundHeader->nameOffset);
				// PrintLine_D("  Background[%u] \"%s\":", bIndex, backgroundName);
				// PrintLine_D("    unknown[0]:     %u", backgroundHeader->unknown[0]);
				// PrintLine_D("    unknown[1]:     %u", backgroundHeader->unknown[1]);
				// PrintLine_D("    unknown[2]:     %u", backgroundHeader->unknown[2]);
				// PrintLine_D("    textureAddress: 0x%08X", backgroundHeader->textureAddress);
				
				GmsBackground_t* background = DynArrayAdd(&packOut->backgrounds, GmsBackground_t);
				Assert(background != nullptr);
				ClearPointer(background);
				background->offset = backgroundOffset;
				if (backgroundName != nullptr) { background->name = ArenaNtString(memArena, backgroundName); }
				background->unknown[0]     = backgroundHeader->unknown[0];
				background->unknown[1]     = backgroundHeader->unknown[1];
				background->unknown[2]     = backgroundHeader->unknown[2];
				background->textureAddress = backgroundHeader->textureAddress;
				
				TempPopMark();
			}
			
			DestroyDynamicArray(&backgroundHeaderList);
		}
		
		// +--------------------------------------------------------------+
		// |                         Parse Paths                          |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "PATH"))
		{
			DynArray_t pathHeaderList = {};
			ConsumeFileList(&gmsFile, memArena, &pathHeaderList, GmsHeader_Path_t);
			
			PrintLine_D("  %u Paths in this chunk", pathHeaderList.length);
			DynArrayExpand(&packOut->paths, packOut->paths.length + pathHeaderList.length);
			for (u32 pIndex = 0; pIndex < pathHeaderList.length; pIndex++)
			{
				TempPushMark();
				const GmsHeader_Path_t* pathHeader = *DynArrayGet(&pathHeaderList, const GmsHeader_Path_t*, pIndex);
				Assert(pathHeader != nullptr);
				u32 pathOffset = GetFilePntrOffset(&gmsFile, pathHeader);
				
				Assert(pathOffset + sizeof(GmsHeader_Path_t) <= gmsFile.size);
				gmsFile.readCursor = pathOffset + sizeof(GmsHeader_Path_t);
				ConsumeFileType(&gmsFile, u32, numPoints);
				
				const char* pathName = GetFileString(&gmsFile, pathHeader->nameOffset);
				// PrintLine_D("  Path[%u] \"%s\":", pIndex, pathName);
				// PrintLine_D("    isSmooth:  %s", pathHeader->isSmooth ? "True" : "False");
				// PrintLine_D("    isClosed:  %u", pathHeader->isClosed);
				// PrintLine_D("    precision: %s", pathHeader->precision ? "True" : "False");
				
				GmsPath_t* path = DynArrayAdd(&packOut->paths, GmsPath_t);
				Assert(path != nullptr);
				ClearPointer(path);
				path->offset = pathOffset;
				if (pathName != nullptr) { path->name = ArenaNtString(memArena, pathName); }
				path->isSmooth  = (pathHeader->isSmooth != 0);
				path->isClosed  = (pathHeader->isClosed != 0);
				path->precision = pathHeader->precision;
				CreateDynamicArray(&path->points, memArena, sizeof(GmsPathPoint_t), 16, numPoints);
				
				// PrintLine_D("    %u Points in this path:", numPoints);
				for (u32 pointIndex = 0; pointIndex < numPoints; pointIndex++)
				{
					ConsumeFileStruct(&gmsFile, GmsHeader_PathPoint_t, pointHeader);
					u32 pointOffset = GetFilePntrOffset(&gmsFile, pointHeader);
					
					// PrintLine_D("    Point[%u]:", pointIndex);
					// PrintLine_D("      position: (%d, %d)", pointHeader->position.x, pointHeader->position.y);
					// PrintLine_D("      speed:    %f", pointHeader->speed);
					
					GmsPathPoint_t* point = DynArrayAdd(&path->points, GmsPathPoint_t);
					Assert(point != nullptr);
					ClearPointer(point);
					point->offset = pointOffset;
					point->position = pointHeader->position;
					point->speed = pointHeader->speed;
				}
				
				TempPopMark();
			}
			
			DestroyDynamicArray(&pathHeaderList);
		}
		
		// +--------------------------------------------------------------+
		// |                        Parse Scripts                         |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "SCPT"))
		{
			DynArray_t scriptHeaderList = {};
			ConsumeFileList(&gmsFile, memArena, &scriptHeaderList, GmsHeader_Script_t);
			
			PrintLine_D("  %u Scripts in this chunk", scriptHeaderList.length);
			DynArrayExpand(&packOut->scripts, packOut->scripts.length + scriptHeaderList.length);
			for (u32 sIndex = 0; sIndex < scriptHeaderList.length; sIndex++)
			{
				TempPushMark();
				const GmsHeader_Script_t* scriptHeader = *DynArrayGet(&scriptHeaderList, const GmsHeader_Script_t*, sIndex);
				Assert(scriptHeader != nullptr);
				u32 scriptOffset = GetFilePntrOffset(&gmsFile, scriptHeader);
				
				const char* scriptName = GetFileString(&gmsFile, scriptHeader->nameOffset);
				// PrintLine_D("  Script[%u] \"%s\" (0x%08X):", sIndex, scriptName, scriptOffset);
				// PrintLine_D("    id: %u", scriptHeader->id);
				
				GmsScript_t* script = DynArrayAdd(&packOut->scripts, GmsScript_t);
				Assert(script != nullptr);
				ClearPointer(script);
				script->offset = scriptOffset;
				if (scriptName != nullptr) { script->name = ArenaNtString(memArena, scriptName); }
				script->id = scriptHeader->id;
				
				TempPopMark();
			}
			
			DestroyDynamicArray(&scriptHeaderList);
		}
		
		// +--------------------------------------------------------------+
		// |                         Parse Fonts                          |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "FONT"))
		{
			#if 0 //TODO: Turn me back on!
			DynArray_t fontHeaderList = {};
			ConsumeFileList(&gmsFile, memArena, &fontHeaderList, GmsHeader_Background_t);
			
			PrintLine_D("  %u Fonts in this chunk", fontHeaderList.length);
			DynArrayExpand(&packOut->fonts, packOut->fonts.length + fontHeaderList.length);
			for (u32 sIndex = 0; sIndex < fontHeaderList.length; sIndex++)
			{
				TempPushMark();
				const GmsHeader_Font_t* fontHeader = *DynArrayGet(&fontHeaderList, const GmsHeader_Font_t*, sIndex);
				Assert(fontHeader != nullptr);
				u32 fontOffset = GetFilePntrOffset(&gmsFile, fontHeader);
				
				Assert(fontOffset + sizeof(GmsHeader_Font_t) <= gmsFile.size);
				gmsFile.readCursor = fontOffset + sizeof(GmsHeader_Font_t);
				DynArray_t glyphHeadersList = {};
				ConsumeFileList(&gmsFile, memArena, &glyphHeadersList, GmsHeader_FontGlyph_t);
				
				const char* fontName = GetFileString(&gmsFile, fontHeader->nameOffset);
				const char* fileNameStr = GetFileString(&gmsFile, fontHeader->fileNameOffset);
				// PrintLine_D("  Font[%u] \"%s\":", sIndex, fontName);
				// PrintLine_D("    fileName: \"%s\"", fileNameStr);
				// PrintLine_D("    pointSize:     %f", fontHeader->pointSize);
				// PrintLine_D("    isBold:        %s", fontHeader->isBold ? "True" : "False");
				// PrintLine_D("    isItalic:      %s", fontHeader->isItalic ? "True" : "False");
				// PrintLine_D("    unknown1:      0x%02X", fontHeader->unknown1);
				// PrintLine_D("    unknown2:      0x%02X", fontHeader->unknown2);
				// PrintLine_D("    unknown3:      0x%02X", fontHeader->unknown3);
				// PrintLine_D("    antiAliasMode: 0x%02X", fontHeader->antiAliasMode);
				// PrintLine_D("    defaultChar:   %u", fontHeader->defaultChar);
				// PrintLine_D("    partOffset:    0x%08X", fontHeader->partOffset);
				// PrintLine_D("    scale:         (%f, %f)", fontHeader->scale.x, fontHeader->scale.y);
				// PrintLine_D("    flags:         0x%08X", fontHeader->flags);
				
				GmsFont_t* font = DynArrayAdd(&packOut->fonts, GmsFont_t);
				Assert(font != nullptr);
				ClearPointer(font);
				font->offset = fontOffset;
				if (fontName != nullptr) { font->name = ArenaNtString(memArena, fontName); }
				if (fileNameStr != nullptr) { font->fileName = ArenaNtString(memArena, fileNameStr); }
				font->pointSize     = fontHeader->pointSize;
				font->isBold        = (fontHeader->isBold != 0);
				font->isItalic      = (fontHeader->isItalic != 0);
				font->unknown1      = fontHeader->unknown1;
				font->unknown2      = fontHeader->unknown2;
				font->unknown3      = fontHeader->unknown3;
				font->antiAliasMode = fontHeader->antiAliasMode;
				font->defaultChar   = fontHeader->defaultChar;
				font->partOffset    = fontHeader->partOffset;
				font->scale         = fontHeader->scale;
				font->flags         = fontHeader->flags;
				CreateDynamicArray(&font->glyphs, memArena, sizeof(GmsFontGlyph_t), 16, glyphHeadersList.length);
				
				// PrintLine_D("    %u Glyphs in this font:", glyphHeadersList.length);
				for (u32 gIndex = 0; gIndex < glyphHeadersList.length; gIndex++)
				{
					const GmsHeader_FontGlyph_t* glyphHeader = *DynArrayGet(&glyphHeadersList, const GmsHeader_FontGlyph_t*, gIndex);
					Assert(glyphHeader != nullptr);
					u32 glyphOffset = GetFilePntrOffset(&gmsFile, glyphHeader);
					
					// PrintLine_D("    Glyph[%u]:", gIndex);
					// PrintLine_D("      codePoint: 0x%04X", glyphHeader->codePoint);
					// PrintLine_D("      drawRec:   (%u, %u, %u, %u)", glyphHeader->x, glyphHeader->y, glyphHeader->width, glyphHeader->height);
					// PrintLine_D("      shift:     %u", glyphHeader->shift);
					// PrintLine_D("      offset:    %u", glyphHeader->offset);
					
					GmsFontGlyph_t* glyph = DynArrayAdd(&font->glyphs, GmsFontGlyph_t);
					Assert(glyph != nullptr);
					ClearPointer(glyph);
					glyph->fileOffset = glyphOffset;
					glyph->codePoint  = glyphHeader->codePoint;
					glyph->drawRec    = NewReci(glyphHeader->x, glyphHeader->y, glyphHeader->width, glyphHeader->height);
					glyph->shift      = glyphHeader->shift;
					glyph->offset     = glyphHeader->offset;
				}
				DestroyDynamicArray(&glyphHeadersList);
				
				TempPopMark();
			}
			
			DestroyDynamicArray(&fontHeaderList);
			#endif
		}
		
		// +--------------------------------------------------------------+
		// |                     Parse Texture Parts                      |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "TPAG"))
		{
			DynArray_t partHeaderList = {};
			ConsumeFileList(&gmsFile, memArena, &partHeaderList, GmsHeader_TexturePart_t);
			
			PrintLine_D("  %u Texture Parts in this chunk", partHeaderList.length);
			DynArrayExpand(&packOut->textureParts, packOut->textureParts.length + partHeaderList.length);
			for (u32 pIndex = 0; pIndex < partHeaderList.length; pIndex++)
			{
				TempPushMark();
				const GmsHeader_TexturePart_t* partHeader = *DynArrayGet(&partHeaderList, const GmsHeader_TexturePart_t*, pIndex);
				Assert(partHeader != nullptr);
				u32 partOffset = GetFilePntrOffset(&gmsFile, partHeader);
				
				// PrintLine_D("  Part[%u] (at 0x%08X):", pIndex, partOffset);
				// PrintLine_D("    sourceRec:   (%u, %u, %u, %u)", partHeader->x, partHeader->y, partHeader->width, partHeader->height);
				// PrintLine_D("    drawOffset:  (%u, %u)", partHeader->drawOffsetX, partHeader->drawOffsetY);
				// PrintLine_D("    boundingRec: (%u, %u, %u, %u)", partHeader->boundingX, partHeader->boundingY, partHeader->boundingWidth, partHeader->boundingHeight);
				// PrintLine_D("    textureId:   %u", partHeader->textureId);
				
				GmsTexturePart_t* part = DynArrayAdd(&packOut->textureParts, GmsTexturePart_t);
				Assert(part != nullptr);
				ClearPointer(part);
				part->fileOffset = partOffset;
				part->sourceRec   = NewReci(partHeader->x, partHeader->y, partHeader->width, partHeader->height);
				part->drawOffset  = NewVec2i(partHeader->drawOffsetX, partHeader->drawOffsetY);
				part->boundingRec = NewReci(partHeader->boundingX, partHeader->boundingY, partHeader->boundingWidth, partHeader->boundingHeight);
				part->textureId   = partHeader->textureId;
				
				TempPopMark();
			}
			
			DestroyDynamicArray(&partHeaderList);
		}
		
		// +--------------------------------------------------------------+
		// |                          Parse Code                          |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "CODE"))
		{
			DynArray_t codeHeaderList = {};
			ConsumeFileList(&gmsFile, memArena, &codeHeaderList, GmsHeader_Code_t);
			
			u32 codeByteSectionStart = (u32)(gmsFile.readCursor);
			u32 firstCodeHeaderOffset = (codeHeaderList.length > 0) ? GetFilePntrOffset(&gmsFile, *DynArrayGet(&codeHeaderList, const u8*, 0)) : (chunkDataOffset + chunkHeader->size);
			Assert(firstCodeHeaderOffset >= codeByteSectionStart);
			u32 codeByteSectionSize = (firstCodeHeaderOffset - codeByteSectionStart);
			
			PrintLine_D("  %u Codes in this chunk", codeHeaderList.length);
			// DynArrayExpand(&packOut->scripts, packOut->scripts.length + codeHeaderList.length);
			for (u32 sIndex = 0; sIndex < codeHeaderList.length; sIndex++)
			{
				TempPushMark();
				const GmsHeader_Code_t* codeHeader = *DynArrayGet(&codeHeaderList, const GmsHeader_Code_t*, sIndex);
				Assert(codeHeader != nullptr);
				u32 codeHeaderOffset = GetFilePntrOffset(&gmsFile, codeHeader);
				
				const char* codeName = GetFileString(&gmsFile, codeHeader->nameOffset);
				u32 codeStartOffset = codeHeaderOffset + STRUCT_VAR_OFFSET(GmsHeader_Code_t, codeRelativeOffset) + codeHeader->codeRelativeOffset;
				Assert(codeStartOffset >= codeByteSectionStart && codeStartOffset + codeHeader->codeLength <= codeByteSectionStart + codeByteSectionSize);
				if ((codeHeader->codeLength % 4) != 0) { WriteLine_W("Warning: Bytecode is not a multiple of 4 bytes in length!"); }
				
				// PrintLine_D("  Code[%u] (0x%08X) \"%s\" [%d, %d]:", sIndex, codeHeaderOffset, codeName, codeHeader->unknown1, codeHeader->unknown2);
				// Print_D("    [%u]:", codeHeader->codeLength);
				// for (u32 bIndex = 0; bIndex+4 <= codeHeader->codeLength; bIndex += 4)
				// {
				// 	u32 codeByte = *(const u32*)GetFilePntrSized(&gmsFile, codeStartOffset + bIndex, sizeof(u32));
				// 	Print_D(" %08X", codeByte);
				// }
				// WriteLine_D("");
				
				GmsCode_t* code = DynArrayAdd(&packOut->codes, GmsCode_t);
				Assert(code != nullptr);
				ClearPointer(code);
				code->offset = codeHeaderOffset;
				if (codeName != nullptr) { code->name = ArenaNtString(memArena, codeName); }
				code->codeRelativeOffset = codeHeader->codeRelativeOffset;
				code->bytecodeOffset = codeStartOffset;
				code->unknown1 = codeHeader->unknown1;
				code->unknown2 = codeHeader->unknown2;
				code->bytecodeLength = codeHeader->codeLength;
				if (codeHeader->codeLength > 0)
				{
					code->bytecodePntr = PushArray(memArena, u8, codeHeader->codeLength);
					NotNull(code->bytecodePntr);
					MyMemCopy(code->bytecodePntr, GetFilePntrSized(&gmsFile, codeStartOffset, codeHeader->codeLength), codeHeader->codeLength);
				}
				
				TempPopMark();
			}
			
			DestroyDynamicArray(&codeHeaderList);
		}
		
		// +--------------------------------------------------------------+
		// |                       Parse Variables                        |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "VARI"))
		{
			ConsumeFileType(&gmsFile, u32, readNumVariables); //TODO: This doesn't actually match what we expect!
			ConsumeFileType(&gmsFile, u32, numSomething1);
			ConsumeFileType(&gmsFile, u32, numSomething2);
			
			//TODO: Figure out if this is really how we are supposed to find the variable count
			u32 restOfChunkSize = (chunkHeader->size - sizeof(u32)*3);
			u32 numVariables = restOfChunkSize / sizeof(GmsHeader_Variable_t);
			if (restOfChunkSize != numVariables * sizeof(GmsHeader_Variable_t)) { PrintLine_W("Warning: The variables section contains a few unknown bytes at the end: %u bytes", restOfChunkSize - (numVariables * sizeof(GmsHeader_Variable_t))); }
			
			PrintLine_D("  %u Variables in this chunk (%u read, %u something1, %u something2)", numVariables, readNumVariables, numSomething1, numSomething2);
			for (u32 vIndex = 0; vIndex < numVariables; vIndex++)
			{
				u32 variableOffset = (u32)gmsFile.readCursor;
				ConsumeFileType(&gmsFile, GmsHeader_Variable_t, variableHeader);
				const char* variableName = GetFileString(&gmsFile, variableHeader.nameOffset);
				// PrintLine_D("    Var[%u] (0x%08X): %u \"%s\"", vIndex, variableOffset, variableHeader.id, variableName);
				// Assert((variableHeader.numCodeReferences > 0) == (variableHeader.firstCodeReference != -1));
				// if (variableHeader.numCodeReferences > 0)
				// {
				// 	PrintLine_D("      Referenced %u times starting at 0x%08X", variableHeader.numCodeReferences, variableHeader.firstCodeReference);
				// }
				
				GmsVariable_t* variable = DynArrayAdd(&packOut->variables, GmsVariable_t);
				NotNull(variable);
				ClearPointer(variable);
				variable->offset = variableOffset;
				variable->unknown0 = variableHeader.unknown0;
				variable->id = variableHeader.id;
				variable->numCodeReferences = variableHeader.numCodeReferences;
				variable->firstCodeReference = variableHeader.firstCodeReference;
				if (variableName != nullptr)
				{
					variable->name = ArenaNtString(memArena, variableName);
					NotNull(variable->name);
				}
			}
		}
		
		// +--------------------------------------------------------------+
		// |                       Parse Functions                        |
		// +--------------------------------------------------------------+
		else if (IsChunkType(chunkHeader, "FUNC"))
		{
			ConsumeFileType(&gmsFile, u32, numFunctions);
			
			PrintLine_D("  %u Functions in this chunk", numFunctions);
			for (u32 fIndex = 0; fIndex < numFunctions; fIndex++)
			{
				u32 functionOffset = (u32)gmsFile.readCursor;
				ConsumeFileType(&gmsFile, GmsHeader_Function_t, functionHeader);
				const char* functionName = GetFileString(&gmsFile, functionHeader.nameOffset);
				// PrintLine_D("    Func[%u] (0x%08X): \"%s\"", fIndex, functionOffset, functionName);
				// PrintLine_D("      unknown0: %u", functionHeader.unknown0);
				// PrintLine_D("      firstCodeReference: 0x%08X", functionHeader.firstCodeReference);
				
				GmsFunction_t* function = DynArrayAdd(&packOut->functions, GmsFunction_t);
				NotNull(function);
				ClearPointer(function);
				function->offset = functionOffset;
				function->unknown0 = functionHeader.unknown0;
				function->firstCodeReference = functionHeader.firstCodeReference;
				if (functionName != nullptr)
				{
					function->name = ArenaNtString(memArena, functionName);
					NotNull(function->name);
				}
			}
		}
		
		// +--------------------------------------------------------------+
		// |                        Unknown Chunk                         |
		// +--------------------------------------------------------------+
		else
		{
			// PrintLine_D("Unknown Chunk[%u]: \"%.*s\" %s bytes", chunkIndex, ArrayCount(chunkHeader->type), &chunkHeader->type[0], FormattedSizeStr(chunkHeader->size));
		}
		
		gmsFile.readCursor = nextReadCursor;
		chunkIndex++;
	}
	
	// +====================================+
	// | Connect Sprites with Texture Parts |
	// +====================================+
	for (u32 sIndex = 0; sIndex < packOut->sprites.length; sIndex++)
	{
		GmsSprite_t* sprite = DynArrayGet(&packOut->sprites, GmsSprite_t, sIndex);
		Assert(sprite != nullptr);
		for (u32 fIndex = 0; fIndex < sprite->frames.length; fIndex++)
		{
			GmsSpriteFrame_t* frame = DynArrayGet(&sprite->frames, GmsSpriteFrame_t, fIndex);
			Assert(frame != nullptr);
			bool foundTexturePart = false;
			for (u32 pIndex = 0; pIndex < packOut->textureParts.length; pIndex++)
			{
				GmsTexturePart_t* part = DynArrayGet(&packOut->textureParts, GmsTexturePart_t, pIndex);
				Assert(part != nullptr);
				if (frame->partOffset == part->fileOffset)
				{
					foundTexturePart = true;
					frame->partPntr = part;
					break;
				}
			}
			// if (!foundTexturePart) { PrintLine_E("Failed to find texture part for sprite[%u] frame[%u] which has texture part address 0x%08X", sIndex, fIndex, frame->partOffset); }
		}
	}
	
	// +==================================+
	// | Connect Fonts with Texture Parts |
	// +==================================+
	for (u32 fIndex = 0; fIndex < packOut->fonts.length; fIndex++)
	{
		GmsFont_t* font = DynArrayGet(&packOut->fonts, GmsFont_t, fIndex);
		Assert(font != nullptr);
		bool foundTexturePart = false;
		for (u32 pIndex = 0; pIndex < packOut->textureParts.length; pIndex++)
		{
			GmsTexturePart_t* part = DynArrayGet(&packOut->textureParts, GmsTexturePart_t, pIndex);
			Assert(part != nullptr);
			if (font->partOffset == part->fileOffset)
			{
				foundTexturePart = true;
				font->partPntr = part;
				break;
			}
		}
		if (!foundTexturePart) { PrintLine_E("Failed to find texture part for font[%u] which has texture part address 0x%08X", fIndex, font->partOffset); }
	}
	
	// +==============================+
	// |    Duplicate Entity Check    |
	// +==============================+
	for (u32 rIndex = 0; rIndex < packOut->rooms.length; rIndex++)
	{
		GmsRoom_t* room = DynArrayGet(&packOut->rooms, GmsRoom_t, rIndex);
		Assert(room != nullptr);
		
		#if 0
		for (u32 oIndex1 = 0; oIndex1 < room->objects.length; oIndex1++)
		{
			GmsRoomObject_t* object1 = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex1);
			Assert(object1 != nullptr);
			for (u32 oIndex2 = oIndex1+1; oIndex2 < room->objects.length; oIndex2++)
			{
				GmsRoomObject_t* object2 = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex2);
				Assert(object2 != nullptr);
				if (object1->id == object2->id && object1->position == object2->position)
				{
					PrintLine_E("Found duplicate entity in room \"%s\" objects[%u] and [%u] type id %u at (%d, %d)", room->name, oIndex1, oIndex2, object1->id, object1->position.x, object1->position.y);
				}
			}
		}
		#endif
	}
	
	// +==============================+
	// | Connect Variables with Code  |
	// +==============================+
	for (u32 vIndex = 0; vIndex < packOut->variables.length; vIndex++)
	{
		GmsVariable_t* variable = DynArrayGet(&packOut->variables, GmsVariable_t, vIndex);
		NotNull(variable);
		variable->codeIndex = -1;
		if (variable->firstCodeReference > 0)
		{
			Assert(variable->numCodeReferences > 0);
			u32 refOffset = variable->firstCodeReference;
			u32 prevOffset = 0;
			for (u32 refIndex = 0; refIndex < (u32)variable->numCodeReferences; refIndex++)
			{
				u32* refWordsPntr = (u32*)GetFilePntrSized(&gmsFile, refOffset, sizeof(u32)*2);
				NotNull(refWordsPntr);
				// if (GetOpCode(refWordsPntr[0]) != GmlOpCode_Call)
				// {
				// 	//NOTE: For some reason the firstCodeReference address in Akurra packs are pointing to the input
				// 	//      word for Call() rather than the Call() itself
				// 	Assert(refOffset >= sizeof(u32));
				// 	refOffset -= sizeof(u32);
				// 	refWordsPntr--;
				// }
				u32 refWord1 = refWordsPntr[0];
				u32 refWord2 = refWordsPntr[1];
				bool knownOpCode = false;
				if (GetOpCode(refWord1) == GmlOpCode_Pop || GetOpCode(refWord1) == GmlOpCode_Push)
				{
					u8 type2 = ((GetUpperByte(refWord1) & 0x0F) >> 0);
					if (type2 == GmlDataType_Variable)
					{
						knownOpCode = true;
					}
				}
				else if (GetOpCode(refWord1) == GmlOpCode_PushLoc || GetOpCode(refWord1) == GmlOpCode_PushGlb || GetOpCode(refWord1) == GmlOpCode_PushBltn)
				{
					//TODO: Update this a bit when we get more information about the syntax of PushLoc and PushGlb?
					knownOpCode = true;
				}
				else
				{
					//TODO: This seems dumb. Maybe it's more complicated than this?
					PrintLine_W("Ended var[%u] walk at %u/%u", vIndex, refIndex+1, variable->numCodeReferences);
					break;
				}
				
				if (variable->codeRefOffsets.items == nullptr)
				{
					CreateDynamicArray(&variable->codeRefOffsets, mainHeap, sizeof(u32), 1, (u32)variable->numCodeReferences);
				}
				u32* newOffsetPntr = DynArrayAdd(&variable->codeRefOffsets, u32);
				NotNull(newOffsetPntr);
				*newOffsetPntr = refOffset;
				
				bool foundCode = false;
				for (u32 cIndex = 0; cIndex < packOut->codes.length; cIndex++)
				{
					GmsCode_t* code = DynArrayGet(&packOut->codes, GmsCode_t, cIndex);
					NotNull(code);
					if (refOffset >= code->bytecodeOffset && refOffset < code->bytecodeOffset + code->bytecodeLength)
					{
						if (code->variableIndices.items == nullptr)
						{
							CreateDynamicArray(&code->variableIndices, mainHeap, sizeof(u32));
						}
						
						bool isAlreadyIndexedInCode = false;
						for (u32 iIndex = 0; iIndex < code->variableIndices.length; iIndex++)
						{
							u32* existingIndex = DynArrayGet(&code->variableIndices, u32, iIndex);
							NotNull(existingIndex);
							if (*existingIndex == vIndex)
							{
								isAlreadyIndexedInCode = true;
								break;
							}
						}
						
						if (!isAlreadyIndexedInCode)
						{
							u32* newIndexPntr = DynArrayAdd(&code->variableIndices, u32);
							NotNull(newIndexPntr);
							*newIndexPntr = vIndex;
						}
						if (refIndex == 0)
						{
							variable->codeIndex = (i32)cIndex;
						}
						
						foundCode = true;
					}
				}
				if (!foundCode)
				{
					PrintLine_W("Couldn't find code for variable[%u] which has it's reference[%u] at 0x%08X", vIndex, refIndex, refOffset);
				}
				
				//The last Call should have a value that matches the index of the string that holds the variable name
				if (refIndex+1 == (u32)variable->numCodeReferences)
				{
					Assert(GetNonOpCode(refWord2) < packOut->strings.length);
					GmsString_t* gmsString = DynArrayGet(&packOut->strings, GmsString_t, GetNonOpCode(refWord2));
					NotNull(gmsString);
					Assert(MyStrCompareNt(gmsString->pntr, variable->name) == 0);
				}
				prevOffset = refOffset;
				refOffset += GetNonOpCode(refWord2);
			}
		}
	}
	
	// +==============================+
	// | Connect Functions with Code  |
	// +==============================+
	for (u32 fIndex = 0; fIndex < packOut->functions.length; fIndex++)
	{
		GmsFunction_t* function = DynArrayGet(&packOut->functions, GmsFunction_t, fIndex);
		NotNull(function);
		function->codeIndex = -1;
		if (function->firstCodeReference > 0)
		{
			Assert(function->unknown0 > 0);
			u32 refOffset = function->firstCodeReference;
			u32 prevOffset = 0;
			for (u32 refIndex = 0; refIndex < (u32)function->unknown0; refIndex++)
			{
				u32* refWordsPntr = (u32*)GetFilePntrSized(&gmsFile, refOffset, sizeof(u32)*2);
				NotNull(refWordsPntr);
				if (GetOpCode(refWordsPntr[0]) != GmlOpCode_Call && GetOpCode(refWordsPntr[0]) != GmlOpCode_Push)
				{
					//NOTE: For some reason the firstCodeReference address in Akurra packs are pointing to the input
					//      word for Call() rather than the Call() itself
					Assert(refOffset >= sizeof(u32));
					refOffset -= sizeof(u32);
					refWordsPntr--;
				}
				u32 refWord1 = refWordsPntr[0];
				u32 refWord2 = refWordsPntr[1];
				if (GetOpCode(refWord1) != GmlOpCode_Call && GetOpCode(refWord1) != GmlOpCode_Push)
				{
					//TODO: This seems dumb. Maybe it's more complicated than this?
					PrintLine_W("Ended func[%u] walk at %u/%u %02X/%02X", fIndex, refIndex+1, function->unknown0, GetOpCode(refWord1), GetOpCode(refWord2));
					break;
				}
				
				if (function->codeRefOffsets.items == nullptr)
				{
					CreateDynamicArray(&function->codeRefOffsets, mainHeap, sizeof(u32), 1, (u32)function->unknown0);
				}
				u32* newOffsetPntr = DynArrayAdd(&function->codeRefOffsets, u32);
				NotNull(newOffsetPntr);
				*newOffsetPntr = refOffset;
				
				bool foundCode = false;
				for (u32 cIndex = 0; cIndex < packOut->codes.length; cIndex++)
				{
					GmsCode_t* code = DynArrayGet(&packOut->codes, GmsCode_t, cIndex);
					NotNull(code);
					if (refOffset >= code->bytecodeOffset && refOffset < code->bytecodeOffset + code->bytecodeLength)
					{
						if (code->functionIndices.items == nullptr)
						{
							CreateDynamicArray(&code->functionIndices, mainHeap, sizeof(u32));
						}
						
						bool isAlreadyIndexedInCode = false;
						for (u32 iIndex = 0; iIndex < code->functionIndices.length; iIndex++)
						{
							u32* existingIndex = DynArrayGet(&code->functionIndices, u32, iIndex);
							NotNull(existingIndex);
							if (*existingIndex == fIndex)
							{
								isAlreadyIndexedInCode = true;
								break;
							}
						}
						
						if (!isAlreadyIndexedInCode)
						{
							u32* newIndexPntr = DynArrayAdd(&code->functionIndices, u32);
							NotNull(newIndexPntr);
							*newIndexPntr = fIndex;
						}
						if (refIndex == 0)
						{
							function->codeIndex = (i32)cIndex;
						}
						
						foundCode = true;
					}
				}
				if (!foundCode)
				{
					PrintLine_W("Couldn't find code for function[%u] which has it's reference[%u] at 0x%08X", fIndex, refIndex, refOffset);
				}
				
				//The last Call should have a value that matches the index of the string that holds the function name
				if (refIndex+1 == (u32)function->unknown0)
				{
					Assert(refWord2 < packOut->strings.length);
					GmsString_t* gmsString = DynArrayGet(&packOut->strings, GmsString_t, refWord2);
					NotNull(gmsString);
					Assert(MyStrCompareNt(gmsString->pntr, function->name) == 0);
				}
				prevOffset = refOffset;
				refOffset += refWord2;
			}
		}
	}
	
	// +==============================+
	// |         Modify Room          |
	// +==============================+
	#if 0
	{
		bool copySuccess = MyCopyFile(filePath, OUTPUT_DATA_FILE_PATH);
		if (!copySuccess)
		{
			PrintLine_D("Failed to copy data file from \"%s\" to \"%s\"", filePath, OUTPUT_DATA_FILE_PATH);
			return false;
		}
		
		bool replacedRoom = false;
		for (u32 rIndex = 0; rIndex < packOut->rooms.length; rIndex++)
		{
			GmsRoom_t* room = DynArrayGet(&packOut->rooms, GmsRoom_t, rIndex);
			Assert(room != nullptr);
			
			if (room->name != nullptr && StrCompareIgnoreCaseNt(room->name, REPLACE_LEVEL_NAME))
			{
				PrintLine_D("Replacing room[%u] \"%s\"", rIndex, room->name);
				
				bool foundTileLayer = false;
				bool foundCollisionLayer = false;
				bool foundObjectsLayer = false;
				u32 numObjectsPlaced = 0;
				u32 numCollisionsPlaced = 0;
				u32 numStuffedObjects = 0;
				u32 numStuffedCollisions = 0;
				for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
				{
					GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
					Assert(layer != nullptr);
					if (layer->type == 4) //tile layer
					{
						Assert(layer->gridSize == levelSize);
						bool replaceSuccess = ReplacePortionOfFile(OUTPUT_DATA_FILE_PATH, layer->tilesFileOffset, tileIds, sizeof(u32)*numTiles);
						Assert(replaceSuccess);
						foundTileLayer = true;
						break;
					}
					if (layer->type == 2 && StrCompareIgnoreCaseNt(layer->name, "collision_layer"))
					{
						foundCollisionLayer = true;
						for (u32 oIndex = 0; oIndex < layer->instances.length; oIndex++)
						{
							u32 instanceId = *DynArrayGet(&layer->instances, u32, oIndex);
							GmsRoomObject_t* roomObject = nullptr;
							for (u32 objIndex = 0; objIndex < room->objects.length; objIndex++)
							{
								GmsRoomObject_t* nextRoomObj = DynArrayGet(&room->objects, GmsRoomObject_t, objIndex);
								Assert(nextRoomObj != nullptr);
								if (nextRoomObj->instanceId >= 0 && (u32)nextRoomObj->instanceId == instanceId)
								{
									roomObject = nextRoomObj;
									break;
								}
							}
							Assert(roomObject != nullptr);
							GmsHeader_RoomObject_t newHeader = {};
							if (numCollisionsPlaced < newCollisions.length)
							{
								GmsRoomObject_t* newObject = DynArrayGet(&newCollisions, GmsRoomObject_t, numCollisionsPlaced);
								Assert(newObject != nullptr);
								newHeader.position = newObject->position;
								newHeader.id = newObject->id;
								numCollisionsPlaced++;
							}
							else
							{
								//"Stuff" the entity with something that doesn't matter
								newHeader.position = NewVec2i(-16, -16);
								newHeader.id = solidWallId;
								numStuffedCollisions++;
							}
							
							Assert(STRUCT_VAR_OFFSET(GmsHeader_RoomObject_t, position) == 0);
							u32 writeLocation = roomObject->offset;
							u32 numBytesToWrite = STRUCT_VAR_END_OFFSET(GmsHeader_RoomObject_t, id);
							bool replaceSuccess = ReplacePortionOfFile(OUTPUT_DATA_FILE_PATH, writeLocation, &newHeader, numBytesToWrite);
							Assert(replaceSuccess);
						}
					}
					if (layer->type == 2 && StrCompareIgnoreCaseNt(layer->name, "object_layer"))
					{
						foundObjectsLayer = true;
						for (u32 oIndex = 0; oIndex < layer->instances.length; oIndex++)
						{
							u32 instanceId = *DynArrayGet(&layer->instances, u32, oIndex);
							GmsRoomObject_t* roomObject = nullptr;
							for (u32 objIndex = 0; objIndex < room->objects.length; objIndex++)
							{
								GmsRoomObject_t* nextRoomObj = DynArrayGet(&room->objects, GmsRoomObject_t, objIndex);
								Assert(nextRoomObj != nullptr);
								if (nextRoomObj->instanceId >= 0 && (u32)nextRoomObj->instanceId == instanceId)
								{
									roomObject = nextRoomObj;
									break;
								}
							}
							Assert(roomObject != nullptr);
							GmsHeader_RoomObject_t newHeader = {};
							if (numObjectsPlaced < newObjects.length)
							{
								GmsRoomObject_t* newObject = DynArrayGet(&newObjects, GmsRoomObject_t, numObjectsPlaced);
								Assert(newObject != nullptr);
								newHeader.position = newObject->position;
								newHeader.id = newObject->id;
								numObjectsPlaced++;
							}
							else
							{
								//"Stuff" the entity with something that doesn't matter
								newHeader.position = NewVec2i(-16, -16);
								newHeader.id = buttonId;
								numStuffedObjects++;
							}
							
							Assert(STRUCT_VAR_OFFSET(GmsHeader_RoomObject_t, position) == 0);
							u32 writeLocation = roomObject->offset;
							u32 numBytesToWrite = STRUCT_VAR_END_OFFSET(GmsHeader_RoomObject_t, id);
							bool replaceSuccess = ReplacePortionOfFile(OUTPUT_DATA_FILE_PATH, writeLocation, &newHeader, numBytesToWrite);
							Assert(replaceSuccess);
						}
					}
				}
				if (!foundTileLayer) { WriteLine_D("Failed to find the tiles layer in the specified room!"); }
				if (!foundCollisionLayer) { WriteLine_D("Failed to find the collision layer in the specified room!"); }
				if (!foundObjectsLayer) { WriteLine_D("Failed to find the objects layer in the specified room!"); }
				if (numObjectsPlaced < newObjects.length) { PrintLine_E("Could only fit %u/%u desired objects in this room without affecting offsets", numObjectsPlaced, newObjects.length); }
				if (numCollisionsPlaced < newCollisions.length) { PrintLine_E("Could only fit %u/%u desired collisions in this room without affecting offsets", numObjectsPlaced, newCollisions.length); }
				if (numStuffedCollisions > 0) { PrintLine_D("Stuffed %u collisions in room", numStuffedCollisions); }
				if (numStuffedObjects > 0) { PrintLine_D("Stuffed %u objects in room", numStuffedObjects); }
				replacedRoom = true;
				break;
			}
		}
		if (!replacedRoom)
		{
			WriteLine_W("WARNING: Couldn't find room to replace");
		}
		
		FreeFileMemory(&pcqLevelFile);
		TempPopMark();
	}
	#endif
	
	r32 memUsagePercent = (r32)memArena->used / (r32)memArena->size;
	PrintLine_D("Memory usage: %.2f%% (%s / %s)", memUsagePercent*100, FormattedSizeStr(memArena->used), FormattedSizeStr(memArena->size));
	
	FreeFileMemory(&gmsFile);
	packOut->filled = true;
	return true;
}

// +--------------------------------------------------------------+
// |                           Helpers                            |
// +--------------------------------------------------------------+
GmsString_t* GetGmsStringInPack(GmsPack_t* pack, const char* str, bool considerReplaced)
{
	NotNull(pack);
	NotNull(str);
	for (u32 sIndex = 0; sIndex < pack->strings.length; sIndex++)
	{
		GmsString_t* gmsString = DynArrayGet(&pack->strings, GmsString_t, sIndex);
		NotNull(gmsString);
		if (considerReplaced && gmsString->replacedValue != nullptr)
		{
			if (gmsString->replacedValue != nullptr && MyStrCompareNt(gmsString->replacedValue, str) == 0)
			{
				return gmsString;
			}
		}
		else if (gmsString->pntr != nullptr && MyStrCompareNt(gmsString->pntr, str) == 0)
		{
			return gmsString;
		}
	}
	return nullptr;
}
const GmsString_t* GetGmsStringInPack(const GmsPack_t* pack, const char* str, bool considerReplaced) //const variant
{
	return (const GmsString_t*)GetGmsStringInPack((GmsPack_t*)pack, str, considerReplaced);
}

void DestroyGmsRoom(MemoryArena_t* memArena, GmsRoom_t* room)
{
	NotNull(memArena);
	NotNull(room);
	if (room->name != nullptr) { ArenaPop(memArena, room->name); }
	if (room->caption != nullptr) { ArenaPop(memArena, room->caption); }
	DestroyDynamicArray(&room->backgrounds);
	DestroyDynamicArray(&room->views);
	DestroyDynamicArray(&room->objects);
	DestroyDynamicArray(&room->tiles);
	for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
	{
		GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
		NotNull(layer);
		if (layer->type == GmsEnum_LayerType_Background)
		{
			DestroyDynamicArray(&layer->backgrounds);
		}
		else if (layer->type == GmsEnum_LayerType_Instance)
		{
			DestroyDynamicArray(&layer->instances);
		}
		else if (layer->type == GmsEnum_LayerType_Tile)
		{
			DestroyDynamicArray(&layer->tiles);
		}
	}
	DestroyDynamicArray(&room->layers);
}
void DestroyGmsRoomList(MemoryArena_t* memArena, DynArray_t* roomList)
{
	NotNull(memArena);
	NotNull(roomList);
	for (u32 rIndex = 0; rIndex < roomList->length; rIndex++)
	{
		GmsRoom_t* room = DynArrayGet(roomList, GmsRoom_t, rIndex);
		NotNull(room);
		DestroyGmsRoom(memArena, room);
	}
	DestroyDynamicArray(roomList);
}
void CopyGmsRoom(MemoryArena_t* memArena, GmsRoom_t* dest, const GmsRoom_t* source)
{
	NotNull(memArena);
	NotNull(dest);
	NotNull(source);
	
	MyMemCopy(dest, source, sizeof(GmsRoom_t));
	
	if (source->name != nullptr) { dest->name = ArenaNtString(mainHeap, source->name); }
	if (source->caption != nullptr) { dest->caption = ArenaNtString(mainHeap, source->caption); }
	
	DynArrayCopy(&dest->backgrounds, &source->backgrounds, mainHeap);
	DynArrayCopy(&dest->views, &source->views, mainHeap);
	DynArrayCopy(&dest->objects, &source->objects, mainHeap);
	DynArrayCopy(&dest->tiles, &source->tiles, mainHeap);
	DynArrayCopy(&dest->layers, &source->layers, mainHeap);
	
	for (u32 lIndex = 0; lIndex < dest->layers.length; lIndex++)
	{
		const GmsRoomLayer_t* sourceLayer = DynArrayGet(&source->layers, GmsRoomLayer_t, lIndex); NotNull(sourceLayer);
		GmsRoomLayer_t* destLayer = DynArrayGet(&dest->layers, GmsRoomLayer_t, lIndex); NotNull(destLayer);
		if (sourceLayer->type == GmsEnum_LayerType_Background)
		{
			DynArrayCopy(&destLayer->backgrounds, &sourceLayer->backgrounds, mainHeap);
		}
		else if (sourceLayer->type == GmsEnum_LayerType_Instance)
		{
			DynArrayCopy(&destLayer->instances, &sourceLayer->instances, mainHeap);
		}
		else if (sourceLayer->type == GmsEnum_LayerType_Tile)
		{
			DynArrayCopy(&destLayer->tiles, &sourceLayer->tiles, mainHeap);
		}
	}
}

GmsRoom_t* FindGmsRoomByName(GmsPack_t* pack, const char* name)
{
	NotNull(pack);
	Assert(pack->filled);
	NotNull(name);
	for (u32 rIndex = 0; rIndex < pack->rooms.length; rIndex++)
	{
		GmsRoom_t* room = DynArrayGet(&pack->rooms, GmsRoom_t, rIndex);
		NotNull(room);
		if (StrCompareIgnoreCaseNt(room->name, name))
		{
			return room;
		}
	}
	return nullptr;
}
GmsRoomObject_t* FindGmsRoomObjectById(GmsRoom_t* room, i32 objectId, u32 skip = 0)
{
	NotNull(room);
	u32 findIndex = 0;
	for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
	{
		GmsRoomObject_t* object = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex);
		NotNull(object);
		if (object->id == objectId)
		{
			if (findIndex >= skip) { return object; }
			findIndex++;
		}
	}
	return nullptr;
}
