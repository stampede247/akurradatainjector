/*
File:   commands.cpp
Author: Taylor Robbins
Date:   12\14\2020
Description: 
	** Holds the HandleCommand function which handles all the text commands that get sent to us by the front-end user input box
*/

const char* commandInfo[] = {
	"help",                             "Displays the list of commands",
	"test",                             "Holds test code",
	"memory",                           "Displays memory usage",
	"room [category] [room_name]",      "Displays info about a room",
	"obj_instance [id] [room_name]",    "Displays info about a specific object instance",
	"image_click [x] [y] [image_name]", "Finds all objects and tiles at a point in a level image",
	"gen_info",                         "Displays the list of commands",
	"code_info {index}",                "Displays information about all codes or about a specific one",
	"variable_info",                    "Displays information about all variables",
	"fucntion_info",                    "Displays information about all functions",
	"script_info",                      "Displays information about all scripts",
	"code_str_usages [str_number]",     "Finds all usages of a specific string in bytecode",
	"var_counts",                       "Displays some counts for variable types",
};

bool HandleCommand(const char* commandStr)
{
	u32 numArgStrs = 0;
	StrSplitPiece_t* argStrs = SplitString(TempArena, commandStr, MyStrLength32(commandStr), " ", 1, &numArgStrs);
	if (numArgStrs < 1)
	{
		PrintLine_E("Empty command: \"%s\"", commandStr);
		return false;
	}
	char* cmdStr = ArenaString(TempArena, argStrs[0].pntr, argStrs[0].length);
	argStrs++;
	numArgStrs--;
	
	// +==============================+
	// |             help             |
	// +==============================+
	if (StrCompareIgnoreCaseNt(cmdStr, "help"))
	{
		PrintLine_N("There are %u commands", ArrayCount(commandInfo)/2);
		for (u32 cIndex = 0; (cIndex*2) + 2 <= ArrayCount(commandInfo); cIndex++)
		{
			PrintLine_I("  %s: %s", commandInfo[cIndex*2 + 0], commandInfo[cIndex*2 + 1]);
		}
	}
	// +==============================+
	// |             test             |
	// +==============================+
	else if (StrCompareIgnoreCaseNt(cmdStr, "test"))
	{
		WriteLine_I("Test would go here");
		
		PrintLine_D("Next Layer ID: %u (0x%08X)", main->pack.nextLayerId, main->pack.nextLayerId);
		PrintLine_D("Next Instance ID: %u (0x%08X)", main->pack.nextInstanceId, main->pack.nextInstanceId);
		
		u32 numInstances = 0;
		i32 minInstanceId = -1;
		i32 maxInstanceId = -1;
		u32 numLayers = 0;
		i32 minLayerId = -1;
		i32 maxLayerId = -1;
		for (u32 rIndex = 0; rIndex < main->pack.rooms.length; rIndex++)
		{
			GmsRoom_t* room = DynArrayGet(&main->pack.rooms, GmsRoom_t, rIndex);
			NotNull(room);
			for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
			{
				GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
				NotNull(layer);
				numLayers++;
				if (minLayerId < 0 || layer->id < minLayerId) { minLayerId = layer->id; }
				if (maxLayerId < 0 || layer->id > maxLayerId) { maxLayerId = layer->id; }
				if (layer->type == GmsEnum_LayerType_Instance)
				{
					for (u32 iIndex = 0; iIndex < layer->instances.length; iIndex++)
					{
						i32* instanceIdPntr = DynArrayGet(&layer->instances, i32, iIndex);
						NotNull(instanceIdPntr);
						i32 instanceId = *instanceIdPntr;
						numInstances++;
						if (minInstanceId < 0 || instanceId < minInstanceId) { minInstanceId = instanceId; }
						if (maxInstanceId < 0 || instanceId > maxInstanceId) { maxInstanceId = instanceId; }
					}
				}
			}
		}
		
		PrintLine_I("Room Count: %u", main->pack.rooms.length);
		PrintLine_I("Layer Count: %u", numLayers);
		PrintLine_D("ID Range: %d - %d", minLayerId, maxLayerId);
		PrintLine_I("Instance Count: %u", numInstances);
		PrintLine_D("ID Range: %d - %d", minInstanceId, maxInstanceId);
	}
	// +==============================+
	// |            memory            |
	// +==============================+
	else if (StrCompareIgnoreCaseNt(cmdStr, "memory"))
	{
		PrintLine_D("Main Heap: %s / %s (%.1f%%)", FormattedSizeStr(mainHeap->used), FormattedSizeStr(mainHeap->size), (r32)mainHeap->used / (r32)mainHeap->size * 100);
		PrintLine_D("Temp Arena: %s / %s (%.1f%%) (%u marks)", FormattedSizeStr(TempArena->used), FormattedSizeStr(TempArena->size), (r32)TempArena->used / (r32)TempArena->size * 100, ArenaNumMarks(TempArena));
	}
	// +==============================+
	// |             room             |
	// +==============================+
	else if (StrCompareIgnoreCaseNt(cmdStr, "room"))
	{
		if (numArgStrs < 2) { WriteLine_E("Usage: room [category] [room_name]"); return true; }
		if (!main->pack.filled) { WriteLine_E("Pack is not loaded"); return true; }
		
		bool foundRoom = false;
		for (u32 rIndex = 0; rIndex < main->pack.rooms.length; rIndex++)
		{
			GmsRoom_t* room = DynArrayGet(&main->pack.rooms, GmsRoom_t, rIndex);
			NotNull(room);
			if (StrCompareIgnoreCaseNt(room->name, argStrs[1].pntr))
			{
				if (DoesSplitPieceEqualIgnoreCaseNt(&argStrs[0], "info"))
				{
					PrintLine_N("Room[%u]: \"%s\" (0x%08X - 0x%08X)", rIndex, room->name, room->offset, room->endOffset);
					PrintLine_D("  caption: \"%s\"", room->caption);
					PrintLine_I("  size: (%d,%d)", room->size.x, room->size.y);
					PrintLine_D("  isPersistent:          %s", room->isPersistent ? "True" : "False");
					PrintLine_D("  creationCodeId:        %d", room->creationCodeId);
					PrintLine_D("  flags:                 0x%08X", room->flags);
					PrintLine_D("  physicsEnabled:        %s", room->physicsEnabled ? "True" : "False");
					PrintLine_D("  gravity:               (%f, %f)", room->gravity.x, room->gravity.y);
					PrintLine_D("  metersPerPixel:        %f", room->metersPerPixel);
					PrintLine_I("  %u backgrounds (0x%08X)", room->backgrounds.length, room->backgroundsOffset);
					PrintLine_I("  %u views (0x%08X)",       room->views.length, room->viewsOffset);
					PrintLine_I("  %u objects (0x%08X)",     room->objects.length, room->objectsOffset);
					PrintLine_I("  %u tiles (0x%08X)",       room->tiles.length, room->tilesOffset);
					PrintLine_I("  %u layers (0x%08X)",      room->layers.length, room->layersOffset);
					if (room->hasUnknownList)
					{
						PrintLine_I("  %u unknowns (0x%08X-0x%08X)", room->unknownListLength, room->unknownListOffset, room->unknownListEndOffset);
					}
					PrintLine_D("  unknown1:        0x%08X", room->unknown1);
					PrintLine_D("  unknown2:        0x%08X", room->unknown2);
					PrintLine_D("  unknown3:        0x%08X", room->unknown3);
					PrintLine_D("  unknown4:        0x%08X 0x%08X 0x%08X 0x%08X", room->unknown4[0], room->unknown4[1], room->unknown4[2], room->unknown4[3]);
				}
				// +==============================+
				// |          Room ASCII          |
				// +==============================+
				else if (DoesSplitPieceEqualIgnoreCaseNt(&argStrs[0], "ascii"))
				{
					TempPushMark();
					u32 numMisalignedEntities = 0;
					DynArray_t objectIds = {};
					CreateDynamicArray(&objectIds, TempArena, sizeof(i32));
					for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
					{
						GmsRoomObject_t* object = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex);
						Assert(object != nullptr);
						bool foundDuplicateId = false;
						for (u32 iIndex = 0; iIndex < objectIds.length; iIndex++)
						{
							i32 foundId = *DynArrayGet(&objectIds, i32, iIndex);
							if (foundId == object->id) { foundDuplicateId = true; break; }
						}
						if (!foundDuplicateId)
						{
							i32* newIdSpace = DynArrayAdd(&objectIds, i32);
							Assert(newIdSpace != nullptr);
							*newIdSpace = object->id;
						}
						if ((object->position.x % 16) != 0 || (object->position.y % 16) != 0)
						{
							numMisalignedEntities++;
						}
					}
					PrintLine_D("%u Unique IDs out of %u objects (%u misaligned)", objectIds.length, room->objects.length, numMisalignedEntities);
					char displayChars[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
					for (u32 iIndex = 0; iIndex < objectIds.length; iIndex++)
					{
						i32 foundId = *DynArrayGet(&objectIds, i32, iIndex);
						if (foundId == 6)   { displayChars[iIndex] = '['; } //game controller            "code_game_controller"
						if (foundId == 18)  { displayChars[iIndex] = ']'; } //music controller           "obj_music_controller"
						if (foundId == 19)  { displayChars[iIndex] = '['; } //ambience_parent            "obj_ambiance_parent"
						if (foundId == 24)  { displayChars[iIndex] = '.'; } //map edge blocker           "obj_block_blocker"
						if (foundId == 34)  { displayChars[iIndex] = '#'; } //wall                       "obj_solid"
						if (foundId == 43)  { displayChars[iIndex] = '-'; } //drop-top                   "obj_drop_top"
						if (foundId == 44)  { displayChars[iIndex] = '-'; } //drop-bottom                "obj_drop_bottom"
						if (foundId == 45)  { displayChars[iIndex] = '/'; } //spike pit                  "obj_hazard_parent"
						if (foundId == 46)  { displayChars[iIndex] = '~'; } //water                      "obj_deep_water"
						if (foundId == 76)  { displayChars[iIndex] = '='; } //boarded hole               "obj_covered_spike_pit"
						if (foundId == 102) { displayChars[iIndex] = '%'; } //player?                    "obj_player_wash_ashore"
						if (foundId == 109) { displayChars[iIndex] = '*'; } //box                        "obj_wooden_block"
						if (foundId == 188) { displayChars[iIndex] = '@'; } //1 key door                 "obj_ancient_lock"
						if (foundId == 189) { displayChars[iIndex] = '@'; } //3 key large door           "obj_lock_tripple_ancient"
						if (foundId == 202) { displayChars[iIndex] = '>'; } //right arrow door           "obj_snake_block_0"
						if (foundId == 204) { displayChars[iIndex] = '<'; } //left arrow door            "obj_snake_block_180"
						if (foundId == 207) { displayChars[iIndex] = '>'; } //right secondary arrow door "obj_snake_block_inert_0"
						if (foundId == 210) { displayChars[iIndex] = 'v'; } //down secondary arrow door  "obj_snake_block_270_inert"
						if (foundId == 218) { displayChars[iIndex] = ']'; } //toggle wall button         "obj_button_square"
						if (foundId == 221) { displayChars[iIndex] = '-'; } //toggle wall down           "obj_pillar_square_down"
						if (foundId == 224) { displayChars[iIndex] = '+'; } //toggle wall up             "obj_pillar_square_up"
						if (foundId == 228) { displayChars[iIndex] = '1'; } //red door                   "obj_bronze_pillar"
						if (foundId == 232) { displayChars[iIndex] = '1'; } //red arch                   "obj_button_bronze"
						if (foundId == 229) { displayChars[iIndex] = '2'; } //blue door                  "obj_silver_pillar"
						if (foundId == 233) { displayChars[iIndex] = '2'; } //blue arch                  "obj_button_silver"
						if (foundId == 230) { displayChars[iIndex] = '3'; } //yellow door                "obj_gold_pillar"
						if (foundId == 234) { displayChars[iIndex] = '3'; } //yellow arch                "obj_button_gold"
						if (foundId == 246) { displayChars[iIndex] = 'o'; } //hole                       "obj_spike_pit"
						if (foundId == 286) { displayChars[iIndex] = '&'; } //stoney boi                 "obj_stone_elemental"
						if (foundId == 307) { displayChars[iIndex] = '"'; } //key                        "obj_ancient_key"
						char displayChar = '?';
						if (iIndex < ArrayCount(displayChars)) { displayChar = displayChars[iIndex]; }
						char* objectName = "[Unknown]";
						if (foundId >= 0 && (u32)foundId < main->pack.objects.length)
						{
							GmsObject_t* objectPntr = DynArrayGet(&main->pack.objects, GmsObject_t, (u32)foundId);
							Assert(objectPntr != nullptr);
							if (objectPntr->name != nullptr) { objectName = objectPntr->name; }
							else { objectName = "[Unnamed]"; }
						}
						PrintLine_D("%c: %u %s", displayChar, foundId, objectName);
					}
					WriteLine_D("!: More than one entity");
					for (i32 gridY = 0; gridY*16 <= room->size.height; gridY++)
					{
						for (i32 gridX = 0; gridX*16 <= room->size.width; gridX++)
						{
							v2i position = NewVec2i(gridX*16, gridY*16);
							char displayChar = ' ';
							u32 numObjectsInSpot = 0;
							for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
							{
								GmsRoomObject_t* object = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex);
								Assert(object != nullptr);
								if (object->position == position)
								{
									if (numObjectsInSpot == 0)
									{
										bool foundObjectIdIndex = false;
										u32 objectIdIndex = 0;
										for (u32 iIndex = 0; iIndex < objectIds.length; iIndex++)
										{
											i32 foundId = *DynArrayGet(&objectIds, i32, iIndex);
											if (object->id == foundId) { objectIdIndex = iIndex; foundObjectIdIndex = true; break; }
										}
										Assert(foundObjectIdIndex);
										if (objectIdIndex < ArrayCount(displayChars)) { displayChar = displayChars[objectIdIndex]; }
										else { displayChar = '?'; }
									}
									numObjectsInSpot++;
								}
							}
							if (numObjectsInSpot > 1) { displayChar = '!'; }
							Print_D("%c", displayChar);
						}
						WriteLine_D("");
					}
					// DestroyDynamicArray(&objectIds);
					TempPopMark();
				}
				// +==============================+
				// |       Room Backgrounds       |
				// +==============================+
				else if (DoesSplitPieceEqualIgnoreCaseNt(&argStrs[0], "backgrounds"))
				{
					PrintLine_N("Room has %u backgrounds:", room->backgrounds.length);
					for (u32 bIndex = 0; bIndex < room->backgrounds.length; bIndex++)
					{
						GmsRoomBackground_t* background = DynArrayGet(&room->backgrounds, GmsRoomBackground_t, bIndex);
						NotNull(background);
						if (background->isEnabled)
						{
							PrintLine_I("Background[%u]: Enabled (0x%08X)", bIndex, background->offset);
							PrintLine_D("  isForeground: %s", background->isForeground ? "True" : "False");
							PrintLine_D("  id:           %d", background->id);
							PrintLine_D("  position:     (%d, %d)", background->position.x, background->position.y);
							PrintLine_D("  isTileX:      %s", background->isTileX ? "True" : "False");
							PrintLine_D("  isTileY:      %s", background->isTileY ? "True" : "False");
							PrintLine_D("  speed:        (%d, %d)", background->speed.x, background->speed.y);
							PrintLine_D("  isStretch:    %s", background->isStretch ? "True" : "False");
						}
						else
						{
							PrintLine_D("Background[%u]: Disabled (0x%08X)", bIndex, background->offset);
						}
					}
				}
				// +==============================+
				// |          Room Views          |
				// +==============================+
				else if (DoesSplitPieceEqualIgnoreCaseNt(&argStrs[0], "views"))
				{
					PrintLine_N("Room has %u views:", room->views.length);
					for (u32 vIndex = 0; vIndex < room->views.length; vIndex++)
					{
						GmsView_t* view = DynArrayGet(&room->views, GmsView_t, vIndex);
						NotNull(view);
						if (view->isEnabled)
						{
							PrintLine_I("View[%u]: Enabled (0x%08X)", vIndex, view->offset);
							PrintLine_D("  position:     (%d, %d)", view->position.x,     view->position.y);
							PrintLine_D("  size:         (%d, %d)", view->size.x,         view->size.y);
							PrintLine_D("  portPosition: (%d, %d)", view->portPosition.x, view->portPosition.y);
							PrintLine_D("  portSize:     (%d, %d)", view->portSize.x,     view->portSize.y);
							PrintLine_D("  border:       (%d, %d)", view->border.x,       view->border.y);
							PrintLine_D("  speed:        (%d, %d)", view->speed.x,        view->speed.y);
							PrintLine_D("  objectId:     %d", view->objectId);
						}
						else
						{
							PrintLine_D("View[%u]: Disabled (0x%08X)", vIndex, view->offset);
						}
					}
				}
				// +==============================+
				// |         Room Objects         |
				// +==============================+
				else if (DoesSplitPieceEqualIgnoreCaseNt(&argStrs[0], "objects"))
				{
					u32 numInstancesPrinted = 0;
					PrintLine_N("Room has %u objects:", room->objects.length);
					for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
					{
						GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
						NotNull(layer);
						if (layer->type == GmsEnum_LayerType_Instance)
						{
							PrintLine_O("Layer[%u]: \"%s\" %u instances (0x%08X)", lIndex, layer->name, layer->instances.length, layer->offset);
							for (u32 iIndex = 0; iIndex < layer->instances.length; iIndex++)
							{
								i32 instanceId = *DynArrayGet(&layer->instances, i32, iIndex);
								GmsRoomObject_t* roomObject = nullptr;
								for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
								{
									GmsRoomObject_t* thisRoomObject = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex);
									NotNull(thisRoomObject);
									if (thisRoomObject->instanceId == instanceId) { roomObject = thisRoomObject; break; }
								}
								if (roomObject != nullptr)
								{
									PrintGmsRoomObject("  ", iIndex, roomObject);
									numInstancesPrinted++;
								}
								else
								{
									PrintLine_W("  Obj[%u]: ID %d [Not Found]", iIndex, instanceId);
								}
							}
						}
					}
					if (numInstancesPrinted < room->objects.length)
					{
						//TODO: Shouldn't this be room->objects.length - numInstancesPrinted?
						PrintLine_E("%u instances are not represented in the any of the %u layers", room->objects.length, room->layers.length);
					}
				}
				// +==============================+
				// |          Room Tiles          |
				// +==============================+
				else if (DoesSplitPieceEqualIgnoreCaseNt(&argStrs[0], "tiles"))
				{
					//TODO: Implement me!
					WriteLine_W("Unimplemented");
				}
				// +==============================+
				// |         Room Layers          |
				// +==============================+
				else if (DoesSplitPieceEqualIgnoreCaseNt(&argStrs[0], "layers"))
				{
					PrintLine_N("%u Layers in this room:", room->layers.length);
					for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
					{
						GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
						NotNull(layer);
						PrintLine_I("Layer[%u]: %s \"%s\" (0x%08X-0x%08X)", lIndex, GetLayerTypeStr(layer->type), layer->name, layer->fileOffset, layer->endOffset);
						PrintLine_D("  id:       %d", layer->id);
						PrintLine_D("  depth:    %d", layer->depth);
						PrintLine_D("  offset:   (%f, %f)", layer->offset.x, layer->offset.y);
						PrintLine_D("  speed:    (%f, %f)", layer->speed.x, layer->speed.y);
						PrintLine_D("  unknown7: 0x%08X", layer->unknown7);
						if (layer->type == GmsEnum_LayerType_Background)
						{
							PrintLine_D("  %u backgrounds:", layer->backgrounds.length);
							for (u32 bIndex = 0; bIndex < layer->backgrounds.length; bIndex++)
							{
								GmsRoomLayerBackground_t* background = DynArrayGet(&layer->backgrounds, GmsRoomLayerBackground_t, bIndex);
								NotNull(background);
								PrintLine_D("  Background[%u]:", bIndex);
								PrintLine_D("    unknown1:          0x%08X", background->unknown1);
								PrintLine_D("    spriteId:          %d", background->spriteId);
								PrintLine_D("    tileX:             %s", background->tileX ? "True" : "False");
								PrintLine_D("    tileY:             %s", background->tileY ? "True" : "False");
								PrintLine_D("    stretch:           %s", background->stretch ? "True" : "False");
								PrintLine_D("    backgroundColor:   0x%08X", background->backgroundColor);
								PrintLine_D("    unknown5:          0x%08X", background->unknown5);
								PrintLine_D("    animationSpeed:    %f", background->animationSpeed);
								PrintLine_D("    isSpeedGameFrames: %s", background->isSpeedGameFrames ? "True" : "False");
							}
						}
						else if (layer->type == GmsEnum_LayerType_Instance)
						{
							PrintLine_I("  %u instances:", layer->instances.length);
						}
						else if (layer->type == GmsEnum_LayerType_Asset)
						{
							//TODO: anything else to print out?
						}
						else if (layer->type == GmsEnum_LayerType_Tile)
						{
							PrintLine_D("  tilesetId: %d", layer->tilesetId);
							PrintLine_D("  gridSize: (%d, %d)", layer->gridSize.width, layer->gridSize.height);
							PrintLine_D("  Tiles (0x%08X - 0x%08X)", layer->tilesFileOffset, layer->tilesFileEndOffset);
						}
					}
				}
				// +==============================+
				// |      Room Objects Layer      |
				// +==============================+
				else if (DoesSplitPieceEqualIgnoreCaseNt(&argStrs[0], "obj_layer"))
				{
					bool foundObjectLayer = false;
					for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
					{
						GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
						NotNull(layer);
						if (layer->type == GmsEnum_LayerType_Instance && StrCompareIgnoreCaseNt(layer->name, "object_layer"))
						{
							PrintLine_N("Layer[%u]: \"%s\" %u instances (0x%08X)", lIndex, layer->name, layer->instances.length, layer->offset);
							for (u32 iIndex = 0; iIndex < layer->instances.length; iIndex++)
							{
								i32 instanceId = *DynArrayGet(&layer->instances, i32, iIndex);
								GmsRoomObject_t* roomObject = nullptr;
								for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
								{
									GmsRoomObject_t* thisRoomObject = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex);
									NotNull(thisRoomObject);
									if (thisRoomObject->instanceId == instanceId) { roomObject = thisRoomObject; break; }
								}
								if (roomObject != nullptr)
								{
									PrintGmsRoomObject("  ", iIndex, roomObject);
								}
								else
								{
									PrintLine_W("  Obj[%u]: ID %d [Not Found]", iIndex, instanceId);
								}
							}
							foundObjectLayer = true;
						}
					}
					if (!foundObjectLayer) { WriteLine_E("There was no layer named \"object_layer\""); }
				}
				// +==============================+
				// |     Room Collision Layer     |
				// +==============================+
				else if (DoesSplitPieceEqualIgnoreCaseNt(&argStrs[0], "col_layer"))
				{
					bool foundCollisionLayer = false;
					for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
					{
						GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
						NotNull(layer);
						if (layer->type == GmsEnum_LayerType_Instance && StrCompareIgnoreCaseNt(layer->name, "collision_layer"))
						{
							PrintLine_N("Layer[%u]: \"%s\" %u instances (0x%08X)", lIndex, layer->name, layer->instances.length, layer->offset);
							for (u32 iIndex = 0; iIndex < layer->instances.length; iIndex++)
							{
								i32 instanceId = *DynArrayGet(&layer->instances, i32, iIndex);
								GmsRoomObject_t* roomObject = nullptr;
								for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
								{
									GmsRoomObject_t* thisRoomObject = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex);
									NotNull(thisRoomObject);
									if (thisRoomObject->instanceId == instanceId) { roomObject = thisRoomObject; break; }
								}
								if (roomObject != nullptr)
								{
									PrintGmsRoomObject("  ", iIndex, roomObject);
								}
								else
								{
									PrintLine_W("  Obj[%u]: ID %d [Not Found]", iIndex, instanceId);
								}
							}
							foundCollisionLayer = true;
						}
					}
					if (!foundCollisionLayer) { WriteLine_E("There was no layer named \"collision_layer\""); }
				}
				else
				{
					PrintLine_E("Unknown category \"%.*s\"", argStrs[0].length, argStrs[0].pntr);
				}
				foundRoom = true;
				break;
			}
		}
		if (!foundRoom) { PrintLine_E("Couldn't find room by the name \"%s\"", argStrs[0].pntr); return true; }
	}
	// +===============================+
	// | obj_instance [id] [room_name] |
	// +===============================+
	else if (StrCompareIgnoreCaseNt(cmdStr, "obj_instance"))
	{
		if (numArgStrs < 2) { WriteLine_E("Usage: obj_instance [id] [room_name]"); return true; }
		if (!main->pack.filled) { WriteLine_E("Pack is not loaded"); return true; }
		
		i32 instanceId = 0;
		if (!TryParseI32(argStrs[0].pntr, argStrs[0].length, &instanceId)) { PrintLine_E("Failed to parse \"%.*s\" as i32", argStrs[0].length, argStrs[0].pntr); }
		
		bool foundInstance = false;
		bool foundRoom = false;
		for (u32 rIndex = 0; rIndex < main->pack.rooms.length; rIndex++)
		{
			GmsRoom_t* room = DynArrayGet(&main->pack.rooms, GmsRoom_t, rIndex);
			NotNull(room);
			if (StrCompareIgnoreCaseNt(room->name, argStrs[1].pntr))
			{
				for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
				{
					GmsRoomObject_t* roomObject = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex);
					NotNull(roomObject);
					if (roomObject->instanceId == instanceId)
					{
						PrintGmsRoomObject("", oIndex, roomObject);
						
						GmsRoomLayer_t* objectLayer = nullptr;
						for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
						{
							GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
							NotNull(layer);
							if (layer->type == GmsEnum_LayerType_Instance)
							{
								for (u32 iIndex = 0; iIndex < layer->instances.length; iIndex++)
								{
									i32* instanceIdPntr = DynArrayGet(&layer->instances, i32, iIndex);
									NotNull(instanceIdPntr);
									if (*instanceIdPntr == roomObject->instanceId)
									{
										objectLayer = layer;
										break;
									}
								}
								if (objectLayer != nullptr) { break; }
							}
						}
						if (objectLayer != nullptr)
						{
							PrintLine_I("Object is in layer \"%s\"", objectLayer->name);
						}
						else
						{
							PrintLine_W("Object is not in any layer! (out of %u)", room->layers.length);
						}
						
						foundInstance = true;
						break;
					}
				}
				foundRoom = true;
				break;
			}
		}
		if (!foundRoom) { PrintLine_E("Couldn't find room by the name \"%s\"", argStrs[0].pntr); return true; }
		if (!foundInstance) { PrintLine_E("Couldn't find object instance with id %u", instanceId); return true; }
	}
	// +==================================+
	// | image_click [x] [y] [image_name] |
	// +==================================+
	else if (StrCompareIgnoreCaseNt(cmdStr, "image_click"))
	{
		if (numArgStrs < 3) { WriteLine_E("Usage: image_click [x] [y] [image_name]"); return true; }
		if (!main->pack.filled) { WriteLine_E("Pack is not loaded"); return true; }
		r32 xValue = 0;
		char* xValueStr = ArenaString(TempArena, argStrs[0].pntr, argStrs[0].length);
		if (!TryParseR32(xValueStr, &xValue)) { PrintLine_E("Couldn't parse X value as r32: \"%s\"", xValueStr); return true; }
		r32 yValue = 0;
		char* yValueStr = ArenaString(TempArena, argStrs[1].pntr, argStrs[1].length);
		if (!TryParseR32(yValueStr, &yValue)) { PrintLine_E("Couldn't parse Y value as r32: \"%s\"", yValueStr); return true; }
		
		v2 clickPos = NewVec2(xValue, yValue);
		bool foundRoom = false;
		for (u32 rIndex = 0; rIndex < main->pack.rooms.length; rIndex++)
		{
			GmsRoom_t* room = DynArrayGet(&main->pack.rooms, GmsRoom_t, rIndex);
			NotNull(room);
			if (StrCompareIgnoreCaseNt(room->name, argStrs[2].pntr))
			{
				foundRoom = true;
				
				rec roomRec = NewRec(Vec2_Zero, NewVec2(room->size));
				for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
				{
					GmsRoomObject_t* roomObject = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex);
					NotNull(roomObject);
					GmsObject_t* object = nullptr;
					if (roomObject->id >= 0 && (u32)roomObject->id < main->pack.objects.length) { object = DynArrayGet(&main->pack.objects, GmsObject_t, (u32)roomObject->id); }
					GmsSprite_t* sprite = nullptr;
					if (object != nullptr && object->spriteId >= 0 && (u32)object->spriteId < main->pack.sprites.length) { sprite = DynArrayGet(&main->pack.sprites, GmsSprite_t, (u32)object->spriteId); }
					v2 objectSize = Vec2_Zero;
					if (sprite != nullptr) { objectSize = Vec2Multiply(NewVec2(sprite->size), roomObject->scale); }
					if (objectSize.width > 0 && objectSize.height > 0)
					{
						rec objectRec = NewRec(NewVec2(roomObject->position), objectSize);
						if (roomRec == Rec_Zero) { roomRec = objectRec; }
						else { roomRec = RecBoth(roomRec, objectRec); }
					}
				}
				
				reci roomReci = NewReci(Vec2Floori(roomRec.topLeft), Vec2i_Zero);
				roomReci.size = Vec2Ceili(roomRec.topLeft + roomRec.size) - roomReci.topLeft;
				
				v2 roomPos = NewVec2(roomReci.topLeft) + clickPos;
				
				PrintLine_N("Room[%u]: \"%s\" %u objects", rIndex, room->name, room->objects.length);
				u32 numObjectsFound = 0;
				for (u32 oIndex = 0; oIndex < room->objects.length; oIndex++)
				{
					GmsRoomObject_t* roomObject = DynArrayGet(&room->objects, GmsRoomObject_t, oIndex);
					NotNull(roomObject);
					GmsObject_t* object = nullptr;
					if (roomObject->id >= 0 && (u32)roomObject->id < main->pack.objects.length) { object = DynArrayGet(&main->pack.objects, GmsObject_t, (u32)roomObject->id); }
					GmsSprite_t* sprite = nullptr;
					if (object != nullptr && object->spriteId >= 0 && (u32)object->spriteId < main->pack.sprites.length) { sprite = DynArrayGet(&main->pack.sprites, GmsSprite_t, (u32)object->spriteId); }
					v2 objectSize = Vec2_Zero;
					if (sprite != nullptr) { objectSize = Vec2Multiply(NewVec2(sprite->size), roomObject->scale); }
					rec objectRec = NewRec(NewVec2(roomObject->position), objectSize);
					GmsRoomLayer_t* objectLayer = nullptr;
					for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
					{
						GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
						NotNull(layer);
						if (layer->type == GmsEnum_LayerType_Instance)
						{
							for (u32 iIndex = 0; iIndex < layer->instances.length; iIndex++)
							{
								i32* instanceIdPntr = DynArrayGet(&layer->instances, i32, iIndex);
								NotNull(instanceIdPntr);
								if (*instanceIdPntr == roomObject->instanceId)
								{
									objectLayer = layer;
									break;
								}
							}
							if (objectLayer != nullptr) { break; }
						}
					}
					if (IsInsideRec(objectRec, roomPos))
					{
						if (roomObject->creationCodeId != -1)
						{
							PrintLine_I("Object[%u]: %d \"%s\" instance %u (%d, %d) layer \"%s\" creation: %d", oIndex, roomObject->id, object->name, roomObject->instanceId, roomObject->position.x, roomObject->position.y, (objectLayer != nullptr) ? objectLayer->name : "[none]", roomObject->creationCodeId);
						}
						else
						{
							PrintLine_I("Object[%u]: %d \"%s\" instance %u (%d, %d) layer \"%s\"", oIndex, roomObject->id, object->name, roomObject->instanceId, roomObject->position.x, roomObject->position.y, (objectLayer != nullptr) ? objectLayer->name : "[none]");
						}
						numObjectsFound++;
					}
				}
				PrintLine_D("Found %u object%s at (%f, %f)", numObjectsFound, (numObjectsFound == 1) ? "" : "s", roomPos.x, roomPos.y);
				
				v2i gridPos = Vec2Floori(roomPos / 16.0f);
				for (u32 lIndex = 0; lIndex < room->layers.length; lIndex++)
				{
					GmsRoomLayer_t* layer = DynArrayGet(&room->layers, GmsRoomLayer_t, lIndex);
					NotNull(layer);
					if (layer->type == GmsEnum_LayerType_Tile)
					{
						if (gridPos.x >= 0 && gridPos.y >= 0 && gridPos.x < layer->gridSize.width && gridPos.y < layer->gridSize.height)
						{
							u32* tileIdPntr = DynArrayGet(&layer->tiles, u32, (u32)((gridPos.y * layer->gridSize.width) + gridPos.x));
							NotNull(tileIdPntr);
							PrintLine_N("Tile Layer[%u] at (%d, %d): %u", lIndex, gridPos.x, gridPos.y, *tileIdPntr);
						}
					}
				}
				
				break;
			}
		}
	}
	// +==============================+
	// |           gen_info           |
	// +==============================+
	else if (StrCompareIgnoreCaseNt(cmdStr, "gen_info"))
	{
		if (!main->pack.filled) { WriteLine_E("Pack is not loaded"); return true; }
		GmsGenInfo_t* genInfo = &main->pack.genInfo;
		PrintLine_D("  isDebug:            %s", genInfo->isDebug ? "True" : "False");
		PrintLine_D("  unknown1:           [ %02X %02X %02X ]", genInfo->unknown1[0], genInfo->unknown1[1], genInfo->unknown1[2]);
		PrintLine_D("  fileName:           \"%s\"", genInfo->fileName);
		PrintLine_D("  configStr:          \"%s\"", genInfo->configStr);
		PrintLine_D("  nextInstanceId:     %d", genInfo->nextInstanceId);
		PrintLine_D("  unknown3:           0x%08X", genInfo->unknown3);
		PrintLine_D("  unknown4:           [0x%08X 0x%08X 0x%08X 0x%08X 0x%08X]", genInfo->unknown4[0], genInfo->unknown4[1], genInfo->unknown4[2], genInfo->unknown4[3], genInfo->unknown4[4]);
		PrintLine_D("  name:               \"%s\"", genInfo->name);
		PrintLine_D("  version:            %u.%u.%u(%u)", genInfo->versionMajor, genInfo->versionMinor, genInfo->versionRelease, genInfo->versionBuild);
		PrintLine_D("  defaultWindowSize:  (%u, %u)", genInfo->defaultWindowWidth, genInfo->defaultWindowHeight);
		PrintLine_D("  unknown5:           0x%08X", genInfo->unknown5);
		PrintLine_D("  unknown6:           0x%08X", genInfo->unknown6);
		PrintLine_D("  unknown7:           0x%08X", genInfo->unknown7);
		PrintLine_D("  unknown8:           [0x%08X 0x%08X 0x%08X]", genInfo->unknown8[0], genInfo->unknown8[1], genInfo->unknown8[2]);
		PrintLine_D("  timestamp:          %lu", genInfo->timestamp);
		PrintLine_D("  displayName:        \"%s\"", genInfo->displayName);
		PrintLine_D("  unknown11:          [0x%08X 0x%08X 0x%08X]", genInfo->unknown11[0], genInfo->unknown11[1], genInfo->unknown11[2]);
		PrintLine_D("  unknown12:          0x%08X", genInfo->unknown12);
		PrintLine_D("  unknown13:          0x%08X", genInfo->unknown13);
		PrintLine_D("  unknown14:          0x%08X", genInfo->unknown14);
		PrintLine_D("  %u numbers in this header:", genInfo->numbers.length);
	}
	// +==============================+
	// |          code_info           |
	// +==============================+
	else if (StrCompareIgnoreCaseNt(cmdStr, "code_info"))
	{
		if (!main->pack.filled) { WriteLine_E("Pack is not loaded"); return true; }
		
		bool specificCode = false;
		u32 targetCodeIndex = 0;
		if (numArgStrs >= 1)
		{
			if (!TryParseU32(argStrs[0].pntr, argStrs[0].length, &targetCodeIndex))
			{
				PrintLine_E("Failed to parse \"%.*s\" as u32", argStrs[0].length, argStrs[0].pntr);
				return true;
			}
			specificCode = true;
		}
		
		if (!specificCode) { PrintLine_N("Pack has %u codes:", main->pack.codes.length); }
		for (u32 cIndex = 0; cIndex < main->pack.codes.length; cIndex++)
		{
			GmsCode_t* code = DynArrayGet(&main->pack.codes, GmsCode_t, cIndex);
			NotNull(code);
			
			if (!specificCode || cIndex == targetCodeIndex)
			{
				PrintLine_I("  Code[%u] \"%s\" 0x%08X", cIndex, code->name, code->bytecodeOffset);
				PrintLine_D("    unknown1: %u", code->unknown1);
				PrintLine_D("    unknown2: %u", code->unknown2);
				PrintLine_D("    codeRelativeOffset: %d", code->codeRelativeOffset);
				if (specificCode)
				{
					if (code->variableIndices.length > 0)
					{
						Print_D("    Variables: [%u]\n    {", code->variableIndices.length);
						for (u32 vIndex = 0; vIndex < code->variableIndices.length; vIndex++)
						{
							NotNull(code->variableIndices.items);
							u32* variableIndexPntr = DynArrayGet(&code->variableIndices, u32, vIndex);
							NotNull(variableIndexPntr);
							Assert(*variableIndexPntr < main->pack.variables.length);
							GmsVariable_t* variable = DynArrayGet(&main->pack.variables, GmsVariable_t, *variableIndexPntr);
							NotNull(variable);
							// Print_D(" %u", *variableIndexPntr);
							Print_D("\n      %s(%u 0x%08X)", variable->name, *variableIndexPntr, variable->firstCodeReference);
						}
						WriteLine_D("\n    }");
					}
					else
					{
						WriteLine_D("    No variables");
					}
					if (code->functionIndices.length > 0)
					{
						Print_D("    Functions: [%u]\n    {", code->functionIndices.length);
						for (u32 fIndex = 0; fIndex < code->functionIndices.length; fIndex++)
						{
							NotNull(code->functionIndices.items);
							u32* functionIndexPntr = DynArrayGet(&code->functionIndices, u32, fIndex);
							NotNull(functionIndexPntr);
							Assert(*functionIndexPntr < main->pack.functions.length);
							GmsFunction_t* function = DynArrayGet(&main->pack.functions, GmsFunction_t, *functionIndexPntr);
							NotNull(function);
							// Print_D(" %u", *functionIndexPntr);
							Print_D("\n      %s(%u 0x%08X)", function->name, *functionIndexPntr, function->firstCodeReference);
						}
						WriteLine_D("\n    }");
					}
					else
					{
						WriteLine_D("    No functions");
					}
					
					Print_D("    [%4u]:", code->bytecodeLength);
					for (u32 wordIndex = 0; (wordIndex+1)*4 <= code->bytecodeLength; wordIndex++)
					{
						u32 wordValue = ((u32*)code->bytecodePntr)[wordIndex];
						Print_D(" %08X", wordValue);
						if (wordIndex > 0 && ((wordIndex+1)*4) < code->bytecodeLength && ((wordIndex+1) % 4) == 0) { Write_D("\n           "); }
					}
					WriteLine_D("");
				}
			}
		}
	}
	// +==============================+
	// |        variable_info         |
	// +==============================+
	else if (StrCompareIgnoreCaseNt(cmdStr, "variable_info"))
	{
		if (!main->pack.filled) { WriteLine_E("Pack is not loaded"); return true; }
		PrintLine_N("Pack has %u variables:", main->pack.variables.length);
		for (u32 vIndex = 0; vIndex < main->pack.variables.length; vIndex++)
		{
			GmsVariable_t* variable = DynArrayGet(&main->pack.variables, GmsVariable_t, vIndex);
			NotNull(variable);
			
			PrintLine_I("  Var[%u]: %u \"%s\"", vIndex, variable->id, variable->name);
			PrintLine_D("    unknown0: %u", variable->unknown0);
			PrintLine_D("    numCodeReferences: %u", variable->numCodeReferences);
			PrintLine_D("    firstCodeReference: 0x%08X", variable->firstCodeReference);
			PrintLine_D("    codeIndex: %d", variable->codeIndex);
		}
	}
	// +==============================+
	// |        function_info         |
	// +==============================+
	else if (StrCompareIgnoreCaseNt(cmdStr, "function_info"))
	{
		if (!main->pack.filled) { WriteLine_E("Pack is not loaded"); return true; }
		PrintLine_N("Pack has %u functions:", main->pack.functions.length);
		for (u32 fIndex = 0; fIndex < main->pack.functions.length; fIndex++)
		{
			GmsFunction_t* function = DynArrayGet(&main->pack.functions, GmsFunction_t, fIndex);
			NotNull(function);
			
			PrintLine_I("  Func[%u] (%08X): \"%s\"", fIndex, function->offset, function->name);
			PrintLine_D("    unknown0: %d", function->unknown0);
			PrintLine_D("    firstCodeReference: 0x%08X", function->firstCodeReference);
			PrintLine_D("    codeIndex: %d", function->codeIndex);
		}
	}
	// +==============================+
	// |         script_info          |
	// +==============================+
	else if (StrCompareIgnoreCaseNt(cmdStr, "script_info"))
	{
		if (!main->pack.filled) { WriteLine_E("Pack is not loaded"); return true; }
		PrintLine_N("Pack has %u scripts:", main->pack.scripts.length);
		for (u32 sIndex = 0; sIndex < main->pack.scripts.length; sIndex++)
		{
			GmsScript_t* script = DynArrayGet(&main->pack.scripts, GmsScript_t, sIndex);
			NotNull(script);
			
			PrintLine_I("  Script[%u]: \"%s\"", sIndex, script->name);
			PrintLine_D("    Offset: 0x%08X", script->offset);
			PrintLine_D("    Id:     %u", script->id);
		}
	}
	// +==============================+
	// |          var_counts          |
	// +==============================+
	else if (StrCompareIgnoreCaseNt(cmdStr, "var_counts"))
	{
		u32 numArguments = 0;
		u32 numReference = 0;
		u32 numNoReference = 0;
		u32 numNonArgNoReference = 0;
		for (u32 vIndex = 0; vIndex < main->pack.variables.length; vIndex++)
		{
			GmsVariable_t* variable = DynArrayGet(&main->pack.variables, GmsVariable_t, vIndex);
			NotNull(variable);
			if (variable->firstCodeReference > 0)
			{
				numReference++;
			}
			else
			{
				numNoReference++;
			}
			if (StrCompareIgnoreCaseNt(variable->name, "arguments"))
			{
				numArguments++;
			}
			else if (variable->firstCodeReference <= 0)
			{
				numNonArgNoReference++;
			}
		}
		PrintLine_I("numArguments:         %u", numArguments);
		PrintLine_I("numReference:         %u", numReference);
		PrintLine_I("numNoReference:       %u", numNoReference);
		PrintLine_I("numNonArgNoReference: %u", numNonArgNoReference);
	}
	// +==============================+
	// | code_str_usages [str_number] |
	// +==============================+
	else if (StrCompareIgnoreCaseNt(cmdStr, "code_str_usages"))
	{
		if (numArgStrs != 1) { WriteLine_E("Usage: code_str_usages [str_number]"); return true; }
		if (!main->pack.filled) { WriteLine_E("Pack is not loaded"); return true; }
		
		u32 strIndex = 0;
		if (!TryParseU32(argStrs[0].pntr, argStrs[0].length, &strIndex))
		{
			PrintLine_E("Failed to parse \"%.*s\" as u32", argStrs[0].length, argStrs[0].pntr);
			return true;
		}
		if (strIndex >= main->pack.strings.length)
		{
			PrintLine_E("Index out of range. There are %u strings found in the pack", main->pack.strings.length);
		}
		
		u32 numUsages = 0;
		GmsString_t* gmsString = DynArrayGet(&main->pack.strings, GmsString_t, strIndex);
		NotNull(gmsString);
		for (u32 cIndex = 0; cIndex < main->pack.codes.length; cIndex++)
		{
			GmsCode_t* code = DynArrayGet(&main->pack.codes, GmsCode_t, cIndex);
			NotNull(code);
			Assert(code->bytecodePntr != nullptr || code->bytecodeLength == 0);
			bool foundUsageInThisCode = false;
			u32* words = (u32*)code->bytecodePntr;
			u32 numWords = code->bytecodeLength/sizeof(u32);
			for (u32 wordIndex = 0; wordIndex+1 < numWords; wordIndex++)
			{
				u32 word = words[wordIndex];
				u32 nextWord = words[wordIndex+1];
				if (GetOpCode(word) == GmlOpCode_Push)
				{
					u8 type = GetUpperByte(word);
					if (type == GmlDataType_String && nextWord == strIndex)
					{
						if (!foundUsageInThisCode) { PrintLine_I("Found push usage in code[%u] \"%s\"", cIndex, code->name); }
						PrintLine_D("  word[%u] address 0x%08X", wordIndex+1, code->bytecodeOffset + (wordIndex+1)*sizeof(u32));
						numUsages++;
					}
				}
				else if (GetOpCode(word) == GmlOpCode_Call)
				{
					if (nextWord == strIndex)
					{
						if (!foundUsageInThisCode) { PrintLine_I("Found function usage in code[%u] \"%s\"", cIndex, code->name); }
						PrintLine_D("  word[%u] address 0x%08X", wordIndex+1, code->bytecodeOffset + (wordIndex+1)*sizeof(u32));
						numUsages++;
					}
				}
			}
		}
		if (numUsages > 0)
		{
			PrintLine_N("Found %u usage%s of \"%s\"", numUsages, (numUsages == 1) ? "" : "s", gmsString->pntr);
		}
		else
		{
			PrintLine_W("There were no usages of \"%s\" in any code that we know how to parse", gmsString->pntr);
		}
	}
	
	// +==============================+
	// |       Unknown Command        |
	// +==============================+
	else
	{
		PrintLine_E("Unknown command \"%s\": \"%s\"", cmdStr, commandStr);
		return false;
	}
	return true;
}
