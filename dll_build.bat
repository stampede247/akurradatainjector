@echo off

rem x64
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"

set DllName=AkurraParser
set BuildFolder=dll_build
set LibFolder=..\lib
set SourceFolder=..\dll_code

set DebugBuild=1
set CopyToOutputFolder=1

rem /GS- /Gm- -GR- /EHa-
set CompilerFlags=/Fe"%DllName%.dll" /FC /nologo /EHsc /Fm /Oi /WX /W4 /wd4100 /wd4127 /wd4201 /wd4189 /wd4996
set Defines=-DWINDOWS_COMPILATION
set IncludeDirs=/I"%LibFolder%\include"
set LibraryDirectories=
set Libraries=
set DllExports=/EXPORT:Parser_Initialize
set DllExports=%DllExports% /EXPORT:Parser_Import
set DllExports=%DllExports% /EXPORT:Parser_StartExportList /EXPORT:Parser_ExportOperation /EXPORT:Parser_Export
set DllExports=%DllExports% /EXPORT:Parser_PushTempMark /EXPORT:Parser_PopTempMark /EXPORT:Parser_HandleCommand
set DllExports=%DllExports% /EXPORT:Parser_GetNumRooms /EXPORT:Parser_GetRoomInfo /EXPORT:Parser_GetRoomImage /EXPORT:Parser_FreeRoomImage
set DllExports=%DllExports% /EXPORT:Parser_GetNumObjects /EXPORT:Parser_GetObjectInfo
set DllExports=%DllExports% /EXPORT:Parser_GetNumSprites /EXPORT:Parser_GetSpriteInfo
set DllExports=%DllExports% /EXPORT:Parser_GetNumStrings /EXPORT:Parser_GetStringInfo
set DllExports=%DllExports% /EXPORT:Parser_GetNumCodes /EXPORT:Parser_GetCodeInfo /EXPORT:Parser_GetCodeBytecode /EXPORT:Parser_GetCodeFormattedBytecode
set DllExports=%DllExports% /EXPORT:Parser_GetNumVariables /EXPORT:Parser_GetVariableInfo
set DllExports=%DllExports% /EXPORT:Parser_GetNumFunctions /EXPORT:Parser_GetFunctionInfo
set LinkerFlags=/DLL -incremental:no /PDB:"%DllName%.pdb"

if "%DebugBuild%"=="1" (
	set CompilerFlags=%CompilerFlags% /Zi /Od /MTd
	set Defines=%Defines% -DDEBUG=1
	set LibraryDirectories=%LibraryDirectories% /LIBPATH:"%LibFolder%\debug"
	set OutputFolder=..\AkurraDataInjector\bin\Debug
) else (
	set CompilerFlags=%CompilerFlags% /O2 /Ot /Oy /MT
	set Defines=%Defines% -DDEBUG=0
	set LibraryDirectories=%LibraryDirectories% /LIBPATH:"%LibFolder%\release"
	set OutputFolder=..\AkurraDataInjector\bin\Release
)

rem echo cl %CompilerFlags% %Defines% %IncludeDirs% "%SourceFolder%\main.c" /link %LibraryDirectories% %Libraries% %DllExports% %LinkerFlags%
cl %CompilerFlags% %Defines% %IncludeDirs% "%SourceFolder%\main.cpp" /link %LibraryDirectories% %Libraries% %DllExports% %LinkerFlags%
del *.obj > NUL 2> NUL

if "%CopyToOutputFolder%"=="1" (
	echo [Copying %DllName%.dll to %OutputFolder%]
	XCOPY ".\%DllName%.dll" "%OutputFolder%\" /Y > NUL
	XCOPY ".\%DllName%.pdb" "%OutputFolder%\" /Y > NUL
)
